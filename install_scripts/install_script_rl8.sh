# Copyright (C) 2014-2020 Syracuse University, European Gravitational Observatory, and Christopher Newport University.  Written by Ryan Fisher and Gary Hemming. See the NOTICE file distributed with this work for additional information regarding copyright ownership.

# This program is free software: you can redistribute it and/or modify

# it under the terms of the GNU Affero General Public License as

# published by the Free Software Foundation, either version 3 of the

# License, or (at your option) any later version.

#

# This program is distributed in the hope that it will be useful,

# but WITHOUT ANY WARRANTY; without even the implied warranty of

# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

# GNU Affero General Public License for more details.

#

# You should have received a copy of the GNU Affero General Public License

# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!/bin/bash

# This is a script to set up one of the data quality segments database (DQsegDB) machines.
# Warning: This script assumes that it is being run by user root.  Installation by any other user will likely produce errors.
# To both view and save the script's output, clone the repo:
#   " mkdir -p /root/git_dqsegdb_server/; cd /root/git_dqsegdb_server/; git clone https://git.ligo.org/computing/dqsegdb/server.git "
#  then run the script with something like:
#   " /root/git_dqsegdb_server/server/install_scripts/install_script_rl8.sh  2>&1  |  tee -a  installation_output_$(date +%Y.%m.%d-%H.%M.%S).txt "
# Sometimes the installation script crashes or gets interrupted.  In that case, we don't always want to re-run everything
#   from the start, but it can be awkward to try to run just part of it by hand or insert "if [ 0 -eq 1 ]; then [...] fi"
#   blocks, to skip completed code.  So the script is broken up into blocks, which can be turned on and off independently.
#   Simply set the blocks you want to run to 1 and the blocks that you don't want to run to 0, then re-run the script.
# Here are the different sections and flags that control whether each is run:
run_block_0=1   # * setup variables, used in other sections
run_block_1=1   # * basic installation items   #basic
run_block_2=1   # * installation of Apache, MariaDB, etc.                    # philosophy: installation done here, not configuration (run_block_3) or starting of services (run_block_4)
run_block_3=1   # * configuration of Apache, MariaDB, SciTokens, etc.        # philosophy: configuration done here, not installation (run_block_2) or starting of services (run_block_4)
run_block_4=1   # * Importing certificates and starting Apache and MariaDB   # philosophy: starting of services done here, not installation (run_block_2) or configuration (run_block_3)
run_block_5=1   # * Installing segments publisher code
run_block_6=1   # * Handling remaining machine-specific items
run_block_7=1   # * restoring main DQSegDB DB
run_block_8=1   # * Handling crontabs, misc. links
run_block_9=1   # * Unspecified additional commands (probably added by hand at run time)

function cp_to_bak {
  # need to find a way to strip the trailing slash from a dir name, or else it will throw an error like this:
  #   cp: cannot copy a directory, ‘/var/log/publishing/state/’, into itself, ‘/var/log/publishing/state/_2023.04.27-02.18.53.bak’
  if [ -e "$1" ]; then cp -pri "$1" "${1}_$(date +%Y.%m.%d-%H.%M.%S).bak" ; fi   # quotes in case filename has spaces
}

function mv_to_bak {
  if [ -e "$1" ]; then mv -i "$1" "${1}_$(date +%Y.%m.%d-%H.%M.%S).bak" ; fi   # quotes in case filename has spaces
}

if [ $run_block_7 -eq 1 ]
then
  echo "### NOTICE ### Current setup will restore the newest DB, which might take a while (30 minutes or maybe (much) long)"
  echo "           ### You have 10 seconds to stop this script before execution continues."
  sleep 10
fi

if [ $run_block_0 -eq 1 ]; then   # * setup variables, used in other sections 
  # This variable set to 1 will output some messages to the screen and log file (or not, if set to 0).
  # These msgs give some useful information, but also report checkpoints along the installation route (in case of trouble).
  verbose=1

  if [ $verbose -eq 1 ]; then echo -e "### Setting up variables (run_block_0);  $(date)"; fi   #setup

  # set host; valid options: segments, segments-backup, segments-web, segments-dev, segments-dev2, segments-other
  # NOTE: the 'host' variable is necessary for this script to run, so this should probably be moved out of a block
  #   of code that can be turned off; this might apply to other parts of this block, too
  host="segments-backup"   # use this to set the hostname manually
  #host=$(uname -n)   # use this to set the hostname automatically
  if [ $host != "segments" ] && [ $host != "segments-backup" ] && [ $host != "segments-web" ] && [ $host != "segments-dev" ] \
      && [ $host != "segments-dev2" ] && [ $host != "segments-other" ]
  then
    echo "### ERROR ### Variable 'host' is not set to a valid value.  Please fix this and run this script again."
    exit
  fi
  if [ "$host" != `uname -n` ]
  then
    echo "### WARNING ### Hostname is `uname -n`, but 'host' variable is set to $host."
    echo "            ### If this is a mistake, you have 10 seconds to stop this script; after that, installation will continue anyway."
    sleep 10
  fi
  if [ `id -un` != "root" ]
  then
    echo "### WARNING ### User should be 'root' but is actually '`id -un`'.  This script will probably not run correctly."
    echo "            ### You have 10 seconds to stop this script; after that, installation will continue anyway."
    sleep 10
  fi

  # The default is to create a 'live' system, with all services, cron jobs, etc. ready to run immediately;
  #    if you don't want that, change the 'live' variable to 0 before running this script.
  # If 'live' is not 1, some services will be started, as part of setting them up, then shut down after that.
  live=1

  if [ $verbose -eq 1 ]; then \
    echo -e "### Starting installation: $(date)  \n### hostname: $host"
  fi
fi   # run_block_0

if [ $run_block_1 -eq 1 ]; then   # * basic installation items   #basic
  if [ $verbose -eq 1 ]; then echo -e "### Starting basic installation items (run_block_1);  $(date)"; fi   #basic
  # first, make sure that we can get to the installation directories that we need
  if [ ! -d /backup/segdb/reference/install_support/ ]
  then
    echo "### ERROR ### The backup dir with installation files ( /backup/segdb/reference/install_support/ ) is not available."
    echo "          ### Installation cannot be completed properly without the files in that dir.  Exiting."
    exit
  fi
  # make backups of user root config files (if they exist) and then import new files
  cp_to_bak /root/.bashrc
  cp_to_bak /root/.vimrc 
  cp_to_bak /root/.pythonrc
  ln -s /root/git_dqsegdb_private/dqsegdb-private/root_.bashrc  /root/.bashrc
  ln -s /root/git_dqsegdb_private/dqsegdb-private/root_.pythonrc  /root/.pythonrc
  ln -s /root/git_dqsegdb_private/dqsegdb-private/root_.vimrc  /root/.vimrc
  chown  root:root  .bashrc  .pythonrc  .vimrc
  .  ./.bashrc
  mkdir -p /root/bin/

  yum -y install git nano mlocate screen telnet symlinks tree wget lsof zip unzip abrt
  # do we still need redhat-lsb-core? what does it provide?
  # let's try without it - see https://git.ligo.org/computing/dqsegdb/server/-/issues/21
  #yum -y install redhat-lsb-core
  yum -y install "dqsegdb >= 2.0.0"
# not doing the lines below so that we don't have to worry about changing the installation script *while running it*
#   instead, users should make sure to update the repo before starting the installation
#  mkdir -p /root/git_dqsegdb_server
#  cd /root/git_dqsegdb_server
#  mv_to_bak ./server
#  git clone https://git.ligo.org/computing/dqsegdb/server.git
#  cd ./server/install_scripts
  cd /root/git_dqsegdb_server/server/install_scripts
  if [ $verbose -eq 1 ]
  then
    install_script=install_script_rl8.sh
    echo "### INFO ### Printing the installation script/instructions (./$install_script);  $(date)"
    echo "##########################################################"
    cat $install_script
    echo "### INFO ### Finished printing the installation script/instructions"
    echo "###################################################################"
  fi

  # used for connecting to git repositories with Kerberos (if this is still used)
  yum -y install ecp-cookie-init
  git config --global http.cookiefile /tmp/ecpcookie.u`id -u`
  #git config --global user.email ryan.fisher@ligo.org
  #git config --global user.name "Ryan Fisher"
  git config --global user.email robert.bruntz@ligo.org
  git config --global user.name "Robert Bruntz"

  # set up repositories with useful materials in them
  mv_to_bak /root/git_dqsegdb_tools
  mkdir -p /root/git_dqsegdb_tools
  cd /root/git_dqsegdb_tools
  git clone https://git.ligo.org/computing/dqsegdb/dqsegdb-tools.git
  mv_to_bak /root/git_dqsegdb_private
  mkdir -p /root/git_dqsegdb_private
  cd /root/git_dqsegdb_private
  # can we automate cloning this repo from within the script?  currently, it ask for username and password
  git clone https://git.ligo.org/computing/dqsegdb/dqsegdb-private.git

  # Set LGMM (LIGO Grid-Mapfile Manager) to run on reboot and start it now
  mkdir -p  /etc/grid-security/  /etc/lgmm/   /var/log/lgmm/   ### should exist; just being safe
  mkdir -p  /etc/grid-security/old_certs
  touch /etc/grid-security/whitelist   ### just in case the config file looks for it; missing an expected file crashes lgmm
  touch /etc/grid-security/blacklist   ### just in case the config file looks for it; missing an expected file crashes lgmm
  cp -p /backup/segdb/reference/lgmm/whitelist  /etc/grid-security/   ### maybe put the whitelist into a repo later?
  #mv_to_bak /etc/lgmm/lgmm_config.py
  #cp /backup/segdb/reference/install_support/lgmm_config.py  /etc/lgmm/   # copy this from dqsegdb-private repo after that's clonable
  touch /etc/grid-security/grid-mapfile
  chown nobody:nobody /etc/grid-security/grid-mapfile
  chmod 644 /etc/grid-security/grid-mapfile
  # set up cron job files; deal with cron job later
  mv_to_bak /root/bin/lgmm_cron_job.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/lgmm/lgmm_cron_job.sh  /root/bin/lgmm_cron_job.sh
  mv_to_bak /etc/lgmm/lgmm_config.py
  ln -s /root/git_dqsegdb_private/dqsegdb-private/lgmm/lgmm_config.py  /etc/lgmm/lgmm_config.py
  mv_to_bak /etc/lgmm/lgmm_config_kagra.py
  ln -s /root/git_dqsegdb_private/dqsegdb-private/lgmm/lgmm_config_kagra.py  /etc/lgmm/lgmm_config_kagra.py
  ### not sure if the order is right for the next 3 lines; 'lgmm -f' seems to need to be run or the service won't stay on
  lgmm -f
#  /sbin/chkconfig lgmm on
  systemctl enable lgmm.service
  systemctl restart lgmm
  # set up tools to update whitelists
  mv_to_bak /root/bin/update_definitive_whitelist.sh
  mv_to_bak /root/bin/update_local_whitelist.sh
  ln -s /root/git_dqsegdb_server/server/bin/update_definitive_whitelist.sh /root/bin/update_definitive_whitelist.sh
  ln -s /root/git_dqsegdb_server/server/bin/update_local_whitelist.sh /root/bin/update_local_whitelist.sh

  # Set ABRT to log info on errors in some of our software (by not requiring software be PGP-signed or installed from a package)
  abrt_config_file=/etc/abrt/abrt-action-save-package-data.conf
  if [ -e $abrt_config_file ]
  then
    echo "### INFO ### Possibly changing ABRT settings.  Old settings:"
    grep -e ProcessUnpackaged -e OpenGPGCheck  $abrt_config_file
    cp -p  $abrt_config_file  ${abrt_config_file}_$(date +%Y.%m.%d-%H.%M.%S).bak
    old_string="OpenGPGCheck = yes"; new_string="OpenGPGCheck = no"; sed -i "s/$old_string/$new_string/g"  $abrt_config_file
    old_string="ProcessUnpackaged = no"; new_string="ProcessUnpackaged = yes"; sed -i "s/$old_string/$new_string/g"  $abrt_config_file
    echo "          ### New settings:"
    grep -e ProcessUnpackaged -e OpenGPGCheck  $abrt_config_file
  else
    echo "### WARNING ### ABRT configuration file ( $abrt_config_file ) was not found, so its settings were not modified."
    echo "            ### You might want to do this by hand, using commands from the installation file."
  fi

  # keytabs; install scripts now; deal with crontabs later
  yum -y install ciecp-utils   # installs ecp-cert-info, needed by renew_proxy_cert.sh
  mv_to_bak /root/bin/renew_proxy_cert.sh
  ln -s	/root/git_dqsegdb_private/dqsegdb-private/renew_proxy_cert.sh  /root/bin/renew_proxy_cert.sh
  mkdir -p /root/proxy_logs/
  echo "### NOTICE ### keytab and cert files referenced in renew_proxy_cert.sh need to be put there by the user."
  # we'll have to figure out how to get the keytab and cert file into place
  # add more keytab stuff here
  # might need to install something for kinit, ligo-proxy-init (ligo-proxy-utils)


  # monitors and various things
  mv_to_bak /root/bin/check_mounted_filesystems.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/monitors/check_mounted_filesystems.sh /root/bin/check_mounted_filesystems.sh
  mv_to_bak /root/bin/strace_df_mounted_filesystems.sh
  ln -s  /root/git_dqsegdb_tools/dqsegdb-tools/monitors/strace_df_mounted_filesystems.sh  /root/bin/strace_df_mounted_filesystems.sh
  mv_to_bak /root/bin/crontab_backup.sh
  ln -s  /root/git_dqsegdb_tools/dqsegdb-tools/crontab_backup.sh  /root/bin/crontab_backup.sh
  mv_to_bak /var/www/nagios
  mkdir -p /var/www/nagios
  mv_to_bak /root/bin/service_monitor_monitor.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/monitors/service_monitor_monitor.sh /root/bin/service_monitor_monitor.sh
  mv_to_bak /root/bin/check_service_status_for_nagios.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/monitors/check_service_status_for_nagios.sh /root/bin/check_service_status_for_nagios.sh
  mv_to_bak /root/bin/check_service_status_for_nagios_wrapper.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/monitors/check_service_status_for_nagios_wrapper.sh  /root/bin/check_service_status_for_nagios_wrapper.sh
  mv_to_bak /root/bin/check_service_lgmm_cron_job.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/monitors/check_service_lgmm_cron_job.sh /root/bin/check_service_lgmm_cron_job.sh
  mv_to_bak /root/bin/test_segments_machine.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/test_segments_machine.sh /root/bin/test_segments_machine.sh
  mv_to_bak /root/bin/ltconvert
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/ltconvert.sh /root/bin/ltconvert
  mv_to_bak /root/bin/check_backup_and_ifocache_space.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/check_backup_and_ifocache_space.sh /root/bin/check_backup_and_ifocache_space.sh
  mv_to_bak /root/bin/now
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/now /root/bin/now

  # make sure firewall is not enabled
  if [ `systemctl is-enabled firewalld`	]; then	systemctl disable firewalld; systemctl stop firewalld; fi
fi   # run_block_1


if [ $run_block_2 -eq 1 ]; then   # * installation of Apache, MariaDB, etc.
  if [ $verbose -eq 1 ]; then echo "### Starting installation of Apache, MariaDB, etc. (run_block_2);  $(date)"; fi
  # Install Apache, MariaDB (successor to MySQL), PHPMyAdmin, etc.
  # philosophy: installation done here, not configuration (run_block_3) or starting of services (run_block_4)

  # Install Apache server.
  yum -y install httpd
  yum -y install mod_ssl
  # Get all cert identifier packages:
  yum -y install osg-ca-certs   # should this be installed? or igtf-ca-certs?
  yum -y install ligo-ca-certs   # is this still used?
  yum -y install ca-certificates

  # Install Apache WSGI module.
  #yum -y install mod_wsgi
  yum -y remove mod_wsgi   # shouldn't be installed, but let's be sure
  yum -y install python3-mod_wsgi

  # Install SciTokens
  yum -y install  "python3-scitokens >= 1.7.4"  python3-pytest  htgettoken

  # Install MariaDB, set it to run on startup, and start it
  yum -y install mariadb-server mariadb mariadb-devel mytop
  ### not started until later, b/c we want to change the buffer pool size first

  # Install PHP (for web interface).
  yum -y install php php-mysqlnd

  # need to figure out if we even *need* ODBC; might not; let's try without it
  # Install pyodbc library for Python. N.B. This also installs unixODBC as a dependency.
  #yum -y install pyodbc
  # for Python 3 on SL7, pyodbc has to run in a virtual environment
  #python3 -m venv /root/p3_for_scitokens
  #source /root/p3_for_scitokens/bin/activate
  #python3 -m pip install --upgrade pip
  #python3 -m pip install pyodbc
  #python3 -c "import pyodbc; print(pyodbc.version)"
  #deactivate
  #echo "WSGIPythonPath /root/p3_for_scitokens/lib/python3.6/site-packages/" >> /etc/httpd/conf.d/wsgi.conf
  # let's find out if it will just work on RL8!
  yum -y install python3-pyodbc

  # By default, unixODBC only installs PostGreSQL connector libraries. Install the MySQL (MariaDB) connectors now.
  # we're going to see if we actually need this package by NOT installing it at first - see https://git.ligo.org/computing/helpdesk/-/issues/3863
  #yum -y install mysql-connector-odbc   # SL7 version; not easily available in RL8
  #yum -y install mariadb-connector-odbc   # see if this works in RL8, but first see if anything breaks; do we need this?

  # Set up DQSegDB Python server
  mkdir -p /opt/dqsegdb/python_server/
  mv_to_bak /opt/dqsegdb/python_server/logs
  mkdir /opt/dqsegdb/python_server/logs
  chmod 777 /opt/dqsegdb/python_server/logs
  # maybe at some point change the above to /opt/dqsegdb/logs/python_server/, and change python code to match
  #  (or create a symlink), so that the code and logs are separate?
  mv_to_bak /opt/dqsegdb/python_server/src
  ln -s /root/git_dqsegdb_server/server/src  /opt/dqsegdb/python_server/src

  ## FIX!!! Replace with openssl!!!
  # if this works, verify that we're happy with it, then take out the warnings and just use M2Crypto
  #   see "Decide whether to change m2crypto in installation scripts" - https://git.ligo.org/computing/dqsegdb/server/-/issues/20
  # Install M2Crypto library.
  yum -y install python3-m2crypto
  ### some code uses m2crypto; need to change that code at the same time as (or before) changing this over to openssl

  # Install phpMyAdmin
  ### do we want to do this for every segments* machine?
  if [ $host == "segments" ] || [ $host == "segments-web" ] || [ $host == "segments-backup" ] ||  \
     [ $host == "segments-dev" ] || [ $host == "segments-dev2" ]
  then
    yum -y install phpmyadmin
  fi

  # more stuff (should probably be moved to the relevant section)
  #yum -y install glue lal lal-python python-pyRXP
  # if these packages are needed, they're probably installed as dependencies for other packages (esp. 'dqsegdb');
  # if not, add them back in one-by-one
  # note that in SL7, dqsegdb-2.0.0-1.el7.noarch pulls in python36-dqsegdb.noarch, python36-glue, python36-lal, python36-pyRXP, liblal, and others
fi   # run_block_2


if [ $run_block_3 -eq 1 ]; then   # * configuration of Apache, MariaDB, SciTokens, etc.
  if [ $verbose -eq 1 ]; then echo "### Starting configuration of Apache, MariaDB, SciTokens, etc. (run_block_3);  $(date)"; fi
  # philosophy: configuration done here, not installation (run_block_2) or starting of services (run_block_4)

  # Configure Apache:
  # See 2019.02.12 work notes for comparisons of SL 6 and SL 7 versions of each file
  if [ $host == "segments" ] || [ $host == "segments-web" ] || [ $host == "segments-backup" ] ||  \
     [ $host == "segments-dev" ] || [ $host == "segments-dev2" ]
  then   ### use new method
    sleep 1   # do nothing; keep /etc/httpd/{conf,conf.d} as they are
    ln -s /root/git_dqsegdb_private/dqsegdb-private/http/dqsegdb.conf_${host}  /etc/httpd/conf.d/dqsegdb.conf
  else   ### use old method
    mv_to_bak /etc/httpd/conf
    mv_to_bak /etc/httpd/conf.d
    mkdir /etc/httpd/conf
    mkdir /etc/httpd/conf.d
    # at some point, maybe replace this with a set of files in dqsegdb-private repo
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf_httpd.conf         /etc/httpd/conf/httpd.conf
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf_magic              /etc/httpd/conf/magic
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_autoindex.conf   /etc/httpd/conf.d/autoindex.conf
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_php.conf         /etc/httpd/conf.d/php.conf
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_phpMyAdmin.conf  /etc/httpd/conf.d/phpMyAdmin.conf
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_ssl.conf         /etc/httpd/conf.d/ssl.conf
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_userdir.conf     /etc/httpd/conf.d/userdir.conf
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_welcome.conf     /etc/httpd/conf.d/welcome.conf
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_README           /etc/httpd/conf.d/README
    cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_dqsegdb.conf     /etc/httpd/conf.d/dqsegdb.conf
  fi

  # Configure MariaDB (MySQL)
  # old method
  ## Increase innodb buffer pool size.
  #echo "[mysqld]" >> /etc/my.cnf
  #echo "max_connections=256" >> /etc/my.cnf
  #case $host in
  #  segments-dev)
  #    # current version of segments-dev (created ~Feb. 2019) only has 125 GB of RAM + 125 GB of swap, but this value seems to work fine
  #    echo "innodb_buffer_pool_size = 40G" >> /etc/my.cnf
  #    ;;
  #  segments-dev2)
  #    # current version of segments-dev2 (created ~Jan. 2019) only has 35 GB of RAM + 35 GB of swap
  #    echo "innodb_buffer_pool_size = 20G" >> /etc/my.cnf
  #    ;;
  #  segments-web)
  #    # current version of segments-web (created ~April 2019) only has 1.8 GB of RAM + 2 GB of swap
  #    echo "innodb_buffer_pool_size = 3G" >> /etc/my.cnf
  #    ;;
  #  *)
  #    # this has been found to work for segments.ligo.org as of May 2023, but it might need to be increased over time
  #    echo "innodb_buffer_pool_size = 60G" >> /etc/my.cnf
  #esac
  # Avoid issue of STRICT_TRANS_TABLES being part of default SQL_MODE setting for MariaDB 10.2.4+ - see https://git.ligo.org/computing/dqsegdb/server/-/issues/37
  #   NOTE: If new settings are added to the default SQL_MODE later, and this script will install an affected version of MariaDB, the following line should be modified
  #     to include the new settings, but still exclude STRICT_TRANS_TABLES - see https://mariadb.com/kb/en/sql-mode/#defaults
  #echo "sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'" >> /etc/my.cnf
  # new method
  mv_to_bak /etc/my.cnf
  ln -s /root/git_dqsegdb_private/dqsegdb-private/mysql/my.cnf_${host}_rl8  /etc/my.cnf
  # check that innodb buffer pool isn't too large for this machine; gets the last line, extracts size (like "60G"), then extracts number
  ibps=`grep innodb_buffer_pool_size /etc/my.cnf | tail -n 1 | grep -oE " [[:digit:]]+G" | grep -oE [[:digit:]]+`
  total_ram=`free -gt | tail -n 1 | awk '{print $2}'`
  if [ "$ibps" -gt $((total_ram * 70/100)) ]; then echo "### WARNING ### innodb_buffer_pool_size is set to $ibps GB, but it should be less than 70% of free RAM,\
    and total RAM is $total_ram GB.  This will likely cause trouble (like 'ERROR 2013 (HY000)' and 'ERROR 2002 (HY000)')."; fi

  # Configure ODBC
  # Setup ODBC Data Source Name (DSN)
  # if we deicde that we don't need ODBC, remove this block, as well
  #   (see "Install mysql-connector-odbc on RL8" - https://git.ligo.org/computing/helpdesk/-/issues/3863)
  if [ $host == "segments" ] || [ $host == "segments-web" ] || [ $host == "segments-backup" ] ||  \
     [ $host == "segments-dev" ] || [ $host == "segments-dev2" ]
  then
    mv_to_bak /etc/odbc.ini
    # old method
    #cp  /backup/segdb/reference/root_files/$host/odbc_rl8.ini  /etc/odbc.ini
    # new method
    ln -s /root/git_dqsegdb_private/dqsegdb-private/odbc/odbc.ini_${host}_rl8  /etc/odbc.ini
  fi

  # Configure WSGI
  #cp  /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_wsgi.conf        /etc/httpd/conf.d/wsgi.conf
  # old way:
  #echo "LoadModule wsgi_module modules/mod_wsgi_python3.so" >> /etc/httpd/conf.d/wsgi.conf
  #echo "WSGIScriptAlias / /opt/dqsegdb/python_server/src/application.py" >> /etc/httpd/conf.d/wsgi.conf
  #echo "WSGIPythonPath /opt/dqsegdb/python_server/src/" >> /etc/httpd/conf.d/wsgi.conf
  # Configure SciTokens
  #echo "WSGIPassAuthorization On" >> /etc/httpd/conf.d/wsgi.conf
  # new way:
  ln -s /root/git_dqsegdb_private/dqsegdb-private/http/wsgi.conf  /etc/httpd/conf.d/wsgi.conf

  # Configure PHP
  # if using regular PHP module (i.e., not PHP-FPM or something), need to use prefork MPM module - see https://git.ligo.org/computing/dqsegdb/server/-/issues/26
  # old method:
  # set MPM (Multi-Processing Module); turn on prefork module (if not set), turn off worker and event modules (if set)
  #sed -i 's+#LoadModule mpm_prefork_module modules/mod_mpm_prefork.so+LoadModule mpm_prefork_module modules/mod_mpm_prefork.so+g'  /etc/httpd/conf.modules.d/00-mpm.conf
  #sed -i 's+^LoadModule mpm_worker_module modules/mod_mpm_worker.so+#LoadModule mpm_worker_module modules/mod_mpm_worker.so+g'  /etc/httpd/conf.modules.d/00-mpm.conf
  #sed -i 's+^LoadModule mpm_event_module modules/mod_mpm_event.so+#LoadModule mpm_event_module modules/mod_mpm_event.so+g'  /etc/httpd/conf.modules.d/00-mpm.conf
  # new method:
  mv_to_bak /etc/httpd/conf.modules.d/00-mpm.conf
  ln -s /root/git_dqsegdb_private/dqsegdb-private/http/00-mpm.conf  /etc/httpd/conf.modules.d/00-mpm.conf

  # Configure phpMyAdmin
  # not sure if the file copied in the following line even does anything:
  #cp /backup/segdb/reference/install_support/segments/etc_httpd_conf.d_config.inc.php    /etc/httpd/conf.d/config.inc.php 
  #   segments.ligo.org has both /etc/phpMyAdmin/config.inc.php and /etc/httpd/conf.d/config.inc.php, and it works fine;
  #   segments-dev2.ligo.org has only /etc/phpMyAdmin/config.inc.php, and it works fine;
  #   leaving that line out for now (but the file it would copy is still there)
  mv_to_bak /etc/phpMyAdmin/config.inc.php
  mkdir -p /etc/phpMyAdmin/
  # old way
  #cp /backup/segdb/reference/install_support/config.inc.php   /etc/phpMyAdmin/
  # new way
  ln -s /root/git_dqsegdb_private/dqsegdb-private/http/config.inc.php  /etc/phpMyAdmin/config.inc.php
  chown root:apache /etc/phpMyAdmin/config.inc.php

  if [ $host == "segments-web" ]
  then
    # we'll do all of the Shibboleth stuff here, in one go, rather than splitting it up
    cd /etc/yum.repos.d/ 
    wget http://download.opensuse.org/repositories/security:/shibboleth/CentOS_7/security:shibboleth.repo
    yum -y install shibboleth   # I think this creates /etc/httpd/conf.d/shib.conf, which will then be backed up
    # yum install -y ligo-shibboleth-sp   # I think this was the old installation package
    mv_to_bak /etc/httpd/conf.d/shib.conf
    mv_to_bak /etc/httpd/conf.d/dqsegdb.conf
    # why did we used to move the backup to a file with that name?
      #if [ -e /etc/httpd/conf.d/dqsegdb.conf ]; then mv /etc/httpd/conf.d/dqsegdb.conf /etc/httpd/conf.d/dqsegdb.conf_other_machines_$(date +%Y.%m.%d-%H.%M.%S).bak; fi
    cp -p  /backup/segdb/reference/install_support/segments-web/etc_httpd_conf.d_dqsegdb.conf  /etc/httpd/conf.d/dqsegdb.conf
    cp -p  /backup/segdb/reference/install_support/segments-web/etc_httpd_conf.d_shib.conf     /etc/httpd/conf.d/shib.conf
    cp -rp /backup/segdb/reference/install_support/segments-web/shib_self_cert                 /root/
    chmod 0600 /root/shib_self_cert/selfsignedkey.pem
    chmod 0644 /root/shib_self_cert/selfsignedcert.pem
 # which of the following do we need to do?
    cp -p /root/shib_self_cert/selfsignedcert.pem          /etc/shibboleth/sp-cert.pem
    cp -p /root/shib_self_cert/selfsignedcert.pem          /etc/shibboleth/sp-signing-cert.pem
    cp -p /root/shib_self_cert/selfsignedkey.pem           /etc/shibboleth/sp-key.pem
    cp -p /root/shib_self_cert/selfsignedkey.pem           /etc/shibboleth/sp-signing-key.pem
    chown shibd:shibd /etc/shibboleth/sp*cert.pem   # do we need to do this?
    chown shibd:shibd /etc/shibboleth/sp*key.pem
    cd /etc/shibboleth
    # for the following 3 files, we rename extant copies (if any), then download new copies
    mv_to_bak login.ligo.org.cert.LIGOCA.pem
    wget https://wiki.ligo.org/pub/AuthProject/DeployLIGOShibbolethSL7/login.ligo.org.cert.LIGOCA.pem
    mv_to_bak shibboleth2.xml
    wget https://wiki.ligo.org/pub/AuthProject/DeployLIGOShibbolethSL7/shibboleth2.xml
    mv_to_bak attribute-map.xml
    wget https://wiki.ligo.org/pub/AuthProject/DeployLIGOShibbolethSL7/attribute-map.xml
    # if the downloads didn't work, then grab local copies (and let the installer know)
    if [ ! -e login.ligo.org.cert.LIGOCA.pem ]; then
      echo "### WARNING ### The file 'login.ligo.org.cert.LIGOCA.pem' could not be downloaded; \
      using the copy '/backup/segdb/reference/install_support/segments-web/login.ligo.org.cert.LIGOCA.pem' instead."
      cp -p /backup/segdb/reference/install_support/segments-web/login.ligo.org.cert.LIGOCA.pem  .
    fi
    if [ ! -e shibboleth2.xml ]; then
      echo "### WARNING ### The file 'shibboleth2.xml' could not be downloaded; \
      using the copy '/backup/segdb/reference/install_support/segments-web/shibboleth2.xml' instead."
      cp -p /backup/segdb/reference/install_support/segments-web/shibboleth2.xml  .
    fi
    if [ ! -e attribute-map.xml ]; then
      echo "### WARNING ### The file 'attribute-map.xml' could not be downloaded; \
      using the copy '/backup/segdb/reference/install_support/segments-web/attribute-map.xml' instead."
      cp -p /backup/segdb/reference/install_support/segments-web/attribute-map.xml  .
    fi
    # the following line replaces 'entityID=""' with 'entityID="https://segments-web.ligo.org/shibboleth-sp"'
    sed -i 's/entityID\=\"\"/entityID\=\"https\:\/\/segments-web.ligo.org\/shibboleth-sp\"/g'  /etc/shibboleth/shibboleth2.xml
    # the following probably duplicates (and adds to) a section within shib.conf, but that's *probably* OK
    echo "<Location /secure>"  >>  /etc/httpd/conf.d/shib.conf
    echo " AuthType shibboleth"  >>  /etc/httpd/conf.d/shib.conf
    echo " ShibRequestSetting requireSession 1"  >>  /etc/httpd/conf.d/shib.conf
    echo " <RequireAll>"  >>  /etc/httpd/conf.d/shib.conf
    echo " require shib-session"  >>  /etc/httpd/conf.d/shib.conf
    echo " require shib-attr isMemberOf Communities:LSCVirgoLIGOGroupMembers"  >>  /etc/httpd/conf.d/shib.conf
    echo " </RequireAll>"  >>  /etc/httpd/conf.d/shib.conf
    echo "</Location>"  >>  /etc/httpd/conf.d/shib.conf
    cp -p  /backup/segdb/reference/install_support/segments-web/lsc-logo.jpg  /etc/shibboleth/
  fi
  chown -R root:root /etc/httpd/conf.d
  chown -R root:root /etc/httpd/conf

  if [ $host == "segments" ] || [ $host == "segments-web" ] || [ $host == "segments-backup" ] ||  \
     [ $host == "segments-dev" ] || [ $host == "segments-dev2" ]
  then   ### use new method
    sleep 1   ### do nothing; the line below was already done previously
    #ln -s /root/git_dqsegdb_private/dqsegdb-private/http/dqsegdb.conf_${host}  /etc/httpd/conf.d/dqsegdb.conf
  else   ### use old method
    # Get the 2 IP addresses (internal network and external network) and the hostname
    # on RL8, the 'grep "inet 10"'-type line looks like "inet 10.14.0.106  netmask 255.255.0.0  broadcast 10.14.255.255"
    int_addr=`ifconfig | grep "inet 10" | awk '{print $2}'`
    # on RL8, the line looks like "inet 131.215.113.159  netmask 255.255.255.0  broadcast 131.215.113.255"
    ext_addr=`ifconfig | grep "inet 131" | awk '{print $2}'`
    echo "internal network address =  $int_addr"
    echo "external network address =  $ext_addr"
    server_name=`hostname -f`
    echo "server name =  $server_name"

    # Replace the IP addresses and hostname in the dqsegdb config file (which has values for segments.ligo.org by default)
    #   Note that segments-web already had its dqsegdb.conf replaced, so these lines (intentionally) have no effect
    cp /etc/httpd/conf.d/dqsegdb.conf /etc/httpd/conf.d/dqsegdb.conf_$(date +%Y.%m.%d-%H.%M.%S).bak
      sed -i "s/segments\.ligo\.org/${server_name}/g"   /etc/httpd/conf.d/dqsegdb.conf
      sed -i "s/131\.215\.113\.156/${ext_addr}/g"   /etc/httpd/conf.d/dqsegdb.conf
      sed -i "s/10\.14\.0\.101/${int_addr}/g"   /etc/httpd/conf.d/dqsegdb.conf
  fi

  # old method:
  #echo "OPENSSL_ALLOW_PROXY_CERTS=1" >> /etc/sysconfig/httpd
  #echo 'PYTHONPATH="/opt/dqsegdb/python_server/src:${PYTHONPATH}"'>> /etc/sysconfig/httpd
  # new method:
  mv_to_bak /etc/sysconfig/httpd
  ln -s /root/git_dqsegdb_private/dqsegdb-private/http/etc_sysconfig_httpd_rl8  /etc/sysconfig/httpd

  # Import data and create main database.
  ### this is now done at the very end of the script, b/c it takes so long to restore the full dqsegdb

  ### Restore an *empty* dqsegdb here
  ### Note that this will have tables, flags, etc., incl. users ‘dqsegdb_user’ and ‘admin’
  if [ 1 -eq 0 ]; then
    ### this part is to restore a *blank* DB, not a backed-up DB
    ### this part isn't currently an option; the code is parked here in case we ever want to make it an option
    mkdir /root/empty_database
    cp /backup/segdb/reference/install_support/empty_database.tgz  /root/empty_database/
    cp /backup/segdb/reference/install_support/segments/populate_from_backup.sh  /root/empty_database/
    cd /root/empty_database/
    tar xvzf empty_database.tgz
    #/bin/bash ./populate_from_backup.sh
    sudo -u ldbd ./populate_from_backup.sh
  fi
fi   # run_block_3


if [ $run_block_4 -eq 1 ]; then   # * Importing certificates and starting Apache and MariaDB
  if [ $verbose -eq 1 ]; then echo "### Importing certificates and starting Apache and MariaDB (run_block_4);  $(date)"; fi
  # philosophy: configuration done here, not installation (run_block_2) or starting of services (run_block_4)

  # move certs to appropriate locations, as referenced by /etc/httpd/conf.d/dqsegdb.conf
  # later note: we don't do it this way anymore; for RL8 under Puppet configuration, the *.pem, *.key, and *.cert files
  #   are saved in /etc/httpd/x509-certs/, but more importantly, they're provisioned by Puppet, not this script
  #   (so the whole next chunk of code is awkwardly if-blocked out)
  #
  #
  if [ 0 -eq 1 ]; then
  if [ $host == "segments" ]; then \
    #cp  /backup/segdb/reference/install_support/segments/ldbd*pem  /etc/grid-security/; \
    cp  /backup/segdb/reference/install_support/segments/robot*pem  /etc/grid-security/; \
    cp  /backup/segdb/reference/install_support/segments/segments.ligo.org.*  /etc/grid-security/; \
  fi
  if [ $host == "segments-backup" ]; then \
    cp  /backup/segdb/reference/install_support/segments/robot*pem  /etc/grid-security/; \
    cp  /backup/segdb/reference/install_support/segments-backup/segments-backup.ligo.org.*  /etc/grid-security/
  fi
  if [ $host == "segments-web" ]; then \
    cp  /backup/segdb/reference/install_support/segments-web/segments-web.ligo.org.*  /etc/grid-security/; \
  fi
  if [ $host == "segments-dev" ]; then \
    cp  /backup/segdb/reference/install_support/segments-dev/ldbd*pem  /etc/grid-security/; \
    cp  /backup/segdb/reference/install_support/segments-dev/robot*pem  /etc/grid-security/; \
    cp  /backup/segdb/reference/install_support/segments-dev/segments-dev.ligo.org.*  /etc/grid-security/
  fi
  if [ $host == "segments-dev2" ]; then \
  #   cp  /backup/segdb/reference/install_support/segments-dev/ldbd*pem  /etc/grid-security/; \   ### do we need this?
    cp  /backup/segdb/reference/install_support/segments-dev2/segments-dev2.ligo.org.*  /etc/grid-security/
  fi
  mv_to_bak /etc/pki/tls/certs/localhost.crt
  mv_to_bak /etc/pki/tls/private/localhost.key
  cp /etc/grid-security/${host}.ligo.org.pem /etc/pki/tls/certs/localhost.crt 
  cp /etc/grid-security/${host}.ligo.org.key /etc/pki/tls/private/localhost.key
  #
  #
  fi

  # Start MariaDB
  if [ $live -eq 1 ]
  then
    systemctl enable mariadb.service
    systemctl restart mariadb.service
    if [ -e /var/lib/mysql/mysql.sock ]
    then
      echo "### INFO ### MariaDB was successfully started."
    else
      echo "### WARNING ### MariaDB does not seem to be running, which will probably have significant effects \
      on the rest of the installation."
      echo "            ### This installation script will pause for 30 seconds, in case you want to kill it now."
      sleep 30
    fi  
  else
    echo "### NOTICE ### The 'live' variable is not set to 1, so the mariadb service is not being started."
  fi
  # Create database users and give them privileges
  ### Note that the ‘empty_database.tgz’ and dqsegdb backups have users ‘dqsegdb_user’ and ‘admin’, 
  ###      but they don’t work right, so we create the users and give them permissions even before the DB is restored
  ### old way of doing it here:
  #cp  /backup/segdb/reference/install_support/mysql_user_commands.sh  /root/bin/
  #/bin/bash  /root/bin/mysql_user_commands.sh
  #rm  /root/bin/mysql_user_commands.sh
  ### new way of doing it here:
  if [ -e /var/lib/mysql/mysql.sock ]
  then
    # old way
    #if [ $host != "" ]; then mysql -uroot -A < /backup/segdb/reference/install_support/${host}/MySQLUserGrants.sql; fi
    # new way
    if [ $host != "" ]; then mysql -uroot -A < /root/git_dqsegdb_private/dqsegdb-private/mysql/MySQLUserGrants.sql_${host}; fi
  else
    echo "### WARNING ### MariaDB does not seem to be running, so we cannot install the DB users and permissions."
    echo "            ### You should probably fix MariaDB, then re-run this block of code (and maybe the blocks after it.)"
    echo "            ### This installation script will pause for 20 seconds, in case you want to kill it now."
    sleep 20
  fi  

  # Start Apache server.
  if [ $live -eq 1 ]
  then
    systemctl enable httpd.service
    systemctl restart httpd.service
    systemctl status httpd.service &> /dev/null
    if [ "$?" -eq 0 ]
    then
      echo "### INFO ### httpd was successfully started."
    else
      echo "### WARNING ### http was not successfully started."
      echo "            ### 'systemctl status httpd.service':"
      systemctl status httpd.service
    fi
  fi
fi   # run_block_4


if [ $run_block_5 -eq 1 ]; then   # * Installing segments publisher code   ### Publishing   #publishing   #publisher
  if [ $verbose -eq 1 ]; then echo -e "### Starting installation of publisher (run_block_5);  $(date)"; fi
  if [ $host == "segments" ] || [ $host == "segments-dev" ] || [ $host == "segments-backup" ] || [ $host == "segments-dev2" ]; then
    if [ $verbose -eq 1 ]; then echo "### Installing publisher code;  $(date)"; fi
    mkdir -p /dqxml/H1
    mkdir -p /dqxml/L1
    mkdir -p /dqxml/V1
    mkdir -p /dqxml/G1
    chown dqxml:dqxml  /dqxml/H1
    chown dqxml:dqxml  /dqxml/L1
    #cp  /backup/segdb/reference/install_support/etc_init.d_dir/dqxml_pull_from_obs  /etc/init.d/
    #cp  /backup/segdb/reference/install_support/root_bin_dir/dqxml_pull_from_obs  /root/bin/
    mv_to_bak /root/bin/manual_rsync_GEO.sh
    mv_to_bak /root/bin/manual_rsync_LHO.sh
    mv_to_bak /root/bin/manual_rsync_LLO.sh
    mv_to_bak /root/bin/manual_rsync_VGO.sh
    ln -s /root/git_dqsegdb_server/server/bin/manual_rsync_GEO.sh /root/bin/manual_rsync_GEO.sh
    ln -s /root/git_dqsegdb_server/server/bin/manual_rsync_LHO.sh /root/bin/manual_rsync_LHO.sh
    ln -s /root/git_dqsegdb_server/server/bin/manual_rsync_LLO.sh /root/bin/manual_rsync_LLO.sh
    ln -s /root/git_dqsegdb_server/server/bin/manual_rsync_VGO.sh /root/bin/manual_rsync_VGO.sh
    mv_to_bak /root/bin/test_rsync.sh
    ln -s /root/git_dqsegdb_server/server/bin/test_rsync.sh /root/bin/test_rsync.sh 
    if [ ! -e /root/bin/ligolw_dtd.txt ]; then ln -s /root/git_dqsegdb_server/server/bin/ligolw_dtd.txt /root/bin/ligolw_dtd.txt; fi
    # we make copies of the O4 publisher script for each server, intead of changing the file in the cloned repo (that we don't want to propagate back to the repo)
    if [ $host == "segments" ]; then
      mv_to_bak /root/bin/run_publishing_O4_segmentsligoorg.sh
      ln -s /root/git_dqsegdb_server/server/bin/run_publishing_O4_segmentsligoorg.sh  /root/bin/run_publishing_O4_segmentsligoorg.sh
    fi
    if [ $host == "segments-dev" ]; then
      mv_to_bak /root/bin/run_publishing_O4_segmentsdevligoorg.sh
      ln -s /root/git_dqsegdb_server/server/bin/run_publishing_O4_segmentsdevligoorg.sh  /root/bin/run_publishing_O4_segmentsdevligoorg.sh
    fi
    if [ $host == "segments-backup" ]; then
      mv_to_bak /root/bin/run_publishing_O4_segmentsbackupligoorg.sh
      ln -s /root/git_dqsegdb_server/server/bin/run_publishing_O4_segmentsbackupligoorg.sh /root/bin/run_publishing_O4_segmentsbackupligoorg.sh
    fi
    mv_to_bak /dqxml/fix_3_commas.sh
    cp /backup/segdb/reference/install_support/segments/root_bin/fix_3_commas.sh  /dqxml/
    mkdir -p /root/bad_dqxml/
    if [ $host == "segments" ] || [ $host == "segments-backup" ]
    then
      # note that the scripts are copied for segments and segments-backup, but the service is only turned on by default for segments
      mv_to_bak /etc/init.d/dqxml_push_to_ifocache
      cp  /backup/segdb/reference/install_support/etc_init.d_dir/dqxml_push_to_ifocache  /etc/init.d/
      mv_to_bak /root/bin/dqxml_push_to_ifocache
      cp  /backup/segdb/reference/install_support/root_bin_dir/dqxml_push_to_ifocache  /root/bin/
    fi
    mkdir -p /var/log/publishing/dev/
    mv_to_bak /var/log/publishing/state/
    mkdir -p /var/log/publishing/state/
    mkdir -p /var/log/publishing/pid/
    #latest_state_dir=`ls -1tr /backup/segdb/segments/publisher/spool/ | tail -n 1`
    latest_state_dir=`ls -1tr /backup/segdb/segments/publisher/spool/ | tail -n 2 | head -n 1`
      ### would it be better to go back one day for state files, to be safe? then just re-publish that one day's segments? 
      ### if so, replace 'tail -n 1' with 'tail -n 2 | head -n 1'
      ### DB backup is done at 00:00; 'spool_backup' is done at 00:07; that means that ~7 minutes of segments will be
      ###   recorded in the state file, but won't show up in the backup
    cp -p /backup/segdb/segments/publisher/spool/$latest_state_dir/*  /var/log/publishing/state/
    if [ 0 -eq 1 ]; then
      ### this wouldn't be done for a regular installation but is here in case it's needed at some point
      cp /backup/segdb/segments-dev/install/blank-DQ_Segments_current_dev.xml  /var/log/publishing/state/
      cp /backup/segdb/segments-dev/install/blank-DQ_Segments_current_dev.xml  /var/log/publishing/state/H-DQ_Segments_current_dev.xml
      cp /backup/segdb/segments-dev/install/blank-DQ_Segments_current_dev.xml  /var/log/publishing/state/L-DQ_Segments_current_dev.xml
      cp /backup/segdb/segments-dev/install/blank-DQ_Segments_current_dev.xml  /var/log/publishing/state/V-DQ_Segments_current_dev.xml
      cp /backup/segdb/segments-dev/install/blank-DQ_Segments_current_dev.xml  /var/log/publishing/state/G-DQ_Segments_current_dev.xml
    fi

    ### something looks for the ligolw_dtd.txt file here, doesn't it? (check; if not, get rid of this)
    if [ ! -e /root/Publisher/etc/ligolw_dtd.txt ]; then 
      mkdir -p /root/Publisher/etc/
      ln -s /root/git_dqsegdb_server/server/bin/ligolw_dtd.txt /root/Publisher/etc/ligolw_dtd.txt
    fi

    mv_to_bak /etc/grid-security/grid-mapfile-insert
    cp -p  /backup/segdb/reference/lgmm/grid-mapfile-insert  /etc/grid-security/

    if [ $live -eq 1 ]
    then
      # we don't use dqxml_pull_from_obs anymore
      #/sbin/chkconfig  dqxml_pull_from_obs  on
      #systemctl start dqxml_pull_from_obs.service
      if [ $host == "segments" ] || [ $host == "segments-backup" ]
      then
        echo 1 > /root/bin/start_manual_rsync_GEO.txt
        echo 1 > /root/bin/start_manual_rsync_LHO.txt
        echo 1 > /root/bin/start_manual_rsync_LLO.txt
        echo 1 > /root/bin/start_manual_rsync_VGO.txt
        # we probably don't want to start these in the installation script, since they'll also be run from crontabs,
        #   which would produce 2 versions of the script running; start by hand, if needed
        #nohup  /root/bin/manual_rsync_GEO.sh  >>  /root/bin/manual_rsync_GEO_log_$(date +%Y.%m.%d).txt  &
        #nohup  /root/bin/manual_rsync_LHO.sh  >>  /root/bin/manual_rsync_LHO_log_$(date +%Y.%m.%d).txt  &
        #nohup  /root/bin/manual_rsync_LLO.sh  >>  /root/bin/manual_rsync_LLO_log_$(date +%Y.%m.%d).txt  &
        #nohup  /root/bin/manual_rsync_VGO.sh  >>  /root/bin/manual_rsync_VGO_log_$(date +%Y.%m.%d).txt  &
      fi
      if [ $host == "segments" ]
      then
        systemctl enable dqxml_push_to_ifocache.service
        systemctl start dqxml_push_to_ifocache.service
      fi
    fi
    # create some useful links
    mv_to_bak /root/bin/pid_files
    ln -s  /var/log/publishing/pid/          /root/bin/pid_files
    mv_to_bak /root/bin/publisher_log_files
    ln -s  /var/log/publishing/dev           /root/bin/publisher_log_files
    mv_to_bak /root/bin/state_files
    ln -s  /var/log/publishing/state/        /root/bin/state_files
  fi
fi   # run_block_5


if [ $run_block_6 -eq 1 ]; then   # * Handling remaining machine-specific items
  if [ $verbose -eq 1 ]; then echo "### Handling remaining machine-specific items (run_block6);  $(date)"; fi
  if [ $host == "segments" ]
  then
    mkdir -p /usr1/ldbd/
    mkdir -p /usr1/ldbd/bin
    cp -p  /backup/segdb/reference/install_support/segments/ldbd/backup_dqsegdb_mysqldatabase_newdir.sh    /usr1/ldbd/bin/
    cp -p  /backup/segdb/reference/install_support/segments/ldbd/weekly_backup_rotate_newdir.py            /usr1/ldbd/bin/
    cp -p  /backup/segdb/reference/install_support/segments/ldbd/monthly_backup_newdir.py                  /usr1/ldbd/bin/
    cp -p  /backup/segdb/reference/install_support/segments/ldbd/daily_backup_rotate_newdir.py             /usr1/ldbd/bin/
    cp -p  /backup/segdb/reference/install_support/segments/ldbd/log_backup.sh                             /usr1/ldbd/bin/
    cp -p  /backup/segdb/reference/install_support/segments/ldbd/spool_backup.sh                           /usr1/ldbd/bin/
    cp -p  /backup/segdb/reference/install_support/segments/ldbd/backup_opt_dqsegdb_python_server_logs.py  /usr1/ldbd/bin/
    chown -R ldbd:ldbd /usr1/ldbd
  fi
  if [ $host == "segments-backup" ]
  then
    mkdir -p /usr1/ldbd/
    mkdir -p /usr1/ldbd/bin
    ### DB restore lines
    # note that root can copy/move to /usr1/ldbd/ directly, but /home/segdb/ will take sudo'ing
    mv_to_bak /usr1/ldbd/bin/run_populate_from_backup_2_dbs.sh
    cp  /root/git_dqsegdb_server/server/bin/run_populate_from_backup_2_dbs.sh  /usr1/ldbd/bin/
    mv_to_bak /usr1/ldbd/bin/populate_from_backup.sh
    cp  /backup/segdb/reference/install_support/segments-backup/ldbd/populate_from_backup.sh  /usr1/ldbd/bin/
    mv_to_bak /usr1/ldbd/bin/populate_from_backup_alpha.sh
    cp  /root/git_dqsegdb_server/server/bin/populate_from_backup_alpha.sh  /usr1/ldbd/bin/
    mv_to_bak /usr1/ldbd/bin/populate_from_backup_beta.sh
    cp  /root/git_dqsegdb_server/server/bin/populate_from_backup_beta.sh  /usr1/ldbd/bin/
    mv_to_bak /root/bin/flip_odbc_target.sh
    ln -s  /root/git_dqsegdb_server/server/bin/flip_odbc_target.sh  /root/bin/flip_odbc_target.sh
    mkdir -p /usr1/ldbd/backup_logging
    cp  /backup/segdb/segments/backup_logging/import_time.log  /usr1/ldbd/backup_logging/
    ### regression tests
    mv_to_bak /root/bin/run_regression.sh
    ln -s  /root/git_dqsegdb_server/server/bin/run_regression.sh  /root/bin/run_regression.sh
    mkdir -p /root/log/
    mv_to_bak /root/bin/check_regression_test_log_file.awk
    ln -s  /root/git_dqsegdb_server/server/bin/check_regression_test_log_file.awk  /root/bin/check_regression_test_log_file.awk
    mv_to_bak /usr1/ldbd/bin/backup_dqsegdb_regression_tests_mysqldatabase.sh
    cp  /root/git_dqsegdb_server/server/bin/backup_dqsegdb_regression_tests_mysqldatabase.sh  /usr1/ldbd/bin/
    #cp  /backup/segdb/reference/install_support/segments-backup/ldbd/backup_dqsegdb_regression_tests_mysqldatabase.sh  \
    #      /usr1/ldbd/bin/
    mv_to_bak /usr1/ldbd/bin/run_backup_dqsegdb_regression_tests_mysqldatabase.sh
    cp  /root/git_dqsegdb_server/server/bin/run_backup_dqsegdb_regression_tests_mysqldatabase.sh  /usr1/ldbd/bin/
  # this part sets up the regression tests themselves
    mkdir -p /usr1/ldbd/log
    chown -R ldbd:ldbd /usr1/ldbd
    mv_to_bak /opt/dqsegdb/regression_test_suite
    mkdir -p /opt/dqsegdb/regression_test_suite
    mv_to_bak /opt/dqsegdb/logs/regression_test_suite
    mkdir -p /opt/dqsegdb/logs/regression_test_suite
    ln -s /root/git_dqsegdb_server/server/db/db_utils/component_interface_data_integrity_test_suite/src \
            /opt/dqsegdb/regression_test_suite/src
    # there is an issue with DAO.py and /usr/lib64/python2.7/site-packages/MySQLdb/cursors.py; this fixes it;
    #   see work notes for 2019.02.20 for details and 2019.04.03 for the (wrong) fix and 2019.04.05 for the kludge fix
    #sed -i 's/str(dataset_id))/["str(dataset_id)"]/g' /opt/dqsegdb/regression_test_suite/src/DAO.py
    if [ ! -e /opt/dqsegdb/regression_test_suite/src/DAO.py_2019.02.20_test.py ]; then
      cp /backup/segdb/reference/install_support/segments-backup/DAO.py_2019.02.20_test.py /opt/dqsegdb/regression_test_suite/src/
    fi
    # do we need or want to do this? - it's already in the repo version, so it's creating an untracked copy of a repo file
    mv_to_bak /opt/dqsegdb/regression_test_suite/src/DAO.py
    ln -s  /root/git_dqsegdb_server/server/db/db_utils/component_interface_data_integrity_test_suite/src/DAO.py_2019.02.20_test.py \
             /opt/dqsegdb/regression_test_suite/src/DAO.py
    # this package (MySQL-python) is for SL7; it isn't available on RL8; its replacement is python3-mysql; do we need it, though?
    #yum -y install MySQL-python
  # this part restores a backed-up regression test DB (dqsegdb DB is restored later)
    if [ -e /var/lib/mysql/mysql.sock ]
    then
      output_date=`date +%Y.%m.%d-%H.%M.%S`
      tmp_dir=/backup/segdb/segments/install_support/tmp/${host}_restore_${output_date}
      mkdir -p  $tmp_dir
      cp /backup/segdb/reference/install_support/populate_from_backup_dqsegdb_regression_tests_for_installation_script.sh  $tmp_dir
      cp /backup/segdb/segments/regression_tests/dqsegdb_regression_tests_backup.tgz  $tmp_dir
      cd $tmp_dir
      tar xvzf dqsegdb_regression_tests_backup.tgz --no-same-owner   # "--no-same-owner" avoids errors
      sudo -u ldbd ./populate_from_backup_dqsegdb_regression_tests_for_installation_script.sh  $tmp_dir  dqsegdb_regression_tests
      cd /root/
      rm -rf  $tmp_dir
      # create the users and privileges associated with the regression test DB
      mysql -uroot -A < /backup/segdb/reference/install_support/${host}/MySQLUserGrants_tables.sql
    else
      echo "### WARNING ### MariaDB does not seem to be running, so we cannot install the regression test DB or users."
      echo "            ### You should probably fix MariaDB, then re-run this block of code (and maybe the blocks after it.)"
      echo "            ### This installation script will pause for 20 seconds, in case you want to kill it now."
      sleep 20
    fi
  fi
  if [ $host == "segments-web" ]
  then
    systemctl enable shibd.service
    systemctl restart shibd.service
    cp -p /backup/segdb/reference/install_support/segments-web/lstatus                          /root/bin/
    cp -p /backup/segdb/reference/install_support/segments-web/backup_dqsegdb_web.sh            /root/bin/
    cp -p /backup/segdb/reference/install_support/segments-web/backup_dqsegdb_web_downloads.sh  /root/bin/
    mkdir -p /usr/share/dqsegdb_web
      # this needs to be modified to use repo in /root/git_dqsegdb_server, rather than dqsegdb_git
    cp -rp  /root/dqsegdb_git/dqsegdb/web/src/*  /usr/share/dqsegdb_web/
    mv_to_bak /usr/share/dqsegdb_web/classes/GetContent.php
    cp -p  /backup/segdb/reference/install_support/segments-web/usr_share_dqsegdb_web_classes_GetContent.php  \
             /usr/share/dqsegdb_web/classes/GetContent.php
    mv_to_bak /usr/share/dqsegdb_web/classes/InitVar.ph
    cp -p  /backup/segdb/reference/install_support/segments-web/usr_share_dqsegdb_web_classes_InitVar.php  \
             /usr/share/dqsegdb_web/classes/InitVar.php
    mv_to_bak /usr/share/dqsegdb_web/classes/JSActions.php
    cp -p  /backup/segdb/reference/install_support/segments-web/usr_share_dqsegdb_web_classes_JSActions.php  \
             /usr/share/dqsegdb_web/classes/JSActions.php
    #mkdir /usr/share/dqsegdb_web/downloads
    cd /usr/share/dqsegdb_web/
    tar xzf  /backup/segdb/reference/install_support/segments-web/downloads.tgz   # this will make the 'downloads' dir itself
    if [ -e /usr/share/dqsegdb_web ]; then ln -s /usr/share/dqsegdb_web/ /root/bin/web_files ;
      else echo "### WARNING ### Directory /usr/share/dqsegdb_web/ does not exist, so we can't link to it.  \
      (But it should exist.)"
    fi
    if [ -e /usr/share/dqsegdb_web/downloads ]; then ln -s /usr/share/dqsegdb_web/downloads/ /root/bin/json_payloads ;
      else echo "### WARNING ### Directory /usr/share/dqsegdb_web/downloads/ does not exist, so we can't link to it.  \
      (But it should exist.)"
    fi
### ownership and permissions for files in /usr/share/dqsegdb_web ??
  # this part restores a backed-up dqsegdb_web DB (contains info on past segments-web queries)
    backup_dir=/backup/segdb/reference/install_support/segments-web/dqsegdb_web_db/
    /backup/segdb/reference/install_support/populate_from_backup_dqsegdb_web_for_installation_script.sh  \
        $backup_dir  dqsegdb_web
  # create the users and privileges associated with the DB
  if [ -e /var/lib/mysql/mysql.sock ]
  then
    mysql -uroot -A < /backup/segdb/reference/install_support/${host}/MySQLUserGrants.sql
  else
    echo "### WARNING ### MariaDB does not seem to be running, so we cannot install the segments-web users."
    echo "            ### You should probably fix MariaDB, then re-run this block of code (and maybe the blocks after it.)"
    echo "            ### This installation script will pause for 20 seconds, in case you want to kill it now."
    sleep 20
  fi
###segments-web
  # /etc/httpd/
    ### change this to pull files from the server's installation dir, rather than ~~/root_files/[host]/ ?
    cp  `ls -1rt /backup/segdb/reference/root_files/$host/crontab_-l_root* | tail -n 1` /var/spool/cron/root
    # if there's trouble with iptables, these files might help:
    # /backup/segdb/reference/install_support/segments-web/etc_sysconfig_iptables__segments-web_old
    # /backup/segdb/reference/install_support/segments-web/etc_sysconfig_iptables-config__segments-web_old
  fi
  if [ $host == "segments-dev" ]
  then
    # segments-dev doesn't have any dedicated tasks that would need to be filled in here at the moment
    mkdir -p /usr1/ldbd/
    mkdir -p /usr1/ldbd/bin
    chown -R ldbd:ldbd /usr1/ldbd
  fi
  # note: no user 'ldbd' used on segments-web; segments-dev2 is done manually
fi   # run_block_6


if [ $run_block_7 -eq 1 ]; then   # * restoring main DQSegDB DB   ###restoredb   ###db
  if [ $verbose -eq 1 ]; then echo -e "### Restoring main DQSegDB DB (run_block_7);  $(date)"; fi
  if [ $host == "segments" ]  || [ $host == "segments-dev" ] || [ $host == "segments-backup" ]
  then
    if [ $verbose -eq 1 ]; then echo "### Starting restoring main DQSegDB DB;  $(date)"; fi
    # this part restores a backed-up segments DB
    if [ -e /var/lib/mysql/mysql.sock ]
    then
      output_date=`date +%Y.%m.%d-%H.%M.%S`
      tmp_dir=/backup/segdb/segments/install_support/tmp/${host}_restore_${output_date}
      mkdir -p  $tmp_dir
      cp /backup/segdb/reference/install_support/populate_from_backup_for_installation_script.sh  $tmp_dir
      cp /backup/segdb/segments/primary/*.tar.gz  $tmp_dir
      cd $tmp_dir
      tar xvzf *.tar.gz --no-same-owner   # "--no-same-owner" flag avoids some errors
      # in the script, first arg is the location of the DB files; second arg is the name of the DB to be restored
      if [ $host == "segments-backup" ]
      then
        # segments-backup has a different name for its DB
        sudo -u ldbd ./populate_from_backup_for_installation_script.sh  $tmp_dir  segments_backup
        # create the users and privileges associated with specific tables, after DB exists (only necessary on segments-backup)
        mysql -uroot -A < /backup/segdb/reference/install_support/${host}/MySQLUserGrants_tables.sql
      else
        sudo -u ldbd ./populate_from_backup_for_installation_script.sh  $tmp_dir  dqsegdb
      fi   # [ $host == "segments-backup" ]
      cd ~
      rm -rf  $tmp_dir
    else
      echo "### WARNING ### MariaDB does not seem to be running, so we cannot install the segments DB or users."
      echo "            ### You should probably fix MariaDB, then re-run this block of code (and maybe the blocks after it.)"
      echo "            ### This installation script will pause for 20 seconds, in case you want to kill it now."
      sleep 20
    fi   # [ "$?" -eq 0 ]
  mv_to_bak /root/bin/db_monitor.sh
  ln -s /root/git_dqsegdb_tools/dqsegdb-tools/monitors/db_monitor.sh /root/bin/db_monitor.sh
  fi   # [ $host == "segments" ]  ||  ...
fi   # run_block_7


if [ $run_block_8 -eq 1 ]; then   # * Handling crontabs, misc. links
  if [ $verbose -eq 1 ]; then echo "### Handling crontabs, misc. links (run_block_8);  $(date)"; fi
  # create crontab files; note that this must happen after the DBs are restored, since some cron jobs will write/publish to DBs
  # first, backup any existing cron files that would be overwritten (though there shouldn't be any)
  mv_to_bak /var/spool/cron/root
  mv_to_bak /var/spool/cron/ldbd
  mv_to_bak /var/spool/cron/segdb
  if [ $host == "segments" ] || [ $host == "segments-dev" ] || [ $host == "segments-backup" ]
  then
    if [ $live -eq 1 ]; then
      ### change this to pull files from the server's installation dir, rather than ~~/root_files/[host]/ ? (and run for user segdb)
      cp  `ls -1rt /backup/segdb/reference/root_files/$host/crontab_-l_root* | tail -n 1` /var/spool/cron/root
      cp  `ls -1rt /backup/segdb/reference/ldbd_files/$host/crontab_-l_ldbd* | tail -n 1` /var/spool/cron/ldbd
    else
      cp  `ls -1rt /backup/segdb/reference/root_files/$host/crontab_-l_root*all_lines_commented* | tail -n 1` /var/spool/cron/root
      cp  `ls -1rt /backup/segdb/reference/ldbd_files/$host/crontab_-l_ldbd*all_lines_commented* | tail -n 1` /var/spool/cron/ldbd
    fi
    # create dir to hold files that are created by a cron job, then grabbed by Nagios, for use on monitor.ligo.org:
    if [ $host == "segments" ]
    then
      if [ ! -d /var/www/nagios/ ]; then mkdir -p /var/www/nagios/ ; fi
      ### these first lines fix the glue issue from March 2019; need to fix the original code, instead
      cp /backup/segdb/reference/install_support/segments/root_bin/check_pending_files_2019.03.05.bak  /root/bin/
      cp /backup/segdb/reference/install_support/segments/root_bin/check_pending_files_2019.03.05.fix  /root/bin/
      ln -sf  /root/bin/check_pending_files_2019.03.05.fix  /root/bin/check_pending_files
      cp /backup/segdb/reference/install_support/segments/root_bin/check_pending_files_wrapper_H.sh    /root/bin/
      cp /backup/segdb/reference/install_support/segments/root_bin/check_pending_files_wrapper_L.sh    /root/bin/
      cp /backup/segdb/reference/install_support/segments/root_bin/check_pending_files_wrapper_V.sh    /root/bin/
    fi
  fi
  # Note that there are currently (May 2019) no crontabs for any users on segments-dev2

  cp  /backup/segdb/reference/root_files/${host}/bin/lstatus*  /root/bin/

  # create some useful links for every machine
  # format reminder: 'ln -s [actual_file]   [link_name]'
  if [ ! -e /root/bin/httpd_logs ]; then ln -s  /var/log/httpd/  /root/bin/httpd_logs; fi
  if [ ! -e /root/bin/python_server_log_files ]; then ln -s  /opt/dqsegdb/python_server/logs/  /root/bin/python_server_log_files; fi
  if [ ! -e /root/bin/python_server_log_files_archive ]; then ln -s /backup/segdb/segments/maintenance/automatic/opt_dqsegdb_python_server_logs/ /root/bin/python_server_log_files_archive; fi
fi   # run_block_8


if [ $run_block_9 -eq 1 ]; then   # * Unspecified additional commands (probably added by hand at run time)
  if [ $verbose -eq 1 ]; then echo -e "### Starting unspecified additional commands (run_block_9);  $(date)"; fi
  sleep 0   # have to do *something* or Bash gets cranky
fi   # run_block_9

### User tasks to be performed manually:
#  * on segments-backup: run these commands as root, if you want to start pulling DQ XML files from GEO and Virgo:
#      nohup  /root/bin/manual_rsync_GEO.sh  >>  /root/bin/manual_rsync_GEO_log_$(date +%Y.%m.%d).txt  &
#      nohup  /root/bin/manual_rsync_LHO.sh  >>  /root/bin/manual_rsync_LHO_log_$(date +%Y.%m.%d).txt  &
#      nohup  /root/bin/manual_rsync_LLO.sh  >>  /root/bin/manual_rsync_LLO_log_$(date +%Y.%m.%d).txt  &
#      nohup  /root/bin/manual_rsync_VGO.sh  >>  /root/bin/manual_rsync_VGO_log_$(date +%Y.%m.%d).txt  &

if [ $verbose -eq 1 ]; then echo -e "### Finished installation:  $(date)"; fi

exit


### to do:
### * do ownership and permissions for files in /usr/share/dqsegdb_web matter??
### * make sure duplicate section in shib.conf won't be an issue (added by the multiple echo >> shib.conf lines)
### * what is/are ldbd*pem used for? does segments-backup now need that?
### * which shib certificate names do we use?
### * verify that the shib installation instructions work
### * fix the 'state files saved after backup' issue
### * where does the 'check_pending_files' script live? fix it there and use that, rather than doing the glue fix here
### * come up with an actual fix for the DAO.py issue; implement it here
### * should segments-web have all of the python_server stuff done? (if not, put it all in if-then blocks that don't run for -web)
### * does anything use /root/Publisher/etc/ligolw_dtd.txt ? if not, get rid of it
### * should "/dqxml/V1" and "/dqxml/G1" also be owned by user dqxml? (change code on the publisher machine, too)
### * figure out /etc/phpMyAdmin/config.inc.php vs. /etc/httpd/conf.d/config.inc.php - just diff. loc's. for same file?
### * do we still need the "connecting to git repositories with Kerberos" part?
### * figure out the m2crypto/"Replace with openssl" part - what do we need to do? can we do it?
### * 
### * expand what the "live" variable controls (to include everything that it should control)
### *   this includes: MariaDB, ...?
### * 



