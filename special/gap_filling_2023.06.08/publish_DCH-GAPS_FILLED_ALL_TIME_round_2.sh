#!/bin/bash
# This is a script to publish new segments to H1:DCH-GAPS_FILLED_ALL_TIME:1 to match publishing of regenerated segments from 2023.06.08 - see https://git.ligo.org/computing/dqsegdb/server/-/issues/47
# Note that the script should be run from the same dir as contains the known and active segments files (probably /root/git_dqsegdb_server/server/special/gap_filling_2023.06.08/).

if [ $# -lt 1 ]; then echo "### Usage: (script)  (server, as 'segments', 'segments-backup', etc.)"; exit; fi

action="append"   ### append b/c the flag already exists
server=$1
ifo=H
flag=DCH-GAPS_FILLED_ALL_TIME
known_file=./known_segments_round_2.txt
active_file=./active_segments_round_2.txt
explain="Regenerated segments for 2023.06.08 published on 2023.08.23.  See https://git.ligo.org/computing/dqsegdb/server/-/issues/47 for details."

cmd="ligolw_segment_insert_dqsegdb  --${action}  --segment-url=https://${server}.ligo.org  --ifos=${ifo}1  --name=$flag  --version=1  --summary-file=$known_file  --segment-file=$active_file  --explain=\"$explain\"  --comment=\"$explain\" "
echo "In 10 seconds, running this command:"
echo $cmd
sleep 10

eval $cmd

exit_code=$?

echo "Exit code:  $exit_code"
