#!/bin/bash
# Note: we no longer have to specify lib directory, PATH, PYTHONPATH, etc., b/c we're using the installed dqsegdb package to get the publisher script, not a reference dir.
# For the 2023.06.08 gap-filling task, this script should be run from the same dir as contains the state file, which is probably /root/git_dqsegdb_server/server/special/gap_filling_2023.06.08 .
# The 'round 2' publishing is because the original regenerated files only had known segments when there were active segments; round 2 fills in the known-but-not-active segments, and some corresponding missing flags.
# Details on all of this in https://git.ligo.org/computing/dqsegdb/server/-/issues/47

inf=$1   # should only be 'H' for 2023.06.08 gap-filling task
server=$2
inf="H"
if [ $# -lt 2 ]; then echo "### Usage: (script) (IFO; should only be 'H' for this task) (server to publish to, as 'segments', 'segments-dev', etc.)"; exit; fi
if [ "$inf" != "H" ]; then echo "### IFO should only be 'H' for this task"; exit; fi
#input_dir=/dqxml/${inf}1
#input_dir=/home/john.zweizig/rerun/20230608-segments/split_16s
#input_dir=/home/john.zweizig/rerun/20230608-segments/split_16b
input_dir=/home/john.zweizig/rerun/20230608-segments/split_extras

# check if the X509 certificate and key variables are set to valid files (mainly useful when this script is run from the command line, when the variables are often forgotten)
if test -v  X509_USER_CERT; then sleep 0; else echo "Variable X509_USER_CERT is not set!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -v  X509_USER_KEY;  then sleep 0; else echo  "Variable X509_USER_KEY is not set!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -a $X509_USER_CERT; then sleep 0; else echo "Variable X509_USER_CERT does not point to an actual file!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -a $X509_USER_KEY;  then sleep 0; else echo  "Variable X509_USER_KEY does not point to an actual file!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
# "-v" checks that the variable is set (note no "$" in the test); "-a" checks that the file exists (note the "$" in the test); need both b/c an empty variable will pass the "-a" test (!)
# Later note: It's not clear that it's possible to change the variable values in the shell running the script.  So if the variable isn't set or points to the wrong file,
#   the user probably just has to end the run, fix it, and start over.

executable=`which ligolw_publish_threaded_dqxml_dqsegdb 2> /dev/null`
if [ $? -ne 0 ]; then echo "### ERROR ### Could not find executable file ligolw_publish_threaded_dqxml_dqsegdb.  Exiting."; exit $?; fi

echo "In 10 seconds, publishing to ${server}.ligo.org from $input_dir."
sleep 10

#/usr/bin/env python3 -W ignore::DeprecationWarning ligolw_publish_threaded_dqxml_dqsegdb \
/usr/bin/env python3 -W ignore::DeprecationWarning $executable \
--segment-url https://${server}.ligo.org \
--state-file=./H-DQ_Segments_2023.06.08_gap_publishing_round_2.xml \
--pid-file=./H-DQ_Segments_cur.pid \
--log-file=./H-DQ_Segments_cur_round_2.log \
--input-directory=$input_dir \
--log-level DEBUG -m 100 -c 1

