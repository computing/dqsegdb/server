#!/bin/bash
# This is a script to publish segments to the new flag H1:DCH-GAPS_FILLED_8JUNE2023:1 to match publishing of regenerated segments from 2023.06.08 - see https://git.ligo.org/computing/dqsegdb/server/-/issues/47
# Note that the script should be run from the same dir as contains the known and active segments files (probably /root/git_dqsegdb_server/server/special/gap_filling_2023.06.08/).

if [ $# -lt 1 ]; then echo "### Usage: (script)  (server, as 'segments', 'segments-backup', etc.)"; exit; fi

action="insert"   ### insert b/c the flag is new
server=$1
ifo=H
flag=DCH-GAPS_FILLED_8JUNE2023
known_file=./known_segments.txt
active_file=./active_segments.txt
explain="Regenerated segments for 2023.06.08 published on 2023.08.22.  See https://git.ligo.org/computing/dqsegdb/server/-/issues/47 for details."

cmd="ligolw_segment_insert_dqsegdb  --${action}  --segment-url=https://${server}.ligo.org  --ifos=${ifo}1  --name=$flag  --version=1  --summary-file=$known_file  --segment-file=$active_file  --explain=\"$explain\"  --comment=\"$explain\" "
echo "In 10 seconds, running this command:"
echo $cmd
sleep 10

eval $cmd

exit_code=$?

echo "Exit code:  $exit_code"
