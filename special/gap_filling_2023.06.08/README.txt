From "Publish regenerated H1 segments for 8 June 2023" - https://git.ligo.org/computing/dqsegdb/server/-/issues/47
[quote]
On 8 June 2023, a disk at LHO filled up, preventing new H1_HOFT_C00 frame files from being saved and new DQXML files from being saved.  This issue has been discussed briefly in this issue and at length in this issue, as well as in numerous meetings that might or might not be linked in those issues.  In particular, Robert Bruntz summarized the issue from the DQSegDB issue in this post and then gave some thoughts about different possible approaches to the issue and the possible problems with each in this post.
When it became clear that the missing DQXML files would likely be regenerated and need to be published, the DQSegDB team began testing various edge cases that had not been encountered before (as discussed in this issue, such as the publishing and querying of overlapping segments that were consistent with each other but did not have the same start and end times.  Those tests were all successful, with results summarized in a new wiki page on errors and failure modes.
Since some DQXML files had been produced and their contents published (seen here), Derek Davis produced a set of segments that would complement those that were already published for H1:DMT-ANALYSIS_READY:1 to produce the full set of segments that would have been produced if the original DQXML files had all been produced (saved here: /home/derek.davis/public_html/detchar/O4/analysis_ready_regeneration/).  In parallel, John Zweizig produced new DQXML files, covering the entire time on 8 June 2023 that LHO was in observing mode, saved on the CIT cluster in /home/john.zweizig/rerun/20230608-segments/, in monolithic DQXML files 20230608_segments.xml and 20230608_ht_segments.xml, and in 16-second DQXML files in the subdirectory split_16s.  Those 16-second files were published into a test DB on segments-dev2, and tests showed that queries of H1:DMT-ANALYSIS_READY:1 on that server exactly matched the union of results from the DB on segments.ligo.org and the suggested segments from Derek Davis, so the suggestion was made to set up a special publisher and publish all of the regenerated 16-sec DQXML files into the DB on segments.ligo.org and publish new segments to the existing flag H1:DCH-GAPS_FILLED_ALL_TIME:1 and the new flag H1:DCH-GAPS_FILLED_8JUNE2023 (as described in this post), pending approval of relevant authorities.

[...]

Testing of publishing of the regenerated segments was done on Monday, 2023.08.21 by creating custom scripts in /root/git_dqsegdb_server/server/special/gap_filling_2023.06.08 (which will be pushed to https://git.ligo.org/computing/dqsegdb/server) on segments-dev.ligo.org and using them to publish the full set of DQXML files in /home/john.zweizig/rerun/20230608-segments/split_16s/, one new segment to the existing flag H1:DCH-GAPS_FILLED_ALL_TIME:1, and the new flag H1:DCH-GAPS_FILLED_8JUNE2023, with the same segment ([1370244992,1370280000), which is the time span covered by the regenerated DQXML files).  All tests confirmed that publishing went as expected.
The same scripts were used on Tuesday, 2023.08.22, to publish the same DQXML files to segments.ligo.org and the same segment to the GAPS flags on segments.ligo.org.  Tests confirmed that publishing went as expected and the new segments were available, as expected.

[...]

In a conversation with John Zweizig about the 21 missing flags, it was discovered that the segments in the regenerated DQXML files only had 'known' segments during times when the flag was 'active' (which led to flags with no 'active' segments not having any 'known' segments either, so the flag was omitted from the regenerated files).  The 'active' segments are fine, as evidenced by the comparison between ('active' segments published on 2023.06.08 + Derek's supplemental 'active' segments) and (regenerated 'active' segments) matching.  It's only known-but-not-active segments that are missing from the regenerated DQXML files, and in particular, the 16-sec split files that we used for publishing.  The monolithic files (20230608_ht_segments.xml and 20230608_segments.xml in /home/john.zweizig/rerun/20230608-segments/) have the missing segments (and thus missing flags), and those were thought to be equivalent to the 16-sec files, which were used because the split files were easier for the publisher to ingest, since the publisher would automatically recognize them as DQXML files, whereas it wouldn't recognize the monolithic files, and it would have to run twice for the 2 monolithic files (after they had been copied and renamed).
Comparison between contents of monolithic files and split files can be done using seg_calc, like this, from /home/john.zweizig/rerun/20230608-segments/: /home/john.zweizig/gds-dev/bin/seg_calc -c index *.xml > index.txt and /home/john.zweizig/gds-dev/bin/seg_calc -c index ./split_16s/*xml > split_index.txt.  Flags can then be extracted into a standard format using a command like grep -o "H1.*summary;1" index.txt | sed 's+;+:+g' | sed 's/_summary//g' | sort > index.txt_flags.txt.
John fixed the issue with seg_calc, to return the correct segments, with results saved to split_16b in the dir above.  Rerunning the seg_calc command on the new dir, saving the results to split_indexb.txt, shows that the segments are the same in the monolithic files as the second set of split files, and grep -c summary /home/john.zweizig/rerun/20230608-segments/*index*txt shows that there are 164 flags in the these files.  Some additional filtering shows that the 5 missing flags, compared to current published flags, are:

H1:DMT-AIRCRAFT_LIKELY:1
H1:DMT-AIRCRAFT_VERY_LIKELY:1
H1:DMT-PRE_LOCKLOSS_1:1
H1:DMT-PRE_LOCKLOSS_16:1
H1:DMT-PRE_LOCKLOSS_4:1

These flags and segments are generated by 2 scripts that run in real time, so they are not included in the regenerated segments.
The regenerated segments are ready to be published, but a question came up about the start and end times of the 2 monolithic files not matching up with each other ([1370245000,1370271796) and [1370244992,1370280000)) and Derek's identified duration ([1370247280,1370280237)), so round 2 of publishing is on hold until that issue is resolved, as noted in the main ticket in this comment.

[...]

John was able to regenerate DQXML files for both sets of flags up through 1370280240, saved in /home/john.zweizig/rerun/20230608-segments/split_extras/, which files contain the full set of known and active segments, including known-but-not-active segments.
After some checks and testing of the new files, they were published on segments-dev and the results were tested, along with updates to H1:DCH-GAPS_FILLED_ALL_TIME:1 and H1:DCH-GAPS_FILLED_8JUNE2023, which previously were known and active for [1370244992,1370280000), but were updated to [1370244992,1370280240).  All tests passed, so the updated segments and updates to the GAPS flags were published to segments.ligo.org.  Those results were also tested, and they passed.
[/quote]

An email was sent after all the work was done: "LHO segment gaps filled for 8 June 2023" - 2023.08.24 - https://sympa.ligo.org/wws/arc/detchar/2023-08/msg00113.html ; sent to detchar mailing list and numerous individuals

Published DQXML files were copied from /home/john.zweizig/rerun/20230608-segments/split_extras/ to /ifocache/DQ/gap_filling/H-DQ_Segments-13702_regenerated

Many of the DQSegDB files used for this procdure should be saved to [DQSegDB]/server/special/gap_filling_2023.06.08/ , which should map to something like https://git.ligo.org/computing/dqsegdb/server/-/tree/master/special/gap_filling_2023.06.08?ref_type=heads

Everything relevant to this issue was copied to /root/publishing_2023.06.08/ on segments-dev, with the plan to tarball it into segment_gap_filling_2023.06.08.tgz, then copy that to /backup/segdb/segments/backups_of_special_projects/ (though that will include this file, so it can't be confirmed here).

Some relevant tickets and web pages:
- "Publish regenerated H1 segments for 8 June 2023" - https://git.ligo.org/computing/dqsegdb/server/-/issues/47 
- https://wiki.ligo.org/Computing/DQSegDBSegmentGaps#May_43_June_2023_missing_HOFT_C00_frames_and_segment_backfill_40gaps_on_8_June_2023_41 
- "Regeneration of lost June 8 LHO segments" - https://git.ligo.org/computing/helpdesk/-/issues/4244
- "Missing L1_HOFT_C00 and H1_HOFT_C00 data in early O4 periods" - https://git.ligo.org/computing/helpdesk/-/issues/4291
- "Missing H1 data in ER15 time" - https://git.ligo.org/computing/helpdesk/-/issues/4190
- https://git.ligo.org/computing/dqsegdb/server/-/tree/master/special/gap_filling_2023.06.08?ref_type=heads


