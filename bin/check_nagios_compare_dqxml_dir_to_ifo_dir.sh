#!/bin/bash
# This is a quick script to check files like '/var/www/nagios/nagios_compare_dqxml_dir_to_ifo_dir_H.json', to see if there are gaps in the dqxml dir that send the monitor yellow or red;
#   if so, it runs /root/bin/run_rsync_as_dqxml.sh, to fill the gap(s).
# Lots of code is copied from 'run_rsync_as_dqxml.sh', which got it from 'compare_two_dqxml_dirs.sh'
# Run like this: "  /root/bin/check_nagios_compare_dqxml_dir_to_ifo_dir.sh  H  "
# Warning: Right now, this only processes the first dir that is missing files; if there is more than one, only the first one (in numerical order) is filled.  The second *should* be filled on the next run (assuming there is one).
# Outline:
# * Check command-line arguments and set things up
# * Check the JSON file, to see if there is a 'warning' or 'critical' status.
# * If so, run the script to fill the gap(s)
# To do:
# * check to make sure that the Nagios (JSON) file exists
# * put in a check, to make sure that the final grep line returns an actual 5-digit GPS major number
# * handle if a Nagios file reports files missing from both dirs that it checked (currently, only handles the 1st one; there should never naturally be 2, but it could happen (maybe unnaturally); this script should get the first one 
#     the first time it runs and the second one the next time it runs; either way, let the user know or something)
# * clean up comments and code
# * add something that checks to make sure that the call to 'run_rsync_as_dqxml.sh' is successful (or something)
# * check if there are any new dirs that should have been created but weren't, e.g., if major GPS time rolls over during a delivery outage; if so, create the needed new dir, so that one can be checked, too
# * maybe set up a check so that if the oldest dir is shown to have gaps, it tries to fill gaps in the dir before that one, and if any gaps are filled in *that* one, try to fill the one before *that* one, etc.
#     (to handle cases of multi-day outages, if only outages in gap-filling ability); maybe have an "aggressive={0,1}" flag to turn that off/on


# set default values
timeout=128
ifo=H
gps_start_time=$((`date +%s` - 315964782))
gps_major_number=`echo $gps_start_time | cut -c1-5`   # default major value (GPS time for dir name) = current (e.g., for GPS time 1248028200, major number = 12480; dir = ${ifo}-DQ_Segments-12480)
verbose=1
ref_base_dir=IFO
test_base_dir=/dqxml/H1
output_dir=/root/bin/monitor_files_2

# get values from command-line args 
if [ "$#" -eq 0 ]   # if *no* command-line args, report how to run, then run with default values
then
  echo "### INFO ### Run like this: '  [script_name]  [ifo_value]  [gps_major_number]  [verbose_value]  [reference_base_dir]  [test_base_dir]  [output_dir]  '"
  echo "         ### Example: '  /root/bin/compare_two_dqxml_dirs.sh  $ifo  $gps_major_number  1  '"
  echo "         ### Example: '  /root/bin/compare_two_dqxml_dirs.sh  $ifo  $gps_major_number  1  $ref_base_dir  $test_base_dir  $output_dir  '"
  echo "### NOTICE ### Script will run with default values, above, in 5 seconds (unless stopped)"
  sleep 5
fi
if [ "$#" -ge 1 ]; then ifo=$1; fi
if [ "$#" -ge 2 ]; then gps_major_number=$2; fi
if [ "$#" -ge 3 ]; then if [ "$3" -eq 0 ]; then verbose=0; fi; fi   # only change verbose from 1 to 0 if 0 is specified on the command line; anything else, it stays at 1

if [ $ifo == "H1" ]; then ifo="H"; fi
if [ $ifo == "L1" ]; then ifo="L"; fi
if [ $ifo == "V1" ]; then ifo="V"; fi
if [ $ifo == "G1" ]; then ifo="G"; fi

output_tmp_file=/tmp/check_nagios_compare_dqxml_dir_to_ifo_dir_output_${ifo}.txt
echo > $output_tmp_file   # to create an empty file
output_file=$output_dir/check_nagios_compare_dqxml_dir_to_ifo_dir_output_${ifo}_${gps_start_time}.txt
if [ $verbose -eq 1 ]; then echo "### INFO ### Run start time:  $(date)  - GPS time: ~$gps_start_time" >> $output_tmp_file; fi

nagios_file=/var/www/nagios/nagios_compare_dqxml_dir_to_ifo_dir_${ifo}.json
if [ ! -e $nagios_file ]; then echo "### ERROR ### Couldn't find Nagios file ($nagios_file).  Unrecoverable error.  Exiting."; exit; fi
grep_check1=`grep -i warning $nagios_file | wc -l`
grep_check2=`grep -i critical $nagios_file | wc -l`
if [ $verbose -eq 1 ]; then echo "### INFO ### IFO = $ifo; Nagios file = $nagios_file; check for 'warning' = $grep_check1; check for 'critical' = $grep_check2" >> $output_tmp_file; fi

if [ $grep_check1 -gt 0 ] || [ $grep_check2 -gt 0 ]
then
  echo "### INFO ### Found a 'warning' and/or 'critical' in file $nagios_file:" >> $output_tmp_file
  cat $nagios_file >> $output_tmp_file
  echo "### INFO ### Running gap-filler procedure" >> $output_tmp_file
  gps_major_number=`grep "is missing" $nagios_file  |  awk 'BEGIN{FS="^^"} {test_loc=index($0, "is missing"); print substr($0, (test_loc-8), 5)}'  |  head -n 1`
  # finds the line with "is missing" in it, then uses awk to find the location of "is missing" in that line, then uses that location to extract the 5 characters that start 8 characters before that string); if "is missing" occurs 2x, only uses 1st
  if [ $verbose -eq 1 ]; then echo "### INFO ### GPS major number = $gps_major_number; command to be run: '/root/bin/run_rsync_as_dqxml.sh  $ifo  $gps_major_number'" >> $output_tmp_file; fi
  /root/bin/run_rsync_as_dqxml.sh  $ifo  $gps_major_number &>> $output_tmp_file
  gps_end_time=$((`date +%s` - 315964782))
  if [ $verbose -eq 1 ]; then echo "### INFO ### Run end time:  $(date)  - GPS time: ~$gps_end_time" >> $output_tmp_file; fi
  cp $output_tmp_file $output_file
else
  echo "### INFO ### No 'warning' or 'critical' found in file $nagios_file." >> $output_tmp_file
  if [ $verbose -eq 1 ]; then echo "### INFO ### Run end time:  $(date)  - GPS time: ~$gps_end_time" >> $output_tmp_file; fi
fi



exit
