#!/bin/bash

# Assumes lib directory is one down from this script:
DIR="/root/dqsegdb_September_11_2018/dqsegdb"

#echo "Setting PYTHONPATH to point to $DIR/lib"
export PYTHONPATH=$PYTHONPATH:$DIR
export PATH=$PATH:$DIR/bin/

inf=$1

# check if the X509 certificate and key variables are set to valid files (mainly useful when this script is run from the command line, when the variables are often forgotten)
if test -v  X509_USER_CERT; then sleep 0; else echo "Variable X509_USER_CERT is not set!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -v  X509_USER_KEY;  then sleep 0; else echo  "Variable X509_USER_KEY is not set!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -a $X509_USER_CERT; then sleep 0; else echo "Variable X509_USER_CERT does not point to an actual file!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -a $X509_USER_KEY;  then sleep 0; else echo  "Variable X509_USER_KEY does not point to an actual file!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
# "-v" checks that the variable is set (note no "$" in the test); "-a" checks that the file exists (note the "$" in the test); need both b/c an empty variable will pass the "-a" test (!)
# Later note: It's not clear that it's possible to change the variable values in the shell running the script.  So if the variable isn't set or points to the wrong file,
#   the user probably just has to end the run, fix it, and start over.

/usr/bin/env python -W ignore::DeprecationWarning $DIR/bin/ligolw_publish_threaded_dqxml_dqsegdb \
--segment-url https://segments.ligo.org \
--state-file=/var/log/publishing/state/${inf}-DQ_Segments_current.xml \
--pid-file=/var/log/publishing/pid/${inf}-DQ_Segments_cur.pid \
--log-file=/var/log/publishing/dev/${inf}-DQ_Segments_cur.log \
--input-directory=/dqxml/${inf}1_test \
--log-level DEBUG -m 100 -c 1

