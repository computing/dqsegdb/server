#!/usr/bin/env python3

import datetime
import os
from datetime import date
import shutil

primary_dir='/opt/dqsegdb/python_server/logs/'
backup_dir='/backup/segdb/segments/maintenance/automatic/opt_dqsegdb_python_server_logs/'

today = date.today()

primaryNames=os.listdir(primary_dir)
if len(primaryNames)==0:
  raise ValueError("There should be a file in the primary log directory... please investigate and make sure backup script did not accidentally consume it.")

# Now deleting anything older than a week in the daily directory:
twodayAgo = today-datetime.timedelta(days = 2)
twodayAgo = datetime.datetime.combine(twodayAgo, datetime.time())

for i in primaryNames:
  dateString = i.split('.')[0]
  dateTimePy = datetime.datetime.strptime(dateString,"%Y-%m-%d")
  if dateTimePy < twodayAgo:  # older than 2 days
    shutil.move(primary_dir+i,backup_dir+i)
