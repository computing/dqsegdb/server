#!/bin/awk -f
# This is a script to scan a file with tbl_processes entries from a dqsegdb DB and verify several characteristics of the file contents:
#   1. That each line contains at least 16 fields
#   2. That the process_id value (field 1 in each line) is incremented by 1 over the line before it.
#   3. That the number of analyzed lines is equal to the difference between the value of the final process_id and the first process_id (as a check).
# The code is brittle in some places, in that some unusual problems might not be handled well.  We don't expect many problems, but if they start showing up,
#   (aside from trying to deal with the root cause upstream), we'll figure out how to handle them at the time.
# To do:
#   * maybe add a "buffer" range for the GPS check, e.g., so that a row that was 1 second before the row preceding it (race condition for the process to be registered/saved?) does not trigger an error?
#   * add code to handle the script being run with no command-line args; in that case, print usage info and exit
#   * 


BEGIN {
FS="\t"   # set this, so that multiple tabs aren't counted as 1
processed_count = 0   # number of rows that have been processed
min_exp_field_count = 16   # minimum number of fields we expect in each row
exp_field_count = 17   # number of fields we expect in each row
blank_line_count = 0   # number of blank lines encountered
field_count_misses = 0   # number of times a row has had the wrong number of fields
proc_increment_misses = 0   # number of times a row has had a process_id value that wasn't 1 over the previous line (excepting the first line)
gps_increment_misses = 0   # number of times a row has had an insertion time (in GPS) lower than the previous row's insertion time
first_proc = -1   # initialization value, to be replaced by first valid process_id
final_proc = -1   # initialization value, to be replaced by final valid process_id
prev_proc = -1   # initialization value, to detect when we're processing the first line
first_gps = -1   # initialization value, to be replaced by first valid GPS insertion time
final_gps = -1   # initialization value, to be replaced by final valid GPS insertion time
prev_gps = -1   # initialization value, to be replaced by first valid GPS insertion time
}   ###BEGIN

{   ###main
processed_count++

# check number of fields
if (NF < exp_field_count) {field_count_misses++; print "### ERROR ### Line number " processed_count " only had " NF " fields in it (not at least " exp_field_count ", as expected) (field_count_misses = " field_count_misses ")"}

# check if process_id incremented from prev line
# possiblities for a non-blank line: 
#   A) process_id is incremented by 1 from previous row (only happens if previous process_id was not initialization value (-1));
#   B) previous process_id was -1, so this is the first one;
#   C) neither of the others, so there was a problem of some kind
if (NF > 0) {
    this_proc = $1
    this_gps = $9
    final_proc = this_proc   # used to track the last process_id in the file
    final_gps = this_gps   # used to track the last GPS insertion time in the file
    if (this_proc == prev_proc + 1) {
        prev_proc = this_proc
        if (this_gps < prev_gps) {
            gps_increment_misses++
            print "### ERROR ### Line number " processed_count " had insertion time = " this_gps " (GPS), which is *lower* than the previous one ( " prev_gps " ), which should never happen. (gps_increment_misses = " gps_increment_misses " )"
            }
        prev_gps = this_gps
        }
    else {
        if (prev_proc == -1) {
            first_proc = $1
            first_gps = $9
            prev_proc = this_proc
            prev_gps = this_gps
            }
        else {
            proc_increment_misses++
            print "### ERROR ### Line number " processed_count " had process_id = " this_proc ", but it should have been " (prev_proc + 1) " (proc_increment_misses = " proc_increment_misses ")"
            prev_proc = this_proc
            if (this_gps < prev_gps) {
                gps_increment_misses++
                print "### ERROR ### Line number " processed_count " had insertion time = " this_gps " (GPS), which is *lower* than the previous one ( " prev_gps " ), which should never happen. (gps_increment_misses = " gps_increment_misses " )"
                }
            prev_gps = this_gps
            }
        }
    }
else {blank_line_count++; this_proc = -999; proc_increment_misses++; print "### ERROR ### Line number " processed_count " had 0 fields, but it should have process_id = " (prev_proc + 1) " (proc_increment_misses = " proc_increment_misses ")"}


}   ###main

END {
if ( (first_proc != -1) && (final_proc != -1) ) {calc_processed_count = final_proc - first_proc + 1} else {calc_processed_count = -1}
print "### INFO ### processed line count: " processed_count
print "### INFO ### calculated processed line count: " calc_processed_count "  (first: " first_proc "; final: " final_proc "; blank lines: " blank_line_count ")"
print "### INFO ### field_count_misses: " field_count_misses
print "### INFO ### proc_increment_misses: " proc_increment_misses
print "### INFO ### gps_increment_misses: " gps_increment_misses " (first GPS time: " first_gps " ; final GPS time: " final_gps " )"
}   ###END



# Reference
# lines from tbl_processes.txt_2015.12.01 
#1	1	another_online_process	12903	234.2.122.1	[]		8	1070184348	10	931011181	931014970	0	0	0	1386149131	0
#2	1	segment_producer	14306	133.423.221.2	[]		8	1070188213	10	931011181	931014970	0	0	0	1386152997	0
#3	1	segment_producer	14306	133.423.221.2	[]		8	1070188213	10	931014970	931016480	0	0	0	1386152997	0
#(1)	(2)	(3)			(4)	(5)		(6)	(7)	(8)	(9)		(10)	(11)		(12)		(13)	(14)	(15)	(16)		(17)
# column headers:
#| process_id | dq_flag_version_fk | process_full_name      | pid   | fqdn        | process_args | process_comment | user_fk | insertion_time | 
#affected_active_data_segment_total | affected_active_data_start | affected_active_data_stop | affected_known_data_segment_total | affected_known_data_start | affected_known_data_stop | process_time_started | process_time_last_used |

