#!/bin/bash
# This is a script to find the lines in tbl_processes that are scheduled to be deleted, verify that they match backups and a reference file, then tell the user how to delete those lines.
# It should extract tbl_processes from the running DB, extract the lines inserted before a user-supplied GPS time into a separate file, find those same lines in a reference file and extract them into a separate file,
#   extract tbl_processes from each of 3 backup files (probably specified by the user, but maybe eventually found automatically), for each of those 3 files extract the same range of lines into separate files,
#   then compare all 5 files with the chosen range of lines, and if all 5 are identical, print out the commands to be used to delete the range of lines from the running DB.
# Outline:
#   * set up variables and make sure that everything exists and is set up as it should be
#   * dump tbl_processes from running DB; make sure that the reference GPS time is before the end of that file; find process_id for first line and line of last insertion time at or before reference GPS time; extract that chunk to a new file
#   * verify that the 2 reference process_id values exist in the ref file; extract that chunk from the ref file to a new file
#   * for each of 3 backup files: extract tbl_processes.txt from the tarball; verify that the 2 reference process_id values exist in the tbl_processes.txt file; extract that chunk from the file to a new file
#   * compare the 5 extracted chunk files (either md5sum of all or diff DB version of file with each of the other 4); report results
#   * print out the commands that would be used to delete the lines corresponding to the extracted chunk file from the running DB
# To do:
#   * check that the reference GPS time is at least 2 months ago (convert GPS time to Unix time; add 58*24*60*60 to converted time; make sure that is lower than current Unix time)
#   * run integrity test on all tbl_processes.txt files and extracted chunk files, then make sure they all pass
#   * verify that all backups being used are at least structurally complete (7 tables, with .sql and .txt files for each) (though extracting the chunk is an imperfect proxy - if the backup is good enough to contain the chunk to be deleted, that's what we care about)
#   * 

# setup variables
start_time=$(date)
start_time_long=$(date +%Y.%m.%d_%H.%M.%S)
db_name=dqsegdb
skip=0   # skip=1 allows the option to skip performing certain actions, such as restoring certain files, if they already exist; it's mainly to save time during debugging
wake_files=1   # this will run a set of commands to briefly read the contents of the ref file and each tarball, to make sure that it's loaded into memory for when it's needed, 
               #   rather than the possibility of each one requiring the file be retrieved from tape when it's first read (so that starts in the background at the beginning)
target_host=segments.ligo.org
actual_host=$(hostname -f)
enforce_host=1   # if set to 1, this will enforce that 'target_host' and 'actual_host' must be the same, else exit with an error
# this is where all of the tbl_processes.txt files will end up, as well as the extracted chunk files
working_dir=/backup/segdb/segments/tmp_dev2/tmp   # this dir is world-writable
if [ ! -d $working_dir ]; then echo "### ERROR ### Working dir ($working_dir) was not found.  Please fix this."; exit 1; fi
bk1_tarball=/backup/segdb/segments/monthly/23-08-01.tar.gz 
bk2_tarball=/backup/segdb/segments/monthly/23-09-01.tar.gz 
bk3_tarball=/backup/segdb/segments/monthly/23-10-01.tar.gz 
db_dir=$working_dir/running_db
ref_dir=$working_dir/ref
bk1_dir=$working_dir/backup1
bk2_dir=$working_dir/backup2
bk3_dir=$working_dir/backup3
ref_file=$working_dir/tbl_processes_ref.txt
#ref_file=$working_dir/tbl_processes.txt_2017.03.01

# check host before starting
if [ "$enforce_host" -eq 1 ]; then
  # following line has the upper -> lower conversion because of a time when seg was renamed segments.ligo.ORG (2022.05.01), which might or might not be permanent
  if [ "$target_host" != "$(echo $actual_host | tr '[:upper:]' '[:lower:]')" ]; then echo "### ERROR ### This script is being run on $actual_host, but it should only be run on $target_host.  Exiting."; exit 1; fi
fi

# create dirs to be used
mkdir -p $db_dir $ref_dir $bk1_dir $bk2_dir $bk3_dir
chmod 777 $db_dir

# get command-line args
if [ $# -eq 0 ]; then
  echo "### This is a script to find the lines in tbl_processes that are scheduled to be deleted, verify that they match backups and a reference file, then tell the user how to delete those lines."
  echo "### Many variables are set in the 'setup variables' section in this script.  Please check that those variables are set properly before you run the script."
  echo "### Usage: ' (script name)  (cutoff insertion time (GPS) for process deletion) ' "
  #echo "### Example: ' /root/bin/process_tbl_processes/compare_processes_for_deletion.sh  1269700000 ' "
  echo "### Example: ' /backup/segdb/reference/root_files/process_tbl_processes//compare_processes_for_deletion.sh  1269700000 ' "
  exit 1
fi
if [ $# -gt 0 ]; then ref_gps=$1; fi
if [ $# -gt 1 ] && [ "$2" == "skip" ]; then skip=1; fi

# verify everything
if [ ! -e $bk1_tarball ] || [ ! -e $bk2_tarball ] || [ ! -e $bk3_tarball ] || [ ! -e $ref_file ]; then
  echo "### ERROR ### Did not find one of the necessary files for this procedure.  This should tell you which one:"
  ls -l  $bk1_tarball   $bk2_tarball   $bk3_tarball  $ref_file
  exit 1
fi
if [ ! -d $db_dir ] || [ ! -d $ref_dir ] || [ ! -d $bk1_dir ] || [ ! -d $bk2_dir ] || [ ! -d $bk3_dir ]; then
  echo "### ERROR ### Did not find one of the necessary dirs that should have been created for this procedure.  This should tell you which one:"
  ls $db_dir $ref_dir $bk1_dir $bk2_dir $bk3_dir
  exit 1
fi
db_check=`mysql -e "show databases;" 2> /dev/null | grep $db_name &> /dev/null; echo $?`
if [ "$db_check" -ne 0 ];then echo "### ERROR ### Did not find the target DB ($db_name) on this machine."; exit 1; fi

# print out info before start of run
echo "### INFO ### Start time = $start_time"
echo "### INFO ### DB name = $db_name ; working dir = $working_dir ; reference tbl_processes file = $ref_file ; reference GPS time = $ref_gps"
echo "### INFO ### Backup tarballs 1, 2, 3 = $bk1_tarball , $bk2_tarball , $bk3_tarball"
if [ "$skip" -eq 1 ]; then echo "### INFO ### 'skip' = ON"; fi
echo "###"

#   * dump tbl_processes from running DB; make sure that the reference GPS time is before the end of that file; find process_id for first line and line of last insertion time at or before reference GPS time; extract that chunk to a new file
table_name=tbl_processes
# check if an output file already exists
if [ $skip -eq 1 ]; then
  echo "### NOTICE ### Skipping checking for existence of various tbl_processes files (b/c if they already exist, they will be used as-is)."
else
  if [ -e $db_dir/${table_name}.sql ]; then echo "### ERROR ### Output file $db_dir/${table_name}.sql already exists.  Remove or rename it before re-running this script."; exit 2; fi
  if [ -e $db_dir/${table_name}.txt ]; then echo "### ERROR ### Output file $db_dir/${table_name}.txt already exists.  Remove or rename it before re-running this script."; exit 2; fi
  if [ -e $bk1_dir/${table_name}.txt ]; then echo "### ERROR ### Output file $bk1_dir/${table_name}.txt already exists.  Remove or rename it before re-running this script."; exit 2; fi
  if [ -e $bk2_dir/${table_name}.txt ]; then echo "### ERROR ### Output file $bk2_dir/${table_name}.txt already exists.  Remove or rename it before re-running this script."; exit 2; fi
  if [ -e $bk3_dir/${table_name}.txt ]; then echo "### ERROR ### Output file $bk3_dir/${table_name}.txt already exists.  Remove or rename it before re-running this script."; exit 2; fi
fi
if [ "$wake_files" -eq 1 ]; then
  echo "### INFO ### Issuing commands to make sure ref file and backup tarballs are loaded into memory."
  head $ref_file &> /dev/null  &
  tar -tf $bk1_tarball &> /dev/null  &
  tar -tf $bk2_tarball &> /dev/null  &
  tar -tf $bk3_tarball &> /dev/null  &
fi

# dump tbl_processes and make sure it worked
if [ -e $db_dir/${table_name}.txt ] && [ $skip -eq 1 ]; then
  echo "### NOTICE ### Skipping dumping of tbl_processes from $db_name, b/c $db_dir/${table_name}.txt already exists."
else
  echo "### INFO ### Dumping $table_name; DB name: $db_name; output dir: $db_dir; table to extract: $table_name"
  /usr/bin/time -f %E -o /tmp/time.txt --   mysqldump --single-transaction -u root $db_name --tab=$db_dir --tables $table_name
  db_exit_code=$?
  echo "Command run time:  `cat /tmp/time.txt`"
  if [ "$db_exit_code" -ne 0 ]; then echo "### ERROR ### There was a problem dumping $table_name from $db_name (exit code = $db_exit_code).  This script cannot continue."; exit 2; fi
fi

# get first and last insert times (GPS) from tbl_processes.txt and check for issues
cd $db_dir
echo "### INFO ### Now in dir $db_dir"
db_first_insert_time=`head -n 1 tbl_processes.txt | awk 'BEGIN {FS="\t"} {print $9}'`
db_last_insert_time=`tail -n 1 tbl_processes.txt | awk 'BEGIN {FS="\t"} {print $9}'`
db_first_pid=`head -n 1 tbl_processes.txt | awk 'BEGIN {FS="\t"} {print $1}'`
db_last_pid=`tail -n 1 tbl_processes.txt | awk 'BEGIN {FS="\t"} {print $1}'`
if [ "$db_first_insert_time" -gt "$ref_gps" ]; then echo "### ERROR ### The first insert time in tbl_processes.txt from $db_name is $db_first_insert_time, which is *after* the reference GPS time ($ref_gps), so no lines can ever be selected."; exit 2; fi
if [ "$db_last_insert_time" -lt "$ref_gps" ]; then
  echo "### WARNING ### The last insert time in tbl_processes.txt from $db_name is $db_last_insert_time, which is *earlier* than the reference GPS time ($ref_gps), but the normal usage has the ref GPS time somewhere inside tbl_processes.txt."
  echo "            ### This might not be an error, but it probably is.  Make sure that this is what you want before you run any of the delete commands."
  ref_gps=$db_last_insert_time
  echo "### NOTICE ### The reference GPS time has been modified to match the last insertion time in tbl_processes.txt from $db_name.  New reference GPS time = $ref_gps"
fi

# get get first and last process_id values and line numbers for the chunk to extract
global_first_pid=`head -n 1 tbl_processes.txt | awk 'BEGIN {FS="\t"} {print $1}'`
# following command is from extract_tbl_processes_chunk.sh 
echo "### INFO ### Finding start and end process_id values and line numbers from tbl_processes.txt"
awk_output_db=`/usr/bin/time -f %E -o /tmp/time.txt --  awk -v ref_gps="$ref_gps" 'BEGIN {FS="\t"; last_pid=-1; line_count=0; extract_line_count = 0; last_pid_line=-1; last_insert_time=-1} \
            {line_count++; if ($9 <= ref_gps) {extract_line_count++; last_pid=$1; last_pid_line=line_count; last_insert_time=$9} } END {print line_count, extract_line_count, last_pid, last_pid_line, last_insert_time}' tbl_processes.txt`
echo "Command run time:  `cat /tmp/time.txt`"
echo "awk_output_db = $awk_output_db"
db_line_count=`echo $awk_output_db | awk '{print $1}'`
db_extract_line_count=`echo $awk_output_db | awk '{print $2}'`
global_last_pid=`echo $awk_output_db | awk '{print $3}'`
global_last_insert_time=`echo $awk_output_db | awk '{print $5}'`
db_first_pid_line=1   ### always, b/c it's the start of the file
db_last_pid_line=`echo $awk_output_db | awk '{print $4}'`
if [ "$global_last_pid" -eq -1 ]; then echo "### ERROR ### No lines were found in tbl_processes.txt that had GPS time less than or equal to the reference GPS time ($ref_gps).  Cannot continue."; exit 2; fi
echo "### INFO ### tbl_processes.txt from $db_name contains $db_line_count lines; first process_id = $db_first_pid; first insert time (GPS) = $db_first_insert_time; last process_id = $db_last_pid; last insert time (GPS) = $db_last_insert_time"
echo "### INFO ### Extracting $db_extract_line_count lines, from line $db_first_pid_line (process_id = $global_first_pid, insert time (GPS) = $global_first_insert_time) to line $db_last_pid_line (process_id = $global_last_pid, insert time (GPS) = $global_last_insert_time)"

# extract the chunk to a new file; note that the first line is always 1, and sed counts starting from 1, not 0
db_extracted_file=$db_dir/db_extracted_chunk_${start_time_long}.txt
echo "### INFO ### Extracting lines $db_first_pid_line to $db_last_pid_line from tbl_processes.txt into $db_extracted_file"
/usr/bin/time -f %E -o /tmp/time.txt --  sed -n "${db_first_pid_line},${db_last_pid_line}p;$((db_last_pid_line + 1))q"  tbl_processes.txt  >  $db_extracted_file
exit_code=$?
echo "Command run time:  `cat /tmp/time.txt`"
if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Something went wrong in extracting $db_extracted_file (exit code = $exit_code).  Cannot continue."; exit 2; fi
echo "###"


# define functions for use in finding and extracting chunks from ref file and backup files
function get_chunk_values {
  # find the 2 reference process_id values in the file; report those values, or report that one wasn't found
  # call like this: ' get_chunk_values reference_file '
  # input: reference file name; note: starting process_id, ending process_id are global variables (global_first_pid, global_last_pid)
  # output: fn_first_pid_line, fn_last_pid_line; these are the line numbers in the input file; they are global, so they should be copied to other variables right after the function is called
  if [ $# -ne 1 ]; then echo "### ERROR ### Function 'get_chunk_values' should have 1 input variable, but it has $# variables.  Please fix this."; exit 3; fi
  local fn_file=$1
  #echo "fn_file = $fn_file; pids = $global_first_pid, $global_last_pid"
  echo "### INFO ### Finding start of chunk to extract"
  fn_first_pid_whole_line=`/usr/bin/time -f %E -o /tmp/time.txt --  grep -n "^$global_first_pid	" $fn_file`
  exit_code=$?
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Could not find process_id $global_first_pid in file $fn_file (exit code = $exit_code).  Cannot continue."; exit 3; fi
  echo "Command run time:  `cat /tmp/time.txt`"
  fn_first_pid_line=`echo $fn_first_pid_whole_line | awk '{print $1}' | awk 'BEGIN {FS=":"} {print $1}'`
  echo "### INFO ### Finding end of chunk to extract"
  fn_last_pid_whole_line=`/usr/bin/time -f %E -o /tmp/time.txt --  grep -n "^$global_last_pid	" $fn_file`
  exit_code=$?
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Could not find process_id $global_last_pid in file $fn_file (exit code = $exit_code).  Cannot continue."; exit 3; fi
  echo "Command run time:  `cat /tmp/time.txt`"
  fn_last_pid_line=`echo $fn_last_pid_whole_line | awk '{print $1}' | awk 'BEGIN {FS=":"} {print $1}'`
  echo "function values: $fn_first_pid_line, $fn_last_pid_line"
}

function extract_chunk {
  # this function takes in a source file, the first and last lines of the chunk to extract, and the extracted chunk filename
  # call like this: ' extract_chunk  input_file  start_line  end_line  output_file '
  if [ $# -ne 4 ]; then echo "### ERROR ### Function 'extract_chunk' should have 4 input variables, but it has $# variables.  Please fix this in the code."; exit 4; fi
  fn_input_file=$1
  fn_first_pid_line=$2
  fn_last_pid_line=$3
  fn_extracted_file=$4
  echo "### INFO ### Extracting line  $fn_first_pid_line  to line  $fn_last_pid_line  from file  $fn_input_file  into  $fn_extracted_file"
  /usr/bin/time -f %E -o /tmp/time.txt --  sed -n "${fn_first_pid_line},${fn_last_pid_line}p;$((fn_last_pid_line + 1))q"  $fn_input_file  >  $fn_extracted_file
  exit_code=$?
  echo "Command run time:  `cat /tmp/time.txt`"
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Something went wrong in extracting $fn_extracted_file (exit code = $exit_code).  Cannot continue."; exit 4; fi
}

#   * verify that the 2 reference process_id values exist in the ref file; extract that chunk from the ref file to a new file
cd $ref_dir
echo "### INFO ### Now in dir $ref_dir"
echo "### INFO ### Checking for matching chunk in reference file, $ref_file"
get_chunk_values $ref_file
ref_first_pid_line=$fn_first_pid_line
ref_last_pid_line=$fn_last_pid_line
echo "### INFO ### Extracting chunk from reference file"
ref_extracted_file=$ref_dir/ref_extracted_chunk_${start_time_long}.txt
extract_chunk  $ref_file  $ref_first_pid_line  $ref_last_pid_line  $ref_extracted_file
echo "###"

#   * for each of 3 backup files: extract tbl_processes.txt from the tarball; verify that the 2 reference process_id values exist in the tbl_processes.txt file; extract that chunk from the file to a new file
cd $bk1_dir
echo "### INFO ### Now in dir $bk1_dir"
# extract tbl_processes from the backup tarball, get the first and last lines of the chunk to extract, then extract that chunk to an output file
if [ -e $bk1_dir/tbl_processes.txt ] && [ $skip -eq 1 ]; then
  echo "### NOTICE ### Skipping	extraction of tbl_processes.txt from $bk1_tarball b/c $bk1_dir/tbl_processes.txt already exists."
else
  echo "### INFO ### Extracting tbl_processes.txt from backup 1"
  /usr/bin/time -f %E -o /tmp/time.txt --  tar xzf  $bk1_tarball  tbl_processes.txt  --no-same-owner
  exit_code=$?
  echo "Command run time:  `cat /tmp/time.txt`"
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Something went wrong in untarballing tbl_processes.txt from $bk1_tarball (exit code = $exit_code).  Cannot continue."; exit 5; fi
fi
bk1_file=$bk1_dir/tbl_processes.txt
echo "### INFO ### Checking for matching chunk in backup file, $bk1_file"
get_chunk_values $bk1_file
bk1_first_pid_line=$fn_first_pid_line
bk1_last_pid_line=$fn_last_pid_line
bk1_extracted_file=$bk1_dir/bk1_extracted_chunk_${start_time_long}.txt
echo "### INFO ### Extracting chunk from backup file 1"
extract_chunk  $bk1_file  $bk1_first_pid_line  $bk1_last_pid_line  $bk1_extracted_file
echo "###"

cd $bk2_dir
echo "### INFO ### Now in dir $bk2_dir"
# extract tbl_processes from the backup tarball, get the first and last lines of the chunk to extract, then extract that chunk to an output file
if [ -e $bk2_dir/tbl_processes.txt ] && [ $skip -eq 1 ]; then
  echo "### NOTICE ### Skipping extraction of tbl_processes.txt from $bk2_tarball b/c $bk2_dir/tbl_processes.txt already exists."
else
  echo "### INFO ### Extracting tbl_processes.txt from backup 2"
  /usr/bin/time -f %E -o /tmp/time.txt --  tar xzf  $bk2_tarball  tbl_processes.txt  --no-same-owner
  exit_code=$?
  echo "Command run time:  `cat /tmp/time.txt`"
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Something went wrong in untarballing tbl_processes.txt from $bk2_tarball (exit code = $exit_code).  Cannot continue."; exit 5; fi
fi
bk2_file=$bk2_dir/tbl_processes.txt
echo "### INFO ### Checking for matching chunk in backup file, $bk2_file"
get_chunk_values $bk2_file
bk2_first_pid_line=$fn_first_pid_line
bk2_last_pid_line=$fn_last_pid_line
bk2_extracted_file=$bk2_dir/bk2_extracted_chunk_${start_time_long}.txt
echo "### INFO ### Extracting chunk from backup file 2"
extract_chunk  $bk2_file  $bk2_first_pid_line  $bk2_last_pid_line  $bk2_extracted_file
echo "###"

cd $bk3_dir
echo "### INFO ### Now in dir $bk3_dir"
# extract tbl_processes from the backup tarball, get the first and last lines of the chunk to extract, then extract that chunk to an output file
if [ -e $bk3_dir/tbl_processes.txt ] && [ $skip -eq 1 ]; then
  echo "### NOTICE ### Skipping extraction of tbl_processes.txt from $bk3_tarball b/c $bk3_dir/tbl_processes.txt already exists."
else
  echo "### INFO ### Extracting tbl_processes.txt from backup 3"
  /usr/bin/time -f %E -o /tmp/time.txt --  tar xzf  $bk3_tarball  tbl_processes.txt  --no-same-owner
  exit_code=$?
  echo "Command run time:  `cat /tmp/time.txt`"
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Something went wrong in untarballing tbl_processes.txt from $bk3_tarball (exit code = $exit_code).  Cannot continue."; exit 5; fi
fi
bk3_file=$bk3_dir/tbl_processes.txt
echo "### INFO ### Checking for matching chunk in backup file, $bk3_file"
get_chunk_values $bk3_file
bk3_first_pid_line=$fn_first_pid_line
bk3_last_pid_line=$fn_last_pid_line
bk3_extracted_file=$bk3_dir/bk3_extracted_chunk_${start_time_long}.txt
echo "### INFO ### Extracting chunk from backup file 3"
extract_chunk  $bk3_file  $bk3_first_pid_line  $bk3_last_pid_line  $bk3_extracted_file
echo "###"

#   * compare the 5 extracted chunk files (either md5sum of all or diff DB version of file with each of the other 4); report results
echo "### INFO ### Comparing DB extracted file to files extracted from ref file and 3 backup files"
diff -qs $db_extracted_file  $ref_extracted_file
diff_exit_code1=$?
diff -qs $db_extracted_file  $bk1_extracted_file
diff_exit_code2=$?
diff -qs $db_extracted_file  $bk2_extracted_file
diff_exit_code3=$?
diff -qs $db_extracted_file  $bk3_extracted_file
diff_exit_code4=$?
if [ $((diff_exit_code1 + diff_exit_code2 + diff_exit_code3 + diff_exit_code4)) -eq 0 ]; then
  echo "### INFO ### All comparisons between extracted files passed."
else
  echo "### WARNING ### Some comparisons did not pass!"
  echo "            ### Script will continue, but this should be investigated."
fi
echo "###"

# integrity tests
echo "### NOTICE ### You should probably run the integrity test on the reference file, the dumped tbl_processes, and the chunk to be deleted.  Something like this:"
echo "    time  /backup/segdb/reference/root_files/process_tbl_processes/verify_tbl_processes_file.awk  $ref_file"
echo "    time  /backup/segdb/reference/root_files/process_tbl_processes/verify_tbl_processes_file.awk  $db_dir/${table_name}.txt"
echo "    time  /backup/segdb/reference/root_files/process_tbl_processes/verify_tbl_processes_file.awk  $db_extracted_file"
echo "###"

#   * print out the commands that would be used to delete the lines corresponding to the extracted chunk file from the running DB
echo "### To delete lines from running DB ($db_name):"
echo "###   mysql -e \"USE $db_name; SELECT * FROM tbl_processes WHERE process_id=$global_first_pid;\" # to confirm that the line exists in this DB (just to be safe) "
echo "###   mysql -e \"USE $db_name; SELECT * FROM tbl_processes WHERE process_id=$global_last_pid;\" # to confirm that the line exists in this DB (just to be safe) "
echo "###   #time mysql -e \"USE $db_name; DELETE FROM tbl_processes WHERE process_id>=$global_first_pid AND process_id<=$global_last_pid;\"   # copy line with the comment #, then delete it only when you're SURE you want to do it "
echo "###"

echo "### INFO ### To clean up:"
echo "###   rm -i  /backup/segdb/segments/tmp_dev2/tmp/*/*"
echo "###"

end_time=$(date)
echo "### INFO ### End time = $end_time"

exit 0

# reference
# time a command
#   /usr/bin/time -f %E -o /tmp/time.txt --  [cmd]
#   echo "Command run time:  `cat /tmp/time.txt`" 

	
