#!/bin/bash
# This is a script to analyze a dumped tbl_processes file and a reference tbl_processes file, then find the chunk in the dumped file that should be appended to the ref file, 
#   and print commands that will extract the chunk from the dumped file and append that chunk to the ref file.
# Note that we *assume* that any process_id that occurrs in a file only occurrs once.
# We also assume that the lines extracted from the dumped file will go from the process_id after the last one in the ref file to the end of the dumped file (i.e., no gaps between the two files and no gap of extracted lines at the end of the dumped file).
# To do:
#   * explicitly verify that there is no gap between the end of the reference file and the start of the chunk to be appended
#   * 


# set up varibles to be used
start_time=$(date)
start_time_detailed=$(date +%Y.%m.%d_%H.%M.%S)
if [ $# -lt 2 ]; then
  echo "### Purpose: analyze a dumped tbl_processes file and a reference tbl_processes file, then find the chunk in the dumped file that should be appended to the ref file"
  echo "    and print commands that will extract the chunk from the dumped file and append that chunk to the ref file."
  echo "### Usage: ' (script name)  (dumped tbl_processes file)  (reference tbl_processes file) ' "
  echo "### Example: ' /root/bin/process_tbl_processes/append_chunk_to_ref.sh  /dqxml/mysql/tbl_processes.txt  /root/bin/process_tbl_processes/ref_tbl_processes.txt ' "
  exit 1
else
  dumped_file=$1
  ref_file=$2
fi
echo "### INFO ### Start time: $start_time"

# verify that the files exist
if [ ! -e "$dumped_file" ]; then echo "### ERROR ### Dumped tbl_processes file ($dumped_file) does not exist."; exit 2; fi
if [ ! -e "$ref_file" ]; then echo "### ERROR ### Reference tbl_processes file ($ref_file) does not exist."; exit 3; fi

# get process_id values that we care about
last_ref_pid=`tail -n 1 $ref_file | awk '{print $1}'`
first_dumped_pid=`head -n 1 $dumped_file | awk '{print $1}'`
last_dumped_pid=`tail -n 1 $dumped_file | awk '{print $1}'`

# check that process_id values can produce useful output
if [ "$first_dumped_pid" -gt $((last_ref_pid + 1)) ]; then    ### we can swap what is done in this troubling case, if it's ever necesary to proceed with it
  #echo "### WARNING ### There is a gap between the last process_id in the ref file ($last_ref_pid) and the first process_id in the dumped file ($first_dumped_pid)."
  #echo "            ### Continuing with this script, but this is probably an error, and the commands should probably not be executed."
  echo "### ERROR ### There is a gap between the last process_id in the ref file ($last_ref_pid) and the first process_id in the dumped file ($first_dumped_pid).  This is not allowed.  Exiting."; exit 4
fi
if [ "$last_dumped_pid" -le "$last_ref_pid" ]; then
  echo "### ERROR ### The last procees_id in the dumped file ($last_dumped_pid) should be *after* the last process_id in the ref file ($last_ref_pid), but it isn't.  There are no lines available to append.  Exiting."
  exit 5
fi
echo "### INFO ### All tests passed; finding relevant lines in files."

# find lines to extract from dumped file   ### note: we are *assuming* that each process_id value occurs only once in a file; if that ever fails to be the case, this section will need to be re-written (though that will be the least of your problems)
first_extract_pid=$((last_ref_pid + 1))
#echo "here1"
extract_line1=`awk -v target_pid="$first_extract_pid" 'BEGIN {FS="\t"; output_pid=-1; line_count=0} {line_count++; if ($1 == target_pid) {output_pid=line_count; exit} } END {print output_pid}'  $dumped_file`
#echo "here2"
extract_line2=`wc --lines $dumped_file | awk '{print $1}'`
if [ "$extract_line1" -eq -1 ]; then echo "### ERROR ### The last process_id in the ref file is $last_ref_pid, but the next process_id ($first_extract_pid) was not found in the dumped file.  Exiting."; exit 6; fi

# print info on files and lines to be extracted and appended; print commands to extract and append chunk from dumped file
extracted_chunk_filename=${dumped_file}_extracted_lines_${extract_line1}-${extract_line2}_${start_time_detailed}
echo "### INFO ### Last process_id in ref file is $last_ref_pid"
echo "### INFO ### Dumped file goes from process_id $first_dumped_pid to $last_dumped_pid"
echo "### INFO ### These commands will extract lines $extract_line1 to $extract_line2 (process_id $first_extract_pid to $last_dumped_pid) and append them to the ref file."
echo "    sed -n '${extract_line1},${extract_line2}p;$((extract_line2 + 1))q'  $dumped_file  >  $extracted_chunk_filename"
echo "NOTE: You probably want to change the output dir to  /backup/segdb/segments/tmp_dev2/tmp/  in the line above and below"
#echo "    sudo -u ldbd  sed -n '${extract_line1},${extract_line2}p;$((extract_line2 + 1))q'  $dumped_file  >  $extracted_chunk_filename"
echo "    cat $extracted_chunk_filename  >>  $ref_file"
end_time=$(date)
echo "### INFO ### End time: $end_time"

exit 0
