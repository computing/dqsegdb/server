#!/bin/bash
# This is a script to backup the DQSegDB state files.  See notes at https://wiki.ligo.org/Computing/DQSegDBMonitorsAndScripts#spool_backup.sh

backup_dir=/backup/segdb/segments/publisher/spool/$(date +%Y.%m.%d)
echo "### INFO ### Backup dir:  $backup_dir"

mkdir $backup_dir
exit_code=$?
if [ "$exit_code" -ne 0 ]; then script_exit_code=$exit_code; exit $script_exit_code; fi

rsync -avP  /var/log/publishing/state/*  $backup_dir/
exit_code=$?
if [ "$exit_code" -ne 0 ]; then script_exit_code=$exit_code; exit $script_exit_code; fi
