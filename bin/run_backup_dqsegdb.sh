#!/bin/bash
# Started by Robert Bruntz, 2019.11.13
# This is a script to run backup_dqsegdb.sh (or an equivalent file), but send the output to a log file, then echo that log file at the end, so it can be emailed by a cron job.
# The main benefits of this method are:
#   1. The status of the process can be monitored in real time, by viewing the log file
#   2. The log file is persistent, so it can be easily read anytime, rather than having to search through emails (which might not be available at a future date)
#   3. If the script crashes for any reason (incl. the machine being rebooted, hard drive filling up, etc.), the incomplete log file is available, whereas a cron job only sends an email at the end of the job
#   4. The output of the script is captured, even if run from the command line (vs. only cron jobs have their output emailed)
#   5. We can do some searching, filtering, post-processing of the log file, if desired
#   6. Probably other reasons, too
# To do (maybe):
#   * Verify that the log dir is writable by the user running this script



# set up variables
error_code=-1
log_dir=/local/ldbd/log
log_file=${log_dir}/backup_dqsegdb_seg_$(date +%Y.%m.%d-%H.%M.%S).txt
echo "### INFO ### Running 'backup_dqsegdb_mysqldatabase_newdir.sh' ( $(date) ).  Log file = $log_file"
ls -l /local/ldbd/bin/backup_dqsegdb_mysqldatabase_newdir.sh

# run the script, with std out and std err redirected to the log file
# note that "&>>" is used by Bash 4+; if used on another system, older Bash, etc., replace with ">>$log_file 2>>$log_file" - https://stackoverflow.com/questions/876239/how-to-redirect-and-append-both-stdout-and-stderr-to-a-file-with-bash
/local/ldbd/bin/backup_dqsegdb_mysqldatabase_newdir.sh  &>> $log_file
error_code=$?

# read the output of the log file, so it will be sent by email for a cron job (same as it would be if the messages were reported to the cron job in real time)
if [ $error_code -eq 0 ]; then echo "### INFO ### Script run completed successfully.  Next, echoing contents of log file. ( $(date) )"; fi
if [ $error_code -ne 0 ]; then echo "### INFO ### Script run failed!  (Error code = $error_code.)  Next, echoing contents of log file. ( $(date) )"; fi
cat $log_file

exit
