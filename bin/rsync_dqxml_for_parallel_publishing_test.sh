#!/bin/bash
# This is a quick script to rsync the latest 2 dirs from ifocache to the appropriate dir in /dqxml/.
# Started by Robert Bruntz on 2022.02.18.
# Usage: ' (script name)  (IFO letter)'  - note that the IFO should be just 'H', 'L', 'V', 'G', 'K', etc.,
#   to match /ifocache/DQ/${ifo}1/${ifo}-DQ_Segments-1*/ and /dqxml/${ifo}1/
#

ifo=H
if [ $# -gt 0 ]; then ifo=$1; fi
log_file=/root/rsync_dqxml_for_parallel_publishing_test_log_${ifo}.txt
echo "### rsync for $ifo started $(date)" >> $log_file

for dq_dir in `ls -1d /ifocache/DQ/${ifo}1/${ifo}-DQ_Segments-1*/ | cut -c1-35 | tail -2`
do
#  ls -lhd $dq_dir
  rsync -aPv $dq_dir /dqxml/${ifo}1/ &>> $log_file
done
#for dq_dir in `ls -1d /ifocache/DQ/L1/L-DQ_Segments-1*/ | tail -2`; do ll -d $dq_dir; done

echo "### rsync for $ifo finished $(date)" >> $log_file
