#!/bin/bash
# Started by Robert Bruntz, 2020.01.08
# This is a script to:
#   * read KAGRA xml files (files created by gwPy; not dqxml files);
#   * extract the unprocessed known and active segments into separate files;
#   * process those files to get files with just known and active segments in them;
#   * publish the known and active segments files.
# to do:
#   * option to publish segments despite mismatch between xml and segments files?
#   * (testing)
#   * test that input file exists and is xml file
#   * test that verbose, save, publish flags are 0 or 1
#   * test that segment files were *successfully* copied before deleting temp files?


verbose=1
save_flag=0
publish_flag=0
temp_dir=/tmp
#save_dir=/dqxml/K1/segment_files
save_dir=/ifocache/DQ/K1
flag_name=GRD-LSC_LOCK_STATE_N_EQ_1000
flag_explain="KAGRA IFO is locked according to LSC guardian specification of state 1000 on 1 December 2019"
#server=http://segments-dev.ldas.cit
server=http://segments-backup.ldas.cit
#server=http://segments.ldas.cit
exit_code=0
start_time=$(date)


# process command-line arguments
if [ $# -eq 0 ]; then echo "### NOTICE ### Usage: ' (script name)  (input file name)  (verbose flag)  (save segment files flag)  (publish flag)' "; 
#                      echo "           ### Example: ' /root/bin/publish_kagra_xml_files.sh  /dqxml/K1_test/SegmentList_FPMI_UTC_2019-12-26.xml  1  1  1 '"; exit 1; fi
                      echo "           ### Example: ' /root/bin/publish_kagra_xml_files.sh  /ifocache/DQ/K1/SegmentList_FPMI_UTC_2019-12-26.xml  1  1  1 '"; exit 1; fi
if [ $# -gt 0 ]; then input_file=$1; fi
if [ $# -gt 1 ]; then verbose=$2; fi
if [ $# -gt 2 ]; then save_flag=$3; fi
if [ $# -gt 3 ]; then publish_flag=$4; fi


# report on state of various varibles (if verbose set)
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Start time: $start_time"
  echo "### INFO ### Variables:  input file name = $input_file ;  save flag = $save_flag ;  save dir = $save_dir ;  publish flag = $publish_flag ;  server = $server ;  temp dir = $temp_dir"
  echo "### INFO ### Pausing for 10 seconds, in case you change your mind..."
  sleep 10
fi


# handle filenames
# example filename: /dqxml/K1_test/SegmentList_FPMI_UTC_2019-12-26.xml
# (handle input filename here)
# (currently, don't need to do anything)


# extract known and active segments into unfinished files
if [ $verbose -eq 1 ]; then echo "### INFO ### Extracting known and active segments from input file ( $input_file ) to unfinished files in temp dir ( $temp_dir )"; fi
xml_basename=`basename $input_file`
known_unprocessed_filename=${temp_dir}/${xml_basename}_known_segments_unprocessed.txt
active_unprocessed_filename=${temp_dir}/${xml_basename}_active_segments_unprocessed.txt
cat $input_file  |  /bin/ligolw_print  -t segment_summary  -c start_time  -c end_time  -c segment_def_id  -d " "  >  $known_unprocessed_filename
cat $input_file  |  /bin/ligolw_print  -t segment          -c start_time  -c end_time  -c segment_def_id  -d " "  >  $active_unprocessed_filename
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Unfinished known and active files:"
  ls -lh $known_unprocessed_filename
  ls -lh $active_unprocessed_filename
fi


# turn unfinished files into segment files
if [ $verbose -eq 1 ]; then echo "### INFO ### Turning unfinished files into segment files"; fi
known_processed_filename=${known_unprocessed_filename}_processed.txt
active_processed_filename=${active_unprocessed_filename}_processed.txt
awk '{len=length($3); def_id=substr($3,len,1); if (def_id == 0) {print $1, $2}}' $known_unprocessed_filename   >  $known_processed_filename
awk '{len=length($3); def_id=substr($3,len,1); if (def_id == 0) {print $1, $2}}' $active_unprocessed_filename  >  $active_processed_filename
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Finished known and active segment files:"
  ls -lh $known_processed_filename
  ls -lh $active_processed_filename
fi


# check that contents of segment files are consistent with contents of xml file
if [ $verbose -eq 1 ]; then echo "### INFO ### Verifying number of segments between xml file and segment files."; fi
known_count_xml=`grep segment_summary:segment_sum_id: $input_file | wc --lines`
active_count_xml=`grep segment:segment_id: $input_file | wc --lines`
known_count_segments_file=`cat $known_processed_filename | wc --lines`
active_count_segments_file=`cat $active_processed_filename | wc --lines`
if [ $known_count_xml -ne $known_count_segments_file ] || [ $active_count_xml -ne $active_count_segments_file ]
then
  echo "### ERROR ### There is a mismatch between the number of known and/or active segments in the xml file and the segments files:"
  echo "known segments in xml file:       $known_count_xml"
  echo "known segments in segments file:  $known_count_segments_file"
  echo "active segments in xml file:       $active_count_xml"
  echo "active segments in segments file:  $active_count_segments_file"
  echo "          ### Exiting now."
  exit 2
else
  echo "### INFO ### Segment counts match between xml and segments files:"
  echo "known segments:   $known_count_xml"
  echo "active segments:  $active_count_xml"
fi


# save segment files and delete temp files
if [ $save_flag -eq 1 ]
then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Saving segment files to $save_dir"; fi
  known_save_filename=$save_dir/${xml_basename}_known_segments.txt
  active_save_filename=$save_dir/${xml_basename}_active_segments.txt
# check known segments file(s)
  if [ -e $known_save_filename ]
  then
    known_save_md5=`md5sum $known_save_filename | awk '{print $1}'`
    known_tmp_md5=`md5sum $known_processed_filename | awk '{print $1}'`
    if [ "$known_save_md5" == "$known_tmp_md5" ]
    then
      echo "### NOTICE ### A known segments file ( $known_save_filename ) already exists and is identical to the new known segments file ( $known_processed_filename ).  Not copying."
    else
      echo "### ERROR ### A known segments file ( $known_save_filename ) already exists, and it is different from the new known segments file ( $known_processed_filename).  You should figure out what has gone wrong.  Exiting."
      exit 3
    fi
  else
   cp -p $known_processed_filename  $known_save_filename
  fi
# check active segments file(s)
  if [ -e $active_save_filename ]
  then
    active_save_md5=`md5sum $active_save_filename | awk '{print $1}'`
    active_tmp_md5=`md5sum $active_processed_filename | awk '{print $1}'`
    if [ "$active_save_md5" == "$active_tmp_md5" ]
    then
      echo "### NOTICE ### An active segments file ( $active_save_filename ) already exists and is identical to the new active segments file ( $active_processed_filename ).  Not copying."
    else
      echo "### ERROR ### An active segments file ( $active_save_filename ) already exists, and it is different from the new active segments file ( $active_processed_filename).  You should figure out what has gone wrong.  Exiting."
      exit 3
    fi
  else
   cp -p $active_processed_filename  $active_save_filename
  fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Saved segments files:"; fi
  ls -lh $known_save_filename
  ls -lh $active_save_filename
  if [ $verbose -eq 1 ]; then echo "### INFO ### Deleting temp files"; fi
  echo "(would delete temp files here)"  
else
  if [ $verbose -eq 1 ]; then echo "### INFO ### Not saving segment files; deleting temp files"; fi
  echo "(would delete temp files here)"  
fi


# publish segment files
if [ $publish_flag -eq 1 ]
then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Publishing segments to DB on $server ."; fi
# if the publishable segment files are not empty, publish them 
  if [ `cat $known_processed_filename | wc --lines` -ne 0 ]
  then
    summary_file=$known_processed_filename
    segment_file=$active_processed_filename
    comment=$flag_explain
    if [ $verbose -eq 1 ]
    then
      echo "### INFO ### This is the command that will be executed:"
      echo "ligolw_segment_insert_dqsegdb  --insert  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain=\"$comment\"  --comment=\"$comment\"  "
    fi
#    echo "(this is where we would publish segment files)"
            ligolw_segment_insert_dqsegdb  --insert  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain="$comment"  --comment="$comment"
  else
    if [ $verbose -eq 1 ]; then echo "### INFO ### No 'known' segments for file $input_file, so no flag will be published."; fi
    if [ `cat $active_processed_filename | wc --lines` -ne 0 ]; then echo "### WARNING ### File $known_processed_filename is empty, but file $active_processed_filename is not empty.  This should never happen.  You should figure this out."; fi
  fi
fi

# check for any errors
echo "(this is where we would check for any errors)"
end_time=$(date)


if [ $verbose -eq 1 ]
then
  echo "### INFO ### End time: $end_time"
fi

exit $exit_code

# Reference
#   if [ $verbose -eq 1 ]; then ; fi
# ligolw_segment_insert_dqsegdb --insert --segment-url=http://segments.ldas.cit  --ifos=${ifo}1  --name=DMT-ANALYSIS_READY_STATIC_6AUG2019  --version=1  -S $summary_file  -G $segment_file  \
#   --explain="Static copy of DMT-ANALYSIS_READY:1, made on 2019.08.06, at GPS time 1249130000 (12:33:02 UTC)"  \
#   --comment="Created for later analysis that wants to use DMT-ANALYSIS_READY preserving the gaps that will be filled on/around 2019.08.07 (i.e., DMT-ANALYSIS_READY:1 will have the gaps filled; this flag preserves those gaps)"
# time ligolw_segment_query_dqsegdb --segment-url https://segments.ligo.org --query-segments --include-segments L1:DMT-ANALYSIS_READY:1  --gps-start-time 1000000000 --gps-end-time 1249130000 \
#   | ligolw_print -t segment_summary -c start_time -c end_time > output_L1_DMT-ANALYSIS_READY_known_segments_start_1000000000_end_1249130000_run_$(date +%Y.%m.%d-%H.%M.%S).txt
