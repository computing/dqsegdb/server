#!/bin/bash

# This is a monitor to compare the latest 2 dqxml dirs (in /dqxml/X1/) to the comparable IFO dirs, to make sure that there are no gaps in the file transfer.
# Currently, it works for LHO and LLO (the two that have been most problematic); later, we plan to modify it to work for Virgo and GEO, as well.
# Outline:
# * Find the latest 2 dirs for the given IFO (by checking in the appropriate /dqxml/X1/ dir)
# * Run 'compare_two_dqxml_dirs.sh' for each dir, comparing that /dqxml dir to the corresponding IFO dir
# * For each result, if there are any xml files missing, test how old the oldest missing file is; if it's over a pre-set age, monitor goes red; if it's over a lower pre-set age, monitor goes yellow;
# *   if there are no missing files over the lower pre-set age in either dir, monitor is green; if there was an error with either query and monitor has not been set to red or yellow, monitor goes purple (unknown).
# * To avoid excess stress on IFO filesystems, monitor will probably run every 10 min.
# Run like this: '  (script name)  (ifo)  [verbose level]  [warning age (sec.)]  [critical age (sec.)]  [log file dir]  [full path to compare_two_dqxml_dirs.sh]  '
# Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir.sh  H  '
# Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir.sh  H  1  300  600  /root/bin/monitor_files_2  '
# To do:
#   * Set up/test to run for Virgo [seems to work...]
#   * Set up/test to run for GEO

### set up variables, incl. default values and GPS major times for 2 dqxml dirs
ifo=L
verbose=1
warning_age=$((5*60))
critical_age=$((10*60))
log_file_dir=/root/bin/monitor_files_2/
path_to_compare_script=/root/bin/compare_two_dqxml_dirs.sh
nagios_dir=/var/www/nagios
critical_flag=0
warning_flag=0
unknown_flag=0
ok_flag=0
gps_start_time=$((`date +%s` - 315964782))
if [ $verbose -eq 1 ]; then echo "### INFO ### Run start time:  $gps_start_time  (~= $(date))"; fi

if [ "$#" -eq 0 ]
then
  echo "### ERROR ### At least 1 command-line argument (ifo) is needed."
  echo "          ### Run like this: '  (script name)  (ifo)  [verbose level]  [warning age (sec.)]  [critical age (sec.)]  [log file dir]  '"
  echo "          ### Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir.sh  H  '"
  echo "          ### Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir.sh  H  1  300  600  /root/bin/monitor_files_2  '"
  exit
fi
if [ "$#" -ge 1 ]; then ifo=$1; fi
if [ "$#" -ge 2 ]; then if [ "$2" -eq 0 ]; then verbose=0; fi; fi   # only change verbose from 1 to 0 if 0 is specified on the command line; anything else, it stays at 1
if [ "$#" -ge 3 ]; then warning_age=$3; fi
if [ "$#" -ge 4 ]; then critical_age=$4; fi
if [ "$#" -ge 5 ]; then log_file_dir=$5; fi
if [ "$#" -ge 6 ]; then path_to_compare_script=$6; fi

if [ $ifo == "H1" ] || [ $ifo == "H" ]; then ifo=H; ifo_dir=H1; fi
if [ $ifo == "L1" ] || [ $ifo == "L" ]; then ifo=L; ifo_dir=L1; fi
if [ $ifo == "V1" ] || [ $ifo == "V" ]; then ifo=V; ifo_dir=V1; fi
#if [ $ifo == "G1" ] || [ $ifo == "G" ]; then ifo=G; ifo_dir=G1_test; fi   # G1_test has the files with the 3-comma fix applied - so they don't match the files on the IFO filesystem; use the downloaded ones, instead
if [ $ifo == "G1" ] || [ $ifo == "G" ]; then ifo=G; ifo_dir=G1; fi
dqxml_dir1=`ls -d1 /dqxml/${ifo_dir}/${ifo}-DQ_Segments-*/ | tail -n 2 | head -n 1`
dqxml_dir2=`ls -d1 /dqxml/${ifo_dir}/${ifo}-DQ_Segments-*/ | tail -n 1`
dqxml_major_val1=`basename $dqxml_dir1 | cut -c15-19`
dqxml_major_val2=`basename $dqxml_dir2 | cut -c15-19`
tmp_file1=/tmp/compare_two_dirs_${ifo}_${dqxml_major_val1}_at_gps_${gps_start_time}.txt
tmp_file2=/tmp/compare_two_dirs_${ifo}_${dqxml_major_val2}_at_gps_${gps_start_time}.txt
if [ $verbose -eq 1 ]; then echo "### INFO ### ifo = $ifo; ifo_dir = $ifo_dir; warning_age = $warning_age; critical_age = $critical_age; log_file_dir = $log_file_dir; path_to_compare_script = $path_to_compare_script; \
major values = $dqxml_major_val1, $dqxml_major_val2"; echo "### INFO ### Log files for this script: $tmp_file1 , $tmp_file2"; fi
###here

### testing error-catching
#dqxml_major_val2=20000

### run test
# Run 'compare_two_dqxml_dirs.sh' like this: '  [script_name]  [ifo_value]  [gps_major_number]  [verbose_value]  [reference_dir]  [test_base_dir]  [output_base_dir]'
# Example: '  /root/bin/compare_two_dqxml_dirs.sh  L  12470  1  IFO  /dqxml/L1  '
for major_value in  $dqxml_major_val1  $dqxml_major_val2; do
  if [ $verbose -eq 1 ]; then echo "### INFO ### Running test for $major_value ..."; fi
  if [ $major_value -eq $dqxml_major_val1 ]; then tmp_file0=$tmp_file1; else tmp_file0=$tmp_file2; fi
  test_gps_time=$((`date +%s` - 315964782))
# here's the command that eventually runs the rsync command (in the 'compare_two_dqxml_dirs.sh' script)
  eval  $path_to_compare_script  $ifo  $major_value  1  IFO  /dqxml/${ifo_dir}  &>  $tmp_file0
  output_file=`grep root $tmp_file0 | grep output | awk '{print $NF}'`   # this finds the name of the output file from the 'compare_two_dqxml_dirs.sh' command
  error_file=`grep root $tmp_file0  | grep error  | awk '{print $NF}'` 	 # this	finds the name of the error  file from the 'compare_two_dqxml_dirs.sh' command
  if [ $verbose -eq 1 ]; then echo "### INFO ### output file = $output_file ; error file = $error_file"; fi 
  xml_count=`grep "\.xml" $output_file | wc --lines`
  if [ $verbose -eq 1 ]; then echo "### INFO ### xml file count in $output_file = $xml_count"; fi
  if [ `cat $error_file | wc --lines` -ne 0 ] && [ $xml_count -eq 0 ]; then unknown_flag=1; echo "### INFO ### Error in test for major value = $major_value"; continue; fi   # if there was an error
# case 1: missing file(s) found
  if [ $xml_count -gt 0 ]
  then
    earliest_gps_time=`grep "\.xml" $output_file | head -n 1 | cut -c15-24`   # take the earliest filename from the list; take the 15-24th chars. (1-based counting); "H-DQ_Segments-1248547856-16.xml" -> "1248547856"
    gps_cadence=`grep "\.xml" $output_file | head -n 1 | cut -d'-' -f 4 | cut -d'.' -f 1`   # split filename by "-", take 4th piece; split that by ".", take the 1st piece; "H-DQ_Segments-1248547856-16.xml" -> "16.xml" -> "16"
      ### this takes care of cases where the dqxml files are of different lengths (e.g., LHO, LLO, Virgo = 16 sec; GEO = 60 sec)
# case 1a: missing file(s) over critical age found
    if [ $((test_gps_time - earliest_gps_time - gps_cadence )) -ge $critical_age ]
    then
      critical_flag=1
      if [ $major_value -eq $dqxml_major_val1 ]
      then
        msg1="Dir 1 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is missing $xml_count files; oldest (${ifo}-DQ_Segments-${earliest_gps_time}) is over $critical_age seconds old."
        if [ $verbose -eq 1 ]; then echo "### INFO ### File older than critical age found:"; echo $msg1; fi
      else
        msg2="Dir 2 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is missing $xml_count files; oldest (${ifo}-DQ_Segments-${earliest_gps_time}) is over $critical_age seconds old."
        if [ $verbose -eq 1 ]; then echo "### INFO ### File older than critical age found:"; echo $msg2; fi
      fi
      continue
    fi
# case 1b: missing file(s) over warning age found
    if [ $((test_gps_time - earliest_gps_time - gps_cadence )) -ge $warning_age ]
    then
      warning_flag=1
      if [ $major_value -eq $dqxml_major_val1 ]
      then
        msg1="Dir 1 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is missing $xml_count files; oldest (${ifo}-DQ_Segments-${earliest_gps_time}) is over $warning_age seconds old."
        if [ $verbose -eq 1 ]; then echo "### INFO ### File older than warning age found:"; echo $msg1; fi
      else
        msg2="Dir 2 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is missing $xml_count files; oldest (${ifo}-DQ_Segments-${earliest_gps_time}) is over $warning_age seconds old."
        if [ $verbose -eq 1 ]; then echo "### INFO ### File older than warning age found:"; echo $msg2; fi
      fi
      continue
    fi
# case 1c: missing file(s) found, but none over warning age found
    if [ $((test_gps_time - earliest_gps_time - gps_cadence )) -lt $warning_age ]
    then
      ok_flag=1
      if [ $major_value -eq $dqxml_major_val1 ]
      then
        msg1="Dir 1 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is missing $xml_count files; oldest (${ifo}-DQ_Segments-${earliest_gps_time}) is less than $warning_age seconds old."
        if [ $verbose -eq 1 ]; then echo "### INFO ### No file older than warning age found:"; echo $msg1; fi
      else
        msg2="Dir 2 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is missing $xml_count files; oldest (${ifo}-DQ_Segments-${earliest_gps_time}) is less than $warning_age seconds old."
        if [ $verbose -eq 1 ]; then echo "### INFO ### No file older than warning age found:"; echo $msg2; fi
      fi
      continue
    fi
  fi
# case 2: no missing files found
  ok_flag=1
  if [ $major_value -eq $dqxml_major_val1 ]
  then
    msg1="Dir 1 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is not missing any files."
    if [ $verbose -eq 1 ]; then echo "### INFO ### No missing files found:"; echo $msg1; fi
  else
    msg2="Dir 2 (/dqxml/${ifo_dir}/${ifo}-DQ_Segments-${major_value}/) is not missing any files."
    if [ $verbose -eq 1 ]; then echo "### INFO ### No missing files found:"; echo $msg2; fi
  fi
done
if [ $verbose -eq 1 ]; then echo "### INFO ### Flags: critical = $critical_flag; warning = $warning_flag; unknown = $unknown_flag; OK = $ok_flag"; fi

### interpret results
gps_run_time=$gps_start_time
host_name=$(uname -n)
human_time=$(date)
output_file=$nagios_dir/nagios_compare_dqxml_dir_to_ifo_dir_${ifo}.json
monitor_status="OK"; monitor_status_code=0   # default values, unless modified in next block of code
if [ $critical_flag -eq 1 ];    then monitor_status="Critical"; monitor_status_code=2; 
else if [ $warning_flag -eq 1 ]; then monitor_status="Warning"; monitor_status_code=1; 
else if [ $unknown_flag -eq 1 ]; then monitor_status="Unknown"; monitor_status_code=3
fi ; fi ; fi
if [ $ifo == "H" ]; then ifo_out=LHO; fi
if [ $ifo == "L" ]; then ifo_out=LLO; fi
if [ $ifo == "V" ]; then ifo_out=Virgo; fi
if [ $ifo == "G" ]; then ifo_out=GEO; fi

# create the message for the JSON file
output_message="{\"created_gps\": $gps_run_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"$monitor_status: DQXML dirs for $ifo_out checked on host '$host_name' at GPS time $gps_run_time ($human_time).  $msg1  $msg2\", \
\"num_status\": $monitor_status_code}, {\"start_sec\": 1200, \"txt_status\": \"UNKNOWN: Cannot verify if these services ('$service_list') are running on host '$host_name' after GPS time $gps_run_time ($human_time).\", \"num_status\": 3}]}"
if [ $verbose -eq 1 ]; then echo "### INFO ### Output message:"; echo $output_message; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Output file: $output_file"; fi
echo $output_message > $output_file

gps_end_time=$((`date +%s` - 315964782))
if [ $verbose -eq 1 ]; then echo "### INFO ### Run end time:  $gps_end_time  (~= $(date))"; fi
exit

### Reference

