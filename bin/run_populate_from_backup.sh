#!/bin/bash
# Started by Robert Bruntz, 2019.11.13
# This is a script to run populate_from_backup.sh, but send the output to a log file, then echo that log file at the end, so it can be emailed by a cron job.
# The main benefits of this method are:
#   1. The status of the process can be monitored in real time, by viewing the log file
#   2. The log file is persistent, so it can be easily read anytime, rather than having to search through emails (which might not be available at a future date)
#   3. If the script crashes for any reason (incl. the machine being rebooted, hard drive filling up, etc.), the incomplete log file is available, whereas a cron job only sends an email at the end of the job
#   4. The output of the script is captured, even if run from the command line (vs. only cron jobs have their output emailed)
#   5. We can do some searching, filtering, post-processing of the log file, if desired
#   6. Probably other reasons, too
# To do (maybe):
#   * Verify that the log dir is writable by the user running this script

# note that "&>>" is used by Bash 4+; if used on another system, older Bash, etc., replace with ">>$log_file 2>>$log_file" - https://stackoverflow.com/questions/876239/how-to-redirect-and-append-both-stdout-and-stderr-to-a-file-with-bash


# set up variables
error_code=-1
log_dir=/usr1/ldbd/log
log_file=${log_dir}/populate_from_backup_$(date +%Y.%m.%d-%H.%M.%S).txt
delay_count=0   # counter for how many times the backup restore has been delayed, due to the backup file not being ready
delay_duration=60   # how long to delay, in sec, each time the backup file is not ready, before testing again
max_delay_count=120   # how many times to delay b/c the backup file isn't ready before giving up
lockout_duration=120   # duration in sec that backup file must be unmodified before it can be used

echo "### INFO ### Running 'run_populate_from_backup.sh' on $(uname -n) ( $(date) ).  Log file = $log_file"  # &>  $log_file

# check if the backup file is there, is alone, and is unmodified for a long enough time, before continuing with the script
loop=1   # modify this to break out of the loop
while [ $loop -eq 1 ]; do
  backup_file_count=`ls -1 /backup/segdb/segments/primary/*tar.gz | wc --lines`
  if [ $backup_file_count -eq 1 ]   # should be exactly 1 tarball file in that dir; 0 or > 1 is bad
  then   # this is for exactly 1 tarball file in backup dir
    backup_file_time=`date -r /backup/segdb/segments/primary/*.tar.gz +%s`   # gets modification time in Unix time (seconds since epoch)
    current_time=`date +%s`    # gets current time in Unix time (seconds since epoch)
    if [ $((current_time - backup_file_time)) -ge $lockout_duration ]   # test if last modification of tarball was 120 sec ago or longer
    then
      loop=0
      echo "### INFO ### Backup file (`ls /backup/segdb/segments/primary/*.tar.gz`) hasn't been modified in over $lockout_duration seconds.  Proceeding with DB restore.  $(date)"  &>> $log_file
    else
      delay_count=$((delay_count + 1))
      echo "### NOTICE ### Backup file (`ls /backup/segdb/segments/primary/*.tar.gz`) has been modified in less than $lockout_duration seconds.  Delaying for $delay_duration seconds before checking again.  (Delay # $delay_count.)  $(date)" &>> $log_file
      sleep $delay_duration
    fi
  else   # this is for != 1 tarball file in backup dir
    delay_count=$((delay_count + 1))
    echo "### NOTICE ### There should be 1 file in /backup/segdb/segments/primary/, but there are $backup_file_count files.  Delaying for $delay_duration seconds before checking again.  (Delay # $delay_count.)  $(date)"  &>> $log_file
    sleep $delay_duration
  fi
  # check if DB restore has been delayed too many times or not
  if [ $delay_count -gt $max_delay_count ]
  then
    echo "### ERROR ### DB restore was delayed for the maximum ($((delay_count - 1))) times (at $delay_duration sec each time).  DB restore has failed.  $(date)"  &>> $log_file
    exit
  fi
done

# run the script, with std out and std err redirected to the log file
/usr1/ldbd/bin/populate_from_backup.sh  &>> $log_file
error_code=$?

# read the output of the log file, so it will be sent by email for a cron job (same as it would be if the messages were reported to the cron job in real time)
if [ $error_code -eq 0 ]; then echo "### INFO ### Script run completed successfully.  Next, echoing contents of log file. ( $(date) )"; fi
if [ $error_code -ne 0 ]; then echo "### INFO ### Script run failed!  (Error code = $error_code.)  Next, echoing contents of log file. ( $(date) )"; fi
cat $log_file

exit
