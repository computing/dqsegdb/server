#!/bin/bash
# publish_kagra_xml_files.sh - Started by Robert Bruntz, 2020.01.08
# publish_kagra_xml_files_updated.sh - modified from publish_kagra_xml_files.sh by Robert Bruntz, starting 2020.10.15, to handle multiple KAGRA flags, rather than just 1 science-mode flag
# This is a script to do some or all of the following:
#   * read KAGRA xml files (files created by gwPy; not dqxml files);
#   * extract the unprocessed known and active segments into separate files;
#   * process those files to get files with just known and active segments in them;
#   * publish the known and active segments files.
# Reference:
# original file lines look like this (from /ifocache/DQ/K1/SegmentList_FPMI_UTC_2020-02-01.xml ):
# '			,,1264636818,0,,,"segment_definer:segment_def_id:0","segment_summary:segment_sum_id:0",1264550418,0' (single quotes are to preserve spaces)
# unfinished file lines look like this:
# 1264550418 1264636818 segment_definer:segment_def_id:0
# finished file lines look like this:
# 1264550418 1264636818
# to do:
#   * option to publish segments despite mismatch between xml and segments files?
#   * (testing)
#   * test that input file exists and is xml file
#   * test that verbose, save, publish flags are 0 or 1
#   * test that segment files were *successfully* copied before deleting temp files?


verbose=1
#sleep=5
sleep=0
# NOTE: action can be 'append' or 'insert'; either one will create a flag if it doesn't exist and append if it does; actually, not sure why there are 2 options
#action=insert
#action="append --ignore-append-check"
action=append
simple_processing=1   # this determines whether def_id is checked when turning unprocessed temp files into processed temp files (set var = 0) or no check is done (set var = 1)
save_flag=0
publish_flag=0
temp_dir=/tmp
#save_dir=/dqxml/K1/segment_files
save_dir=/ifocache/DQ/K1
flag_name=GRD-LSC_LOCK_STATE_N_EQ_1000
flag_explain="KAGRA IFO is locked according to LSC guardian specification of state 1000 on 1 December 2019"
flag_explain=""
#server=http://segments-dev.ldas.cit
#server=http://segments-backup.ldas.cit
server=http://segments.ldas.cit
exit_code=0
start_time=$(date)


# process command-line arguments
if [ $# -eq 0 ]; then echo "### NOTICE ### Usage: ' (script name)  (input file name)  (verbose flag)  (save segment files flag)  (publish flag)  (flag name) \"(comment)\" ' "; 
#                      echo "           ### Example: ' /root/bin/publish_kagra_xml_files.sh  /dqxml/K1_test/SegmentList_FPMI_UTC_2019-12-26.xml  1  1  1 '"; exit 1; fi
                      echo "           ### Example: ' /root/bin/publish_kagra_xml_files_updated.sh  /ifocache/DQ/K1/SegmentList_FPMI_UTC_2019-12-26.xml  1  1  1  GRD-LSC_LOCK_STATE_N_EQ_1000  \"KAGRA IFO is locked according to LSC guardian specification of state 1000 on 1 December 2019\" ' "; exit 1; fi
if [ $# -gt 0 ]; then input_file=$1; fi
if [ $# -gt 1 ]; then verbose=$2; fi
if [ $# -gt 2 ]; then save_flag=$3; fi
if [ $# -gt 3 ]; then publish_flag=$4; fi
if [ $# -gt 4 ]; then flag_name=$5; fi
if [ $# -gt 5 ]; then flag_explain=$6; fi

# report on state of various varibles (if verbose set)
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Start time: $start_time"
  echo "### INFO ### Variables:  input file name = $input_file  ;  flag name = $flag_name  ;  flag comment = \"$flag_explain\" "
  echo "         ###             temp dir = $temp_dir  ;  save flag = $save_flag  ;  save dir = $save_dir  ;  publish flag = $publish_flag  ;  server = $server  ;  action = $action"
fi


# handle filenames
# example filename: /dqxml/K1_test/SegmentList_FPMI_UTC_2019-12-26.xml
if [ ! -e $input_file ]; then echo "### ERROR ### Could not find input file ( $input_file ).  Cannot continue.  Exiting."; exit 2; fi
input_file_size=`cat $input_file | wc --bytes`
if [ $input_file_size -eq 0 ]; then echo "### ERROR ### Input file ( $input_file ) is empty.  Cannot continue.  Exiting."; exit 2; fi
# (handle input filename here)
# (currently, don't need to do anything)


# extract known and active segments into unfinished files
if [ $verbose -eq 1 ]; then echo "### INFO ### Extracting known and active segments from input file ( $input_file ) to unfinished files in temp dir ( $temp_dir )"; fi
xml_basename=`basename $input_file`
known_unprocessed_filename=${temp_dir}/${xml_basename}_known_segments_unprocessed.txt
active_unprocessed_filename=${temp_dir}/${xml_basename}_active_segments_unprocessed.txt
cat $input_file  |  ligolw_print  -t segment_summary  -c start_time  -c end_time  -c segment_def_id  -d " "  >  $known_unprocessed_filename
cat $input_file  |  ligolw_print  -t segment          -c start_time  -c end_time  -c segment_def_id  -d " "  >  $active_unprocessed_filename
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Unfinished known and active files:"
  ls -lh $known_unprocessed_filename
  ls -lh $active_unprocessed_filename
fi


# turn unfinished files into segment files
# 2020.10.15 - for SegmentList_FPMI_UTC_YYYY-MM-DD.xml, this worked fine (def_id == 0); for the 9 new flags from Chihiro Kozakai's 2020.09.24 email, def_id is often a 2-digit number that varies from file to file
#              one way to get that number: 
#                seg_def_id=`grep \"K1\" /dqxml/K1_test/K1-OMC_OVERFLOW_OK/2020/K1-OMC_OVERFLOW_OK_SEGMENT_UTC_2020-10-06.xml | awk '{split($0,mid_array,"segment_definer:segment_def_id:"); split(mid_array[2],mid_array2,"\""); print mid_array2[1]}'`
#              but then we also have to get it from the unprocessed temp file and compare the two, and it isn't clear if/when they could ever be different, b/c it isn't clear where the number comes from anyway
#              instead, to handle these 9 flags, I'm just extracting the segments, regardless of the 'def_id' value - so the check to make sure that the number of lines in unprocessed and processed temp files should always match (unless something unexpected went wrong)
if [ $verbose -eq 1 ]; then echo "### INFO ### Turning unfinished files into segment files"; fi
known_processed_filename=${known_unprocessed_filename}_processed.txt
active_processed_filename=${active_unprocessed_filename}_processed.txt
if [ "$simple_processing" -eq 1 ]
then
  awk '{print $1, $2}' $known_unprocessed_filename   >  $known_processed_filename
  awk '{print $1, $2}' $active_unprocessed_filename  >  $active_processed_filename
else
  awk '{len=length($3); def_id=substr($3,len,1); if (def_id == 0) {print $1, $2}}' $known_unprocessed_filename   >  $known_processed_filename
  # - abandoned attempt: awk -v seg_def_id="$seg_def_id" '{def_id=substr($3,len,1); if (def_id == 0) {print $1, $2}}' $known_unprocessed_filename   >  $known_processed_filename
  awk '{len=length($3); def_id=substr($3,len,1); if (def_id == 0) {print $1, $2}}' $active_unprocessed_filename  >  $active_processed_filename
fi
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Finished known and active segment files:"
  ls -lh $known_processed_filename
  ls -lh $active_processed_filename
fi


# check that contents of segment files are consistent with contents of xml file
if [ $verbose -eq 1 ]; then echo "### INFO ### Verifying number of segments between xml file and segment files."; fi
known_count_xml=`grep segment_summary:segment_sum_id: $input_file | wc --lines`
active_count_xml=`grep segment:segment_id: $input_file | wc --lines`
known_count_segments_file=`cat $known_processed_filename | wc --lines`
active_count_segments_file=`cat $active_processed_filename | wc --lines`
if [ $known_count_xml -ne $known_count_segments_file ] || [ $active_count_xml -ne $active_count_segments_file ]
then
  echo "### ERROR ### There is a mismatch between the number of known and/or active segments in the xml file and the segments files:"
  echo "known segments in xml file:       $known_count_xml"
  echo "known segments in segments file:  $known_count_segments_file"
  echo "active segments in xml file:       $active_count_xml"
  echo "active segments in segments file:  $active_count_segments_file"
  echo "          ### Exiting now."
  exit 2
else
  echo "### INFO ### Segment counts match between xml and segments files:"
  echo "known segments:   $known_count_xml"
  echo "active segments:  $active_count_xml"
fi


# save segment files and delete temp files
if [ $save_flag -eq 1 ]
then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Saving segment files to $save_dir"; fi
  known_save_filename=$save_dir/${xml_basename}_known_segments.txt
  active_save_filename=$save_dir/${xml_basename}_active_segments.txt
# check known segments file(s)
  if [ -e $known_save_filename ]
  then
    known_save_md5=`md5sum $known_save_filename | awk '{print $1}'`
    known_tmp_md5=`md5sum $known_processed_filename | awk '{print $1}'`
    if [ "$known_save_md5" == "$known_tmp_md5" ]
    then
      echo "### NOTICE ### A known segments file ( $known_save_filename ) already exists and is identical to the new known segments file ( $known_processed_filename ).  Not copying."
    else
      echo "### ERROR ### A known segments file ( $known_save_filename ) already exists, and it is different from the new known segments file ( $known_processed_filename).  You should figure out what has gone wrong.  Exiting."
      exit 3
    fi
  else
   cp -p $known_processed_filename  $known_save_filename
  fi
# check active segments file(s)
  if [ -e $active_save_filename ]
  then
    active_save_md5=`md5sum $active_save_filename | awk '{print $1}'`
    active_tmp_md5=`md5sum $active_processed_filename | awk '{print $1}'`
    if [ "$active_save_md5" == "$active_tmp_md5" ]
    then
      echo "### NOTICE ### An active segments file ( $active_save_filename ) already exists and is identical to the new active segments file ( $active_processed_filename ).  Not copying."
    else
      echo "### ERROR ### An active segments file ( $active_save_filename ) already exists, and it is different from the new active segments file ( $active_processed_filename).  You should figure out what has gone wrong.  Exiting."
      exit 3
    fi
  else
   cp -p $active_processed_filename  $active_save_filename
  fi
  if [ $verbose -eq 1 ]; then echo "### INFO ### Saved segments files:"; fi
  ls -lh $known_save_filename
  ls -lh $active_save_filename
  if [ $verbose -eq 1 ]; then echo "### INFO ### Deleting temp files"; fi
  echo "(would delete temp files here)"  
else
  if [ $verbose -eq 1 ]; then echo "### INFO ### Not saving segment files; deleting temp files"; fi
  echo "(would delete temp files here)"  
fi


# publish segment files
if [ $publish_flag -eq 1 ]
then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Publishing segments to DB on $server ."; fi
# if the publishable segment files are not empty, publish them 
  if [ `cat $known_processed_filename | wc --lines` -ne 0 ]
  then
    summary_file=$known_processed_filename
    segment_file=$active_processed_filename
    comment=$flag_explain
    if [ $verbose -eq 1 ]
    then
      echo "### INFO ### This is the command that will be executed:"
#      echo "ligolw_segment_insert_dqsegdb  --insert  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain=\"$comment\"  --comment=\"$comment\"  "
#      echo "ligolw_segment_insert_dqsegdb  --append  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain=\"$comment\"  --comment=\"$comment\"  "
      echo "ligolw_segment_insert_dqsegdb  --$action  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain=\"$comment\"  --comment=\"$comment\"  "
    fi
    echo "### NOTICE ### You have  $sleep  seconds to cancel before publishing will be attempted."
    sleep $sleep
    echo "(this is where we would publish segment files)"
#            ligolw_segment_insert_dqsegdb  --insert  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain="$comment"  --comment="$comment"
#            ligolw_segment_insert_dqsegdb  --append  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain="$comment"  --comment="$comment"
            ligolw_segment_insert_dqsegdb  --$action  --segment-url=$server  --ifos=K1  --name=$flag_name  --version=1  -S $summary_file  -G $segment_file  --explain="$comment"  --comment="$comment"
            exit_code=$?
            echo
            if [ "$exit_code" -ne 0 ]; then echo; echo; echo; fi   # useful when running the script in a loop, to spot problems in a sea of output
            echo "### INFO ### exit code = $exit_code"
  else
    if [ $verbose -eq 1 ]; then echo "### INFO ### No 'known' segments for file $input_file, so no flag will be published."; fi
    if [ `cat $active_processed_filename | wc --lines` -ne 0 ]; then echo "### WARNING ### File $known_processed_filename is empty, but file $active_processed_filename is not empty.  This should never happen.  You should figure this out."; fi
  fi
fi

# check for any errors
echo "(this is where we would check for any errors)"
end_time=$(date)


if [ $verbose -eq 1 ]
then
  echo "### INFO ### End time: $end_time"
fi

exit $exit_code

# Reference
#   if [ $verbose -eq 1 ]; then ; fi
# ligolw_segment_insert_dqsegdb --insert --segment-url=http://segments.ldas.cit  --ifos=${ifo}1  --name=DMT-ANALYSIS_READY_STATIC_6AUG2019  --version=1  -S $summary_file  -G $segment_file  \
#   --explain="Static copy of DMT-ANALYSIS_READY:1, made on 2019.08.06, at GPS time 1249130000 (12:33:02 UTC)"  \
#   --comment="Created for later analysis that wants to use DMT-ANALYSIS_READY preserving the gaps that will be filled on/around 2019.08.07 (i.e., DMT-ANALYSIS_READY:1 will have the gaps filled; this flag preserves those gaps)"
# time ligolw_segment_query_dqsegdb --segment-url https://segments.ligo.org --query-segments --include-segments L1:DMT-ANALYSIS_READY:1  --gps-start-time 1000000000 --gps-end-time 1249130000 \
#   | ligolw_print -t segment_summary -c start_time -c end_time > output_L1_DMT-ANALYSIS_READY_known_segments_start_1000000000_end_1249130000_run_$(date +%Y.%m.%d-%H.%M.%S).txt
