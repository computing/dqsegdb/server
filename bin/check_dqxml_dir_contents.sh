#!/bin/bash
# This is a script to do some checks on the contents of a dqxml dir.  The checks will probably increase over time.  Basic checks include number of files in the dir, expected number of files in the dir, and any missing expected files.
# Started by Robert Bruntz on 2020.07.16.
# Planned and implemented checks include:
#   * Count number of xml files in a dir [done]
#   * Calculate number of expected files in a dir [done]
#   * Report which files are definitely missing in a dir (e.g., a gap between 2 existing files or a file missing at the end past some grace period) [done] (maybe separate into files missing from between other files vs. files missing at the end?)
#   * Report which files might be missing in a dir (e.g., a file missing at the end but before some grace period)
#   * Check for files other than validly-named dqxml files (e.g., temp files)
#   * Files below a certain size
#   * Files missing certain key flags [see notes at end for a key piece for this]
#   * Files with fewer than a minimum number of flags in them [see notes at end for a key piece for this]
#   * Files with structural issues (maybe split into 'valid but problematic' and 'invalid')
# To do:
#   * get IFO and GPS major time from a dqxml dir name (e.g., /dqxml/H1/H-DQ_Segments-12789/)
#   * implement checks above
#   * maybe separate missing files into files missing from between other files vs. files missing at the end?
#   * [done] handle different IFO representations (e.g., H==h==H1==LHO, etc.)
#   * handle Virgo dirs, by finding time of first file in dir and incrememnting from that
#   * handle GEO dirs


# setup variables
start_time=$(date)
start_time_long=$(date +%Y.%m.%d-%H.%M.%S)
gps_start_time=$((`date +%s` - 315964782))
dqxml_dir=/dev/nosuchdir   # default, for detecting bad/missed assignment
ifo=X   # default, for detecting bad/missed assignment
grace_len=32   # no. of seconds that a file can be late by and still not be considered missing
grace_len=120   # no. of seconds that a file can be late by and still not be considered missing
dqxml_increment=16   # default value; might be changed for GEO

# process command-line varibles
if [ $# -eq 0 ]; then
  echo "$(date)  - GPS time: ~$((`date +%s` - 315964782))"
  echo "### Usage 1: ' (script name)  (dqxml dir) ' "
  echo "### Usage 2: ' (script name)  (IFO letter)  (GPS major number) ' "
  echo "### Example 1: ' /root/bin/check_dqxml_dir_contents.sh  /dqxml/H1/H-DQ_Segments-12700/ ' "
  echo "### Example 2: ' /root/bin/check_dqxml_dir_contents.sh  H  12700 ' "
  exit 1
fi
if [ $# -eq 1 ]; then 
  dqxml_dir=$1
  if [ ! -d "$dqxml_dir" ]; then echo "### ERROR ### Single argument should be a directory, but  $dqxml_dir  is not a valid dir.  Run script name without arguments for usage."; exit; fi
  # assume user input valid dqxml dir; extract ifo and gps_major
  dqxml_dir_basename=`basename $dqxml_dir`   # should be something like 'H-DQ_Segments-12700'
  ifo=`basename $dqxml_dir | cut -c1`
  gps_major=`basename $dqxml_dir | cut -c15-19`
fi
if [ $# -ge 2 ]; then 
  ifo=$1
  gps_major=$2
  dqxml_dir=/dqxml/${ifo}1/${ifo}-DQ_Segments-${gps_major}
fi
if [ $# -ge 3 ]; then grace_len=$3; fi
if [ $ifo == "H" ] || [ $ifo == "h" ] || [ $ifo == "H1" ] || [ $ifo == "LHO" ]; then ifo=H; fi
if [ $ifo == "L" ] || [ $ifo == "l" ] || [ $ifo == "L1" ] || [ $ifo == "LLO" ]; then ifo=L; fi
if [ $ifo == "V" ] || [ $ifo == "v" ] || [ $ifo == "V1" ] || [ $ifo == "VGO" ] || [ $ifo == "Virgo" ] || [ $ifo == "VIRGO" ]; then ifo=V; fi
if [ $ifo == "K" ] || [ $ifo == "k" ] || [ $ifo == "K1" ] || [ $ifo == "KAG" ] || [ $ifo == "KAGRA" ] || [ $ifo == "Kagra" ]; then ifo=K; fi
if [ $ifo == "G" ] || [ $ifo == "g" ] || [ $ifo == "G1" ] || [ $ifo == "GEO" ] || [ $ifo == "Geo" ]; then ifo=G; fi

# print out run variables and run some checks
echo "### INFO ### Start time: $start_time  =  GPS time ~$gps_start_time"
echo "### INFO ### dqxml dir = $dqxml_dir ; grace period length = $grace_len seconds"
if [ ! -d "$dqxml_dir" ]; then echo "### ERROR ### dqxml file dir ( $dqxml_dir ) does not exist."; exit 2; fi
echo "###"


#   * Count number of xml files in a dir
first_file=`ls -1 $dqxml_dir/?-DQ_Segments-??????????-16.xml  |  head -1`
dqxml_count=`ls -1 $dqxml_dir/?-DQ_Segments-??????????-16.xml  |  wc --lines`
last_file=`ls -1 $dqxml_dir/?-DQ_Segments-??????????-16.xml  |  tail -1`
echo "### INFO ### Normal number of dqxml files in a dqxml dir = 6250"
echo "### Number of normal dqxml files in $dqxml_dir :  $dqxml_count"

#   * Calculate number of expected files in a dir
if [ "$dqxml_count" -gt 0 ]; then
  last_gps_file=`ls -1 $dqxml_dir/?-DQ_Segments-??????????-16.xml | tail -n 1`
  last_gps_time=`basename $last_gps_file | cut -c20-24`
  last_gps_time=$(basename $last_gps_file | cut -c20-24)
  echo "last gps time = $last_gps_time"
  echo "### Expected number of files (based on last file in dir):  $((last_gps_time/16 + 1))"   # +1 to account for 00000
fi
echo "###"


#   * Report which files are definitely missing in a dir (e.g., a gap between 2 existing files or a file missing at the end past some grace period)
# Idea: find the last file in a dir; if a file is missing *before* that file, it's definitely a gap between existing files, vs. a file missing at the end
# Idea: find a way to consolidate the contiguous blocks of missing files and report those in shortened form, rather than listing every single file
echo "### Checking for missing files"
# note that we have to print in front-padded decimal (00000, 00016, 00032, etc.), but Bash reads the leading 0 to mean that the number is octal, so we have to count in un-padded decimal (0, 16, 32, etc.)
missing_file_count=0
test_gps_minor=0
last_expected_minor=99999

while [ $test_gps_minor -le "$last_expected_minor" ]; do
  printf -v test_gps_minor_print "%05d" $test_gps_minor   # front-pads 'test_gps_minor' with 0s, so 16 -> 00016, etc., and saves the output to 'test_gps_minor_print'
  test_file=$dqxml_dir/?-DQ_Segments-?????${test_gps_minor_print}-16.xml
  #echo "test file =  $test_file"
  if [ ! -e $test_file ]; then
    gps_current=$((`date +%s` - 315964782))
    gps_current_test=${gps_major}${test_gps_minor_print}
    # if the expected file isn't there, check if we're inside the grace period; if not, report the file missing; if so, break out of the loop b/c we don't need to check anymore
    if [ $((gps_current - gps_current_test)) -gt $grace_len ]; then echo "Missing file: $test_file"; let missing_file_count++; else break; fi
  fi
  test_gps_minor=$((test_gps_minor + dqxml_increment))
done
echo "### Count of expected files missing from $dqxml_dir : $missing_file_count"
echo "###"

# list DMT and GRD flags
echo "### DMT and GRD flags:"
echo "DMT-UP"
echo "GRD-ISC_LOCK_OK"
echo "###"


# check for specific flags in first and last files
echo "### Checking for DMT and GRD flags in first and last files"
echo "First file: $first_file"
echo "DMT:  $(ligolw_print -t segment_definer -c name $first_file | grep DMT-UP | wc --lines)"
echo "GRD:  $(ligolw_print -t segment_definer -c name $first_file | grep GRD-ISC_LOCK_OK | wc --lines)"
echo "Last file: $last_file"
echo "DMT:  $(ligolw_print -t segment_definer -c name $last_file | grep DMT-UP | wc --lines)"
echo "GRD:  $(ligolw_print -t segment_definer -c name $last_file | grep GRD-ISC_LOCK_OK | wc --lines)"
echo "###"

# check for counts of specific flags in all files (not catching possible duplicates in any files)
echo "### Counting DMT and GRD flags in all files in the dir (should be around $dqxml_count)"
echo "DMT count: $(grep DMT-UP $dqxml_dir/* | wc --lines)"
echo "GRD count: $(grep GRD-ISC_LOCK_OK..1 $dqxml_dir/* | wc --lines)"   # have to use the "..1" format b/c the string alone shows up 2x in each file; see Reference at end
echo "###"


end_time=$(date)
echo "### INFO ### End time: $end_time"
exit 0


# Reference
# /dqxml/H1/H-DQ_Segments-12789/H-DQ_Segments-1278948176-16.xml
# echo "$(date)  - GPS time: ~$((`date +%s` - 315964782))"
# ls -Ss --block-size=1  /dqxml/H1/H-DQ_Segments-12883/ | tail   # -S = sort by size; -s = display size, in blocks; --block-size=# = set block size to # bytes (or use K, M, etc., but it will display those units, as well)
# get all flags from a file:
#     ligolw_print -t segment_definer -c name  [filename]
# get all known segments in a file:
#     ligolw_print  -t segment_summary -c start_time -c end_time -d ' '  [filename]
# get all active segments in a file:
#     ligolw_print  -t segment -c start_time -c end_time -d ' '  [filename]
#
#$ grep DMT-UP $dqxml_dir/* | tail
#/dqxml/H1/H-DQ_Segments-13238//H-DQ_Segments-1323805392-16.xml:      "process:process_id:2","segment_definer:segment_def_id:160","H1","DMT-UP",1,
#/dqxml/H1/H-DQ_Segments-13238//H-DQ_Segments-1323805408-16.xml:      "process:process_id:2","segment_definer:segment_def_id:160","H1","DMT-UP",1,
#$ grep GRD-ISC_LOCK_OK $dqxml_dir/* | tail
#/dqxml/H1/H-DQ_Segments-13238//H-DQ_Segments-1323805488-16.xml:      "GRD-ISC_LOCK_OK",1,
#/dqxml/H1/H-DQ_Segments-13238//H-DQ_Segments-1323805488-16.xml:      "H1:GRD-ISC_LOCK_OK reported by Guardian system\, flag is on when system is in requested state and guardian node is connected and nominal",
#/dqxml/H1/H-DQ_Segments-13238//H-DQ_Segments-1323805504-16.xml:      "GRD-ISC_LOCK_OK",1,
#/dqxml/H1/H-DQ_Segments-13238//H-DQ_Segments-1323805504-16.xml:      "H1:GRD-ISC_LOCK_OK reported by Guardian system\, flag is on when system is in requested state and guardian node is connected and nominal",

