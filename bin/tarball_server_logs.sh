#!/bin/bash
# This is a script to tarball the Python server logs (in /backup/segdb/segments/maintenance/automatic/opt_dqsegdb_python_server_logs/) every month.
# This is done because the individual files are stored on tape, so accessing many files could require accessing many tapes.  This way, each month is 1 file, so N months only require at most N tape acccesses.
# Might be worth tarballing the entire year at the start of the next year (or tarballing months 1-6 and 7-12 separately), to require fewer tape accesses (and delays) at a later date.
# It is intended to be run in the month after the target month; 'this month' = month it's run in; 'target month' = month to have its Python server log files tarballed.
# Normally just run by a cron job like this, from user ldbd's account: "00 06 02 * *  /usr1/ldbd/bin/tarball_server_logs.sh" (so run at 0600 PT on the 2nd of every month).
# The script should account for what month and year it's run in (e.g., Jan. of year N will tarball Dec. of year (N-1)) and should be unaffected by leap years, etc.
# The script can be run manually (from user ldbd's account) by specifying month (1 or 2 digits) AND year (4 digits) on the command line, to run as if it were run in that month and year 
#   - e.g., '/usr1/ldbd/bin/tarball_server_logs.sh 04 2020' will run as if run in April 2022, and so try to tarball March 2022 files (which won't work, if the target tarball file already exists, but will create an empty tarball if it is run before March 2022 - so be careful).
# Run like this: ' /usr1/ldbd/bin/tarball_server_logs.sh ' or ' /usr1/ldbd/bin/tarball_server_logs.sh 09 2020 '

echo "### INFO ### Start time = $(date)"

source_dir=/backup/segdb/segments/maintenance/automatic/opt_dqsegdb_python_server_logs
target_dir=/backup/segdb/segments/maintenance/automatic
this_month=`date +%m`
this_month_dec=`printf $((10#$this_month))`
this_year=`date +%Y`

# for testing only!
if [ $# -gt 0 ]; then this_month=$1; fi
if [ $# -gt 1 ]; then this_year=$2; fi

if [ "$this_month" -eq 01 ]
then 
  target_month=12
  target_year=$((this_year - 1))
else
  target_month=$(( 10#$this_month - 1))   # 'N#$string' converts the (decimal? un-assigned?) number stored in '$string' to the base specified by 'N' - so "09" (which Bash would think is an invalid octal number) is converted to "9", which is fine
  target_month=`printf "%02d" $target_month`
  target_year=$this_year
fi
target_file=$target_dir/opt_dqsegdb_python_server_logs_${target_year}.${target_month}.tgz

echo "### INFO ### This month = $this_month; this year = $this_year; target month = $target_month; target year = $target_year"
echo "### INFO ### Target file = $target_file"

if [ -e $target_file ]
then
  echo "### ERROR ### Target file ( $target_file ) already exists!  Unrecoverable error.  Exiting."
  ls -lh $target_file
  exit 1
else
  #echo "### Would execute: ' cd $target_dir/ ;  tar czf  opt_dqsegdb_python_server_logs_${target_year}.${target_month}.tgz  $source_dir/${target_year}-${target_month}-* '"
  #echo "### Would execute: ' cd $target_dir/ ;  tar czf  $target_file  $source_dir/${target_year}-${target_month}-* '"
  echo "### Executing: ' cd $target_dir/ ;  tar czf  $target_file  $source_dir/${target_year}-${target_month}-* '"
                         cd $target_dir/ ;  tar czf  $target_file  $source_dir/${target_year}-${target_month}-*
  exit_code=$?
  if [ "$?" -eq 0 ]
  then
    echo "### INFO ### Success!  Output file:"
    ls -lh $target_file
  else
    echo "### ERROR ### Something went wrong.  You should probably run this task by hand, figure out the problem, and fix this script."
    if [ -e $target_file ]; then echo "          ### Target file:"; ls -lh $target_file; fi
    exit 2
  fi
fi

echo "### INFO ### End time = $(date)"

exit 0




# Reference
# https://linuxhint.com/convert_hexadecimal_decimal_bash/

# 
# cd /backup/segdb/segments/maintenance/automatic/
# tar czf opt_dqsegdb_python_server_logs_2020.01.tgz opt_dqsegdb_python_server_logs/2020-01-*
