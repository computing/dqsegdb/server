#!/bin/bash
### This script checks all of the logs for a user-specified IFO.  For each day searched, it loops over hours 0-23; it checks if all of the minutes are accounted for in that hour; if so, it counts how many publishing gaps in that hour;
###   at the end of each day that it analyzes, it prints the date and the number of gaps (from 00 to 60) for each hour from 00-23; if an hour didn't have all 60 minutes accounted for, it prints -1.
### LHO, LLO, and Virgo are published every 1 minute, so the number of publisher gaps is a good indicator of network issues; GEO is published every 5 minutes, so a gap there indicates a longer-duration problem.
### Note that the date is an input, but it isn't actually used inside the program; it's only used in the output file name.
### To do:
###   * [done] Not require/take a date as an input argument, or at least make it clearer in the output that that date isn't used.
###   * [probably not going to do this] Modify the program to accept an input date, then loop from that date to current date, or maybe input a start date and number of days to analyze (plus input the IFO).
###   * Add something to check the format of the input date (or maybe just search for the input in the log files, to make sure that it occurs)
###   * [done] Add the reference lines/header before the start of each month (i.e., add before adding the lines for 20xx-xx-01)
###   * Add check that the right number of publishing attempts were made on a given day (1440 for 1-min cadence, 288 for 5-min cadence, etc.)?
###   * Add a check that would spot much higher number of attempts than expected, to detect an h_ref value that is wrong (e.g., h_ref=11, for 12 attempts in an hour, but actual cadence was 1-min, so 60 attempts per hour; detect that count is > 1.2*h_ref)
###   * Add a check that would spot if number of attempts is lower than h_ref, but still within a consistennt, non-zero range (e.g., h_ref=55, for 1-min cadence, but counts keep coming up at 12, b/c cadence is actually 5-min)
###   * [done] Clean up h_ref value setup and move it to the 'setup variables' section
###   * Deal with GEO - tries to publish every 5 min (12x/hr), but only gets files every 30 min (2x/hr), so always misses 10+x/hr (or just leave that and understand that 10/hr = normal?)


# setup variables
verbose=0
# h_ref = how many attempts at publishing must be done in an hour for the hour to be checked for attempts that find files to publish; if count is below this value, output is "-1"
#h_ref_h=55
h_ref_h=11   # changed due to change in publishing cadence from 1 min to 5 min in Oct. 2020
#h_ref_l=55
h_ref_l=11   # changed due to change in publishing cadence from 1 min to 5 min in Oct. 2020
h_ref_v=55
h_ref_g=11
error_message="### ERROR ### Needs an IFO and a run date as command-line arguments.  Valid IFOs are: H, L, V, G. \n\
          ### Format should be: ' (script)  IFO  YYYY-MM-DD  [log file directory (default = /root/bin/publisher_log_files/)] ' \n\
          ### e.g., run as ' /root/bin/publisher_gap_checker_single_IFO.sh  H  2019-03-05 ' to add 2019.03.05 to 'publisher_gaps_H.txt') \n\
          ### or           ' /root/bin/publisher_gap_checker_single_IFO.sh  L  2019-01-01  /backup/segdb/segments/publisher/log/2019.01.02/ ' to add 2019.01.01 to 'publisher_gaps_L.txt' "
header_line1="IFO: $ifo"
header_line2="Date + hour:  00  01  02  03  04  05  06  07  08  09  10  11  12  13  14  15  16  17  18  19  20  21  22  23     total"
header_line3="------------------------------------------------------------------------------------------------------------     -----"


# parse command-line variables and use them
if [ $# -lt 2 ]
then
  echo -e "$error_message"
  exit 1
else
#  output_file=$1
  ifo=$1
  output_file=/root/bin/publisher_gaps_${ifo}.txt
  analysis_date=$2
  search_date=$2
  if [ $# -ge 3 ]
  then
    log_dir=$3
    if [ ! -d $log_dir ]
    then
      echo "### ERROR ### Log dir specified on command line ( $3 ) does not exist.  Please fix this and run this script again (or leave that argument off, to use the default log dir)."
      exit 2
    fi
  else
    log_dir=/root/bin/publisher_log_files/
  fi
fi

h_ref=0
if [ "$ifo" == "H" ]; then h_ref="$h_ref_h"; fi
if [ "$ifo" == "L" ]; then h_ref="$h_ref_l"; fi
if [ "$ifo" == "V" ]; then h_ref="$h_ref_v"; fi
if [ "$ifo" == "G" ]; then h_ref="$h_ref_g"; fi
if [ "$h_ref" -eq 0 ]; then echo "### ERROR ### IFO value ( $ifo ) didn't match known IFOs."; echo -e "$error_message"; exit 3; fi   # if h_ref wasn't changed, then the IFO wasn't matched

if [ ! -e $output_file ]
then
  echo "$header_line1"  >> $output_file   ### note that we're only using ">>", just to be safe
  echo "$header_line2"  >> $output_file
  echo "$header_line3"  >> $output_file
fi

if [ "$verbose" -eq 1 ]
then
  echo "### INFO ### Run variables: output_file = $output_file ; ifo = $ifo ; search_date = $search_date ; log_dir = $log_dir ; h_ref = $h_ref"
fi


# main program
minute_total=0
for hour in {0..23}
do
  hourf=`printf "%02d" $hour`
  check_minutes=`grep "DEBUG pending files" ${log_dir}/${ifo}-DQ_Segments_cur.lo* | grep $search_date | grep " ${hourf}\:" | wc -l`
  if [ $check_minutes -ge $h_ref ]
  then
    minute_count[$hour]=`grep "= \[\]" ${log_dir}/${ifo}-DQ_Segments_cur.lo* | grep $search_date | grep " ${hourf}\:" | wc -l`
    minute_total=$(($minute_total+${minute_count[$hour]}))
  else
    minute_count[$hour]=-1
  fi
done


# print results
# print a new set of header/reference lines before printing info for the 1st day of the month
DOM=`echo $search_date | cut -c9-10`
if [ "$DOM" -eq 01 ]
then
  echo "$header_line3"  >> $output_file
  echo "$header_line2"  >> $output_file
  echo "$header_line3"  >> $output_file
fi
printf "%10s:   %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d  %02d     %5d\n"  $search_date  \
  ${minute_count[0]}  ${minute_count[1]}  ${minute_count[2]}  ${minute_count[3]}  ${minute_count[4]}  ${minute_count[5]}  ${minute_count[6]}  ${minute_count[7]}  ${minute_count[8]}  ${minute_count[9]}  \
  ${minute_count[10]} ${minute_count[11]} ${minute_count[12]} ${minute_count[13]} ${minute_count[14]} ${minute_count[15]} ${minute_count[16]} ${minute_count[17]} ${minute_count[18]} ${minute_count[19]}  \
  ${minute_count[20]} ${minute_count[21]} ${minute_count[22]} ${minute_count[23]} $minute_total  >> $output_file


exit 0

