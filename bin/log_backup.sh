#!/bin/bash
# This is a script to backup the DQSegDB publisher log files.  See notes at https://wiki.ligo.org/Computing/DQSegDBMonitorsAndScripts#log_backup.sh

backup_dir=/backup/segdb/segments/publisher/log/$(date +%Y.%m.%d)
echo "### INFO ### Backup dir:  $backup_dir"

mkdir $backup_dir
exit_code=$?
if [ $exit_code -ne 0 ]; then exit $exit_code; fi

rsync -avP  /var/log/publishing/dev/*  $backup_dir/
exit_code=$?
if [ $exit_code -ne 0 ]; then exit $exit_code; fi
