#!/bin/bash

# Set backup start date.
backup_date=$(date +%Y-%m-%d)

# tmp dir is where a new dir will be created
tmp_dir=/backup/segdb/segments/tmp/
mysql_dir=$tmp_dir/mysql_import_segments-backup_$(date +%Y.%m.%d-%H.%M.%S)

echo "### INFO ### Starting populate from backup on $(uname -n).  $(date)"

# Set backup import start time.
backup_import_start_time=$(date +'%H:%M:%S')
# Add backup import start date/time.
mysql -u dqsegdb_backup -pdqsegdb_backup_pw -e "UPDATE dqsegdb_regression_tests.tbl_backups SET import_time_start = '${backup_import_start_time}' WHERE backup_date LIKE '${backup_date}%'"

# First, get the appropriate file and copy it over to the temp directory
echo "### INFO ### Copying tarballed DB to tmp dir ($mysql_dir).  $(date)"
#mkdir $tmp_dir/mysql_dump
mkdir -p $mysql_dir
if [ ! -d $mysql_dir ]; then echo "### ERROR ### Could not create dir for mysql files ($mysql_dir).  Exiting script."; exit; fi
N=$(date +%y-%m-%d).tar.gz
#cd $tmp_dir/mysql_dump 
cd $mysql_dir
cp /backup/segdb/segments/primary/$N  .
if [ ! -e $mysql_dir/$N ]; then echo "### ERROR ### Did not find tarballed DB ($N) in tmp dir ($mysql_dir).  Exiting script."; exit; fi
echo "### INFO ### Unpackinging tarballed DB ($N).  $(date)"
tar -xvzf $N
if [ -e $mysql_dir/tbl_values.sql ]; then
  echo "Files seem to have unpacked correctly.  $(date)"
else
  echo "Import from backup failed somewhere in scp or tar -x step.  $(date)"
  exit 1
fi

# To import from --tab option

date1=$(date -u +"%s")
# First clean the database out:
echo "### INFO ### Cleaning out old DB (deleting 'segments_backup').  $(date)"
mysql -u root -e "DROP DATABASE IF EXISTS segments_backup"
echo "### INFO ### Creating new DB ('segments_backup').  $(date)"
mysql -u root -e "CREATE DATABASE segments_backup"
mysql -u root -e "use segments_backup"
mysql -u root -e "GRANT SELECT, INSERT, UPDATE ON segments_backup.* TO 'dqsegdb_user'@'localhost'"
mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost'"

# Next create the structure:
echo "### INFO ### Creating structure of new DB.  $(date)"
cat $mysql_dir/tbl_value_groups.sql     | mysql -u root segments_backup
cat $mysql_dir/tbl_values.sql           | mysql -u root segments_backup
cat $mysql_dir/tbl_dq_flags.sql         | mysql -u root segments_backup
cat $mysql_dir/tbl_dq_flag_versions.sql | mysql -u root segments_backup
cat $mysql_dir/tbl_processes.sql        | mysql -u root segments_backup
#cat $mysql_dir/tbl_process_args.sql    | mysql -u root segments_backup
cat $mysql_dir/tbl_segments.sql         | mysql -u root segments_backup
cat $mysql_dir/tbl_segment_summary.sql  | mysql -u root segments_backup

# Then import the data
echo "### INFO ### Importing data to new DB.  $(date)" 
mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_value_groups.txt
mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_values.txt
mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_dq_flags.txt
mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_dq_flag_versions.txt
mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_processes.txt
echo "### INFO ### Roughly half-way through importing DB data.  (About to start tbl_segments*)  $(date)"
#mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_process_args.txt
#mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_segment*.txt
mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_segments.txt
mysqlimport -u root --use-threads=4 --local segments_backup $mysql_dir/tbl_segment_summary.txt
# Note that doing it this way doubles the import speed by running the two segment tables in parallel
echo "### INFO ### Finished importing data.  $(date)"
date2=$(date -u +"%s")
diff=$(($date2-$date1))
time_msg="$(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import (cleaning out old DB, creating new DB, and importing data to new DB)."
#echo "$(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import." >> /usr1/ldbd/backup_logging/import_time.log
echo $time_msg >> /usr1/ldbd/backup_logging/import_time.log                                                         
echo "### INFO #### $time_msg"   

# Lastly, clean up the temp directory:
echo "### INFO ### Deleting new temp dir.  $(date)"
rm -rf $mysql_dir

# Set backup import end time.
backup_import_stop_time=$(date +'%H:%M:%S')
# Add backup import end date/time.
mysql -u dqsegdb_backup -pdqsegdb_backup_pw -e "UPDATE dqsegdb_regression_tests.tbl_backups SET import_time_stop = '${backup_import_stop_time}' WHERE backup_date LIKE '${backup_date}%'"

echo $(date +%Y-%m-%d) $(date +'%H:%M:%S') >> /backup/segdb/segments/monitor/populate_from_backup.txt
echo "### INFO ### Finished populate from backup on segments-backup.  $(date)" 
