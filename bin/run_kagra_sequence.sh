#!/bin/bash
# This is a script to rsync KAGRA files to segments-dev, then run the script to process and publish them.
# Started by Robert Bruntz on 2023.02.09.
# Note that JST = UTC + 9 = PST + 17 = PDT + 16.  KAGRA files for a given day are generally created a little after 24:00 UTC = 16:00 PST = 17:00 PDT = 09:00 JST the next day.
# Commands to run manually:
# 1. See the last published segments and what date is ready to be published next:
#   ligolw_segment_query_dqsegdb --segment-url https://segments.ligo.org --query-segments --include-segments K1:GRD_SCIENCE_MODE:1  --gps-start-time 0 --gps-end-time 1500000000  | grep segment_definer:segment_def_id:0  > /tmp/ligolw_k1_output.txt; tail /tmp/ligolw_k1_output.txt; ltconvert `tail -n 1 /tmp/ligolw_k1_output.txt | cut -c7-16`
# 2. rsync new files to this machine (probably only works on seg or -dev):
#   rsync -avP --port 10873 gwdet.icrr.u-tokyo.ac.jp::kagrasegments  /dqxml/K1_test/
# 3. Process files for a given date and publish their segments:
#   time /root/bin/print_kagra_publisher_commands.sh  2023-02-09 1
# Note that KAGRA produces 6 XML files every day, but currently only 5 are published.  We're waiting to hear if we should publish the 6th.  The processing and publishing script knows what to do with the 5.  (Manual commands at the end of this script.)

publish_count_limit=10
output_file=/root/kagra_logs/kagra_publishing_$(date +%Y.%m.%d-%H.%M.%S)
echo "Output file = $output_file"
date >> $output_file
echo "### INFO ### Starting KAGRA publishing sequence." >> $output_file

function get_listing {
  # get listing of last published segments and what date is ready to be published next (based on end of last published segment)
  date >> $output_file
  # output should look like this (without single quotes): ' "",1359936018,0,"process:process_id:0","segment_definer:segment_def_id:0","segment_summary:segment_sum_id:25",1338255296,0, '
  ligolw_segment_query_dqsegdb --segment-url https://segments.ligo.org --query-segments --include-segments K1:GRD_SCIENCE_MODE:1  --gps-start-time 0 --gps-end-time 1500000000  | grep segment_definer:segment_def_id:0  > /tmp/ligolw_k1_output.txt; \
    tail -n 5 /tmp/ligolw_k1_output.txt >> $output_file; /root/bin/ltconvert `tail -n 1 /tmp/ligolw_k1_output.txt | cut -c7-16` >> $output_file
}

function get_end_date_time {
  # get formatted date of end of last published segment; assumes function 'get_listing' was run to populate /tmp/ligolw_k1_output.txt
  # note that the date and time of the last published segment are actually the date of the next day, at 00:00:00 UTC, e.g.:
  #   "",1366502418,0,"process:process_id:0","segment_definer:segment_def_id:0","segment_summary:segment_sum_id:26",1365552018,0,
  #   GPS time 1366502418 = Wed Apr 26 00:00:00 UTC 2023
  tmp_size=`cat /tmp/ligolw_k1_output.txt | wc --chars`
  if [ "$tmp_size" -lt 100 ]; then echo "### ERROR ### File /tmp/ligolw_k1_output.txt should be 120+ characters, but it's only $tmp_size characters.  Exiting." >> $output_file; exit 1; fi
  last_gps=`tail -n 1 /tmp/ligolw_k1_output.txt | cut -c7-16`
  last_unix=$((last_gps + 315964782))
  last_date=`date -u -d @${last_unix} "+%Y-%m-%d"`
  last_time_utc=`date -u -d @${last_unix} "+%H:%M:%S"`
}

# rsync KAGRA files; note that this only needs to be done once per run
echo "### INFO ### rsync'ing files from KAGRA" >> $output_file
rsync -avP --port 10873 gwdet.icrr.u-tokyo.ac.jp::kagrasegments  /dqxml/K1_test/  &>> $output_file

# processing and publishing loop
# get date for files to be published from end of last published segment; check for matching files to be published based on date; process and publish files; repeat until no matching files found
publish_count=0
while [ 1 -eq 1 ]
do
  # check for files to publish
  # get listing of last published segments, incl. GPS time and UTC date and time of end of last segment; saves output to a temp file
  echo "### INFO ### Getting last 5 published segments, and GPS time and UTC date and time of end of last segment." >> $output_file
  get_listing
  # get the date (last_date) and UTC time (last_time_utc) of the end of the last published segment, from the temp file created above
  get_end_date_time
  # verify that the end of the last published segment is at midnight UTC (should be)
  #if [ "$last_time_utc" != "00:00:00" ]; then echo "### ERROR ### End time of last published segment should have been 00:00:00 UTC but was actually $last_time_utc.  Aborting." >> $output_file; exit 2; fi
  if [ "$last_time_utc" != "00:00:00" ]; then echo "### ERROR ### End time of last published segment should have been 00:00:00 UTC but was actually $last_time_utc.  Aborting." >> $output_file; break; fi
  echo "### INFO ### Next date to publish segments for is:  $last_date" >> $output_file
  # make sure there are exactly 6 xml files for the date to be published; if so, process and publish them
  # files look like this: /dqxml/K1_test/K1-OMC_OVERFLOW_OK/2023/K1-OMC_OVERFLOW_OK_SEGMENT_UTC_2023-02-08.xml
  publishable_count=`ls -1 /dqxml/K1_test/*/*/*${last_date}.xml 2> /dev/null | wc --lines`
  if [ "$publishable_count" -eq 6 ]
  then
    echo "### INFO ### Processing and publishing files for date $last_date." >> $output_file
    date >> $output_file
    /root/bin/print_kagra_publisher_commands.sh  $last_date 1  &>> $output_file
    exit_code=$?
    date >> $output_file
    #if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Publishing for date $last_date produced exit code $exit_code.  This should be investigated before publishing is tried again." >> $output_file; exit 3; fi
    if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Publishing for date $last_date produced exit code $exit_code.  This should be investigated before publishing is tried again." >> $output_file; break; fi
  else
    echo "### NOTICE ### Did not find exactly 6 files to publish for date $last_date; found $publishable_count files instead, which is not acceptable.  Ending publishing." >> $output_file
    break
  fi
#echo "end of loop"
let publish_count++
if [ "$publish_count" -ge "$publish_count_limit" ]; then echo "### NOTICE ### Publish count reached its limit ($publish_count_limit), so ending publishing loop, for safety." >> $output_file; break; fi
done

echo "### INFO ### Done with KAGRA publishing sequence." >> $output_file
date >> $output_file

# error checking and messages for user running script or crontab output - printed to std out, not output file
# didn't find the files it expected to publish
missing_file_count=`grep "### ERROR ###.*does not exist." $output_file | wc --lines`
if [ "$missing_file_count" -gt 0 ]; then echo "### ERROR ### One or more xml files were not found for date $last_date.  Publishing probably failed."; fi
# didn't produce enough messages about publishing working
success_count=`grep "Insert worked!" $output_file | wc --lines`
if [ "$success_count" -lt 5 ]; then echo "### ERROR ### Fewer than 5 flags were published for date $last_date.  Publishing might have failed."; fi
# output file is just too short; should be > 200 lines for a typical day, but might be shorter if someone downloaded all of the files manually; probably never < 170
output_file_size=`cat $output_file | wc --lines`
if [ "$output_file_size" -lt 150 ]; then echo "### ERROR ### Output file ( $output_file) is $output_file_size lines, but should be over 150 lines.  Something might have gone wrong."; fi
if [ "$missing_file_count" -eq 0 ] && [ "$success_count" -ge 5 ] && [ "$output_file_size" -ge 150 ]; then echo "### INFO ### Looks like publishing succeeded."; fi

exit 0

# manual publishing commands (for 2023.02.09):
# /root/bin/publish_kagra_xml_files_updated.sh  /dqxml/K1_test/K1-OMC_OVERFLOW_OK/2023/K1-OMC_OVERFLOW_OK_SEGMENT_UTC_2023-02-09.xml  1  0  1  OMC_OVERFLOW_OK  "OMC overflow does not happened. K1:FEC-32_ADC_OVERFLOW_0_0 == 0" 
# /root/bin/publish_kagra_xml_files_updated.sh  /dqxml/K1_test/K1-OMC_OVERFLOW_VETO/2023/K1-OMC_OVERFLOW_VETO_SEGMENT_UTC_2023-02-09.xml  1  0  1  OMC_OVERFLOW_VETO  "OMC overflow happened. K1:FEC-32_ADC_OVERFLOW_0_0 != 0" 
# /root/bin/publish_kagra_xml_files_updated.sh  /dqxml/K1_test/K1-GRD_LOCKED/2023/K1-GRD_LOCKED_SEGMENT_UTC_2023-02-09.xml  1  0  1  GRD_LOCKED  "GRD-LSC_LOCK_STATE_N == 1000" 
# /root/bin/publish_kagra_xml_files_updated.sh  /dqxml/K1_test/K1-GRD_SCIENCE_MODE/2023/K1-GRD_SCIENCE_MODE_SEGMENT_UTC_2023-02-09.xml  1  0  1  GRD_SCIENCE_MODE  "Observation mode. K1:GRD-IFO_STATE_N == 1000" 
# /root/bin/publish_kagra_xml_files_updated.sh  /dqxml/K1_test/K1-GRD_UNLOCKED/2023/K1-GRD_UNLOCKED_SEGMENT_UTC_2023-02-09.xml  1  0  1  GRD_UNLOCKED  "GRD-LSC_LOCK_STATE_N not_equal 1000" 
