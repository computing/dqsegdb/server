#!/bin/bash
# Started by Robert Bruntz, 2019.11.18
# This is a script to update the cumulative additions to the whitelist (in file /backup/segdb/reference/lgmm/whitelist_additions_starting_2018.12.xx), then create a new definitive whitelist (such as /backup/segdb/reference/lgmm/whitelist_definitive_2019.11.15a_sorted_uniq).
# This script is not intended to update the whitelist on individual machines.  For that, see /backup/segdb/reference/root_files/update_local_whitelist.sh .
# To do:
#   * Check if the whitelist additions file already contains the cert subject
#   * Check if the newest definitive whitelist file already contains the cert subject
#   * Check if the whitelist additions file already contains the cert subject after addition
#   * Check if the newest definitive whitelist file already contains the cert subject after addition
#   * Make sure that /backup/segdb/ is online
#   * Make sure that the subject starts and ends with double quotes, with exactly 2 double quotes in the whole thing
#   * Check if the subject contains a CR or LF (as they often do in emails)
#   * Check if the date given is different from today's date; if so, warn and ask for verification
#   * Check if the newest definitive whitelist file matches (today's date)(a, b, or c); create default file for the earliest of those 3

lgmm_ref_dir=/backup/segdb/reference/lgmm
whitelist_additions_file=/backup/segdb/reference/lgmm/whitelist_additions_starting_2018.12.xx

if [ $# -eq 2 ]
then
  cert_date=$1
  cert_subject=\"${2}\"
else
  echo "### ERROR ### Usage: (script name)  (date)  \"(certificate subject)\""
  echo "          ### Example:  ' /backup/segdb/reference/root_files/update_definitive_whitelist.sh  2019.11.18  \"/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=emfollow-test.ligo.caltech.edu/CN=emfollow-test/CN=Leo Singer/CN=UID:leo.singer.robot\" '"
  echo "          ### Note the date format (YYYY.MM.DD) and double quotes around the certificate subject; cert date is the date it was issued/emailed to the user (or as close as you can figure it); cert subject should all be on one line, not split"
  exit
fi

echo $cert_date
echo $cert_subject
echo "### INPUT ### Does this look correct? (Y/N)"
read user_input

if [ $user_input == "Y" ] || [ $user_input == "y" ]
then
  echo "### INFO ### Adding cert subject to $whitelist_additions_file"
  echo $cert_date  >>  $whitelist_additions_file
  echo $cert_subject  >>  $whitelist_additions_file
  echo "### INFO ### Last 3 lines of $whitelist_additions_file:"
  cat $whitelist_additions_file | tail -n 6
  echo "### --- ###"
else
  echo "### ERROR ### Response wasn't 'Y' or 'y' (it was '$user_input'), so script will now exit."
  exit
fi

old_definitive_file=`ls -1 /backup/segdb/reference/lgmm/whitelist_definitive_20??.??.???_sorted_uniq | tail -n 1`
new_definitive_file=/backup/segdb/reference/lgmm/whitelist_definitive_$(date +%Y.%m.%d)a_sorted_uniq
echo "### INFO ### Last 3 definitive whitelist files:  ( ' ls -l /backup/segdb/reference/lgmm/whitelist_definitive_20??.??.???_sorted_uniq | tail -n 3 ' )"
ls -l /backup/segdb/reference/lgmm/whitelist_definitive_20??.??.???_sorted_uniq | tail -n 3
echo "### INFO ### You probably want to run something like these lines:"
echo "cp -i  `ls -1 /backup/segdb/reference/lgmm/whitelist_definitive_20??.??.???_sorted_uniq | tail -n 1`  $new_definitive_file"
echo "nano $new_definitive_file"
echo "OR"
echo "echo \\\"$cert_subject\\\" > /tmp/new_cert; cat $old_definitive_file /tmp/new_cert | sort -u > $new_definitive_file"
echo "ALSO: run something like ' ln -sf  $new_definitive_file  /backup/segdb/reference/lgmm/whitelist '"

exit


