#!/bin/bash
# This is a script to run 'log_backup.sh', to send all output to a log file, check the contents of that file, and report a message if anything went wrong.  If everything worked, no message is sent.
# It is planned that this script will be run by cron, so any un-redirected output from this script will be forwarded by cron to the appropriate users.
# Log from this script is saved in a temp file (probably /tmp/log_backup.sh_log.txt ), which is simply overwritten each time the script is run.  It is only copied and saved if there is an error or it's a certain date.
# Run this just by setting the variables in the first block of code, then executing this script.  No command-line args are needed or used.
# Note that this script generally exits with the exit code of the script that it ran.
# Also, the script is set up such that it can be reused to run other scripts the same way, just by changing the names in this comment block and the appropriate variables in the first block of code.  Everything else should stay the same.

script_dir=/local/ldbd/bin
script_name=log_backup.sh
tmp_log_dir=/tmp   # note that this is the log dir for output from the script, not the dir that contains the log files that are being backed up (which is controlled within 'log_backup.sh')
save_log_dir=/local/ldbd/log
time_log_file=/backup/segdb/segments/monitor/publisher_log_backup.txt
exit_code=-1   # so we can tell if it's been changed
start_time=$(date +%Y.%m.%d-%H.%M.%S)
start_date=$(date +%Y.%m.%d)
start_time2=$(date +%H:%M:%S)
start_sec=$(date -u +"%s")

# wipe the old log file and save the start date and time to it
echo "### INFO ### Running $script_dir/$script_name on $(uname -n) .  Start:  $start_time"  >  $tmp_log_dir/${script_name}_log.txt

# run the main script, saving output to the log file
${script_dir}/${script_name}  &>>  $tmp_log_dir/${script_name}_log.txt
exit_code=$?
end_time=$(date +%Y.%m.%d-%H.%M.%S)

# check the exit code from the run; if not 0, save the log file for analysis and write messages to output from this script, which cron will send to admins
if [ $exit_code -ne 0 ]
then
  echo "### INFO ### Script had some issues during run.  Exit code = $exit_code.  End:  $end_time"  >>  $tmp_log_dir/${script_name}_log.txt
  cp -p  $tmp_log_dir/${script_name}_log.txt  $save_log_dir/${script_name}_log_${start_time}.txt
  echo "### ERROR ### $script_name did not complete successfully at $start_time."   # note: this output will be sent to admins by cron
  echo "          ### Log file $save_log_dir/${script_name}_log_${start_time}.txt is printed below:"
  echo
  cat $save_log_dir/${script_name}_log_${start_time}.txt
  exit $exit_code
else   # this means everything ran fine
  echo "### INFO ### Script ran successfully.  End:  $end_time"  >>  $tmp_log_dir/${script_name}_log.txt
  end_time=$(date +%H:%M:%S)
  end_sec=$(date -u +"%s")
  duration_sec=$((end_sec - start_sec))
  duration_sec_tmp=$duration_sec; day_count=0; hour_count=0; minute_count=0; sec_count=0
  while [ $duration_sec_tmp -ge $((24*60*60)) ]; do day_count=$((day_count + 1)); duration_sec_tmp=$((duration_sec_tmp - 24*60*60)); done   # count up any days in the run time
  while [ $duration_sec_tmp -ge $((60*60)) ]; do hour_count=$((hour_count + 1)); duration_sec_tmp=$((duration_sec_tmp - 60*60)); done   # count up any remaining hours in the run time
  while [ $duration_sec_tmp -ge $((60)) ]; do minute_count=$((minute_count + 1)); duration_sec_tmp=$((duration_sec_tmp - 60)); done   # count up any remaining minutes in the run time
  sec_count=$duration_sec_tmp   # anything left over after removing days, hours, and minutes is just seconds
  echo "$start_date  $start_time2 - $end_time = $duration_sec sec  (= $day_count days  +  $hour_count hours  +  $minute_count minutes  +  $sec_count seconds)" >> $time_log_file
  if [ $(date +%d) -eq 1 ]; then cp -p  $tmp_log_dir/${script_name}_log.txt  $save_log_dir/${script_name}_log_${start_time}.txt; fi   # on the 1st of every month, save the log file, even if there was no error
  exit $exit_code
fi

exit
