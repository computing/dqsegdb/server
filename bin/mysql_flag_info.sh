#!/bin/bash
# This is a script to get some info on a flag in the mariadb (mysql) DB.

# Run as: '  (command name)  (flag name)  [IFO (default=H)]  [DB name (defalt matches machine, else dqsegdb)]  '
# to do:
#   * check DB name, to make sure that it exists on the current machine
#   * check flag name, to make sure it's all 1 word (no space or non-underscore punctuation)
#   * check IFO, to make sure that it's a valid option (H,L,V,K,G,H1,L1,V1,K1,G1,LHO,LLO,VGO,KAG,GEO,Virgo,Kagra)
#   * check the flag name, to make sure that it exists in the DB
#   * [done] when Kagra info is in the DB, set the "ifo_number=" part for Kagra
#   * when Kagra info is in the DB, give a useful example flag for Kagra in the "flag_example_string=" line


usage_string="### INFO ### Run as: '  (command name)  (flag name)  [IFO (default=H)]  [DB name (defalt matches machine, else dqsegdb)]  ' (valid IFO names: H,L,V,K,G,H1,L1,V1,K1,G1,LHO,LLO,VGO,KAG,GEO,Virgo,Kagra)"
flag_example_string="         ### Flag examples include: GRD-ISC_LOCK_OK, DMT-ANALYSIS_READY, ODC-MASTER_SUMMARY, ITF_SCIENCE (on Virgo), GEO-UP (on GEO)"
# set a default DB, in case one isn't entered as a cmd-line arg
ifo=X
ifo_number=0
db_name=dqsegdb

# set db_name based on machine name
if [ `uname -n` == "segments" ]; then db_name=dqsegdb; fi
if [ `uname -n` == "segments-backup" ]; then db_name=segments_backup; fi
if [ `uname -n` == "segments-web" ]; then db_name=dqsegdb_web; echo "### WARNING ### Much of this program will not function as expected for segments-web.  This script will wait 10 seconds, then continue.  (You have been warned.)"; sleep 10; fi
if [ `uname -n` == "segments-dev" ]; then db_name=dqsegdb; fi
#if [ `uname -n` == "segments-dev2" ]; then db_name=dqsegdb; fi
if [ `uname -n` == "segments-dev2" ]; then db_name=segments_backup; fi

# use command-line variables
if [ "$#" -eq 0 ]; then echo $usage_string; echo $flag_example_string; exit; fi
if [ "$#" -ge 1 ]; then flag_name=$1; fi
if [ "$#" -ge 2 ]; then ifo_name=$2; else ifo_name=H; echo "### NOTICE ### No IFO given on the command line.  Using default: IFO = $ifo_name"; sleep 2; fi
if [ "$#" -ge 3 ]; then db_name=$3; else echo "### NOTICE ### No DB name given on the command line.  Using default: DB name = $db_name"; sleep 2; fi

# set ifo_name
if [ "$ifo_name" == "H" ]  || [ "$ifo_name" == "H1" ]  || [ "$ifo_name" == "LHO" ]; then ifo="H"; ifo_number=2; fi
if [ "$ifo_name" == "L" ]  || [ "$ifo_name" == "L1" ]  || [ "$ifo_name" == "LLO" ]; then ifo="L"; ifo_number=3; fi
if [ "$ifo_name" == "V" ]  || [ "$ifo_name" == "V1" ]  || [ "$ifo_name" == "VGO" ]  || [ "$ifo_name" == "Virgo" ]; then ifo="V"; ifo_number=1; fi
if [ "$ifo_name" == "K" ]  || [ "$ifo_name" == "K1" ]  || [ "$ifo_name" == "KAG" ]  || [ "$ifo_name" == "Kagra" ]; then ifo="K"; ifo_number=29; fi   # note: 
if [ "$ifo_name" == "G" ]  || [ "$ifo_name" == "G1" ]  || [ "$ifo_name" == "GEO" ]; then ifo="G"; ifo_number=24; fi
if [ "$ifo" == "X" ]; then echo "### ERROR ### IFO name was not recognized.  Recognized values: (H,L,V,K,G,H1,L1,V1,K1,G1,LHO,LLO,VGO,KAG,GEO,Virgo,Kagra)"; exit; fi   # 'ifo' is set to "X" at the start and only changed for a recognized IFO name

# print values for testing
echo "### INFO ### Using these values: flag name = $flag_name; IFO = $ifo; DB name = $db_name; IFO number = $ifo_number"
if [ "$ifo_number" -eq 0 ]; then echo "### ERROR ### 'ifo_number' is 0, which is not a usable value.  You should figure out what happened here."; exit; fi

# print DB names and tables and IFO values in the DB being used
echo "### INFO ### DBs on this machine:  (excluding 'information_schema', 'mysql', 'performance_schema', and 'test' (if present); to see all, run ' mysql -e \"show databases;\" ')"
mysql -e "show databases;" | grep -v Database | grep -v information_schema | grep -v mysql | grep -v performance_schema | grep -v test
echo "### INFO ### Tables in this DB ($db_name):  (from ' mysql -e \"use $db_name; show tables;\" ')"
mysql -e "use $db_name; show tables;"
echo "### INFO ### IFOs in this DB ($db_name):  (to see all values, run ' mysql -e \"use $db_name; select value_txt,value_id from tbl_values;\" ')"
mysql -e "use $db_name; select value_txt,value_id from tbl_values where (value_id <= 3 OR value_id=24 OR value_id=29);"

# print flag info
echo "### INFO ### Flag name, flag ID, and IFO  (from table 'tbl_dq_flags') (using ' mysql -e \"use $db_name;  select dq_flag_name,dq_flag_id,dq_flag_ifo from tbl_dq_flags\
 where dq_flag_name = \\\"$flag_name\\\" and dq_flag_ifo = \\\"$ifo_number\\\";\" ':"
flag_name_count=`mysql -e "use $db_name; select dq_flag_name,dq_flag_id,dq_flag_ifo from tbl_dq_flags where dq_flag_name = \"$flag_name\" and dq_flag_ifo = \"$ifo_number\";" | grep $flag_name | wc --lines`

if [ $flag_name_count -lt 0 ]; then echo "### WARNING ### There was an error in the query for the requested flag ($flag_name) for the given IFO number ($ifo_number) in this DB ($db_name).  This could cause trouble."; fi
if [ $flag_name_count -eq 0 ]
then
  echo "### ERROR ### Could not find the requested flag ($flag_name) for the given IFO number ($ifo_number) in this DB ($db_name)."
  echo "          ### You can explore this more with variations of a command like this: ' mysql -e \"use $db_name;\
 select dq_flag_name,dq_flag_id,dq_flag_ifo from tbl_dq_flags where dq_flag_name = \\\"$flag_name\\\" and dq_flag_ifo = \\\"$ifo_number\\\";\" '"
  exit
fi
if [ $flag_name_count -gt 1 ]; then echo "### WARNING ### There were $flag_name_count responses the requested flag ($flag_name) for the given IFO number ($ifo_number) in this DB ($db_name).  There should be 1 or 0.  This could cause trouble."; fi
if [ $flag_name_count -eq 1 ]
then
  mysql -e "use $db_name; select dq_flag_name,dq_flag_id,dq_flag_ifo from tbl_dq_flags where dq_flag_name = \"$flag_name\" and dq_flag_ifo = \"$ifo_number\";"
  dq_flag_id=`mysql -e "use $db_name; select dq_flag_id from tbl_dq_flags where dq_flag_name = \"$flag_name\" and dq_flag_ifo = \"$ifo_number\";" | grep -v dq_flag_id`
fi
echo "### INFO ### Value 'dq_flag_id' in table 'tbl_dq_flags'  is the same as  value 'dq_flag_fk' in table 'tbl_dq_flag_versions'"

echo "### INFO ### More flag info  (from table 'tbl_dq_flag_versions'):"
mysql -e "use $db_name; select dq_flag_fk, dq_flag_version_id, dq_flag_version, dq_flag_version_known_segment_total, dq_flag_version_active_segment_total  from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id"
mysql -e "use $db_name; select dq_flag_version, dq_flag_version_known_earliest_segment_time, dq_flag_version_known_latest_segment_time, dq_flag_version_active_earliest_segment_time, dq_flag_version_active_latest_segment_time \
  from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id"
flag_name_count2=`mysql -e "use $db_name; select dq_flag_fk, dq_flag_version_id, dq_flag_version  from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id" | grep [0-9] | wc --lines`   # the 'grep' changes the format of the output to include 
   # only values, not column headers, etc., so we can see how many lines of actual results (if any)

if [ $flag_name_count2 -lt 0 ]; then echo "### WARNING ### There was an error in the query for the requested flag ID number (dq_flag_id = dq_flag_fk = $dq_flag_id) in table 'tbl_dq_flag_versions'.  This could cause trouble."; fi
if [ $flag_name_count2 -eq 0 ]
then
  echo "### ERROR ### Could not find the requested flag ID number (dq_flag_id = dq_flag_fk = $dq_flag_id) in table 'tbl_dq_flag_versions'."
  echo "          ### You can explore this more with variations of a command like this: ' mysql -e \"use $db_name; select dq_flag_fk, dq_flag_version_id, dq_flag_version  from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id;\" '"
  exit
fi
if [ "$flag_name_count2" -eq 1 ]
then
  earliest_known_segment_time=`mysql -e "use $db_name; select dq_flag_version_known_earliest_segment_time from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id" | grep [0-9]`
  latest_known_segment_time=`mysql   -e "use $db_name; select dq_flag_version_known_latest_segment_time   from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id" | grep [0-9]`
  earliest_active_segment_time=`mysql -e "use $db_name; select dq_flag_version_active_earliest_segment_time from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id" | grep [0-9]`
  latest_active_segment_time=`mysql   -e "use $db_name; select dq_flag_version_active_latest_segment_time   from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id" | grep [0-9]`
  echo "### INFO ### On some systems, you can convert GPS time to human time with 'tconvert (GPS time)'"
  echo "         ### On some systems, you can convert one or two GPS time(s) to human time with something like ' ltconvert  $earliest_known_segment_time  $latest_known_segment_time '"
  dq_flag_version_id=`mysql -e "use $db_name; select dq_flag_version_id from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id" | grep [0-9]`
else
  echo "### INFO ### On some systems, you can convert GPS time to human time with 'tconvert (GPS time)'"
  echo "         ### On some systems, you can convert one or two GPS time(s) to human time with something like 'ltconvert (GPS time)' or 'ltconvert  (GPS time 1)  (GPS time 2)'"
fi
echo "### INFO ### You can get *all* of the info about a given flag with something like ' mysql -e \"use $db_name; select  *  from tbl_dq_flag_versions where dq_flag_fk = $dq_flag_id;\" ' (output might be a little bit cluttered)"
echo "         ### You can replace the '*' with column names (e.g., 'dq_flag_version_comment, dq_flag_version_date_created, dq_flag_version_date_last_modified' (no quotes)) to get just those columns."
# print	segment	info
echo "### INFO ### Value 'dq_flag_version_id' in table 'tbl_dq_flag_versions'  is the same as  'dq_flag_version_fk' in table 'tbl_segments'"
echo "### INFO ### Segment info  (from table 'tbl_segment_summary' (for 'known' segments) or 'tbl_segments' [sic] (for 'active' segments), using 'dq_flag_version_fk' to identify this flag's segments):"

if [ "$flag_name_count2" -eq 1 ]
then
  echo "         ### To get 'known' segments,  run something like ' mysql -e \"use $db_name; select  *  from tbl_segment_summary where dq_flag_version_fk = $dq_flag_version_id and\
 segment_start_time >= $((latest_known_segment_time - 160)) and segment_stop_time <= $((latest_known_segment_time));\" ':"
  mysql -e "use $db_name; select  *  from tbl_segment_summary where dq_flag_version_fk = $dq_flag_version_id and segment_start_time >= $((latest_known_segment_time - 160)) and segment_stop_time <= $((latest_known_segment_time));"
  echo "         ### Note that only segments which both start *and* end within the time being searched are included in the output."
  echo "         ### To get 'active' segments, run something like ' mysql -e \"use $db_name; select  *  from tbl_segments where dq_flag_version_fk = $dq_flag_version_id and\
 segment_start_time >= $((latest_active_segment_time - 160)) and segment_stop_time <= $((latest_active_segment_time));\" ':"
  mysql -e "use $db_name; select  *  from tbl_segments where dq_flag_version_fk = $dq_flag_version_id and segment_start_time >= $((latest_active_segment_time - 160)) and segment_stop_time <= $((latest_active_segment_time));"
  echo "         ### Note that only segments which both start *and* end within the time being searched are included in the output."
  echo "### INFO ### These results could be compared to a query, such as ' ligolw_segment_query_dqsegdb -t https://segments.ligo.org -q -a ${ifo}1:${flag_name}:1 -s $((latest_known_segment_time - 160)) -e $((latest_known_segment_time)) '."
else
  echo "         ### To get 'known' segments,  run something like ' mysql -e \"use $db_name; select  *  from tbl_segment_summary where dq_flag_version_fk = (dq_flag_version_id from table 'tbl_dq_flag_versions')\
 and segment_start_time >= 1000000000 and segment_stop_time <= 2000000000;\" | less '"
  echo "         ### Note that only segments which both start *and* end within the time being searched are included in the output."
  echo "         ### To get 'active' segments, run something like ' mysql -e \"use $db_name; select  *  from tbl_segments where dq_flag_version_fk = (dq_flag_version_id from table 'tbl_dq_flag_versions')\
 and segment_start_time >= 1000000000 and segment_stop_time <= 2000000000;\" | less '"
  echo "         ### Note that only segments which both start *and* end within the time being searched are included in the output."
  echo "### INFO ### These results could be compared to a query, such as ' ligolw_segment_query_dqsegdb -t https://segments.ligo.org -q -a ${ifo}1:${flag_name}:1 -s 1000000000 -e 2000000000 '."
  echo "         ### Note that the flag version in this query (':1') should be changed to match the flag version used to get (dq_flag_version_id from table 'tbl_dq_flag_versions')."
fi

exit

