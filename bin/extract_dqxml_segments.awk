#!/bin/awk -f
# This script scans DQ XML files and calcluates	the known and active segments for a flag in those files.
# Script started by Robert Bruntz on 2019.04.16.
# Note that the script is intended to be used for files created in 2019 or later; it may not work on files created before April 2019.  (Not that we expect anyone to try, but just in case.)
# Inputs:
#   * IFO (H, L, V, or G)
#   * Flag name	(usually DMT-ANALYSIS_READY or GRD-ISC_LOCK_OK, but can be any flag name)
#   * GPS start	time
#   * GPS end time
#   * flag for 'verbose' mode
# Outputs:
#   * (output file)

# to do:
# * fix problem with how Virgo stashes time in DQ XML files (esp. for GPS minor start time <= 4 and for rollover to new dir)
# * figure out how to handle GEO files
# * [done (for H, L)] check if files are available before adding to list of files to be read


BEGIN {
# there should be at least 5 command-line arguments; note that ARGV[0] = 'awk', so ARGC should = 6 (= 1 + 5)
  if (ARGC < 6) {print "### ERROR ### There should be 5 command-line arguments to extract_dqxml_segments.awk; $ARGC were supplied.";
                 print "          ### Usage: 'extract_dqxml_segments.awk  [IFO]  [flag_name]  [GPS_start_time]  [GPS_end_time]  [verbose_flag]'"; exit }
# check for a valid IFO value
  switch (ARGV[1]) {
    case "H": {ifo="H"; xml_dir="/dqxml/H1/"; break}
    case "L": {ifo="L"; xml_dir="/dqxml/L1/"; break}
    case "V": {ifo="V"; xml_dir="/dqxml/V1/"; break}
    case "G": {ifo="G"; xml_dir="/dqxml/G1_test/"; break}
    default: {printf "### ERROR ### Command-line argument 1 should be one of {H, L, V, G}, but was '%s'.\n", ARGV[1]; print "          ### Can't fix this; exiting."; exit}
  }
# assign other variables
  flag_name=ARGV[2]   ### don't think there are any tests for this one - maybe that there are no spaces?
  gps_start=ARGV[3]   ### tests: make sure it's a number; make sure that it's GT the lowest LIGO GPS time 
    if (gps_start < 1000000000) {printf "### ERROR ### GPS start time must be later than 1000000000.  (Value was %d.)  Please fix this and re-run.\n", gps_start; exit}
    if (gps_start > 1800000000) {printf "### ERROR ### GPS start time must be earlier than 1800000000.  (Value was %d.)  Please fix this and re-run.\n", gps_start; exit}
  gps_end=ARGV[4]     ### tests: make sure it's a number; make sure taht it's GT the lowest LIGO GPS time; make sure that it's GT GPS_start
    if (gps_end < 1000000000) {printf "### ERROR ### GPS end time must be later than 1000000000.  (Value was %d.)  Please fix this and re-run.\n", gps_end; exit}
    if (gps_end > 1800000000) {printf "### ERROR ### GPS end time must be earlier than 1800000000.  (Value was %d.)  Please fix this and re-run.\n", gps_end; exit}
    if (gps_end <= gps_start) {printf "### ERROR ### GPS start time (%d) is later than GPS end time (%d).  Please fix this and re-run.\n", gps_start, gps_end; exit}
  verbose=0
    if (ARGV[5] == 1) {verbose=1}   # note that only if the flag = 1 is verbose set to 1; all other inputs keep verbose at 0
#  ARGC=0   # set this to 0 so that awk doesn't think that there are any files to be processed [but then it needs some keyboard input to run, which is a problem]
  ARGC=2   # set this to 2 so that awk thinks there's only 1 file to be processed; see next line
  ARGV[1]="/etc/fstab"   # awk thinks that this is the file to be processed (but it doesn't actually do anything with that file; all file-handling is done independent of that file); this file is on every system
  if (verbose==1) {printf "ifo = %s; xml_dir = %s; flag_name = %s; gps_start = %d; gps_end = %d\n", ifo, xml_dir, flag_name, gps_start, gps_end}

  gps_duration = gps_end - gps_start
  gps_start_major = int(gps_start/100000); gps_start_minor = gps_start - gps_start_major*100000
  gps_end_major   = int(gps_end/100000);   gps_end_minor   = gps_end   - gps_end_major*100000
  if (verbose==1) {printf "gps_start_major = %d; gps_start_minor = %05d; gps_end_major = %d; gps_end_minor = %05d\n", gps_start_major, gps_start_minor, gps_end_major, gps_end_minor}
  known_segments_count  = 0  # note that the arrays increment, then use these values, so the first known (start, end) times are for index 1
  active_segments_count = 0  # same as for known_segments_count
  coalesced_known_segments_count  = 0
  coalesced_active_segments_count = 0
  split("", coalesced_known_segments_start_times)
  split("", coalesced_known_segments_end_times)
  split("", coalesced_active_segments_start_times)
  split("", coalesced_active_segments_end_times)
  split("", raw_known_chunks_array)
  split("", known_segments_start_times)
  split("", known_segments_end_times)
  split("", raw_active_chunks_array)
  split("", active_segments_start_times)
  split("", active_segments_end_times)
  printf "### INFO ### Search parameters: IFO = %s ; flag = %s ; GPS start time = %d ; GPS end time = %d \n", ifo, flag_name, gps_start, gps_end
}   # BEGIN

# main program
{
# generate file list
  if (ifo == "H"  ||  ifo == "L") {generate_file_list_lho_llo()}   # populates file_list[]; index goes 0 to (file_count - 1)
  if (ifo == "V") {generate_file_list_virgo()}
  if (ifo == "G") {generate_file_list_geo()}
  if (verbose == 1) {print "number of files in file_list = ", length(file_list)}
#  file_list_unsorted = file_list
#  asort(file_list)


# loop over files
#  for (file_loop_index in file_list)   # not using this form b/c it doesn't scan through the files in sequential order
  for (file_loop_index=0; file_loop_index < file_count; file_loop_index++)   # in file_list[index], 'index' goes 0 to (file_count - 1)
  {
  if (verbose == 1) {print}
  # read in the file contents
    RS_old = RS
    RS = ":::::"   # RS = 'record separator'; we set it to a string that should never occur in a DQ XML file, so the whole file gets read in as one record
    current_file = file_list[file_loop_index]
    getline xml_contents < current_file
    #printf "### INFO ### file contents from %s: \n %s\n", file_list[0], xml_contents
    close(file_list[file_loop_index])
    RS = RS_old

  # pull ID, known, and active chunks from file
    parse_file()   # this produces 'raw_id_chunks', 'raw_active_chunks', and 'raw_known_chunks', each with the contents between its respective <Stream [stuff]> ... </Stream> tags

  # find the ID # for the flag being analyzed
    get_flag_index()   # this produces 'flag_index' (the 'segment_def_id' value in the DQ XML files, used to identify the flag in the active and known segment tables)

  # find known segments for the flag
    get_known_segments()   # fills values in known_segments_start_times and known_segments_end_times arrays, updating accumulator 'known_segments_count' in the process (starts at 0; arrays start at 1)
 
  # find active segments for the flag
    get_active_segments()   # fills values in active_segments_start_times and active_segments_end_times arrays, updating accumulator 'active_segments_count' in the process (starts at 0; arrays start at 1)
  }    # for


  # coalesce known segments for the flag
    coalesce_known_segments()   # coalesces known_segments_start_times and known_segments_end_times arrays into coalesced_known_segments_start_times and coalesced_known_segments_end_times

  # coalesce active segments for the flag 
    coalesce_active_segments()   # coalesces active_segments_start_times and active_segments_end_times arrays into coalesced_active_segments_start_times and coalesced_active_segments_end_times


  if (verbose == 1)
  {
    printf "known_segments_count = %d \n", known_segments_count
    for (loop_index = 1; loop_index <= known_segments_count; loop_index++)
    {
      printf "known start, end times = [%d, %d] \n", known_segments_start_times[loop_index], known_segments_end_times[loop_index]
    }   # for
    printf "active_segments_count = %d \n", active_segments_count
    for (loop_index = 1; loop_index <= active_segments_count; loop_index++)
    {
      printf "active start, end times = [%d, %d] \n", active_segments_start_times[loop_index], active_segments_end_times[loop_index]
    }   # for
  }   # if
  printf "coalesced_known_segments_count = %d \n", coalesced_known_segments_count
  for (loop_index = 1; loop_index <= coalesced_known_segments_count; loop_index++)
  {
    printf "coalesced known start, end times = [%d, %d] \n", coalesced_known_segments_start_times[loop_index], coalesced_known_segments_end_times[loop_index]
  }   # for
  printf "coalesced_active_segments_count = %d \n", coalesced_active_segments_count
  for (loop_index = 1; loop_index <= coalesced_active_segments_count; loop_index++)
  {
    printf "coalesced active start, end times = [%d, %d] \n", coalesced_active_segments_start_times[loop_index], coalesced_active_segments_end_times[loop_index]
  }   # for

exit   ###here1




}   # main program

END {
# write output file
}   # END



### functions


function coalesce_active_segments()   # coalesces active_segments_start_times and active_segments_end_times arrays into coalesced_active_segments_start_times and coalesced_active_segments_end_times
{
  # process: 
  #  * read in the first pair of start and end times (current times)
  #  * read in the next pair of times (next times)
  #  * if 'current' end time and 'next' start time are the same, replace 'current' end time with 'next' end time; go back to 'read in the next pair of times' step
  #  * if 'current' end time and 'next' start time are different, write 'current' start and end times to the next start, end time in coalesced times array; make 'next' times new 'current' times; read next, etc.
  asort(active_segments_start_times, sorted_active_segments_start_times)   # times in first array are sorted and saved in second array
  asort(active_segments_end_times,   sorted_active_segments_end_times)     # same as above
  if (active_segments_count == 0) {return}
  for (loop_index = 1; loop_index <= active_segments_count; loop_index++)
  {
    if (loop_index == 1) {current_start_time = sorted_active_segments_start_times[loop_index]; current_end_time = sorted_active_segments_end_times[loop_index]; continue}
    next_start_time = sorted_active_segments_start_times[loop_index]
    next_end_time   = sorted_active_segments_end_times[loop_index]
    if (current_end_time == next_start_time) {current_end_time = next_end_time; continue}
    else
    {
      coalesced_active_segments_count++
      coalesced_active_segments_start_times[coalesced_active_segments_count] = current_start_time 
      coalesced_active_segments_end_times[coalesced_active_segments_count]   = current_end_time
      current_start_time = next_start_time
      current_end_time   = next_end_time
      continue
    }   # else
  }   # for
  # after the last line is processed, we have to save the last start-end pair (which might be a result of coalescence or might be the last 'next' pair read in), b/c they haven't been saved yet
  coalesced_active_segments_count++
  coalesced_active_segments_start_times[coalesced_active_segments_count] = current_start_time
  coalesced_active_segments_end_times[coalesced_active_segments_count]   = current_end_time
}   # coalesce_active_segments()


function coalesce_known_segments()   # coalesces known_segments_start_times and known_segments_end_times arrays into coalesced_known_segments_start_times and coalesced_known_segments_end_times
{
  # process: 
  #  * read in the first pair of start and end times (current times)
  #  * read in the next pair of times (next times)
  #  * if 'current' end time and 'next' start time are the same, replace 'current' end time with 'next' end time; go back to 'read in the next pair of times' step
  #  * if 'current' end time and 'next' start time are different, write 'current' start and end times to the next start, end time in coalesced times array; make 'next' times new 'current' times; read next, etc.
  asort(known_segments_start_times, sorted_known_segments_start_times)   # times in first array are sorted and saved in second array
  asort(known_segments_end_times,   sorted_known_segments_end_times)     # same as above
  if (known_segments_count == 0) {return}
  for (loop_index = 1; loop_index <= known_segments_count; loop_index++)
  {
    if (loop_index == 1) {current_start_time = sorted_known_segments_start_times[loop_index]; current_end_time = sorted_known_segments_end_times[loop_index]; continue}
    next_start_time = sorted_known_segments_start_times[loop_index]
    next_end_time   = sorted_known_segments_end_times[loop_index]
#if (verbose == 1) {printf "current start time = %d ; current end time = %d ; next start time = %d ; next end time = %d\n", current_start_time, current_end_time, next_start_time, next_end_time}
    if (current_end_time == next_start_time) {current_end_time = next_end_time; continue}
    else
    {
      coalesced_known_segments_count++
      coalesced_known_segments_start_times[coalesced_known_segments_count] = current_start_time 
      coalesced_known_segments_end_times[coalesced_known_segments_count]   = current_end_time
      current_start_time = next_start_time
      current_end_time   = next_end_time
      continue
    }   # else
  }   # for
  # after the last line is processed, we have to save the last start-end pair (which might be a result of coalescence or might be the last 'next' pair read in), b/c they haven't been saved yet
  coalesced_known_segments_count++
  coalesced_known_segments_start_times[coalesced_known_segments_count] = current_start_time
  coalesced_known_segments_end_times[coalesced_known_segments_count]   = current_end_time
}   # coalesce_known_segments()


function generate_file_list_lho_llo()
{
  if (ifo == "H") {cmd = "ls -1 /dqxml/H1/`ls -1 /dqxml/H1/ | tail -n 1` | tail -n 1"; cmd | getline newest_lhlo_file; close(cmd)}   # this line: creates a command; runs the command and pipes its output into getline > variable; closes the 'open file'
  if (ifo == "L") {cmd = "ls -1 /dqxml/L1/`ls -1 /dqxml/L1/ | tail -n 1` | tail -n 1"; cmd | getline newest_lhlo_file; close(cmd)}   # this line: creates a command; runs the command and pipes its output into getline > variable; closes the 'open file'
  # filenames look like this:  H-DQ_Segments-1243820960-16.xml
    if (length(newest_lhlo_file) == 31) 	 # if the filename isn't 31 characters long, something has changed, so we don't know where the file duration is in the name, and/or other stuff	will break very	soon
    {
      file_duration = substr(newest_lhlo_file, 26, 2)
      if (file_duration != 16)
      {
        printf "### WARNING #### The latest LHO/LLO file (%s) should be 16 seconds long, but the filename indicates that it is %d seconds long.\n", newest_lhlo_file, file_duration
        print  "            #### This duration is not tested, so the analysis will continue, but the results could be fatally flawed."
      }   # if
    }   # if
  temp_minor = gps_start_minor - gps_start_minor%16   # this gets the (minor) file number that includes the earliest GPS time
  if (verbose==1) {printf "temp_minor = %05d\n", temp_minor}
  file_index = 0   # this is the 0-based index for the array of filenames
  for (file_dir = gps_start_major; file_dir <= gps_end_major; file_dir++) {   # this will loop over as many dirs as needed, to get from the GPS start time to the GPS end time
    if (file_dir == gps_end_major) {temp_minor_end = gps_end_minor}   # if the current dir is the final dir, only go up to the minor number
    else {temp_minor_end = 99999}                                   # if the current dir is NOT the final dir, go all the way to the end of the dir
    while (temp_minor < 99999  && ((temp_minor - temp_minor_end) <= 16) ) {
      file_name = sprintf("%s%s-DQ_Segments-%d/%s-DQ_Segments-%d%05d-16.xml", xml_dir, ifo, file_dir, ifo, file_dir, temp_minor)
      file_test = getline _ < file_name
      close(file_name)
      if (file_test > 0)
      {
        file_list[file_index] = file_name
        if (verbose==1) {printf "temp_minor = %05d; file_list[%d] = %s\n", temp_minor, file_index, file_list[file_index]}
        file_index++
      }  # if
      else
      {
        if (verbose==1) {printf "temp_minor = %05d; no file found:  %s\n", temp_minor, file_name}
      }   # else
      temp_minor += 16   # step through the files until we get to the file past the GPS end time or the last file in the dir
    }   # while
    temp_minor = 0
  }   # for
  file_count = file_index   # note that it isn't "+ 1" b/c 'file_index' is the value for the *next* element into the filename array; if there are none at all, file_index stays at 0 and file_count is set to 0; add 1 to both for 1 file, etc.
}   # generate_file_list_lho_llo()


function generate_file_list_virgo()
{
# Virgo DQ XML files are similar to LHO/LLO in that they are produced every 16 seconds and contain 16 seconds of data, but they differ in that the first file in a new 'major' dir. is at 'minor' number 00002;
#   so the first 2 seconds of a new 'major' number are stored in the last file of the last 'major' dir; 
#   this makes predicting filenames slightly more difficult, but it makes predicting the first file much more difficult;
#   to get around that, we test the GPS start time; if it's within the first 2 seconds of a major number, we just decrement the start time by 2 seconds
#   to deal with both of these issues, we decrease the start time by 16 seconds, calculate the file names the same as LHO/LLO, but we add 2 seconds to the final time on each file, and we tack an extra file on the end
#   (at least, that's the plan; fingers crossed) (so time 1234599990 would be file 1234599984 for LLO/LHO; for VGO, we calculate LHO/LLO(1234599990 - 16) = LHO/LLO(1234511174) = LHO/LLO file 1234599968
#     -> VGO file = (1234599968 + 14) = 1234599982 (which is correct)
#   edge cases: 1234500001: LHO(1234500001-16) = LHO(1234499985) = 1234499984; VGO file = (1234499984 + 14) = 
# Nope.  This won't work, b/c Virgo doesn't have a fixed starting time.  Apparently they just start creating files at any arbitrary second and keep going from there.
#   To demonstrate, try this: 'for i in `ls -1 /dqxml/V1/`; do echo $i; ls /dqxml/V1/$i | wc --lines; ls /dqxml/V1/$i | head -n 1; done | less'; the first file in each dir is all over the place, incl. odd and even #s
# New plan:
#  * Find the newest Virgo DQXML filename ("ls -l /dqxml/V1/`ls -1 /dqxml/V1/ | tail -n 1` | tail -n 1") and extract the 'minor' number
#  * Calculate the modulus 16 of the Virgo minor number (= V_minor%16); this is the "Virgo offset"
#  * Subtract 16 from the GPS start time, then run the same algorithm as for LHO/LLO, to generate GPS times for the filenames
#  * Add the "Virgo offset" to every GPS time for a Virgo filename
#  * This will sometimes cause 1 extra file to be included, before the actual file that contains the GPS start time, but the extra time will be truncated after coalescence
#  * Do we need to add an extra file on the end?
#  * Note that if the "Virgo offset" changes during the time period being analyzed, this script may produce erroneous results, in that the predicted files will not be found until the new offset creates sufficient files

#print "### ERROR ### Function 'generate_file_list_virgo()' has not been completed yet"
print "### WARNING ### Function 'generate_file_list_virgo()' is still being tested; watch for any issues"
#here2

  cmd = "ls -1 /dqxml/V1/`ls -1 /dqxml/V1/ | tail -n 1` | tail -n 1"; cmd | getline newest_virgo_file; close(cmd)   # this line: creates a command; runs the command and pipes its output into getline > variable; closes the 'open file'
  # filenames look like this:  H-DQ_Segments-1243820960-16.xml
  if (length(newest_virgo_file) == 31)   # if the filename isn't 31 characters long, something has changed, so we don't know where the file duration is in the name, and/or other stuff will break very soon
  {
    file_duration = substr(newest_virgo_file, 26, 2)
    if (file_duration != 16)
    {
      printf "### WARNING #### The latest Virgo file (%s) should be 16 seconds long, but the filename indicates that it is %d seconds long.\n", newest_virgo_file, file_duration
      print  "            #### This duration is not tested, so the analysis will continue, but the results could be fatally flawed."
    }   # if
  }   # if
  #newest_virgo_file = system("ls -1 /dqxml/V1/`ls -1 /dqxml/V1/ | tail -n 1` | tail -n 1")   ### e.g., "V-DQ_Segments-1243563772-16.xml"
  newest_virgo_file_gps_time = substr(newest_virgo_file, 15, 10)
  virgo_offset = newest_virgo_file_gps_time%16
  if (verbose==1) {printf "newest virgo file = %s; file gps time = %d; virgo offset = %d\n", newest_virgo_file, newest_virgo_file_gps_time, virgo_offset}
  gps_start_major_modV = int((gps_start - 16)/100000); gps_start_minor_modV = (gps_start - 16) - (gps_start_major_modV)*100000
  temp_minor = gps_start_minor_modV - gps_start_minor_modV%16 + virgo_offset  # this gets the (minor) file number that includes the earliest GPS time
  if (verbose==1) {printf "temp_minor = %05d\n", temp_minor}
  file_index = 0   # this is the 0-based index for the array of filenames
  for (file_dir = gps_start_major; file_dir <= gps_end_major; file_dir++) {   # this will loop over as many dirs as needed, to get from the GPS start time to the GPS end time
    if (file_dir == gps_end_major) {temp_minor_end = gps_end_minor}   # if the current dir is the final dir, only go up to the minor number
    else {temp_minor_end = 99999}                                   # if the current dir is NOT the final dir, go all the way to the end of the dir
    while (temp_minor < 99999  && ((temp_minor - temp_minor_end) <= 16) ) {
      file_name = sprintf("%s%s-DQ_Segments-%d/%s-DQ_Segments-%d%05d-16.xml", xml_dir, ifo, file_dir, ifo, file_dir, temp_minor)
      if (verbose==1) {printf "testing for file %s ...\n", file_name}
      file_test = getline _ < file_name
      close(file_name)
      if (file_test > 0)
      {
        file_list[file_index] = file_name
        if (verbose==1) {printf "temp_minor = %05d; file_list[%d] = %s\n", temp_minor, file_index, file_list[file_index]}
        file_index++
      }  # if
      else
      {
        if (verbose==1) {printf "temp_minor = %05d; no file found:  %s\n", temp_minor, file_name}
      }   # else
      temp_minor += 16   # step through the files until we get to the file past the GPS end time or the last file in the dir
    }   # while
    temp_minor = 0 + virgo_offset
  }   # for
  file_count = file_index   # note that it isn't "+ 1" b/c 'file_index' is the value for the *next* element into the filename array; if there are none at all, file_index stays at 0 and file_count is set to 0; add 1 to both for 1 file, etc.


#  gps_start_major = int(gps_start/100000); gps_start_minor = gps_start - gps_start_major*100000
#  gps_end_major   = int(gps_end/100000);   gps_end_minor   = gps_end   - gps_end_major*100000
#exit
#    if (ifo == "V"  &&  temp_minor >= 16) {temp_minor = temp_minor - 4}   # b/c Virgo files start with minor number 00012
}   # generate_file_list_virgo()


function generate_file_list_geo()
{
# The old function wasn't working out, due to the trouble with the difference between (GPS time)%60 and (GPS minor time)%60 (at least, difference for 2/3 of the directories, depending on (GPS major time)%60)
# So here's the new plan:
#  * No fancy looping over directories
#  * Find the newest GEO DQXML filename ("ls -l /dqxml/G1/`ls -1 /dqxml/G1/ | tail -n 1` | tail -n 1") and extract the 'minor' number; note that we don't care about the "3 commas" issue for these files
#  * Calculate the modulus 60 of the GEO minor number (= G_minor%60); this is the "GEO offset"
#  * Start with the file that contains the start of the time period of interest
#  * Extract the major GPS time from the filename, to determine what dir the file is in, and use that to determine if the file exists
#  * Increment the GPS time and do the previous step over again, until the new file starts after the end of the time of interest
#  * Use the GEO offset in all of the above calculations.  That offset is currently 0, and will probably remain 0, but it will be calculated and used, just in case.
#  * That's it.  We have to extract the major GPS time from each potential filename, rather than having it determined beforehand and just looping over files in that dir, but this is simpler

#print "### ERROR ### Function 'generate_file_list_geo()' has not been completed yet"
print "### WARNING ### Function 'generate_file_list_geo()' is still being tested"
#here3

  cmd = "ls -1 /dqxml/G1/`ls -1 /dqxml/G1/ | tail -n 1` | tail -n 1"; cmd | getline newest_geo_file; close(cmd)   # this line: creates a command; runs the command and pipes its output into getline > variable; closes the 'open file'
  # example file: G-DQ_Segments-1243739880-60.xml
  if (length(newest_geo_file) == 31)   # if the filename isn't 31 characters long, something has changed, so we don't know where the file duration is in the name, and/or other stuff will break very soon
  {
    file_duration = substr(newest_geo_file, 26, 2)
    if (file_duration != 60)
    {
      printf "### WARNING #### The latest GEO file (%s) should be 60 seconds long, but the filename indicates that it is %d seconds long.\n", newest_geo_file, file_duration
      print  "            #### This duration is not tested, so the analysis will continue, but the results could be fatally flawed."
    }   # if
  }   # if
  newest_geo_file_gps_time = substr(newest_geo_file, 15, 10)
  geo_offset = newest_geo_file_gps_time%60   # this is (GPS time)%60, not (GPS major time)%60 or (GPS minor time)%60
  first_geo_file_time = gps_start - (gps_start - geo_offset)%60   ### I *think* this algorithm is right
  if (verbose==1) {printf "newest geo file = %s; file gps time = %d; geo offset = %d; first geo file time = %d\n", newest_geo_file, newest_geo_file_gps_time, geo_offset, first_geo_file_time}
  file_index = 0   # this is the 0-based index for the array of filenames
  for (file_gps_time = first_geo_file_time; file_gps_time <= gps_end; file_gps_time += 60) {   # this will loop over the GPS times, regardless of whether the GPS major time (and thus file dir) changes or not
    temp_major = substr(file_gps_time, 1, 5)
    temp_minor = substr(file_gps_time, 6, 5)
    file_name = sprintf("%s%s-DQ_Segments-%d/%s-DQ_Segments-%d-60.xml", xml_dir, ifo, temp_major, ifo, file_gps_time)   # note that file_gps_time should never be < 1000000000, so we don't need to pad %d
    file_test = getline _ < file_name
    close(file_name)
    if (file_test > 0)
    {
      file_list[file_index] = file_name
      if (verbose==1) {printf "file_list[%d] = %s\n", file_index, file_list[file_index]}
      file_index++
    }  # if
    else
    {
      if (verbose==1) {printf "no file found:  %s\n", file_name}
    }   # else
  }   # for
  file_count = file_index   # note that it isn't "+ 1" b/c 'file_index' is the value for the *next* element into the filename array; if there are none at all, file_index stays at 0 and file_count is set to 0; add 1 to both for 1 file, etc.
}   # generate_file_list_geo()


function generate_file_list_geo_original()
{
# The plan here is mostly the same as for Virgo, with tweaks for the fact that GEO files are 60 seconds long, not 16 seconds long
# The plan (based on the new Virgo plan):
#  * Find the newest GEO DQXML filename ("ls -l /dqxml/G1/`ls -1 /dqxml/G1/ | tail -n 1` | tail -n 1") and extract the 'minor' number; note that we don't care about the "3 commas" issue for these files
#  * Calculate the modulus 60 of the GEO minor number (= G_minor%60); this is the "GEO offset"
#  * Subtract 60 from the GPS start time, then run the same algorithm as for LHO/LLO, to generate GPS times for the filenames
#  * Add the "GEO offset" to every GPS time for a GEO filename
#  * This will sometimes cause 1 extra file to be included, before the actual file that contains the GPS start time, but the extra time will be truncated after coalescence
#  * Do we need to add an extra file on the end?
#  * Note that if the "GEO offset" changes during the time period being analyzed, this script may produce erroneous results, in that the predicted files will not be found until the new offset creates sufficient files
#print "### ERROR ### Function 'generate_file_list_geo()' has not been completed yet"
print "### WARNING ### Function 'generate_file_list_geo()' is still being tested"
  cmd = "ls -1 /dqxml/G1/`ls -1 /dqxml/G1/ | tail -n 1` | tail -n 1"; cmd | getline newest_geo_file; close(cmd)   # this line: creates a command; runs the command and pipes its output into getline > variable; closes the 'open file'
  # example file: G-DQ_Segments-1243739880-60.xml
  newest_geo_file_gps_time = substr(newest_geo_file, 15, 10)
#  geo_offset = newest_geo_file_gps_time%60
  if (verbose==1) {printf "newest geo file = %s; file gps time = %d; geo offset = %d\n", newest_geo_file, newest_geo_file_gps_time, geo_offset}
  gps_start_major_modG = int((gps_start - 60)/100000); gps_start_minor_modG = (gps_start - 60) - (gps_start_major_modG)*100000
  geo_offset = gps_start_minor_modG%60   ### unlike for LHO/LLO/Virgo, where 100000%16 = 0, for GEO, 100000%60 = 40 != 0; so (GPS time)%60 is not always = to (GPS minor time)%60; we have to do (GPS time)%60 to predict filenames
  temp_minor = gps_start_minor_modG - gps_start_minor_modG%60 + geo_offset  # this gets the (minor) file number that includes the earliest GPS time
#  temp_minor = gps_start_minor - gps_start_minor%60   # this gets the (minor) file number that includes the earliest GPS time
  if (verbose==1) {printf "temp_minor = %05d\n", temp_minor}
  file_index = 0   # this is the 0-based index for the array of filenames
  for (file_dir = gps_start_major; file_dir <= gps_end_major; file_dir++) {   # this will loop over as many dirs as needed, to get from the GPS start time to the GPS end time
    if (file_dir == gps_end_major) {temp_minor_end = gps_end_minor}   # if the current dir is the final dir, only go up to the minor number
    else {temp_minor_end = 99999}                                   # if the current dir is NOT the final dir, go all the way to the end of the dir
    while (temp_minor < 99999  && ((temp_minor - temp_minor_end) <= 60) ) {
      file_name = sprintf("%s%s-DQ_Segments-%d/%s-DQ_Segments-%d%05d-60.xml", xml_dir, ifo, file_dir, ifo, file_dir, temp_minor)
      file_test = getline _ < file_name
      close(file_name)
      if (file_test > 0)
      {
        file_list[file_index] = file_name
        if (verbose==1) {printf "temp_minor = %05d; file_list[%d] = %s\n", temp_minor, file_index, file_list[file_index]}
        file_index++
      }  # if
      else
      {
        if (verbose==1) {printf "temp_minor = %05d; no file found:  %s\n", temp_minor, file_name}
      }   # else
      temp_minor += 60   # step through the files until we get to the file past the GPS end time or the last file in the dir
    }   # while
    temp_minor = 0 + geo_offset
  }   # for
  file_count = file_index   # note that it isn't "+ 1" b/c 'file_index' is the value for the *next* element into the filename array; if there are none at all, file_index stays at 0 and file_count is set to 0; add 1 to both for 1 file, etc.
}   # generate_file_list_geo_original()


function get_active_segments()
{      # raw_active_chunks
  # "rows" (sets of 6 columns; not necessarily/probably one literal row in the file) probably look like this:
  #      "segment:segment_id:273",1239501743,1239501744,
  #      "segment_definer:segment_def_id:152","process:process_id:2"
  # process: split 'raw_active_chunks' by commas; loop over the array; for each time an element (N) contains :152" (using example above), add elements (N-3) and (N-2) to arrays of start and end times, respectively;
  raw_active_chunks_count = split(raw_active_chunks[2], raw_active_chunks_array, ",")
#  if (raw_active_chunks_count ... # don't think we can perform any validity checks on the output; could possibly be 0, if there are no active segments
  if (verbose == 1) {printf "raw_active_chunks_count = %d \n", raw_active_chunks_count}
  if (raw_active_chunks_count == 0) {return}
  flag_string="segment_def_id:"flag_index"\""   # have to have the final double quote, to avoid ambiguities like ":15" vs ":150"
  if (verbose == 1) {printf "active segments flag string = '%s' \n", flag_string}
  for (loop_val = 1; loop_val <= raw_active_chunks_count; loop_val++)
  {
#    printf "raw_active_chunks_array[%d] = %s \n", loop_val, raw_active_chunks_array[loop_val]
    match_count = match(raw_active_chunks_array[loop_val], flag_string)
    if (match_count > 0) {   # if true, this means that the string was found in the current array element
      active_segments_count++
      if (ifo == "G")
      {
      # ' 1243740060,0,"process:process_id:4","segment_definer:segment_def_id:55","segment:segment_id:9",1243740000,0, '
      active_segments_start_times[active_segments_count] = raw_active_chunks_array[(loop_val + 2)]
      active_segments_end_times[active_segments_count]   = raw_active_chunks_array[(loop_val - 3)]
      }   # if
      else
      {
      active_segments_start_times[active_segments_count] = raw_active_chunks_array[(loop_val - 2)]
      active_segments_end_times[active_segments_count]   = raw_active_chunks_array[(loop_val - 1)]
      }   # else
      if (verbose == 1) {printf "Found an active segment in file %s, %d to %d \n", current_file, active_segments_start_times[active_segments_count], active_segments_end_times[active_segments_count]}
#      if (verbose == 1) {printf "Found an active segment in file %s , chunk %d: %s \n", current_file, loop_val, raw_active_chunks_array[loop_val]}
    }   # if
  }   # for
}   # get_active_segments()


function get_flag_index()
{
  flag_split_count = split(raw_id_chunks[2], raw_id_array, ",")
  if (verbose == 1) {printf "flag_split_count = %d \n", flag_split_count}
  if (flag_split_count < 6) {printf "### ERROR ### There were only %d elements in row_id_chunks[2] (in function 'get_flag_index()'), which is too low, so something has gone wrong in file %s .  Exiting.\n", \
     flag_split_count, current_file; exit}   # this test is probably too weak, but it's not clear what a better lower-limt value is, so we'll go with it for now
  match_count = 0
  flag_string = "\""flag_name"\""   # we search for the string in quotes to avoid matching it in a description phrase (unless someone writes a phrase like that, which will then require this to be patched)
  if (verbose == 1) {print "get_flag_index flag_string = ", flag_string}
  for (temp_val = 1; temp_val <= flag_split_count; temp_val++)   # loop over the whole array, to find the string
  {
    if (match(raw_id_array[temp_val], flag_string) > 0) {   # this checks for the string (including enclosing quotes) somewhere in the array element, b/c some array elements have spaces, which kill an exact match
      # LHO: ' "process:process_id:2","segment_definer:segment_def_id:164","H1",      "DMT-ANALYSIS_READY",1, ' (newline removed)
      # LLO: ' "process:process_id:0","segment_definer:segment_def_id:144","L1",      "DMT-ANALYSIS_READY",1, ' (newline removed)
      # VGO: ' "process:process_id:0","segment_definer:segment_def_id:16","V1",      "DQ_META_STATUS_ITF",1,"Full ITF locked", ' (newline removed)
      # GEO: ' "","G1","GEO-UP","process:process_id:4","segment_definer:segment_def_id:60",1, '
      if (ifo == "G") 
      {
        flag_index_chunk = raw_id_array[(temp_val + 2)]   # the chunk with the ID # is two elements *later* in the array than the element with just the name (in quotes, plus spaces)
        match_count++
      }   # if
      else
      {
        flag_index_chunk = raw_id_array[(temp_val - 2)]   # the chunk with the ID # is two elements *earlier* in the array than the element with just the name (in quotes, plus spaces)
        match_count++
      }   # else
    }   # if
#  if (verbose == 1) {print match(raw_id_array[temp_val], flag_string), match_count, temp_val, raw_id_array[temp_val]}
  }   # for
  if (match_count > 1) {printf "### ERROR ### There were %d flags matches in file %s , but there should have been only 1.  The file is probably malformed.  Exiting.\n", match_count, current_file; exit}
#  if (match_count == 0) {printf "### ERROR ### There were no flags matches in file %s , but there should have been 1.  The file is probably malformed.  Exiting.\n", current_file; exit}
  ### if there are no matches, we probably want to respond that known and active are empty for this file
  if (match_count == 0) {flag_index = -1; return}
  if (verbose == 1) {printf "match count = %d \nmatch chunk = '%s'\n", match_count, flag_index_chunk}
  flag_index_chunk_split_count = split(flag_index_chunk, flag_index_chunk_split, /[":]/, seps)   # the string probably looks like this: "segment_definer:segment_def_id:150" (*with* quotes)
    # for some reason, the split command above splits it into *5* chunks, with chunks 1 and 5 being empty (""), before and after the first and last double quotes; chunk 4 has the actual number 
#  for (temp_val in flag_index_chunk_split) {printf "chunk %d: '%s'; sep: '%s'\n", temp_val, flag_index_chunk_split[temp_val], seps[temp_val]}
  if (flag_index_chunk_split_count < 3) {printf "### ERROR ### In function 'get_flag_index()', flag_index_chunk_split_count was %d, which was lower than the expected value of 3.\n", flag_index_chunk_split_count; \
     printf "          ### The file ( %s ) is probably malformed.  Exiting.\n", current_file; exit}
  flag_index = flag_index_chunk_split[4]
  if (verbose == 1) {printf "flag index = %d \n", flag_index}
}   # get_flag_index()


function get_known_segments()
{      # raw_known_chunks
  # "rows" (sets of 6 columns; not necessarily/probably one literal row in the file) probably look like this:
  #      "segment_summary:segment_sum_id:0",1239501728,1239501744,"",
  #      "segment_definer:segment_def_id:149","process:process_id:0",
  # process: split 'raw_known_chunks' by commas; loop over the array; for each time an element (N) contains :149" (using example above), add elements (N-3) and (N-2) to arrays of start and end times, respectively;
  raw_known_chunks_count = split(raw_known_chunks[2], raw_known_chunks_array, ",")
#  if (raw_known_chunks_count ... # don't think we can perform any validity checks on the output; could possibly be 0, if there are no known segments
  if (verbose == 1) {printf "raw_known_chunks_count = %d \n", raw_known_chunks_count}
  if (raw_known_chunks_count == 0) {return}
  flag_string="segment_def_id:"flag_index"\""
  if (verbose == 1) {printf "known segments flag string = '%s' \n", flag_string}
  for (loop_val = 1; loop_val <= raw_known_chunks_count; loop_val++)
  {
#    printf "raw_known_chunks_array[%d] = %s \n", loop_val, raw_known_chunks_array[loop_val]
    match_count = match(raw_known_chunks_array[loop_val], flag_string)
    if (match_count > 0) {   # if true, this means that the string was found in the current array element
      known_segments_count++
      if (ifo == "G")
      {
      # '  "G1_RDS_C01_L3",1243740060,0,"process:process_id:4","segment_definer:segment_def_id:52","segment_summary:segment_sum_id:52",1243740000,0, '
      known_segments_start_times[known_segments_count] = raw_known_chunks_array[(loop_val + 2)]
      known_segments_end_times[known_segments_count]   = raw_known_chunks_array[(loop_val - 3)]
      }   # if
      else
      {
      known_segments_start_times[known_segments_count] = raw_known_chunks_array[(loop_val - 3)]
      known_segments_end_times[known_segments_count]   = raw_known_chunks_array[(loop_val - 2)]
      }   # else
      if (verbose == 1) {printf "Found a known segment in file %s, %d to %d \n", current_file, known_segments_start_times[known_segments_count], known_segments_end_times[known_segments_count]}
    }   # if
  }   # for
}   # get_known_segments()


function parse_file()
{
  # tags are the same in H, L, and V; different in G
#  id_start =     "<Table Name=\"segment_definergroup:segment_definer:table\">"   # this is the opening tag for the 'id' section
#  known_start =  "<Table Name=\"segment_summarygroup:segment_summary:table\">"   # this is the opening tag for the 'known' section
#  active_start = "<Table Name=\"segmentgroup:segment:table\">"                   # this is the opening tag for the 'active' section
  id_start     = "<Stream Name=\"segment_definergroup:segment_definer:table\" Type=\"Local\" Delimiter=\",\">"   # this is the opening tag for the 'id' section
  known_start  = "<Stream Name=\"segment_summarygroup:segment_summary:table\" Type=\"Local\" Delimiter=\",\">"   # this is the opening tag for the 'known' section
  active_start = "<Stream Name=\"segmentgroup:segment:table\" Type=\"Local\" Delimiter=\",\">"                   # this is the opening tag for the 'active' section
  expected_sep_count = 5
  if (ifo == "G") {
#    id_start=    "<Table Name=\"segment_definer:table\">"
#    known_start= "<Table Name=\"segment_summary:table\">"
#    active_start="<Table Name=\"segment:table\">"
    id_start     = "<Stream Delimiter=\",\" Name=\"segment_definer:table\" Type=\"Local\">"
    known_start  = "<Stream Delimiter=\",\" Name=\"segment_summary:table\" Type=\"Local\">"
    active_start = "<Stream Delimiter=\",\" Name=\"segment:table\" Type=\"Local\">"
  expected_sep_count = 6
  }   # if
  stream_end = "</Stream>"
  if (verbose == 1) {printf "id_start = %s \nknown_start = %s \nactive_start = %s \nstream_end = %s \n", id_start, known_start, active_start, stream_end }

# the following doesn't work - it only matches "Stream>", b/c awk uses POSIX regexp's
#  sep_count = split(xml_contents, xml_chunks, "<*Stream*>", seps)
# tried these out, as well; "no" = didn't match anything; "yes" = matched only "</Stream>" (5 chunks); "?yes?no?" = matched numerous things, besides "</Stream>", some for unclear reasons
#no: sep_count = split(xml_contents, xml_chunks, "^</Stream{0,60}>&", seps)   #this means that the separator string must start with a "<"; then a "/" after the "<" is optional; then the word "Stream";
      # then from 0 to 60 characters; then it must end with ">"
#no:  sep_count = split(xml_contents, xml_chunks, "</Stream>", seps)   #this means that the separator string must start with a "<"; then a "/" after the "<" is optional; then the word "Stream";
#yes:  sep_count = split(xml_contents, xml_chunks, /<\/Stream>/, seps)   #this means that the separator string must start with a "<"; then a "/" after the "<" is optional; then the word "Stream";
#no:  sep_count = split(xml_contents, xml_chunks, /^<\/Stream>$/, seps)   #this means that the separator string must start with a "<"; then a "/" after the "<" is optional; then the word "Stream";
#yes:  sep_count = split(xml_contents, xml_chunks, /<\/?Stream>/, seps)   #this means that the separator string must start with a "<"; then a "/" after the "<" is optional; then the word "Stream";
#no:  sep_count = split(xml_contents, xml_chunks, /<\/?Stream{0,60}>/, seps)   #this means that the separator string must start with a "<"; then a "/" after the "<" is optional; then the word "Stream";
#?yes?no?:  sep_count = split(xml_contents, xml_chunks, /<\/?Stream.{0,60}>/, seps)   #this means that the separator string must start with a "<"; then a "/" after the "<" is optional; then the word "Stream";
# gave up on using regep's after that, b/c it doesn't make sense to me
#    for (print_var in seps) {print seps[print_var]}
#    }   # if
  sep_count = split(xml_contents, xml_chunks, "</Stream>")   # splits xml_contents into (sep_count) chunks, separated by "</Stream>", stored in array xml_chunks
     # the chunks for H, L, V are: pre-id_chunk, id_chunk, active_chunk, known_chunk, post-known_chunk; we only care about the middle 3 (xml_chunks[2-4])
     # pre-id_chunk includes: Table Name = Stream Name = "processgroup:process:table"
     # id_chunk includes: Table Name = Stream Name = "segment_definergroup:segment_definer:table"
     # active_chunk includes: Table Name = Stream Name = "segmentgroup:segment:table"
     # known_chunk includes: Table Name = Stream Name = "segment_summarygroup:segment_summary:table"
     # post-known_chunk is just some closing tags
     # the chunks for G are: pre_param_chunk, param_chunk, id_chunk, known_chunk, active_chunk, post-active_chunk; we only care about the middle 3 (xml_chunks[3-5]); also, note the swapped known/active vs HLV
     # pre-param_chunk includes: Table Name = Stream Name = "process:table"
     # param_chunk includes: Table Name = Stream Name = "process_params:table"
     # id_chunk includes: Table Name = Stream Name = "segment_definer:table"
     # known_chunk includes: Table Name = Stream Name = "segment_summary:table"
     # active_chunk includes: Table Name = Stream Name = "segment:table"
     # post-active_chunk is just some closing tags
  if (sep_count != expected_sep_count) {printf "### ERROR ### XML file ( %s ) should have %d \"</Stream>\" tags, but it has %d of them.  Exiting.\n", current_file, (expected_sep_count-1), (sep_count-1); exit}
  if (ifo == "G")
  {
    id_count     = split(xml_chunks[(expected_sep_count-3)], raw_id_chunks,     id_start)     # should produce only 2 chunks; there is stuff between one </Stream> tag and the next <Stream [stuff]> tag,
                                                                                              #    so we only care about raw_id_chunks[2]
    known_count  = split(xml_chunks[(expected_sep_count-2)], raw_known_chunks,  known_start)  # same as above; note that the chunk is in a different order from HLV
    active_count = split(xml_chunks[(expected_sep_count-1)], raw_active_chunks, active_start) # same as above; note that the chunk is in a different order from HLV
  }   # if
  else
  {
    id_count     = split(xml_chunks[(expected_sep_count-3)], raw_id_chunks,     id_start)     # should produce only 2 chunks; there is stuff between one </Stream> tag and the next <Stream [stuff]> tag,
                                                                                              #    so we only care about raw_id_chunks[2]
    active_count = split(xml_chunks[(expected_sep_count-2)], raw_active_chunks, active_start) # same as above
    known_count  = split(xml_chunks[(expected_sep_count-1)], raw_known_chunks,  known_start)  # same as above
  }   # else
  if (verbose == 1) {printf "sep_count string = %d \nid_count string = %d \nactive_count string = %d \nknown_count = %d \n", sep_count, id_count, active_count, known_count}
  if (id_count     != 2) {printf "### ERROR ### Processing XML file ( %s ) should produce id_count = 2, but it is %d.  This implies the XML file is malformed.  Exiting.\n", current_file, id_count; exit}
  if (active_count != 2) {printf "### ERROR ### Processing XML file ( %s ) should produce active_count = 2, but it is %d.  This implies the XML file is malformed.  Exiting.\n", current_file, active_count; exit}
  if (known_count  != 2) {printf "### ERROR ### Processing XML file ( %s ) should produce known_count = 2, but it is %d.  This implies the XML file is malformed.  Exiting.\n", current_file, known_count; exit}
#  if (verbose == 1) {print "raw_id_chunks[2] = ", raw_id_chunks[2]; print "raw_active_chunks[2] = ", raw_active_chunks[2]; print "raw_known_chunks[2] = ", raw_known_chunks[2]}
}   # parse_file()



### reference
# /dqxml/L1/L-DQ_Segments-12395/L-DQ_Segments-1239501744-16.xml

# string functions - https://www.gnu.org/software/gawk/manual/html_node/String-Functions.html
# asort(source [, dest [, how ] ])   # sort by values, then assign new (integer) indices, indicating the new ordering
# asorti(source [, dest [, how ] ])
# split(string, array [, fieldsep [, seps ] ])
# match(string, regexp [, array])

