#!/bin/sh
#
### BEGIN INIT INFO
# Provides:          dqxml_push_to_ifocache 
# Required-Start:    $remote_fs $network
# Required-Stop:     $remote_fs $network
# Default-Start:     
# Default-Stop:      0 1 2 3 4 5 6
# chkconfig: 2345 96 04
# Short-Description: Start dqxml_push_to_ifocache.
# Description:       Start the dqxml_push_to_ifocache
### END INIT INFO

# Author: Ryan Fisher <ryan.fisher@ligo.org>
# Author: Dan Kozak <dkozak@ligo.caltech.edu>

# Source function library.
. /etc/rc.d/init.d/functions

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="dqxml_push_to_ifocache"
NAME=dqxml_push_to_ifocache
# USER=nobody
# prog="/root/bin/dqxml_push_to_ifocache"
# OPTIONS="-i -l 1 -sv"
#DAEMON=/usr/sbin/$NAME
DAEMON=/root/bin/$NAME
PIDFILE="/var/run/$NAME/$NAME.pid"
# DAEMON_ARGS="-f --daemon --user=$USER --pid-file=$PIDFILE"
# quiet version: DAEMON_ARGS="-i -l 1"
DAEMON_ARGS="-i -l 2 &"
#DAEMON_ARGS="-i -l 1 -sv"
SCRIPTNAME=/etc/init.d/$NAME

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

#
# Function that starts the daemon/service
#
do_start()
{
    echo -n $"Starting $NAME: "
    if [ -f $PIDFILE ]; then
            read kpid < $PIDFILE
            if checkpid $kpid 2>&1; then
                    echo "process already running"
                            return 1;
                    else
                            echo "lock file found but no process running for pid $kpid, continuing"
            fi
    fi

    daemon --pidfile $PIDFILE $DAEMON $DAEMON_ARGS

    RETVAL=$?
    echo
        [ $RETVAL -eq 0 ] && rm -f $PIDFILE
    return $RETVAL
}

#
# Function that stops the daemon/service
#
do_stop()
{
    echo -n $"Stopping $NAME: "
    killproc $DAEMON

    RETVAL=$?
    echo
        [ $RETVAL -eq 0 ] && rm -f $PIDFILE
    return $RETVAL

}

restart () 
{
    stop
    sleep 5
    start
}

case "$1" in
  start)
    do_start
    ;;
  stop)
    do_stop
    ;;
  status)
    status -p $PIDFILE $NAME
    ;;
  restart)
    do_stop
    do_start
    ;;
  *)
    echo "Usage: $SCRIPTNAME {start|stop|status|restart}" >&2
    exit 3
    ;;
esac

exit $?
