#!/bin/bash
# This is a program to compare a prediction of the files that a DQXML segment publisher should publish from to the files that the publisher itself says it should publish from. (reword that)
# Run with something like this:  ./verify_file_list_monitor.sh  [IFO]  [state_file_dir]  [dqxml_file_dir]  [verbose_flag]
# Run with something like this:  ./verify_file_list_monitor.sh  H  /root/bin/state_files  /dqxml  1
# The program predicts what files will contain segments to be published, saves that list to a file, then runs the mock-publisher to get a list of files that it says will contain segments to be published and saves that list,
#   then again predicts what files will contain segments to be published (to deal with the race condition that a new file might show up during this process), and saves that list to a file.  If either the first or second
#   script-generated file list matches the mock-publisher-generated file list, the test passes; if neither matches the mock-publisher-generated file list, the test fails.  An output file is then saved, for Nagios.
# Inputs:
#   * IFO (H, L, V, or G)
#   * state file location (typically, /root/bin/state_files/${IFO}-DQ_Segments_current.xml; note that /root/bin/state_files is a link to /var/log/publishing/state/)
#   * DQXML file location (typically, /dqxml/${IFO}1/, though G1 files are in /dqxml/G1_test/)
#   * verbose level (0 = only print final output file; 1 = print messages and values along the way
# Outputs:
#   * /var/www/nagios/verify_publisher_list_${IFO}.json
# To do:
#   * increase number of possible FAIL messages, with useful info in each (compare number of files in predicted list vs. mock-publisher list; ??)


verbose=0   # default value; will be changed if command-line arg. #4 = "1"
monitor_dir=/root/bin/verify_file_list_monitor
nagios_dir=/var/www/nagios
gps_current_time=$((`date +%s` - 315964782))
current_time=$(date +%Y.%m.%d-%H.%M.%S)
save_to_monitor_file=1
monitor_file=$monitor_dir/monitor_run_results.txt


# set up and check command-line arguments
if [ $# -lt 3 ]; then echo "### ERROR ### There should be at least 3 command-line arguments, but there are only $#.  Please fix this.  Exiting."; 
  echo "          ### Run like this: './verify_file_list_monitor.sh  [IFO]  [state_file_dir]  [dqxml_file_dir]  [verbose_flag]";
  echo "          ### Run like this: './verify_file_list_monitor.sh  H  /root/bin/state_files  /dqxml  1"; exit; fi
ifo=$1
state_file_dir=$2
state_file=${state_file_dir}/${ifo}-DQ_Segments_current.xml
if [ ! -e $state_file ]; then echo "### ERROR ### The expected state file ('$state_file') could not be found.  Please fix this.  Exiting."; exit; fi
dqxml_file_dir=$3/${ifo}1
if [ $ifo == "G" ]; then dqxml_file_dir=$3/${ifo}1_test; fi
if [ ! -d $dqxml_file_dir ]; then echo "### ERROR ### The expected DQXML file dir ('$dqxml_file_dir') could not be found.  Please fix this.  Exiting."; exit; fi
if [ $# -gt 3 ]; then if [ "$4" -eq 1 ]; then verbose=$4; fi; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Run started:  $(date); IFO = $ifo; state file = $state_file; DQXML file dir = $dqxml_file_dir"; fi
first_file=/tmp/verify_file_list_${ifo}1_first_list.txt
second_file=/tmp/verify_file_list_${ifo}1_second_list.txt
publisher_file=/tmp/verify_file_list_${ifo}1_publisher_list.txt
nagios_file=$nagios_dir/verify_publisher_file_list_${ifo}1.json
log_file_basename=/tmp/verify_file_list_monitor_${ifo}1_log   # we leave off the ".txt" so that we can append something to make a new filename


# generate the first to-be-published list
if [ $verbose -eq 1 ]; then echo "### INFO ### Generating first to-be-published list"; fi
if [ -e $first_file ]; then rm $first_file; fi; touch $first_file   # clear out the file for this run
last_pub_segment_line=`grep "segment_definer:segment_def_id:1\",\"segment:segment_id" $state_file | tail -n 1`
  # output should look like this: "1241413568,0,"process:process_id:0","segment_definer:segment_def_id:1","segment:segment_id:359",1241221696,0" (no leading/trailing double quotes); note that first GPS time > second GPS time
  # note that the output actually starts with 3 tabs, which are often not printed, even though they're there, which is really annoying; so we have to add 3 to the subsequent string extraction
#last_pub_time=${last_pub_segment_line:0:10}   # Bash uses 0-based indices; this gets the 10-digit GPS time
last_pub_time=${last_pub_segment_line:3:10}   # Bash uses 0-based indices; this gets the 10-digit GPS time
if [ $verbose -eq 1 ]; then echo "### INFO ### In first to-be-published list, end of last segment = $last_pub_time"; fi
last_pub_time_major=${last_pub_time:0:5}   # this is the 1st 5 digits, used for dir names
# we are going to make the *assumption* that published segments always end at the end of a DQXML file, so we can compare the time of the end of the last published segment to the time at the start of new files (i.e., part of filename)
# this might not always be true, and will likely be violated by Virgo from time to time (since their file start times aren't on a strictly fixed schedule; they're every 16 seconds from whatever time a new contiguous sequence starts at)
# if it's a problem, we'll modify this next section, to check if the end of the last published segment might be in the range of times included in a file (which is non-trivially more complicated to do)
#for filename in `ls -1 $dqxml_file_dir/${ifo}-DQ_Segments-$last_pub_time_major/${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
cd $dqxml_file_dir/${ifo}-DQ_Segments-$last_pub_time_major
for filename in `ls -1 ${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
do
  filename_gps_time=${filename:14:10}
  if [ "$filename_gps_time" -ge "$last_pub_time" ]   # testing to produce a fail by changing this from "-ge" to "-gt"
  then
    echo $filename >> $first_file
    if [ $verbose -eq 1 ]; then echo "### INFO ### In first to-be-published list, unpublished filename saved:  $filename"; fi
  fi
done
next_pub_time_major=$((last_pub_time_major + 1))   # this accounts for the times when files have started being saved to the next dir, as well; it won't handle if unpublished files are in more than 2 dirs (very unlikely)
if [ -d $dqxml_file_dir/${ifo}-DQ_Segments-$next_pub_time_major ]
then
  cd $dqxml_file_dir/${ifo}-DQ_Segments-$next_pub_time_major
#  for filename in `ls -1 $dqxml_file_dir/${ifo}-DQ_Segments-$next_pub_time_major/${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
  for filename in `ls -1 ${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
  do
    echo "### INFO ### Filename being analyzed:  ", $filename
    filename_gps_time=${filename:14:10}
    if [ "$filename_gps_time" -ge "$last_pub_time" ]
    then 
      echo $filename >> $first_file
      if [ $verbose -eq 1 ]; then echo "### INFO ### In first to-be-published list, unpublished filename saved:  $filename"; fi
    fi
  done
fi


# run the mock-publisher, to generate the publisher's to-be-published list
if [ $verbose -eq 1 ]; then echo "### INFO ### Generating mock-publisher to-be-published list"; fi
if [ -e $publisher_file ]; then rm $publisher_file; fi; touch $publisher_file   # clear out the file for this run
### following 3 lines commented out for Python 3 fix (P3 fix)
#DIR="/root/bin/dqsegdb_current_code/dqsegdb/"
#export PYTHONPATH=$PYTHONPATH:$DIR
#export PATH=$PATH:$DIR/bin/
$monitor_dir/check_pending_files_for_monitor  --state-file=$state_file  --input-directory=$dqxml_file_dir  >>  $publisher_file
if [ $verbose -eq 1 ]; then cat $publisher_file; fi


# generate the second to-be-published list
# we generate a new to-be-published list, exactly like the first one, in case a new file was saved between generation of the first list and generation of the mock-publisher's list
if [ $verbose -eq 1 ]; then echo "### INFO ### Generating second to-be-published list"; fi
if [ -e $second_file ]; then rm $second_file; fi; touch $second_file   # clear out the file for this run
last_pub_segment_line=`grep "segment_definer:segment_def_id:1\",\"segment:segment_id" $state_file | tail -n 1`
  # output should look like this: "1241413568,0,"process:process_id:0","segment_definer:segment_def_id:1","segment:segment_id:359",1241221696,0" (no leading/trailing double quotes); note that first GPS time > second GPS time
  # note that the output actually starts with 3 tabs, which are often not printed, even though they're there, which is really annoying; so we have to add 3 to the subsequent string extraction
#last_pub_time=${last_pub_segment_line:0:10}   # Bash uses 0-based indices; this gets the 10-digit GPS time
last_pub_time=${last_pub_segment_line:3:10}   # Bash uses 0-based indices; this gets the 10-digit GPS time
if [ $verbose -eq 1 ]; then echo "### INFO ### In second to-be-published list, end of last segment = $last_pub_time"; fi
last_pub_time_major=${last_pub_time:0:5}   # this is the 1st 5 digits, used for dir names
# we are going to make the *assumption* that published segments always end at the end of a DQXML file, so we can compare the time of the end of the last published segment to the time at the start of new files (i.e., part of filename)
# this might not always be true, and will likely be violated by Virgo from time to time (since their file start times aren't on a strictly fixed schedule; they're every 16 seconds from whatever time a new contiguous sequence starts at)
# if it's a problem, we'll modify this next section, to check if the end of the last published segment might be in the range of times included in a file (which is non-trivially more complicated to do)
#for filename in `ls -1 $dqxml_file_dir/${ifo}-DQ_Segments-$last_pub_time_major/${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
cd $dqxml_file_dir/${ifo}-DQ_Segments-$last_pub_time_major
for filename in `ls -1 ${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
do
  filename_gps_time=${filename:14:10}
  if [ "$filename_gps_time" -ge "$last_pub_time" ]   # testing to produce a fail by changing this from "-ge" to	"-gt"
  then
    echo $filename >> $second_file
    if [ $verbose -eq 1 ]; then echo "### INFO ### In second to-be-published list, unpublished filename saved:  $filename"; fi
  fi
done
next_pub_time_major=$((last_pub_time_major + 1))   # this accounts for the times when files have started being saved to the next dir, as well; it won't handle if unpublished files are in more than 2 dirs (very unlikely)
if [ -d $dqxml_file_dir/${ifo}-DQ_Segments-$next_pub_time_major ]
then
  cd $dqxml_file_dir/${ifo}-DQ_Segments-$next_pub_time_major
#  for filename in `ls -1 $dqxml_file_dir/${ifo}-DQ_Segments-$next_pub_time_major/${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
  for filename in `ls -1 ${ifo}-DQ_Segments-*.xml`   # should produce only filenames (without dir name prepended); should look like this:  H-DQ_Segments-1241416176-16.xml
  do
    filename_gps_time=${filename:14:10}
    if [ "$filename_gps_time" -ge "$last_pub_time" ]
    then 
      echo $filename >> $second_file
      if [ $verbose -eq 1 ]; then echo "### INFO ### In second to-be-published list, unpublished filename saved:  $filename"; fi
    fi
  done
fi


# compare the lists and determine pass/fail
if [ $verbose -eq 1 ]; then echo "### INFO ### Comparing to-be-published lists"; fi
diff $first_file $publisher_file &> /dev/null
if [ $? -eq 0 ]
then
  test_result="pass_first"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Result: PASS; first list and mock-publisher list match"; fi
else
  diff $second_file $publisher_file &> /dev/null
  if [ $? -eq 0 ]
  then
    test_result="pass_second"
    if [ $verbose -eq 1 ]; then echo "### INFO ### Result: PASS; second list and mock-publisher list match"; fi
  else
    test_result="fail"
    if [ $verbose -eq 1 ]; then echo "### INFO ### Result: FAIL; neither first list nor second list match mock-publisher list"; fi
  fi
fi


# write the output file
if [ $verbose -eq 1 ]; then echo "### INFO ### Saving output file(s)"; fi
# Note: for num_status: pass = 0; undefined = 1; fail = 2; unknown = 3
if [ $test_result == "pass_first" ]  ||  [ $test_result == "pass_second" ]
then
  status_message="PASS: Publisher-discovered file list for ${ifo}1 is same as expected file list at $gps_current_time (`wc -l $publisher_file | awk '{print $1}'` files to be published)."
  num_status=0
else
  status_message="FAIL: Publisher-discovered file list for ${ifo}1 differs from expected file list at $gps_current_time\
  (files to be published: first test: `wc -l $first_file | awk '{print $1}'`; mock-publisher test: `wc -l $publisher_file | awk '{print $1}'`; second test: `wc -l $second_file | awk '{print $1}'`."
  num_status=2
fi
unknown_message="UNKNOWN: There are no ${ifo}1 results available after the time period $gps_current_time - $(($gps_current_time + 600))."
# sample: "{"created_gps": 1241420959, "status_intervals": [{"start_sec": 0, "txt_status": "PASS: LHO published segments in the DB and segments in DQ XML files match for the specified time period (1241420659 - 1241420959).", \
# "num_status": 0}, {"start_sec": 600, "txt_status": "UNKNOWN: There are no LHO results available after the time period 1241420659 - 1241420959.", "num_status": 3}]}"
output_file_contents="{\"created_gps\": $gps_current_time, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"$status_message\", \"num_status\": $num_status}, \
{\"start_sec\": 600, \"txt_status\": \"$unknown_message\", \"num_status\": 3}]}"
echo $output_file_contents > $nagios_file

if [ $save_to_monitor_file -eq 1 ]
then
  first_second_msg=""
  if [ $test_result == "pass_first" ];  then first_second_msg=" (first comparison)";  fi
  if [ $test_result == "pass_second" ]; then first_second_msg=" (second comparison)"; fi
  echo "$current_time  $gps_current_time  ${ifo}1  $num_status  $status_message $first_second_msg" >> $monitor_file
fi
if [ $verbose -eq 1 ]; then echo "### INFO ### JSON output message:"; echo $output_file_contents; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Run ended:  $(date)"; fi

# if something went wrong, we want some info, to check into it, so let's save a copy of the log file
#if [ $verbose -eq 1 ]  &&  [ "$num_status" -ne 0 ]
if [ "$num_status" -ne 0 ]   # we don't want to only do this if in verbose mode; if there's an error, we want to save that, either way
then
  cp ${log_file_basename}.txt  ${log_file_basename}_${gps_current_time}.txt
fi
exit

# Reference
# https://www.tldp.org/LDP/abs/html/string-manipulation.html
# Substring extraction: output=${string:position:length}   # position is 0-based

