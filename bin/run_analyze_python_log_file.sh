#!/bin/bash
# A simple script to run the python server log file analyzer, intended to be run every morning, to analyze the previous day's log file.
# Started by Robert Bruntz on 2020.05.31
# Usage: ' (script name)  (run date, as yyyy-mm-dd (optional))  (output log file (optional))'
# Example: '/root/bin/run_analyze_python_log_file.sh  2020-05-31'
# Example (for use in crontab): ' /root/bin/run_analyze_python_log_file.sh  $(date +\%Y-\%m-\%d --date="1 day ago")  /backup/segdb/segments/monitor/python_log_file_analysis.txt '

run_date=`date +%Y-%m-%d --date="1 day ago"`
output_log_file=""
if [ $# -gt 0 ]; then run_date=$1; fi
if [ $# -gt 1 ]; then output_log_file=$2; fi
#echo "### INFO ### run_date = $run_date; output_log_file = $output_log_file"

out_msg=`/root/bin/analyze_python_log_file.sh /opt/dqsegdb/python_server/logs/$run_date.log "$run_date" summary`
if [ "$output_log_file" != "" ]; then
  echo $run_date: $out_msg >> $output_log_file
else
  echo $run_date: $out_msg
fi

exit 0
