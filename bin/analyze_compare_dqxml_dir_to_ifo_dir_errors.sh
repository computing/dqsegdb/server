#!/bin/bash
# This is a script to analyze the errors saved by the monitor that compares DQXML dirs to IFO dirs.
# Started by Robert Bruntz on 2022.10.24.
usage1="Usage: ' (script name)  [metric start day]  [metric end day] '; both metric days are optional; if neither given, current metric day is used"
usage2="Example: ' /root/bin/analyze_compare_dqxml_dir_to_ifo_dir_errors.sh ' (analyze today's metric day) "
usage3="Example: ' /root/bin/analyze_compare_dqxml_dir_to_ifo_dir_errors.sh  13500 ' (analyze metric day 13500)"
usage4="Example: ' /root/bin/analyze_compare_dqxml_dir_to_ifo_dir_errors.sh  13500  13510 ' (analyze all metric days between 13500-13510, inclusive)"
# to do:
#   * check input, to make sure that input days are metric days; maybe check for valid range
#   * check that input start day is before end day
#   * sort out how to get "-" to set that day to today, for any combination of explicit days and -'s for start and end days


echo "$(date)  - GPS time: ~$((`date +%s` - 315964782))"
today=$((`date +%s` - 315964782))
mday_today=`echo $today | cut -c1-5`
mday_start=$mday_today
mday_end=$mday_today
log_dir=/root/bin/monitor_files_2

if [ $# -eq 0 ]; then echo -e "$usage1\n$usage2\n$usage3\n$usage4"; fi
if [ $# -gt 0 ] && [ "$1" != "-" ]; then mday_start=$1; mday_end=$1; fi
if [ $# -gt 1 ] && [ "$2" != "-" ]; then mday_end=$2; fi

echo "Run values: mday_start = $mday_start ; mday_end = $mday_end ; log dir = $log_dir"

# functions - search for all error lines with in a given metric day (e.g., 13500) for a given IFO (H,L,G,V), or for just a specific one, or for all except for specific ones
function find_all_ifo_day {
  ifo=$1
  mday=$2
  grep "which indicates an error occurred" ${log_dir}/compare_dqxml_dir_to_ifo_dir_${ifo}_${mday}?????_log.txt 2> /dev/null | wc --lines
}

function find_10_ifo_day {
  ifo=$1
  mday=$2
  grep -e "which indicates an error occurred" ${log_dir}/compare_dqxml_dir_to_ifo_dir_${ifo}_${mday}?????_log.txt 2> /dev/null | grep -e "Return code from rsync command is 10" | wc --lines
}

function find_23_ifo_day {
  ifo=$1
  mday=$2
  grep -e "which indicates an error occurred" ${log_dir}/compare_dqxml_dir_to_ifo_dir_${ifo}_${mday}?????_log.txt 2> /dev/null | grep -e "Return code from rsync command is 23" | wc --lines
}

function find_30_ifo_day {
  ifo=$1
  mday=$2
  grep -e "which indicates an error occurred" ${log_dir}/compare_dqxml_dir_to_ifo_dir_${ifo}_${mday}?????_log.txt 2> /dev/null | grep -e "Return code from rsync command is 30" | wc --lines
}

function find_non_excluded_ifo_day {
  ifo=$1
  mday=$2
  grep -e "which indicates an error occurred" ${log_dir}/compare_dqxml_dir_to_ifo_dir_${ifo}_${mday}?????_log.txt 2> /dev/null | grep -v "Return code from rsync command is [10,23,30]" | wc --lines
}


echo "Error code reference:"
echo "Code 10 = failed to connect to server"
echo "Code 23 = too many open files in system"
echo "Code 30 = timeout in data send/receive"

for mday in $(eval echo "{$mday_start..$mday_end}")
do
  echo "-"
  echo "metric day $mday (`ltconvert ${mday}00000`)"
  lho_all=`find_all_ifo_day H $mday`
  llo_all=`find_all_ifo_day L $mday`
  vgo_all=`find_all_ifo_day V $mday`
  geo_all=`find_all_ifo_day G $mday`
  lho_10=`find_10_ifo_day H $mday`
  llo_10=`find_10_ifo_day L $mday`
  vgo_10=`find_10_ifo_day V $mday`
  geo_10=`find_10_ifo_day G $mday`
  lho_23=`find_23_ifo_day H $mday`
  llo_23=`find_23_ifo_day L $mday`
  vgo_23=`find_23_ifo_day V $mday`
  geo_23=`find_23_ifo_day G $mday`
  lho_30=`find_30_ifo_day H $mday`
  llo_30=`find_30_ifo_day L $mday`
  vgo_30=`find_30_ifo_day V $mday`
  geo_30=`find_30_ifo_day G $mday`
  lho_non_excluded=`find_non_excluded_ifo_day H $mday`
  llo_non_excluded=`find_non_excluded_ifo_day L $mday`
  vgo_non_excluded=`find_non_excluded_ifo_day V $mday`
  geo_non_excluded=`find_non_excluded_ifo_day G $mday`
  #echo "LHO:  all: $lho_all   23: $lho_23   non-23: $lho_non_23"
  #echo "LLO:  all: $llo_all   23: $llo_23   non-23: $llo_non_23"
  #echo "VGO:  all: $vgo_all   23: $vgo_23   non-23: $vgo_non_23"
  #echo "GEO:  all: $geo_all   23: $geo_23   non-23: $geo_non_23"
  printf "LHO:   all: %3d ;     #10: %3d ;     #23: %3d ;     #30: %3d ;     non-excluded: %3d\n" $lho_all $lho_10 $lho_23 $lho_30 $lho_non_excluded
  printf "LLO:   all: %3d ;     #10: %3d ;     #23: %3d ;     #30: %3d ;     non-excluded: %3d\n" $llo_all $llo_10 $llo_23 $lho_30 $llo_non_excluded
  printf "VGO:   all: %3d ;     #10: %3d ;     #23: %3d ;     #30: %3d ;     non-excluded: %3d\n" $vgo_all $vgo_10 $vgo_23 $lho_30 $vgo_non_excluded
  printf "GEO:   all: %3d ;     #10: %3d ;     #23: %3d ;     #30: %3d ;     non-excluded: %3d\n" $geo_all $geo_10 $geo_23 $lho_30 $geo_non_excluded
done
echo "-"
echo "### See comments in the end of this script for some useful commands for investigating these errors."



echo "$(date)  - GPS time: ~$((`date +%s` - 315964782))"
exit 0

# for mday in {13490..13506}; do echo "metric day: $mday"; grep "which indicates an error occurred" /root/bin/monitor_files_2/compare_dqxml_dir_to_ifo_dir_H_${mday}?????_log.txt | wc --lines; done
# grep "which indicates an error occurred" /root/bin/monitor_files_2/compare_dqxml_dir_to_ifo_dir_H_1349??????_log.txt | less
# grep "which indicates an error occurred" /root/bin/monitor_files_2/compare_dqxml_dir_to_ifo_dir_H_1349??????_log.txt | grep -v -e "command is 10" -e "command is 23" -e "command is 30" | less
# grep "which indicates an error occurred" /root/bin/monitor_files_2/compare_dqxml_dir_to_ifo_dir_H_{13490..13506}?????_log.txt | less
