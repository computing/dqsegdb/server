#!/bin/awk -f
# This is a script to compare the output of a script that ananlyzes DQXML files	to the output of commands that query the MySQL DB (dqsegdb).
# Run with something like this:	'compare_dqxml_to_db_lho_or_llo.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time'
#                               'compare_dqxml_to_db_2019.04.23.awk  L1  693  1459  1239992000  1239990600  1239992000  1'
#                               './compare_dqxml_to_db.awk  L1  693  1459  1239992000  1239990600  1239992000  1  /tmp/test_output.txt'
# Here's the process:
#   * check for DQXML and DB files (verify they're there) (for a given ifo, 1 DQXML file per id (proxy for flag) (contains both active and known) (so 2 per ifo); 1 DB file per combination of id and active/known (so 4 per ifo)) 
#   * extract segments from DQXML file
#   * extract segments from DB file
#   * coalesce DQXML segments
#   * coalesce DB segments
#   * truncate DQXML segments
#   * truncate DB segments
#   * compare truncated DQXML and DB segments
#   * save file corresponding to results (for Nagios, in a location Nagios can read it from)
# to do:
#   * add checks on input values (ifo = h1 or l1 only, etc.)
#   * set up script to handle V and G, as well?

BEGIN {
  verbose = 0
  print_to_monitor_file = 1
  monitor_file = "/root/bin/monitor_files/monitor_run_results.txt"
  if (ARGV[7] == 1) {verbose = 1}
#  file_dir = "/root/bin/monitor_files"
  file_dir = "/tmp/monitor_logs"
  if (ARGC < 8) {printf "### ERROR ### This script should be called with %d arguments, but there were only %d.  Please fix this and re-run.\n", 8, ARGC; 
                 printf "          ### Run like this: '/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose_flag   $output_file\n";
                 printf "          ### Such as: '/root/bin/compare_dqxml_to_db.awk  H1  694  1492  1240815439  1240814539  1240814839  1  /tmp/verify_published_H1.json  &>  /tmp/verify_published_h1_log.txt'\n"; exit}
  ifo = ARGV[1]
  id1 = ARGV[2]
  id2 = ARGV[3]
  gps_run = ARGV[4]
  gps_start = ARGV[5]
  gps_end = ARGV[6]
#  output_file_name = "/tmp/compare_dqxml_to_db_test.txt"   # default value, if no filename given on command line
  output_file_name = "/tmp/monitor_logs/compare_dqxml_to_db_test.txt"   # default value, if no filename given on command line
  if (ARGC >= 9) {output_file_name = ARGV[8]}

  if (ifo == "h1" ||  ifo == "H"  ||  ifo == "H1") {ifo = "h"; ifo_long = "LHO"}   # it's easier to know that we have just the letter and add the "1" later
  if (ifo == "l1" ||  ifo == "L"  ||  ifo == "L1") {ifo = "l"; ifo_long = "LLO"}
  if (ifo == "v1" ||  ifo == "V"  ||  ifo == "V1") {ifo = "v"; ifo_long = "Virgo"}
  if (ifo == "g1" ||  ifo == "G"  ||  ifo == "G1") {ifo = "g"; ifo_long = "GEO"}

  ARGC = 2   # set this to 2 so that awk thinks there's only 1 file to be processed; see next line
  ARGV[1] = "/etc/fstab"   # awk thinks that this is the file to be processed (but it doesn't actually do anything with that file; all file-handling is done independent of that file); use this file b/c it's on every system
  if (verbose == 1) {printf "### INFO ### gps_run = %s; ifo = %s; file_dir = %s; id1 (flag1) = %d; id2 (flag2) = %d; gps_start = %d; gps_end = %d\n", gps_run, ifo, file_dir, id1, id2, gps_start, gps_end}

  # create blank arrays, so that even if no segments are found, the (empty) array still exists and can be used in other steps
  split("", id1_active_segments_start_db)
  split("", id1_active_segments_end_db)
  split("", id1_known_segments_start_db)
  split("", id1_known_segments_end_db)
  split("", id2_active_segments_start_db)
  split("", id2_active_segments_end_db)
  split("", id2_known_segments_start_db)
  split("", id2_known_segments_end_db)
  # create blank arrays, so that even if no segments are found, the (empty) array still exists and can be used in other steps
  split("", id1_active_segments_start_dqxml)
  split("", id1_active_segments_end_dqxml)
  split("", id1_known_segments_start_dqxml)
  split("", id1_known_segments_end_dqxml)
  split("", id2_active_segments_start_dqxml)
  split("", id2_active_segments_end_dqxml)
  split("", id2_known_segments_start_dqxml)
  split("", id2_known_segments_end_dqxml)

}   # BEGIN


# Main
{
# set up filenames
  # DQXML example: dqxml_h1_1462_1239990600_1239992000.txt
  dqxml_file1 = sprintf("%s/dqxml_%s1_%d_%d_%d.txt", file_dir, ifo, id1, gps_start, gps_end)
  dqxml_file2 = sprintf("%s/dqxml_%s1_%d_%d_%d.txt", file_dir, ifo, id2, gps_start, gps_end)
  # DB example: dqsegdb_h1_1462_active_1239990600_1239992000.txt
  db_file1 = sprintf("%s/dqsegdb_%s1_%d_active_%d_%d.txt", file_dir, ifo, id1, gps_start, gps_end)
  db_file2 = sprintf("%s/dqsegdb_%s1_%d_known_%d_%d.txt", file_dir, ifo, id1, gps_start, gps_end)
  db_file3 = sprintf("%s/dqsegdb_%s1_%d_active_%d_%d.txt", file_dir, ifo, id2, gps_start, gps_end)
  db_file4 = sprintf("%s/dqsegdb_%s1_%d_known_%d_%d.txt", file_dir, ifo, id2, gps_start, gps_end)

# check for DQXML and DB files (verify they're there)
  if (verbose == 1) {print "### INFO ### Checking if DQXML and DB files are available."}
  dqxml_file_count = verify_dqxml_files()   # this is how many of the expected dqxml files that are expected actually exist
  db_file_count = verify_db_files()   # this is how many of the expected dqxml files that are expected actually exist
### we should do something if the expected files are not available

# extract segments from DQXML file
  if (verbose == 1) {print "### INFO ### Extracting segments from DQXML files."}
  extract_dqxml_segments(dqxml_file1, id1, "active")   # creates and populates arrays 'id1_active_segments_start_dqxml', 'id1_active_segments_end_dqxml'
  extract_dqxml_segments(dqxml_file1, id1, "known")   # creates and populates arrays 'id1_known_segments_start_dqxml', 'id1_known_segments_end_dqxml'
  if (length(id1_known_segments_start_dqxml) != length(id1_known_segments_end_dqxml)) 
    {printf "### ERROR ### arrays 'id1_known_segments_start_dqxml', 'id1_known_segments_end_dqxml' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id1_known_segments_start_dqxml), length(id1_known_segments_end_dqxml); exit}
  if (length(id1_active_segments_start_dqxml) != length(id1_active_segments_end_dqxml)) 
    {printf "### ERROR ### arrays 'id1_active_segments_start_dqxml', 'id1_active_segments_end_dqxml' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id1_active_segments_start_dqxml), length(id1_active_segments_end_dqxml); exit}
  extract_dqxml_segments(dqxml_file2, id2, "active")   # creates and populates arrays 'id2_active_segments_start_dqxml', id2_active_segments_end_dqxml'
  extract_dqxml_segments(dqxml_file2, id2, "known")   # creates and populates arrays 'id2_known_segments_start_dqxml', id2_known_segments_end_dqxml'
  if (length(id2_known_segments_start_dqxml) != length(id2_known_segments_end_dqxml)) 
    {printf "### ERROR ### arrays 'id2_known_segments_start_dqxml', 'id2_known_segments_end_dqxml' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id2_known_segments_start_dqxml), length(id2_known_segments_end_dqxml); exit}
  if (length(id2_active_segments_start_dqxml) != length(id2_active_segments_end_dqxml)) 
    {printf "### ERROR ### arrays 'id2_active_segments_start_dqxml', 'id2_active_segments_end_dqxml' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id2_active_segments_start_dqxml), length(id2_active_segments_end_dqxml); exit}
  if (verbose == 1) {printf "### INFO ### DQXML line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_dqxml), length(id1_active_segments_start_dqxml), length(id2_known_segments_start_dqxml), length(id2_active_segments_start_dqxml)}

# extract segments from DB file
  # Note: id1 corresponds to db_file{1,2}; id2 corresponds to db_file{3,4}; active flags correspond to db_file{1,3}; known flags correspond to db_file{2,4}
  if (verbose == 1) {print "### INFO ### Extracting segments from DB files."}
  extract_db_segments(db_file1, id1, "active")   # creates and populates arrays 'id1_active_segments_start_db', 'id1_active_segments_end_db'
  extract_db_segments(db_file2, id1, "known")   # creates and populates arrays 'id1_known_segments_start_db', 'id1_known_segments_end_db'
  if (length(id1_known_segments_start_db) != length(id1_known_segments_end_db)) 
    {printf "### ERROR ### arrays 'id1_known_segments_start_db', 'id1_known_segments_end_db' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id1_known_segments_start_db), length(id1_known_segments_end_db); exit}
  if (length(id1_active_segments_start_db) != length(id1_active_segments_end_db)) 
    {printf "### ERROR ### arrays 'id1_active_segments_start_db', 'id1_active_segments_end_db' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id1_active_segments_start_db), length(id1_active_segments_end_db); exit}
  extract_db_segments(db_file3, id2, "active")   # creates and populates arrays 'id2_active_segments_start_db', id2_active_segments_end_db'
  extract_db_segments(db_file4, id2, "known")   # creates and populates arrays 'id2_known_segments_start_db', id2_known_segments_end_db'
  if (length(id2_known_segments_start_db) != length(id2_known_segments_end_db)) 
    {printf "### ERROR ### arrays 'id2_known_segments_start_db', 'id2_known_segments_end_db' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id2_known_segments_start_db), length(id2_known_segments_end_db); exit}
  if (length(id2_active_segments_start_db) != length(id2_active_segments_end_db)) 
    {printf "### ERROR ### arrays 'id2_active_segments_start_db', 'id2_active_segments_end_db' should have the same number of elements, but don't (%d and %d).  Exiting.\n", length(id2_active_segments_start_db), length(id2_active_segments_end_db); exit}
  if (verbose == 1) {printf "### INFO ### DB line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_db), length(id1_active_segments_start_db), length(id2_known_segments_start_db), length(id2_active_segments_start_db)}

# coalesce DQXML segments
# coalesce DB segments
  if (verbose == 1) {print "### INFO ### Coalescing DQXML and DB segments."}
  coalesce_dqxml_and_db_segments()   # this produces the arrays like 'id1_known_segments_start_dqxml_coalesced' (2 ids x 2 seg_types x 2 start/end arrays x 2 sources (dqxml, db) = 16 arrays)

# truncate DQXML segments
# truncate DB segments
  if (verbose == 1) {print "### INFO ### Truncating DQXML and DB segments."}
  truncate_coalesced_dqxml_and_db_arrays()   # this produces the arrays like 'id1_known_segments_start_dqxml_coalesced_truncated' (2 ids x 2 seg_types x 2 start/end arrays x 2 sources (dqxml, db) = 16 arrays)

# compare truncated DQXML and DB segments
  if (verbose == 1) {print "### INFO ### Comparing truncated DQXML and DB segments arrays."}
  # this will compare each of the 8 pairs of arrays (e.g., cf. 'id1_known_segments_start_dqxml_coalesced_truncated' to 'id1_known_segments_start_db_coalesced_truncated')
  # it will populate 2 arrays - segments_arrays_match and segments_arrays_mismatch - with strings like 'id1_known_segments_start'; if the first array has 8 elements and the second has 0, it's a perfect match
  split("", segments_arrays_match)
  split("", segments_arrays_mismatch)
  compare_truncated_dqxml_and_db_segments()
  count_match    = length(segments_arrays_match)
  count_mismatch = length(segments_arrays_mismatch)
  if (verbose == 1)
  {
    printf "### INFO ### There are %d matching arrays:\n", count_match
    for (loop_index = 1; loop_index <= count_match; loop_index++) {print segments_arrays_match[loop_index]}
    printf "### INFO ### There are %d mis-matching arrays:\n", count_mismatch
    for (loop_index = 1; loop_index <= count_mismatch; loop_index++) {print segments_arrays_mismatch[loop_index]}
  }   # if

# save file corresponding to results (for Nagios, in a location Nagios can read it from)
  if (count_match > 0  &&  count_mismatch == 0) {test_result = "PASS"; json_state = 0; 
    json_message = sprintf("PASS: %s published segments in the DB and segments in DQ XML files match for the specified time period (%d - %d).", ifo_long, gps_start, gps_end)}
  if (count_match == 0  &&  count_mismatch == 0) {test_result = "UNDEFINED"; json_state = 1;
    json_message = sprintf("UNKNOWN: There were no %s segments to compare in either the published segments in the DB or the DQ XML files.", ifo_long)}
  if (count_mismatch > 0) {test_result = "FAIL"; json_state = 2;
    json_message = sprintf("FAIL: %s published segments in the DB do not match segments in DQ XML files for flags %d and/or %d for the specified time period (%d - %d).", ifo_long, id1, id2, gps_start, gps_end)}
  if (verbose == 1) {printf "### INFO ### Saving output file for Nagios.  Test result: %s.\n", test_result}
  json_unknown_message = sprintf("UNKNOWN: There are no %s results available after the time period %d - %d.", ifo_long, gps_start, gps_end)

### hack - 2020.10.14 - in the following line, bumped start_sec for UNKNOWN up by 5 min (from 600 to 900), b/c we moved the start time in /root/bin/verify_published_segments.sh back by 5 min, 
###                     b/c we increased the publishing cadence from 1 min to 5 min in Oct. 2020, and that was causing failures b/c there were never (?) any files published in the last 5 min
  output_file_contents = sprintf("{\"created_gps\": %d, \"status_intervals\": [{\"start_sec\": 0, \"txt_status\": \"%s\", \"num_status\": %d}, {\"start_sec\": 900, \"txt_status\": \"%s\", \"num_status\": 3}]}", \
gps_run, json_message, json_state, json_unknown_message)

  if (verbose == 1) {print "### INFO ### JSON file contents:\n", output_file_contents}
  print output_file_contents > output_file_name
  close(output_file_name)

  if (print_to_monitor_file == 1)
  {
    format = "%Y.%m.%d-%H.%M.%S"
    monitor_file_message = sprintf("%s  %d-%d  %s1  %d  \"%s\"", strftime(format), gps_start, gps_end, toupper(ifo), json_state, json_message)
    print monitor_file_message >> monitor_file
  }   # if
###here1




  exit   # otherwise, the program will keep looping (why?)
}   # main



END {
}   # END


# functions


function coalesce_dqxml_and_db_segments()
{
# asort the start and end arrays, saving the sorted versions into a generic array name
# modify the 'coalesce_active_segments()' and 'coalesce_known_segments()' functions to use the generic array name (maybe just use one, not even need both)
# run the coalsce function on the gereric (sorted) arrays
# save the generic coalesce function output to arrays specific to the data being coalesced (maybe using another call to asort)
# this produces the arrays like 'id1_known_segments_start_dqxml_coalesced' (2 ids x 2 seg_types x 2 start/end types x 2 sources (dqxml, db) = 16 arrays)

# id1, active, dqxml
  asort(id1_active_segments_start_dqxml, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id1_active_segments_end_dqxml,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id1_active_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id1_active_segments_end_dqxml_coalesced)     # same as above
  asort(coalesced_segments_start_times, id1_active_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id1_active_segments_end_dqxml_coalesced)     # same as above
  if (length(id1_active_segments_start_dqxml_coalesced) != length(id1_active_segments_end_dqxml_coalesced)) 
    {printf "### ERROR ### arrays 'id1_active_segments_start_dqxml_coalesced', 'id1_active_segments_end_dqxml_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id1_active_segments_start_dqxml_coalesced), length(id1_active_segments_end_dqxml_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced active segments for id %d from DQXML files is %d \n", id1, length(id1_active_segments_start_dqxml_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DQXML line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_dqxml), length(id1_active_segments_start_dqxml), length(id2_known_segments_start_dqxml), length(id2_active_segments$
  if (verbose == 1) {printf "### INFO ### Coalesced active segments for id %d from DQXML files:\n", id1; print_segments_arrays(id1_active_segments_start_dqxml_coalesced, id1_active_segments_end_dqxml_coalesced)}

# id1, known, dqxml
  asort(id1_known_segments_start_dqxml, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id1_known_segments_end_dqxml,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id1_known_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id1_known_segments_end_dqxml_coalesced)     # same as above
  asort(coalesced_segments_start_times, id1_known_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id1_known_segments_end_dqxml_coalesced)     # same as above
  if (length(id1_known_segments_start_dqxml_coalesced) != length(id1_known_segments_end_dqxml_coalesced)) 
    {printf "### ERROR ### arrays 'id1_known_segments_start_dqxml_coalesced', 'id1_known_segments_end_dqxml_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id1_known_segments_start_dqxml_coalesced), length(id1_known_segments_end_dqxml_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced known segments for id %d from DQXML files is %d \n", id1, length(id1_known_segments_start_dqxml_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DQXML line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_dqxml), length(id1_active_segments_start_dqxml), length(id2_known_segments_start_dqxml), length(id2_active_segments$
  if (verbose == 1) {printf "### INFO ### Coalesced known segments for id %d from DQXML files:\n", id1; print_segments_arrays(id1_known_segments_start_dqxml_coalesced, id1_known_segments_end_dqxml_coalesced)}


# id2, active, dqxml
  asort(id2_active_segments_start_dqxml, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id2_active_segments_end_dqxml,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id2_active_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id2_active_segments_end_dqxml_coalesced)     # same as above
  asort(coalesced_segments_start_times, id2_active_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id2_active_segments_end_dqxml_coalesced)     # same as above
  if (length(id2_active_segments_start_dqxml_coalesced) != length(id2_active_segments_end_dqxml_coalesced)) 
    {printf "### ERROR ### arrays 'id2_active_segments_start_dqxml_coalesced', 'id2_active_segments_end_dqxml_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id2_active_segments_start_dqxml_coalesced), length(id2_active_segments_end_dqxml_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced active segments for id %d from DQXML files is %d \n", id2, length(id2_active_segments_start_dqxml_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DQXML line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_dqxml), length(id1_active_segments_start_dqxml), length(id2_known_segments_start_dqxml), length(id2_active_segments$
  if (verbose == 1) {printf "### INFO ### Coalesced active segments for id %d from DQXML files:\n", id2; print_segments_arrays(id2_active_segments_start_dqxml_coalesced, id2_active_segments_end_dqxml_coalesced)}

# id2, known, dqxml
  asort(id2_known_segments_start_dqxml, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id2_known_segments_end_dqxml,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id2_known_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id2_known_segments_end_dqxml_coalesced)     # same as above
  asort(coalesced_segments_start_times, id2_known_segments_start_dqxml_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id2_known_segments_end_dqxml_coalesced)     # same as above
  if (length(id2_known_segments_start_dqxml_coalesced) != length(id2_known_segments_end_dqxml_coalesced)) 
    {printf "### ERROR ### arrays 'id2_known_segments_start_dqxml_coalesced', 'id2_known_segments_end_dqxml_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id2_known_segments_start_dqxml_coalesced), length(id2_known_segments_end_dqxml_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced known segments for id %d from DQXML files is %d \n", id2, length(id2_known_segments_start_dqxml_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DQXML line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_dqxml), length(id1_active_segments_start_dqxml), length(id2_known_segments_start_dqxml), length(id2_active_segments
  if (verbose == 1) {printf "### INFO ### Coalesced known segments for id %d from DQXML files:\n", id2; print_segments_arrays(id2_known_segments_start_dqxml_coalesced, id2_known_segments_end_dqxml_coalesced)}

# id1, active, db
  asort(id1_active_segments_start_db, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id1_active_segments_end_db,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id1_active_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id1_active_segments_end_db_coalesced)     # same as above
  asort(coalesced_segments_start_times, id1_active_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id1_active_segments_end_db_coalesced)     # same as above
  if (length(id1_active_segments_start_db_coalesced) != length(id1_active_segments_end_db_coalesced)) 
    {printf "### ERROR ### arrays 'id1_active_segments_start_db_coalesced', 'id1_active_segments_end_db_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id1_active_segments_start_db_coalesced), length(id1_active_segments_end_db_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced active segments for id %d from DB files is %d \n", id1, length(id1_active_segments_start_db_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DB line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_db), length(id1_active_segments_start_db), length(id2_known_segments_start_db), length(id2_active_segments$
  if (verbose == 1) {printf "### INFO ### Coalesced active segments for id %d from DB files:\n", id1; print_segments_arrays(id1_active_segments_start_db_coalesced, id1_active_segments_end_db_coalesced)}

# id1, known, db
  asort(id1_known_segments_start_db, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id1_known_segments_end_db,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id1_known_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id1_known_segments_end_db_coalesced)     # same as above
  asort(coalesced_segments_start_times, id1_known_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id1_known_segments_end_db_coalesced)     # same as above
  if (length(id1_known_segments_start_db_coalesced) != length(id1_known_segments_end_db_coalesced)) 
    {printf "### ERROR ### arrays 'id1_known_segments_start_db_coalesced', 'id1_known_segments_end_db_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id1_known_segments_start_db_coalesced), length(id1_known_segments_end_db_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced known segments for id %d from DB files is %d \n", id1, length(id1_known_segments_start_db_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DB line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_db), length(id1_active_segments_start_db), length(id2_known_segments_start_db), length(id2_active_segments
  if (verbose == 1) {printf "### INFO ### Coalesced known segments for id %d from DB files:\n", id1; print_segments_arrays(id1_known_segments_start_db_coalesced, id1_known_segments_end_db_coalesced)}

# id2, active, db
  asort(id2_active_segments_start_db, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id2_active_segments_end_db,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id2_active_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id2_active_segments_end_db_coalesced)     # same as above
  asort(coalesced_segments_start_times, id2_active_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id2_active_segments_end_db_coalesced)     # same as above
  if (length(id2_active_segments_start_db_coalesced) != length(id2_active_segments_end_db_coalesced)) 
    {printf "### ERROR ### arrays 'id2_active_segments_start_db_coalesced', 'id2_active_segments_end_db_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id2_active_segments_start_db_coalesced), length(id2_active_segments_end_db_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced active segments for id %d from DB files is %d \n", id2, length(id2_active_segments_start_db_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DB line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_db), length(id1_active_segments_start_db), length(id2_known_segments_start_db), length(id2_active_segments$
  if (verbose == 1) {printf "### INFO ### Coalesced active segments for id %d from DB files:\n", id2; print_segments_arrays(id2_active_segments_start_db_coalesced, id2_active_segments_end_db_coalesced)}

# id2, known, db
  asort(id2_known_segments_start_db, segments_start_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  asort(id2_known_segments_end_db,   segments_end_times)   # this sorts the specifically-named first array and saves the results in the generically-named second array, ready to be passed to a function that uses the generic array
  coalesce_segments_array()   # this uses the 'segments_start_times' and 'segments_end_times' arrays as input and produces 'segments_start_times_coalesced' and 'segments_end_times_coalesced' arrays as output
#  asort(segments_start_times_coalesced, id2_known_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
#  asort(segments_end_times_coalesced,   id2_known_segments_end_db_coalesced)     # same as above
  asort(coalesced_segments_start_times, id2_known_segments_start_db_coalesced)   # this is just an easy way to copy the array, since the array elements *should* already be sorted
  asort(coalesced_segments_end_times,   id2_known_segments_end_db_coalesced)     # same as above
  if (length(id2_known_segments_start_db_coalesced) != length(id2_known_segments_end_db_coalesced)) 
    {printf "### ERROR ### arrays 'id2_known_segments_start_db_coalesced', 'id2_known_segments_end_db_coalesced' should have the same number of elements, but don't (%d and %d).  Exiting.\n", \
     length(id2_known_segments_start_db_coalesced), length(id2_known_segments_end_db_coalesced); exit}
  if (verbose == 1) {printf "### INFO ### Number of coalesced known segments for id %d from DB files is %d \n", id2, length(id2_known_segments_start_db_coalesced)}
#  if (verbose == 1) {printf "### INFO ### DB line counts for id1 known, active; id2 known, active = %d, %d; %d, %d \n", length(id1_known_segments_start_db), length(id1_active_segments_start_db), length(id2_known_segments_start_db), length(id2_active_segments$
  if (verbose == 1) {printf "### INFO ### Coalesced known segments for id %d from DB files:\n", id2; print_segments_arrays(id2_known_segments_start_db_coalesced, id2_known_segments_end_db_coalesced)}
}   # coalesce_dqxml_and_db_segments()


function coalesce_segments_array()   # coalesces segments_start_times and segments_end_times arrays into coalesced_segments_start_times and coalesced_segments_end_times
{
# this function is a modification of a function taken from 'extract_dqxml_segments.awk'
# note that this function assumes that the input arrays (segments_start_times and segments_end_times) are already sorted; that must be done as a separate step (e.g., 'asort(some_start_times_array, segments_start_times)', etc.)
  # process:
  #  * read in the first pair of start and end times (current times)
  #  * read in the next pair of times (next times)
  #  * if 'current' end time and 'next' start time are the same, replace 'current' end time with 'next' end time; go back to 'read in the next pair of times' step
  #  * if 'current' end time and 'next' start time are different, write 'current' start and end times to the next start, end time in coalesced times array; make 'next' times new 'current' times; read next, etc.
  coalesced_segments_count = 0
  split("", coalesced_segments_start_times)
  split("", coalesced_segments_end_times)
  segments_count1 = length(segments_start_times)
  segments_count2 = length(segments_end_times)
  if (segments_count1 != segments_count2)
    {printf "### ERROR ### arrays 'segments_start_times' and 'segments_end_times' in function 'coalesce_segments_array()' should have the same number of elements, but they don't (%d and %d).  Exiting.\n", segments_count1, segments_count2; exit}
  else {segments_count = segments_count1}
  if (verbose == 1) {printf "### INFO ### Arrays 'segments_start_times' and 'segments_end_times' both have %d elements in them.\n", segments_count}
  asort(segments_start_times, sorted_segments_start_times)   # times in first array are sorted and saved in second array
  asort(segments_end_times,   sorted_segments_end_times)     # same as above
  if (segments_count == 0) {return}
  for (loop_index = 1; loop_index <= segments_count; loop_index++)
  {
    if (loop_index == 1) {current_start_time = sorted_segments_start_times[loop_index]; current_end_time = sorted_segments_end_times[loop_index]; continue}
    next_start_time = sorted_segments_start_times[loop_index]
    next_end_time   = sorted_segments_end_times[loop_index]
    if (current_end_time == next_start_time) {current_end_time = next_end_time; continue}
    else
    {
      coalesced_segments_count++
      coalesced_segments_start_times[coalesced_segments_count] = current_start_time
      coalesced_segments_end_times[coalesced_segments_count]   = current_end_time
      current_start_time = next_start_time
      current_end_time   = next_end_time
      continue
    }   # else
  }   # for
  # after the last line is processed, we have to save the last start-end pair (which might be a result of coalescence or might be the last 'next' pair read in), b/c they haven't been saved yet
  coalesced_segments_count++
  coalesced_segments_start_times[coalesced_segments_count] = current_start_time
  coalesced_segments_end_times[coalesced_segments_count]   = current_end_time
}   # coalesce_segments_array()


function compare_segments_arrays(array1, array2, array_name)
{
  # process: try to find any way that the two arrays are not the same; if found, record the name and return immediately; if none found, then record as 
  length1 = length(array1)
  length2 = length(array2)
  if (length1 != length2) 
  {
    segments_arrays_mismatch[(length(segments_arrays_mismatch) + 1)] = array_name
    if (verbose == 1) {printf "### INFO ### Length mismatch for arrays starting with '%s' (%d and %d elements).\n", array_name, length1, length2}
    return
  }   # if
  for (loop_index = 1; loop_index <= length1; loop_index++)
  {
    if (array1[loop_index] != array2[loop_index])
    {
      segments_arrays_mismatch[(length(segments_arrays_mismatch) + 1)] = array_name 
      if (verbose == 1) {printf "### INFO ### Mismatch in element %d for arrays starting with '%s'.\n", loop_index, array_name}
      return 
    }   # if
  # if we got to here, the arrays are identical
  }   # for
  segments_arrays_match[(length(segments_arrays_match) + 1)] = array_name
  if (verbose == 1) {printf "### INFO ### Match between arrays starting with '%s'.\n", array_name}
  return
}   # compare_segments_arrays()


function compare_truncated_dqxml_and_db_segments()
{
  # this will compare each of the 8 pairs of arrays (e.g., cf. 'id1_known_segments_start_dqxml_coalesced_truncated' to 'id1_known_segments_start_db_coalesced_truncated')
  # it will populate 2 arrays - segments_arrays_match and segments_arrays_mismatch - with strings like 'id1_known_segments_start'; if the first array has 8 elements and the second has 0, it's a perfect match
  compare_segments_arrays(id1_active_segments_start_dqxml_coalesced_truncated, id1_active_segments_start_db_coalesced_truncated, "id1_active_segments_start")
  compare_segments_arrays(id2_active_segments_start_dqxml_coalesced_truncated, id2_active_segments_start_db_coalesced_truncated, "id2_active_segments_start")
  compare_segments_arrays(id1_known_segments_start_dqxml_coalesced_truncated,  id1_known_segments_start_db_coalesced_truncated,  "id1_known_segments_start")
  compare_segments_arrays(id2_known_segments_start_dqxml_coalesced_truncated,  id2_known_segments_start_db_coalesced_truncated,  "id2_known_segments_start")
  compare_segments_arrays(id1_active_segments_end_dqxml_coalesced_truncated,   id1_active_segments_end_db_coalesced_truncated,   "id1_active_segments_end")
  compare_segments_arrays(id2_active_segments_end_dqxml_coalesced_truncated,   id2_active_segments_end_db_coalesced_truncated,   "id2_active_segments_end")
  compare_segments_arrays(id1_known_segments_end_dqxml_coalesced_truncated,    id1_known_segments_end_db_coalesced_truncated,    "id1_known_segments_end")
  compare_segments_arrays(id2_known_segments_end_dqxml_coalesced_truncated,    id2_known_segments_end_db_coalesced_truncated,    "id2_known_segments_end")
}   compare_truncated_dqxml_and_db_segments()


function extract_db_segments(filename, flag_id, seg_type)   # creates arrays 'dqxml_known_segments_id{1,2}' and 'dqxml_active_segments_id{1,2}'
{
# this reads from files created by DB queries in 'verify_published_segments.sh', saved to files like 'dqsegdb_h1_693_active_1240899739_1240900039.txt'
# sample lines:  (1st line in the file should be like the 1st line; rest of the lines should be like the 2nd line)
#dq_flag_version_fk     segment_start_time	segment_stop_time
#693    1239990240	1239990304
  if (verbose == 1) {printf "### INFO ### Checking for %s segments for flag ID %d in file %s\n", seg_type, flag_id, filename}
  line_count = 0
  match_count = 0
  while ( (getline file_line < filename) > 0)
  {
    line_count++   # why are we keeping track of the *line* count?  there are a bunch of lines we don't care about! array indices should be based on the number of *matching* lines (changing that now...)
    split(file_line, file_line_fields)
    if (file_line_fields[1] == flag_id)
    {
      match_count++
      start_fixed = file_line_fields[2]
      end_fixed = file_line_fields[3]
#      if (verbose == 1) {printf "### INFO ### Found %s segment in file %s; start time = %d, end time = %d\n", seg_type, filename, start_fixed, end_fixed}
#      if (flag_id == id1  &&  seg_type == "active") {id1_active_segments_start_db[line_count] = start_fixed; id1_active_segments_end_db[line_count] = end_fixed}
#      if (flag_id == id1  &&  seg_type == "known")  {id1_known_segments_start_db[line_count]  = start_fixed; id1_known_segments_end_db[line_count]  = end_fixed}
#      if (flag_id == id2  &&  seg_type == "active") {id2_active_segments_start_db[line_count] = start_fixed; id2_active_segments_end_db[line_count] = end_fixed}
#      if (flag_id == id2  &&  seg_type == "known")  {id2_known_segments_start_db[line_count]  = start_fixed; id2_known_segments_end_db[line_count]  = end_fixed}
      if (flag_id == id1  &&  seg_type == "active") {id1_active_segments_start_db[match_count] = start_fixed; id1_active_segments_end_db[match_count] = end_fixed}
      if (flag_id == id1  &&  seg_type == "known")  {id1_known_segments_start_db[match_count]  = start_fixed; id1_known_segments_end_db[match_count]  = end_fixed}
      if (flag_id == id2  &&  seg_type == "active") {id2_active_segments_start_db[match_count] = start_fixed; id2_active_segments_end_db[match_count] = end_fixed}
      if (flag_id == id2  &&  seg_type == "known")  {id2_known_segments_start_db[match_count]  = start_fixed; id2_known_segments_end_db[match_count]  = end_fixed}
    }   # if
  }   # while
  close(filename)
}   # extract_db_segments()


function extract_dqxml_segments(filename, flag_id, seg_type)   # creates arrays 'dqxml_known_segments_id{1,2}' and 'dqxml_active_segments_id{1,2}'
{
# sample line:
#coalesced known start, end times = [1239990592, 1239992032]
  if (verbose == 1) {printf "### INFO ### Checking for %s segments for flag ID %d in file %s\n", seg_type, flag_id, filename}
  line_count = 0
  match_count = 0
  while ( (getline file_line < filename) > 0)
  {
    line_count++   # why are we keeping track of the *line* count?  there are a bunch of lines we don't care about! array indices should be based on the number of *matching* lines (changing that now...)
    split(file_line, file_line_fields)
    if (file_line_fields[1] == "coalesced"  &&  file_line_fields[2] == seg_type)
    {
      match_count++
      start_raw = file_line_fields[7]   # this will look like "[1239990592,"
      end_raw   = file_line_fields[8]   # this will look like "1239992032]"
      start_fixed = int(substr(start_raw, 2,10))
      end_fixed = int(substr(end_raw, 1,10))
#      if (verbose == 1) {printf "### INFO ### Found %s segment in file %s; start time = %d, end time = %d\n", seg_type, filename, start_fixed, end_fixed}
#      if (flag_id == id1  &&  seg_type == "active") {id1_active_segments_start_dqxml[line_count] = start_fixed; id1_active_segments_end_dqxml[line_count] = end_fixed}
#      if (flag_id == id1  &&  seg_type == "known")  {id1_known_segments_start_dqxml[line_count]  = start_fixed; id1_known_segments_end_dqxml[line_count]  = end_fixed}
#      if (flag_id == id2  &&  seg_type == "active") {id2_active_segments_start_dqxml[line_count] = start_fixed; id2_active_segments_end_dqxml[line_count] = end_fixed}
#      if (flag_id == id2  &&  seg_type == "known")  {id2_known_segments_start_dqxml[line_count]  = start_fixed; id2_known_segments_end_dqxml[line_count]  = end_fixed}
      if (flag_id == id1  &&  seg_type == "active") {id1_active_segments_start_dqxml[match_count] = start_fixed; id1_active_segments_end_dqxml[match_count] = end_fixed}
      if (flag_id == id1  &&  seg_type == "known")  {id1_known_segments_start_dqxml[match_count]  = start_fixed; id1_known_segments_end_dqxml[match_count]  = end_fixed}
      if (flag_id == id2  &&  seg_type == "active") {id2_active_segments_start_dqxml[match_count] = start_fixed; id2_active_segments_end_dqxml[match_count] = end_fixed}
      if (flag_id == id2  &&  seg_type == "known")  {id2_known_segments_start_dqxml[match_count]  = start_fixed; id2_known_segments_end_dqxml[match_count]  = end_fixed}
    }   # if
  }   # while
  close(filename)
}   # extract_dqxml_segments()


function print_segments_arrays(seg_array_1, seg_array_2)
{
  seg_array_1_length = length(seg_array_1)
  seg_array_2_length = length(seg_array_2)
  if (seg_array_1_length != seg_array_2_length) {printf "### ERROR ### Segments arrays sent to print_segments_arrays() are not equal lengths (%d and %d).  Exiting.\n", seg_array_1_length, seg_array_2_length}
  for (loop_index = 1; loop_index <= seg_array_1_length; loop_index++)
  {
    printf "[%d, %d]\n", seg_array_1[loop_index], seg_array_2[loop_index]
  }   # for
}   # print_segments_arrays()


function truncate_coalesced_dqxml_and_db_arrays()   # this produces the arrays like 'id1_known_segments_start_dqxml_coalesced_truncated' (2 ids x 2 seg_types x 2 start/end arrays x 2 sources (dqxml, db) = 16 arrays)
{
# this function modifies arrays like 'id1_known_segments_start_dqxml_coalesced' to produces arrays like 'id1_known_segments_start_dqxml_coalesced_truncated'
# call another function with the matching start and end arrays for each (source, flag ID, seg type) combination (another fn so that we don't have to create 8 copies of the code in that function)
# that function will loop over the arrays in parallel and make any necessary modifications so that no segment starts before gps_start or ends after gps_end; see notes in function truncate_segments_arrays() for how it does this
# the arrays *should* be passed by reference, so they'll be modified in place
  if (verbose == 1) {printf "### INFO ### GPS start and end times: [%d, %d]; Coalesced and truncated arrays:\n", gps_start, gps_end}

# id1, active, dqxml
  asort(id1_active_segments_start_dqxml_coalesced, id1_active_segments_start_dqxml_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id1_active_segments_end_dqxml_coalesced,   id1_active_segments_end_dqxml_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id1_active_segments_start_dqxml_coalesced_truncated, id1_active_segments_end_dqxml_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "active", id1, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id1_active_segments_start_dqxml_coalesced, id1_active_segments_end_dqxml_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "active", id1, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id1_active_segments_start_dqxml_coalesced_truncated, id1_active_segments_end_dqxml_coalesced_truncated)}

# id1, known, dqxml
  asort(id1_known_segments_start_dqxml_coalesced, id1_known_segments_start_dqxml_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id1_known_segments_end_dqxml_coalesced,   id1_known_segments_end_dqxml_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id1_known_segments_start_dqxml_coalesced_truncated, id1_known_segments_end_dqxml_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "known", id1, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id1_known_segments_start_dqxml_coalesced, id1_known_segments_end_dqxml_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "known", id1, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id1_known_segments_start_dqxml_coalesced_truncated, id1_known_segments_end_dqxml_coalesced_truncated)}

# id2, active, dqxml
  asort(id2_active_segments_start_dqxml_coalesced, id2_active_segments_start_dqxml_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id2_active_segments_end_dqxml_coalesced,   id2_active_segments_end_dqxml_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id2_active_segments_start_dqxml_coalesced_truncated, id2_active_segments_end_dqxml_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "active", id2, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id2_active_segments_start_dqxml_coalesced, id2_active_segments_end_dqxml_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "active", id2, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id2_active_segments_start_dqxml_coalesced_truncated, id2_active_segments_end_dqxml_coalesced_truncated)}

# id2, known, dqxml
  asort(id2_known_segments_start_dqxml_coalesced, id2_known_segments_start_dqxml_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id2_known_segments_end_dqxml_coalesced,   id2_known_segments_end_dqxml_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id2_known_segments_start_dqxml_coalesced_truncated, id2_known_segments_end_dqxml_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "known", id2, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id2_known_segments_start_dqxml_coalesced, id2_known_segments_end_dqxml_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "known", id2, "dqxml"}
  if (verbose == 1) {print_segments_arrays(id2_known_segments_start_dqxml_coalesced_truncated, id2_known_segments_end_dqxml_coalesced_truncated)}

# id1, active, db
  asort(id1_active_segments_start_db_coalesced, id1_active_segments_start_db_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id1_active_segments_end_db_coalesced,   id1_active_segments_end_db_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id1_active_segments_start_db_coalesced_truncated, id1_active_segments_end_db_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "active", id1, "db"}
  if (verbose == 1) {print_segments_arrays(id1_active_segments_start_db_coalesced, id1_active_segments_end_db_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "active", id1, "db"}
  if (verbose == 1) {print_segments_arrays(id1_active_segments_start_db_coalesced_truncated, id1_active_segments_end_db_coalesced_truncated)}

# id1, known, db
  asort(id1_known_segments_start_db_coalesced, id1_known_segments_start_db_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id1_known_segments_end_db_coalesced,   id1_known_segments_end_db_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id1_known_segments_start_db_coalesced_truncated, id1_known_segments_end_db_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "known", id1, "db"}
  if (verbose == 1) {print_segments_arrays(id1_known_segments_start_db_coalesced, id1_known_segments_end_db_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "known", id1, "db"}
  if (verbose == 1) {print_segments_arrays(id1_known_segments_start_db_coalesced_truncated, id1_known_segments_end_db_coalesced_truncated)}

# id2, active, db
  asort(id2_active_segments_start_db_coalesced, id2_active_segments_start_db_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id2_active_segments_end_db_coalesced,   id2_active_segments_end_db_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id2_active_segments_start_db_coalesced_truncated, id2_active_segments_end_db_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "active", id2, "db"}
  if (verbose == 1) {print_segments_arrays(id2_active_segments_start_db_coalesced, id2_active_segments_end_db_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "active", id2, "db"}
  if (verbose == 1) {print_segments_arrays(id2_active_segments_start_db_coalesced_truncated, id2_active_segments_end_db_coalesced_truncated)}

# id2, known, db
  asort(id2_known_segments_start_db_coalesced, id2_known_segments_start_db_coalesced_truncated)   # this easily duplicates the array, which should already be in the correct order
  asort(id2_known_segments_end_db_coalesced,   id2_known_segments_end_db_coalesced_truncated)     # this easily duplicates the array, which should already be in the correct order
  truncate_segments_arrays(id2_known_segments_start_db_coalesced_truncated, id2_known_segments_end_db_coalesced_truncated)
  if (verbose == 1) {printf "### INFO ### coalesced %s arrays for id %d from %s\n", "known", id2, "db"}
  if (verbose == 1) {print_segments_arrays(id2_known_segments_start_db_coalesced, id2_known_segments_end_db_coalesced)}
  if (verbose == 1) {printf "### INFO ### truncated %s arrays for id %d from %s\n", "known", id2, "db"}
  if (verbose == 1) {print_segments_arrays(id2_known_segments_start_db_coalesced_truncated, id2_known_segments_end_db_coalesced_truncated)}
                    #print_segments_arrays(id2_known_segments_start_dqxml_coalesced, id2_known_segments_end_dqxml_coalesced)
}   # truncate_coalesced_dqxml_and_db_arrays()


function truncate_segments_arrays(start_times_array, end_times_array)
{
# this function looks at the 10 possible relationships between a coalesced segment (defined by its start and end time) and the interval defined by gps_start and gps_end
# note that the ordering is important, as more than one case may apply to a coalesced segment (e.g., if its start time is before gps_start AND its end time is after gps_end)
  array_length1 = length(start_times_array)
  array_length2 = length(end_times_array)
  if (array_length1 != array_length2)
    {printf "### ERROR ### Arrays 'start_times_array' and 'end_times_array' at start of function 'truncate_segments_array()' should have the same number of elements, but they don't (%d and %d).  Exiting.\n", array_length1, array_length2; exit}
  else {array_length = array_length1}
if (verbose == 1) {printf "### INFO ### In 'truncate_segments_arrays', first check, array length = %d\n", array_length}
  if (array_length == 0) {return}
  for (loop_index = 1; loop_index <= array_length; loop_index++)
  {
    # case 1: both times are before gps_start; delete both
      if (start_times_array[loop_index] < gps_start  &&  end_times_array[loop_index] < gps_start) {
printf "Length of start_times_array = %d\n", length(start_times_array)
delete start_times_array[loop_index]; delete end_times_array[loop_index]
printf "Length of start_times_array = %d\n", length(start_times_array); continue}   # note that an empty array element == 0, so we have to break out of the loop, or else later cases 
    # case 2: start time is before gps_start, end time is equal to gps_start; set both equal to gps_start (which means change only start time)
      if (start_times_array[loop_index] < gps_start  &&  end_times_array[loop_index] == gps_start) {start_times_array[loop_index] = gps_start}
    # case 3: start time is before gps_start, end time is after gps_start; set start time equal to gps_start, don't change end time
      if (start_times_array[loop_index] < gps_start  &&  end_times_array[loop_index] > gps_start) {start_times_array[loop_index] = gps_start}
    # case 4: start time is equal to gps_start, end time is after gps_start; don't do anything (don't even test for this)
    # case 5: both times are after gps_start; don't do anything (don't even test for this)
    # case 6: both times are before gps_end; don't do anything (don't even test for this)
    # case 7: start time is before gps_end, end time is equal to gps_end; don't do anything (don't even test for this)
    # case 8: start time is before gps_end, end time is after gps_end; set end time equal to gps_end, don't change start time
      if (start_times_array[loop_index] < gps_end  &&  end_times_array[loop_index] > gps_end) {end_times_array[loop_index] = gps_end}
    # case 9: start time is equal to gps_end, end time is after gps_end; set both equal to gps_end (which means change only end time
      if (start_times_array[loop_index] == gps_end  &&  end_times_array[loop_index] > gps_end) {end_times_array[loop_index] = gps_end}
    # case 10: both times are after gps_end; delete both times
      if (start_times_array[loop_index] > gps_end  &&  end_times_array[loop_index] > gps_end) {delete start_times_array[loop_index]; delete end_times_array[loop_index]}
  }   # for
  array_length1 = length(start_times_array)
  array_length2 = length(end_times_array)
  if (array_length1 != array_length2)
    {printf "### ERROR ### Arrays 'start_times_array' and 'end_times_array' at end of function 'truncate_segments_array()' should have the same number of elements, but they don't (%d and %d).  Exiting.\n", array_length1, array_length2; exit}
if (verbose == 1) {printf "### INFO ### In 'truncate_segments_arrays', second check, array length = %d\n", array_length1}
  # if the segment with array index N is completely deleted, segments N+1, N+2, etc. still have the same array indices, even though the array is smaller (fewer elements); comparisons with other arrays, which loop over indices,
  #   will then fail, b/c the other array element with index N has values, whereas this one doesn't;
  # so we sort the start and end arrays, to 'compactify' them
  asort(start_times_array)
  asort(end_times_array)
if (verbose == 1) {printf "### INFO ### In 'truncate_segments_arrays', third check, array length = %d\n", length(start_times_array)}
}   # truncate_segments_arrays()


function verify_db_files()
{
  # this verifies that the db files exist and are readable; returns the count of such files
  # db files are db_file1 through db_file4
  file_count = 0
  file_test = getline _ < db_file1
  close(db_file1)
  if (file_test >= 0) {file_count++}
  else if (verbose == 1) {printf "### WARNING ### File %s returned value %d from getline.  (0 = end of file; -1 = error)\n", db_file1, file_test}
  file_test = getline _ < db_file2
  close(db_file2)
  if (file_test >= 0) {file_count++}
  else if (verbose == 1) {printf "### WARNING ### File %s returned value %d from getline.  (0 = end of file; -1 = error)\n", db_file2, file_test}
  file_test = getline _ < db_file3
  close(db_file3)
  if (file_test >= 0) {file_count++}
  else if (verbose == 1) {printf "### WARNING ### File %s returned value %d from getline.  (0 = end of file; -1 = error)\n", db_file3, file_test}
  file_test = getline _ < db_file4
  close(db_file4)
  if (file_test >= 0) {file_count++}
  else if (verbose == 1) {printf "### WARNING ### File %s returned value %d from getline.  (0 = end of file; -1 = error)\n", db_file4, file_test}
  if (verbose == 1) {printf "### INFO ### Number of readable DB files = %d (out of 4)\n", file_count}
  return file_count
}   # verify_db_files()


function verify_dqxml_files()
{
  # this verifies that the dqxml files exist and are readable; returns the count of such files
  # dqxml files are dqxml_file1 and dqxml_file2
  file_count = 0
  file_test = getline _ < dqxml_file1
  close(dqxml_file1)
  if (file_test >= 0) {file_count++}
  else if (verbose == 1) {printf "### WARNING ### File %s returned value %d from getline.  (0 = end of file; -1 = error)\n", db_file1, file_test}
  file_test = getline _ < dqxml_file2
  close(dqxml_file2)
  if (file_test >= 0) {file_count++}
  else if (verbose == 1) {printf "### WARNING ### File %s returned value %d from getline.  (0 = end of file; -1 = error)\n", db_file2, file_test}
  if (verbose == 1) {printf "### INFO ### Number of readable DQXML files = %d (out of 2)\n", file_count}
  return file_count
}   # verify_dqxml_files()





###here2






# refernce
# DQXML filesnames look like this:
# dqxml_h1_1462_1239990600_1239992000.txt
# dqxml_h1_694_1239990600_1239992000.txt
# dqxml_l1_1459_1239990600_1239992000.txt
# dqxml_l1_693_1239990600_1239992000.txt
# coalesced segments look like this:
#coalesced_known_segments_count = 1 
#coalesced known start, end times = [1239990592, 1239992032] 
#coalesced_active_segments_count = 1 
#coalesced active start, end times = [1239990592, 1239991140] 
# DB filenames look like this:
# dqsegdb_l1_693_active_1239990600_1239992000.txt
# dqsegdb_l1_693_known_1239990600_1239992000.txt
# dqsegdb_h1_694_active_1239990600_1239992000.txt
# dqsegdb_h1_694_known_1239990600_1239992000.txt
# dqsegdb_l1_1459_active_1239990600_1239992000.txt
# dqsegdb_l1_1459_known_1239990600_1239992000.txt
# dqsegdb_h1_1462_active_1239990600_1239992000.txt
# dqsegdb_h1_1462_known_1239990600_1239992000.txt
# un-coalesced segments look like this:
#dq_flag_version_fk	segment_start_time	segment_stop_time
#693	1239990240	1239990304
#693	1239990304	1239990368
#693	1239990368	1239990432


# possible problem: if a time span is double-published, that could create problems with the coalesce step, if the start and/or end time of the segment that is actuall published doesn't match - 
#   i.e., if the published segments are not identical; e.g., (example that couldn't happen in DQSegDB, but demonstrates the idea) if times 1 to 100 are published as 1-10, 11-21, ..., 91-100, then later
#   times 75-100 are published again, as 75-84, 85-94, 95-100; start and end times (71,80), (81,90), (75,84), (85,94) will be sorted to (71,80), (75,84), (81,90), (85,94); when the coalesce step
#   is applied, coalesce must be able to sort that out properly... and I have to think about this more (also: what happens if the second set of segments included (75,100))
