#!/bin/bash
# Note: we no longer have to specify lib directory, PATH, PYTHONPATH, etc., b/c we're using the installed dqsegdb package to get the publisher script, not a reference dir.

start_date=$(date +%Y.%m.%d)
start_time=$(date +%H:%M:%S)
start_sec=$(date -u +"%s")
inf=$1
time_log_file=/root/bin/publisher_time_log_file_${inf}.txt

# check if the X509 certificate and key variables are set to valid files (mainly useful when this script is run from the command line, when the variables are often forgotten)
if test -v  X509_USER_CERT; then sleep 0; else echo "Variable X509_USER_CERT is not set!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -v  X509_USER_KEY;  then sleep 0; else echo  "Variable X509_USER_KEY is not set!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -a $X509_USER_CERT; then sleep 0; else echo "Variable X509_USER_CERT does not point to an actual file!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
if test -a $X509_USER_KEY;  then sleep 0; else echo  "Variable X509_USER_KEY does not point to an actual file!  You have 5 seconds to pause this process and fix that."; sleep 5; fi
# "-v" checks that the variable is set (note no "$" in the test); "-a" checks that the file exists (note the "$" in the test); need both b/c an empty variable will pass the "-a" test (!)
# Later note: It's not clear that it's possible to change the variable values in the shell running the script.  So if the variable isn't set or points to the wrong file,
#   the user probably just has to end the run, fix it, and start over.

executable=`which ligolw_publish_threaded_dqxml_dqsegdb 2> /dev/null`
if [ $? -ne 0 ]; then echo "### ERROR ### Could not find executable file ligolw_publish_threaded_dqxml_dqsegdb.  Exiting."; exit $?; fi

# we *should* need to publish modified GEO files from /dqxml/G1_test, but for some reason the original files, in /dqxml/G1/ work (for now)
#if [ "$inf" == "G" ]
#then
#  input_dir=/dqxml/${inf}1_test
#else
#  input_dir=/dqxml/${inf}1
#fi
input_dir=/dqxml/${inf}1

#/usr/bin/env python3 -W ignore::DeprecationWarning ligolw_publish_threaded_dqxml_dqsegdb \
/usr/bin/env python3 -W ignore::DeprecationWarning $executable \
--segment-url https://segments-dev.ligo.org \
--state-file=/var/log/publishing/state/${inf}-DQ_Segments_current.xml \
--pid-file=/var/log/publishing/pid/${inf}-DQ_Segments_cur.pid \
--log-file=/var/log/publishing/dev/${inf}-DQ_Segments_cur.log \
--input-directory=$input_dir \
--log-level DEBUG -m 100 -c 1

end_time=$(date +%H:%M:%S)
end_sec=$(date -u +"%s")
duration_sec=$((end_sec - start_sec))

echo "$start_date  $start_time - $end_time = $duration_sec sec" >> $time_log_file
