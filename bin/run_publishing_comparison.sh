#!/bin/bash
# This is a quick script to rsync files, then publish them, for separate comparison between seg and -dev.
# Started on 2022.02.21 by Robert Bruntz.

/root/bin/rsync_dqxml_for_parallel_publishing_test.sh H
/root/bin/rsync_dqxml_for_parallel_publishing_test.sh L

#/root/bin/run_publishing_O3_segmentsdevligoorg.sh_new_2022.02.18 H
#/root/bin/run_publishing_O3_segmentsdevligoorg.sh_new_2022.02.18 L
/root/bin/run_publishing_O4_segmentsdevligoorg.sh H
/root/bin/run_publishing_O4_segmentsdevligoorg.sh L



