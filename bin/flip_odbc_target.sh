#!/bin/bash
# Started by Robert Bruntz, 2019.11.20
# This is a script to flip the DB referred to in /etc/odbc.ini between two values.
# Option 1:
#   * Have 2 files; 1 with a symlink that changes between them (/etc/odbc_segments_backup_alpha.ini and /etc/odbc_segments_backup_beta.ini)
#   * Use sed to change the file
# Note: a new setup of segments-backup will have /etc/odbc.ini with "DATABASE=segments_backup", but this script only flips between segments_backup_alpha and segments_backup_beta.
#   The script cannot know whether the first DB was created by populate_from_backup_alpha.sh or populate_from_backup_beta.sh, so it can't automatically flip from segments_backup to the new DB name.
#   To handle this, manually change the DB name in /etc/odbc.ini to the *oppposite* of the one that is first installed (first script run), so that the flip will set it to the right one.


verbose=1
odbc_file=/etc/odbc.ini   # useful b/c we can change this for testing on test files
alpha_string="DATABASE=segments_backup_alpha"
beta_string="DATABASE=segments_backup_beta"

# check if /etc/odbc.ini contains either of these entire lines; note that the return code for a match = 0 (not 1) and for no match = 1 (not 0); thus, the '_inverted' part of the variable names
if [ $verbose -eq 1 ]; then echo "### INFO ### Checking for expected DB lines"; fi
#grep "^DATABASE=segments_backup_alpha$" /etc/odbc.ini
grep ^${alpha_string}$ $odbc_file
is_alpha_inverted=$?
#grep "^DATABASE=segments_backup_beta$" /etc/odbc.ini
grep ^${beta_string}$ $odbc_file
is_beta_inverted=$?

if [ $verbose -eq 1 ]
then
  echo "### INFO ### Listing of /etc/odbc.ini:"
  ls -l /etc/odbc.ini
  echo "### INFO ### Partial contents of /etc/odbc.ini:"
  echo "### --- ###"
#  cat /etc/odbc.ini
  grep -v PASSWORD /etc/odbc.ini
  echo "### --- ###"
  echo "### INFO ### is_alpha_inverted = $is_alpha_inverted"
  echo "### INFO ### is_beta_inverted = $is_beta_inverted"
fi

test -h /etc/odbc.ini
return_code=$?   # should be 0 for a symlink, 1 for a regular file
if [ $return_code -eq 1 ]
then
  echo "### INFO ### /etc/odbc.ini is a regular file, not a symlink."
  if [ $is_alpha_inverted -eq 0 ] && [ $is_beta_inverted -eq 1 ]
  then
    if [ $verbose -eq 1 ]; then echo "### INFO ### /etc/odbc.ini is set to use segments_backup_alpha.  Swapping it to use segments_backup_beta, using a sed command."; fi
    # running into trouble with this line on RL8 system, like "sed: couldn't open temporary file /etc/sedD80E5B: Permission denied"; trying the next line as a fix; see 2024.03.22 work notes
    sed -i "s/$alpha_string/$beta_string/g"  /etc/odbc.ini
    #sed "s/$alpha_string/$beta_string/g" /etc/odbc.ini > /etc/odbc.ini
    if [ $verbose -eq 1 ]; then echo "### INFO ### New contents of /etc/odbc.ini:"; fi
    echo "### --- ###"
#   cat /etc/odbc.ini
    grep -v PASSWORD /etc/odbc.ini
    echo "### --- ###"
  fi
  if [ $is_alpha_inverted -eq 1 ] && [ $is_beta_inverted -eq 0 ]
  then
    if [ $verbose -eq 1 ]; then echo "### INFO ### /etc/odbc.ini is set to use segments_backup_beta.  Swapping it to use segments_backup_alpha, using a sed command."; fi
    # running into trouble with this line on RL8 system, like "sed: couldn't open temporary file /etc/sedD80E5B: Permission denied"; trying the next line as a fix; see 2024.03.22 work notes
    sed -i "s/$beta_string/$alpha_string/g"  /etc/odbc.ini
    #sed "s/$beta_string/$alpha_string/g" /etc/odbc.ini > /etc/odbc.ini
    if [ $verbose -eq 1 ]; then echo "### INFO ### New partial contents of /etc/odbc.ini:"; fi
    echo "### --- ###"
#    cat /etc/odbc.ini
    grep -v PASSWORD /etc/odbc.ini
    echo "### --- ###"
  fi
  if [ $is_alpha_inverted -eq 0 ] && [ $is_beta_inverted -eq 0 ]
  then
    if [ $verbose -eq 1 ]; then echo "### ERROR ### /etc/odbc.ini contains lines for both segments_backup_alpha and segments_backup_beta.  This is a problem this script can't handle.  Nothing done.  Exiting."; exit 1; fi
  fi
  if [ $is_alpha_inverted -eq 1 ] && [ $is_beta_inverted -eq 1 ]
  then
    if [ $verbose -eq 1 ]; then echo "### ERROR ### /etc/odbc.ini doesn't have lines for either segments_backup_alpha or segments_backup_beta.  This is a problem this script can't handle.  Nothing done.  Exiting."; exit 1; fi
  fi
fi
if [ $return_code -eq 0 ]
then
  echo "### INFO ### /etc/odbc.ini is a symlink, not a regular file."
  echo "### WARNING ### There should be some code in this part of the script, but no one has bothered to write it yet, so nothing is being done."
  exit 1
fi


exit  0
# Reference
# sed -i 's/old-text/new-text/g'

