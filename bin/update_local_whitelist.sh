#!/bin/bash
# Started by Robert Bruntz on 2019.11.18
# This is a script to replace the whitelist on a machine with a new 'definitive whitelist' file (such as /backup/segdb/reference/lgmm/whitelist_definitive_2019.11.18a_sorted_uniq).
# To do:
#   * Add a check that ./whitelist exists; is a symlink
#   * Confirm that new whitelist file was copied over before deleting whitelist link
#   * Confirm that new whitelist link was created
#   * 



if [ $# -ne 1 ]
then
  echo "### ERROR ### Usage: (script name)  (absolute path to definitive whitelist file)"
  echo "          ### Example:   /backup/segdb/reference/root_files/update_local_whitelist.sh  /backup/segdb/reference/lgmm/whitelist_definitive_2019.11.18a_sorted_uniq"
  exit
else
  new_file=$1
  if [ ! -e $new_file ]; then echo "### ERROR ### Input file ($new_file) does not exist.  Please check the filename."; exit; fi
fi

cd /etc/grid-security/
echo "### INFO ### In /etc/grid-security/;  current grid-mapfile and whitelist:"
ls -l --color=auto grid-mapfile
ls -l --color=auto whitelist

echo "### INPUT ### Do you want to diff the current whitelist with the new one? [Y/N]"
read user_input1
if [ $user_input1 == "Y" ] || [ $user_input1 == "y" ]
then
  echo "### --- ###"
  diff ./whitelist  $new_file
  echo "### --- ###"
  echo "### INPUT ### You may wish to use ^C to exit, then 'cd /etc/grid-security/' to search for a name in the grid-mapfile before updating the whitelist (e.g., ' grep -i emfollow grid-mapfile ')"
fi

echo "### INFO ### New definitive whitelist will be copied to this dir (/etc/grid-security/); old whitelist link will be removed; new file will be linked as 'whitelist'"
echo "         ### Proceed? [Y/N]"
read user_input2
if [ $user_input2 == "Y" ] || [	$user_input2 ==	"y" ]
then
  cp $new_file .
  if [ -e ./whitelist ]; then rm -f ./whitelist; fi
  new_file_local=$(basename $new_file)
  ln -s ./$new_file_local whitelist
  echo "### INFO ### New whitelist link:"
  ls -l --color=auto ./whitelist
  echo "### INFO ### You may wish to run ' lgmm -f ', then re-run your search of the grid-mapfile (if one was run)."
else
  echo "### INFO ### User response: '$user_input2'; exiting"
  exit
fi

exit
diff 
