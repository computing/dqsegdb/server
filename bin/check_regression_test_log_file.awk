#!/bin/awk -f
# Check the log file that is output by run_regression.sh.  Check if any values (DB vs. JSON) don't match each other.  If all match, no output.  If any don't match or log file has other issues, report on that.

# Any other checks we should run?
# here's a way to see the output that either does not have 7 fields or has db_val != json_val:
#   awk 'BEGIN{FS="[; \t\n]+"} {if (NF != 7) {print $0} else {db_val = $5; json_val = $7; if (db_val != json_val) {print $0} } }'
# not sure if we can incorporate that into this script, though


BEGIN{
input_file=ARGV[1]
verbose=0
verbose=ARGV[2]
ARGV[2]=""
#print "verbose = " verbose
total_line_count=0
pass_line_count=0
fail_line_count=0
skip_line_count=0
nonskip_line_count=0
FS="[; \t\n]+"   # this is to match any number of spaces, tabs, newlines, or semicolons (mainly to remove the semicolon on the end of field 5)
min_line_count=7900
max_skip_count=2
min_pass_line_count=7900
min_nonskip_line_count=7900
exit_code=0
}


{   ### main
total_line_count++
if (NF == 7) {
  db_val = $5
  json_val = $7
  if (db_val == json_val) {pass_line_count++}
  else {fail_line_count++}
}
else {skip_line_count++}

}   ### main


END{
if (verbose == 1) {
  print "Total = " total_line_count
  print "pass  = " pass_line_count
  print "fail  = " fail_line_count
  print "skip  = " skip_line_count
}
# check first for any internal failures of this script (exit if any - results are unreliable); then check for failed tests (don't exit yet); then check for formatting and content issues in the log file (then exit)
# check that the total number of lines matches the sum of categorized lines (pass + fail + skip)
if (total_line_count != (pass_line_count + fail_line_count + skip_line_count)) {
  print "### ERROR ### Regression tests: total line count should == pass line count + fail line count + skip line count"
  print "          ### Actual values: " total_line_count " != " pass_line_count " + " fail_line_count " + " skip_line_count " (== " (pass_line_count + fail_line_count + skip_line_count) ")"
  exit 1
}
# check for any failed tests
if (fail_line_count != 0) {
  print "### FAIL ### Regression tests: There were " fail_line_count " failed lines in the log file (and " pass_line_count " passed lines, " skip_line_count " skipped lines, out of " total_line_count " total lines)"
  exit_code = exit_code + 1
}
# check if the total number of lines in the log file is lower than the minimum number of lines expected
if (total_line_count < min_line_count) {
  print "### ERROR ### Regression tests: There were only " total_line_count " lines in the log file (" input_file "), but there should be at least " min_line_count " lines in every log file from 2019.11.18 on.  This should be investigated."
  exit_code = exit_code + 2
}
# check if there are more skipped lines than expected
if (skip_line_count > max_skip_count) {
  print "### WARNING ### Regression tests: There were " skip_line_count " skipped lines in the log file (" input_file "), but there should be at most " max_skip_count " skipped lines.  This should be investigated."
  exit_code = exit_code + 4
}
# check if the number of non-skipped lines is lower than expected
nonskip_line_count = pass_line_count + fail_line_count
if (nonskip_line_count < min_nonskip_line_count) {
  print "### ERROR ### Regression tests: There were only " nonskip_line_count " non-skipped (i.e., pass + fail) lines in the log file (" input_file "), but there should be at least " min_nonskip_line_count " non-skipped lines.  This should be investigated."
  exit_code = exit_code + 8
}
exit exit_code
}





# Sample line in file:
#1                       2 3      4   5             6     7
#/dq/H1/GRD-SQZ_CLF_OK/1 - known; DB: 1234911264.0; JSON: 1234911264.0

# useful awk commands:
# find out how many lines there are with each count of fields:
#   awk '{print NF}' /root/log/regression_tests_2020.03.28* | sort | uniq -c
# see the last few lines in which DB and JSON values don't match:
#   awk 'BEGIN{FS="[; \t\n]+"} {if (NF == 7) {if ($5 != $7) {print $5, $7}}}' /root/log/regression_tests_2020.05.03-04.30.01.txt | tail
# print any non-standard lines (i.e., lines that don't have 7 fields):
#   awk '{if (NF != 7) print $0}' /root/log/regression_tests_2020.05.03-04.30.01.txt 

