#!/bin/bash
# This is a quick script to check a specific DQXML file for certain things and try to diagnose any issues.
# Started by Robert Bruntz on 2023.03.03.
# Options to add:
#   * [maybe don't do - possible confusion if named file exists in current dir] enter just a filename that looks like "[HLGVK]*xml" and the script will figure out which dir it's in and check it
#   * [done] enter just an IFO ([HLVGK] or [HLVGK]1) and the script will figure out the newest file and check it
#   * print command user can use to check the file's whole dir (based on the IFO and the pattern in the filename), using check_dqxml_dir_contents.sh
# Things to check (and their status):
#   * [done] file exists and is not a directory
#   * [done] file is over some minimum length
#   * [done] file meets some minimum standards to verify it's a DQXML file (check for strings that must exist in a DQXML file)
#   * [done] for H1 or L1 file, file contains DMT-UP and GRD-ISC_LOCK_OK flags
#   * [done] for V1 file, file contains DQ_META_STATUS_ITF flag
#   * [done] for G1 file, file contains GEO-SCIENCE flag
#   * for H1 file, file contains at least (minimum number) flags
#   * for L1 file, file contains at least (minimum number) flags
#   * for V1 file, file contains at least (minimum number) flags
# For any of the above, if a check fails, suggest how to fix it, who to notify, etc.


verbose=1
issue_count=0

# check for arguments; if none, print usage
if [ $# -eq 0 ]; then
  echo "Description: Run several checks on input DQXML file.  More info here: https://wiki.ligo.org/Computing/DQSegDBMonitorsAndScripts#check_dqxml_file_contents.sh and in file comments."
  echo "Usage: ' (script name)  (IFO or file)  [verbose level] '"
  echo "Example: ' /root/bin/check_dqxml_file_contents.sh  H  0 ' (find the latest LHO DQXML file and check it; print no output unless checks fail; IFO can be {H,H1,L,L1,V,V1,G,G1})"
  echo "Example: ' /root/bin/check_dqxml_file_contents.sh  /dqxml/H1/H-DQ_Segments-13571/H-DQ_Segments-1357100000-16.xml ' (defaults to verbose=1 = print all output)"
  exit -1
fi
if [ $# -ge 1 ]; then input=$1; fi
if [ $# -ge 2 ]; then verbose=$2; fi

if [ "$verbose" -gt 0 ]; then
  echo "$(date)  - GPS time: ~$((`date +%s` - 315964782))"
  echo -
fi


# get name of file to be analyzed, either from an IFO abbreviation or from the command line
function newest_hlv_file() {
  # find the newest H, L, or V file
  newest_file=""
  newest_dir=`ls -1 /dqxml/${1}1/ | tail -n 1`   # returns the dir *without* /dqxml/[HLV]1/ - so just something like H-DQ_Segments-13571
  newest_file=`ls -1 /dqxml/${1}1/$newest_dir/[HLV]*.xml | tail -n 1`   # returns the full path and file name (e.g., /dqxml/H1/H-DQ_Segments-13571/H-DQ_Segments-1357100000-16.xml)
  if [ "$verbose" -gt 0 ]; then echo "For IFO $input, newest dir = $newest_dir and newest file = $newest_file"; fi
}

case $input in
#  H)  newest_hlv_file H; file=$newest_file;;
  H)  file=$(ls -1 /dqxml/H1/`ls -1 /dqxml/H1/ | tail -n 1`/H*xml | tail -n 1);;
  H1) file=$(ls -1 /dqxml/H1/`ls -1 /dqxml/H1/ | tail -n 1`/H*xml | tail -n 1);;
  L)  file=$(ls -1 /dqxml/L1/`ls -1 /dqxml/L1/ | tail -n 1`/L*xml | tail -n 1);;
  L1) file=$(ls -1 /dqxml/L1/`ls -1 /dqxml/L1/ | tail -n 1`/L*xml | tail -n 1);;
  V)  file=$(ls -1 /dqxml/V1/`ls -1 /dqxml/V1/ | tail -n 1`/V*xml | tail -n 1);;
  V1) file=$(ls -1 /dqxml/V1/`ls -1 /dqxml/V1/ | tail -n 1`/V*xml | tail -n 1);;
  G)  file=$(ls -1 /dqxml/G1_test/`ls -1 /dqxml/G1_test/ | tail -n 1`/G*xml | tail -n 1);;
  G1) file=$(ls -1 /dqxml/G1_test/`ls -1 /dqxml/G1_test/ | tail -n 1`/G*xml | tail -n 1);;
  *) file="$1"
esac
if [ "$verbose" -gt 0 ]; then echo "File = $file"; fi
if [ ! -e "$file" ]; then echo "### ERROR ### File $file does not exist.  Exiting."; exit 1; fi
if [ -d "$file" ]; then echo "### ERROR ### $file is a directory, not a file.  Exiting."; exit 2; fi

if [ "$verbose" -gt 0 ]; then echo "Checking file $file ..."; fi
file_base=`basename $file`
ifo=`echo $file_base | cut -c1`
if [ "$verbose" -gt 0 ]; then echo "IFO = $ifo"; fi


# file is over some minimum length
# expected_line_count is taken from a regular file, then deleting all lines between the <Stream.*> and </Stream> tags
file_char_count=`cat $file | wc --char`
file_line_count=`cat $file | wc --lines`
if [ "$file_char_count" -eq 0 ]
then
  echo "### ISSUE ### File $file is  0 bytes  long."
  let issue_count++
else
  expected_line_count=49
  if [ "$file_line_count" -lt "$expected_line_count" ]
  then
    echo "### ISSUE ### File $file is only $file_line_count lines long, but it should be at least $expected_line_count lines."
    let issue_count++
  fi
fi


# file meets some minimum standards to verify it's a DQXML file (check for strings that must exist in a DQXML file)
function check_string {
  # global variables used by function: $file (file to check); $fn_string (string to search for); $fn_exp_count (expectect count of string); $issue_count (running count tally of issues in $file)
  fn_count=`grep  "$fn_string"  "$file"  |  wc --lines`
  if [ "$fn_count" -ne "$fn_exp_count" ]; then echo "### ISSUE ### $file has $fn_count lines matching '$fn_string'.  Should be $fn_exp_count."; let issue_count++; fi
}
function compare_string_count {
  # global variables used by function: $cmp_string_1, $cmp_string_2 (2 strings to compare counts of); $count_string_1, $count_string_2 (counts of 2 strings to compare)
  if [ "$count_string_1" -ne "$count_string_2" ]; then echo "### ISSUE ### Count of lines matching '$cmp_string_1' ($count_string_1) should match count of lines matching '$cmp_string_2' ($count_string_2), but they don't."; let issue_count++; fi
}

# set up expected values
xml_string='<?xml version="1.0"?>'
doctype_string='<!DOCTYPE LIGO_LW SYSTEM "http://ldas-sw.ligo.caltech.edu/doc/ligolwAPI/html/ligolw_dtd.txt">'
ligolw_on_string="<LIGO_LW>"
ligolw_off_string="</LIGO_LW>"
group_count=4
table_on_string="<Table.*>"
table_off_string="</Table>"
stream_on_string="<Stream.*>"
stream_off_string="</Stream>"
if [ "$ifo" == "G" ]; then
  xml_string="<?xml version='1.0' encoding='utf-8'?>"
  group_count=5
fi

# check xml string
fn_string="$xml_string"; fn_exp_count=1
check_string

# check doctype string
fn_string="$doctype_string"; fn_exp_count=1
check_string

# check ligolw tags (element names)
fn_string="$ligolw_on_string"; fn_exp_count=1
check_string
ligolw_on_count="$fn_count"

fn_string="$ligolw_off_string"; fn_exp_count=1
check_string
ligolw_off_count="$fn_count"

cmp_string_1="$ligolw_on_string"; cmp_string_2="$ligolw_off_string"; count_string_1="$ligolw_on_count"; count_string_2="$ligolw_off_count"
compare_string_count

# check Table tags (element names)
fn_string="$table_on_string"; fn_exp_count="$group_count"
check_string
table_on_count="$fn_count"

fn_string="$table_off_string"; fn_exp_count="$group_count"
check_string
table_off_count="$fn_count"

cmp_string_1="$table_on_string"; cmp_string_2="$table_off_string"; count_string_1="$table_on_count"; count_string_2="$table_off_count"
compare_string_count

# check Stream tags (element names)
fn_string="$stream_on_string"; fn_exp_count="$group_count"
check_string
stream_on_count="$fn_count"

fn_string="$stream_off_string"; fn_exp_count="$group_count"
check_string
stream_off_count="$fn_count"

cmp_string_1="$stream_on_string"; cmp_string_2="$stream_off_string"; count_string_1="$stream_on_count"; count_string_2="$stream_off_count"
compare_string_count


# check for important flags
case $ifo in
  [HL])
    dmt_up_count=`grep DMT-UP $file | wc --lines`
    if [ "$verbose" -gt 0 ]; then echo "DMT-UP count = $dmt_up_count"; fi
    if [ "$dmt_up_count" -lt 1 ]; then echo "### ISSUE ### File does not contain a DMT-UP flag.  (A message about which SegGener or service is having trouble should go here.)"; let issue_count++; fi
    grd_isc_count=`grep GRD-ISC_LOCK_OK $file | wc --lines`
    if [ "$verbose" -gt 0 ]; then echo "GRD-ISC_LOCK_OK count = $grd_isc_count"; fi
    if [ "$grd_isc_count" -lt 1 ]; then echo "### ISSUE ### File does not contain a GRD-ISC_LOCK_OK flag.  (A message about which SegGener or service is having trouble should go here.)"; let issue_count++; fi
    ;;
  [V])
    virgo_itf_count=`grep DQ_META_STATUS_ITF $file | wc --lines`
    if [ "$verbose" -gt 0 ]; then echo "DQ_META_STATUS_ITF count = $virgo_itf_count"; fi
    if [ "$virgo_itf_count" -lt 1 ]; then echo "### ISSUE ### File does not contain a DQ_META_STATUS_ITF flag.  (A message about which SegGener or service is having trouble should go here.)"; let issue_count++; fi
    ;;
  [G])
    geo_science_count=`grep GEO-SCIENCE $file | wc --lines`
    if [ "$verbose" -gt 0 ]; then echo "GEO-SCIENCE count = $geo_science_count"; fi
    if [ "$geo_science_count" -lt 1 ]; then echo "### ISSUE ### File does not contain a GEO-SCIENCE flag.  (A message about which SegGener or service is having trouble should go here.)"; let issue_count++; fi
    ;;
  [K])
    echo "KAGRA files aren't supported yet.";;
  *)
    echo "IFO ('$ifo') wasn't recognized.  Exiting."
    exit 3
    ;;
esac

if [ "$issue_count" -eq 0 ]; then
  if [ "$verbose" -gt 0 ]; then echo "File $file passed all checks."; fi
  exit 0
else
  echo "File $file failed $issue_count checks."
  exit 5
fi

echo -
date

exit 0

if [ "$verbose" -eq 1 ]; then xxx; fi
