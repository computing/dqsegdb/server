#!/bin/bash
# Started by Robert Bruntz on 2020.12.31
# This script takes in a date and prints out commands to publish all 5 KAGRA flags that we publish (as of 2020.12.31).
# If instructed to, it will run the commands to publish, as well.  Doing so will run  /root/bin/publish_kagra_xml_files_updated.sh
# For 202x.12.31, files end up in the wrong year's dir - e.g., K1-GRD_LOCKED/2021/K1-GRD_LOCKED_SEGMENT_UTC_2020-12-31.txt; in that case, run with the optional 3rd argument for the year
#   e.g., for 2021.12.31, run 'time /root/bin/print_kagra_publisher_commands.sh  2021-12-31 1 2022', which will find files like /dqxml/K1_test/K1-OMC_OVERFLOW_OK/2022/K1-OMC_OVERFLOW_OK_SEGMENT_UTC_2021-12-31.xml
# to do
#   * 


year=2022
pause=2
#pause=0
publish=0
exit_early=0

if [ $# -eq 0 ]; then echo "Usage: ' (script)  (date)  (publish flag - optional) ', where 'date' is in YYYY-MM-DD format, and 'publish flag' is 0 or 1"; exit 1; fi
date_in=$1
year=`echo $date_in | cut -c1-4`
if [ $# -gt 1 ]; then publish=$2; fi
if [ $# -gt 2 ] && [ $3 != "-" ]; then year=$3; fi   # this is useful for cases like 2020-12-31, where the files in are in dirs that don't match the file, like  K1-GRD_LOCKED/2021/K1-GRD_LOCKED_SEGMENT_UTC_2020-12-31.txt

echo "Commands to publish KAGRA segments for $date_in (after running 'source /root/virtualenv_p2.7_dqsegdb-1.6.1/bin/activate', probably on -dev):"
echo "Note: You should probably check which server  /root/bin/publish_kagra_xml_files_updated.sh  is set to publish to before running anything. ( `grep ^server= /root/bin/publish_kagra_xml_files_updated.sh` )"
if [ "$publish" -eq 1 ]; then echo "### NOTICE ### These publishing commands *will* be executed."; fi

file1=/dqxml/K1_test/K1-OMC_OVERFLOW_OK/${year}/K1-OMC_OVERFLOW_OK_SEGMENT_UTC_${date_in}.xml
file2=/dqxml/K1_test/K1-OMC_OVERFLOW_VETO/${year}/K1-OMC_OVERFLOW_VETO_SEGMENT_UTC_${date_in}.xml
file3=/dqxml/K1_test/K1-GRD_LOCKED/${year}/K1-GRD_LOCKED_SEGMENT_UTC_${date_in}.xml
file4=/dqxml/K1_test/K1-GRD_SCIENCE_MODE/${year}/K1-GRD_SCIENCE_MODE_SEGMENT_UTC_${date_in}.xml
file5=/dqxml/K1_test/K1-GRD_UNLOCKED/${year}/K1-GRD_UNLOCKED_SEGMENT_UTC_${date_in}.xml

# format: (script name)  (input file name)  (verbose flag)  (save segment files flag)  (publish flag)  (flag name) "(comment)" 
echo "/root/bin/publish_kagra_xml_files_updated.sh  $file1  1  0  1  OMC_OVERFLOW_OK  \"OMC overflow does not happened. K1:FEC-32_ADC_OVERFLOW_0_0 == 0\" "
echo "/root/bin/publish_kagra_xml_files_updated.sh  $file2  1  0  1  OMC_OVERFLOW_VETO  \"OMC overflow happened. K1:FEC-32_ADC_OVERFLOW_0_0 != 0\" "
echo "/root/bin/publish_kagra_xml_files_updated.sh  $file3  1  0  1  GRD_LOCKED  \"GRD-LSC_LOCK_STATE_N == 1000\" "
echo "/root/bin/publish_kagra_xml_files_updated.sh  $file4  1  0  1  GRD_SCIENCE_MODE  \"Observation mode. K1:GRD-IFO_STATE_N == 1000\" "
echo "/root/bin/publish_kagra_xml_files_updated.sh  $file5  1  0  1  GRD_UNLOCKED  \"GRD-LSC_LOCK_STATE_N not_equal 1000\" "

if [ ! -e "$file1" ]; then echo "### ERROR ### File 1 ( $file1 ) does not exist."; exit_early=1; exit_code=2; fi
if [ ! -e "$file2" ]; then echo "### ERROR ### File 2 ( $file2 ) does not exist."; exit_early=1; exit_code=2; fi
if [ ! -e "$file3" ]; then echo "### ERROR ### File 3 ( $file3 ) does not exist."; exit_early=1; exit_code=2; fi
if [ ! -e "$file4" ]; then echo "### ERROR ### File 4 ( $file4 ) does not exist."; exit_early=1; exit_code=2; fi
if [ ! -e "$file5" ]; then echo "### ERROR ### File 5 ( $file5 ) does not exist."; exit_early=1; exit_code=2; fi
if [ "$exit_early" -eq 1 ]; then exit $exit_code; fi


if [ "$publish" -eq 1 ]
then
  echo "Pausing for $pause sec, in case you want to exit before publishing starts."; sleep $pause

  /root/bin/publish_kagra_xml_files_updated.sh  $file1  1  0  1  OMC_OVERFLOW_OK  "OMC overflow does not happened. K1:FEC-32_ADC_OVERFLOW_0_0 == 0"
  exit_code=$?
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Publishing failed (exit code = $exit_code).  This and other remaining publishing commands will need to be run by hand."; exit 3; fi

  /root/bin/publish_kagra_xml_files_updated.sh  $file2  1  0  1  OMC_OVERFLOW_VETO  "OMC overflow happened. K1:FEC-32_ADC_OVERFLOW_0_0 != 0"
  exit_code=$?
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Publishing failed (exit code = $exit_code).  This and other remaining publishing commands will need to be run by hand."; exit 3; fi

  /root/bin/publish_kagra_xml_files_updated.sh  $file3  1  0  1  GRD_LOCKED  "GRD-LSC_LOCK_STATE_N == 1000"
  exit_code=$?
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Publishing failed (exit code = $exit_code).  This and other remaining publishing commands will need to be run by hand."; exit 3; fi

  /root/bin/publish_kagra_xml_files_updated.sh  $file4  1  0  1  GRD_SCIENCE_MODE  "Observation mode. K1:GRD-IFO_STATE_N == 1000"
  exit_code=$?
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Publishing failed (exit code = $exit_code).  This and other remaining publishing commands will need to be run by hand."; exit 3; fi

  /root/bin/publish_kagra_xml_files_updated.sh  $file5  1  0  1  GRD_UNLOCKED  "GRD-LSC_LOCK_STATE_N not_equal 1000"
  exit_code=$?
  if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Publishing failed (exit code = $exit_code).  This and other remaining publishing commands will need to be run by hand."; exit 3; fi

  echo "###"
  echo "### INFO ### All publishing commands succeeded!"
fi
exit 0
