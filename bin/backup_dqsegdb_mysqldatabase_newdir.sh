#!/bin/bash

# Set backup date.
backup_date=$(date +%Y-%m-%d)
start_date=$(date +%Y.%m.%d)
start_time=$(date +%H:%M:%S)
start_sec=$(date -u +"%s")

# Set backup export start time.
backup_export_start_time=$(date +'%H:%M:%S')
echo "### INFO ### Backup started on $(uname -n) on  $backup_date  at  $backup_export_start_time"
# Insert backup export start date/time.
mysql -h segments-backup.ligo.org -u dqsegdb_backup -pdqsegdb_backup_pw -e "INSERT INTO dqsegdb_regression_tests.tbl_backups (backup_date, export_time_start) VALUES ('${backup_date}', '${backup_export_start_time}')"

#mysql_dir=/backup/segdb/segments/tmp/mysql_dump_seg_$(date +%Y.%m.%d-%H.%M.%S)
mysql_dir=/local/ldbd/tmp/mysql_dump_seg_$(date +%Y.%m.%d-%H.%M.%S)
echo "### INFO ### Temp dir: $mysql_dir"
mkdir -p $mysql_dir
if [ ! -d $mysql_dir ]; then echo "### ERROR ### Temp dir ($mysql_dir) was not created!  This should be investigated and fixed.  Script will exit."; echo "          ### Usage on /backup/:"; df -h /backup/; exit 1; fi
chmod a+rwx $mysql_dir
echo "### INFO ### Starting mysqldump.   $(date)"
time mysqldump --single-transaction -u root dqsegdb --tab=$mysql_dir
echo "### INFO ### mysqldump completed.  Starting tarballing of DB backup.   $(date)"
cd $mysql_dir
date_string=$(date +%y-%m-%d)
tar -zcvf "${date_string}.tar.gz" *
error_code=$?
if [ $error_code -eq 0 ]
then
  echo "### INFO ### Tarballing of DB backup complete.   $(date)"
else
  echo "### ERROR ### There was an error in the tarballing of the DB backup in $mysql_dir (error code = $error_code).  You should figure out why and fix it.  Script will exit.   $(date)"
  echo "          ### Usage on /local/:"
  df -h /local/
  exit 1
fi
cp -p ${mysql_dir}/${date_string}.tar.gz /backup/segdb/segments/primary/${date_string}.tar.gz
if [ -s /backup/segdb/segments/primary/${date_string}.tar.gz ]   # "-s" = file exists and is size greater than zero
then
  # Found the tarball, so we can remove the mysql_dump temp directory
  echo "### INFO ### Tarballed DB (/backup/segdb/segments/primary/${date_string}.tar.gz) found.  Deleting temp dir.   $(date)"
  cd ${HOME}
  rm -rf $mysql_dir
  ls -l /backup/segdb/segments/primary/${date_string}.tar.gz
  ls -lh /backup/segdb/segments/primary/${date_string}.tar.gz
  # rsync to backup/segdb/segments!
  #rsync /backup/segdb/segments/primary/* 10.14.0.105::dqsegdb
else
  echo "### ERROR ### Tarballed DB (${date_string}.tar.gz) not found in dir /backup/segdb/segments/primary/.  You should investigate what happened and fix it.  Script will exit.   $(date)"
  echo "          ### Usage on /backup/:"
  df -h /backup/
  echo "          ### File in temp dir:"
  ls -l ${mysql_dir}/${date_string}.tar.gz
  exit 1
fi

# Set backup export end time.
backup_export_stop_time=$(date +'%H:%M:%S')
# Add backup export end date/time.
mysql -h segments-backup.ligo.org -u dqsegdb_backup -pdqsegdb_backup_pw -e "UPDATE dqsegdb_regression_tests.tbl_backups SET export_time_stop = '${backup_export_stop_time}' WHERE backup_date LIKE '${backup_date}%'"

echo "### INFO ### Backup finished on  $backup_date  at  $backup_export_stop_time"
end_time=$(date +%H:%M:%S)
end_sec=$(date -u +"%s")
duration_sec=$((end_sec - start_sec))

# changing format to show start date and time, end time, and duration, rather than just end time (with start time having to be known)
#echo $backup_date $backup_export_stop_time >> /backup/segdb/segments/monitor/backup_dqsegdb.txt
echo "$start_date  $start_time - $end_time = $duration_sec sec" >> /backup/segdb/segments/monitor/backup_dqsegdb.txt

