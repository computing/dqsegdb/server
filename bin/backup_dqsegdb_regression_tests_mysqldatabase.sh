#!/bin/bash

echo "### INFO ### Starting dqsegdb_regression_tests DB backup:  " `date`
# Set backup date.
backup_date=$(date +%Y-%m-%d)
start_date=$(date +%Y.%m.%d)
start_time=$(date +%H:%M:%S)
start_sec=$(date -u +"%s")

# Set backup export start time.
backup_export_start_time=$(date +'%H:%M:%S')
# Insert backup export start date/time.
#mysql -h segments-backup.ligo.org -u dqsegdb_backup -pdqsegdb_backup_pw -e "INSERT INTO dqsegdb_regression_tests.tbl_backups (backup_date, export_time_start) VALUES ('${backup_date}', '${backup_export_start_time}')"

mkdir /backup/segdb/segments/tmp/mysql_dump
chmod a+rwx /backup/segdb/segments/tmp/mysql_dump
#time mysqldump --single-transaction -u root dqsegdb --tab=/backup/segdb/segments/tmp/mysql_dump
time mysqldump --single-transaction -u root dqsegdb_regression_tests --tab=/backup/segdb/segments/tmp/mysql_dump
cd /backup/segdb/segments/tmp/mysql_dump
tar -zcvf  /backup/segdb/segments/regression_tests/dqsegdb_regression_tests_backup.tgz  *
if [ ! -e /backup/segdb/segments/regression_tests/dqsegdb_regression_tests_backup.tgz ]; then
  echo "Didn't find my tar'd file: raise error!" 1>&2
  exit 1
else
  # Found the tarball, so we can remove the mysql_dump temp directory
  cd ${HOME}
  rm -rf /backup/segdb/segments/tmp/mysql_dump
  # rsync to backup/segdb/segments!
  #rsync /backup/segdb/segments/primary/* 10.14.0.105::dqsegdb
fi

# Set backup export end time.
backup_export_stop_time=$(date +'%H:%M:%S')
# Add backup export end date/time.
#mysql -h segments-backup.ligo.org -u dqsegdb_backup -pdqsegdb_backup_pw -e "UPDATE dqsegdb_regression_tests.tbl_backups SET export_time_stop = '${backup_export_stop_time}' WHERE backup_date LIKE '${backup_date}%'"

end_time=$(date +%H:%M:%S)
end_sec=$(date -u +"%s")
duration_sec=$((end_sec - start_sec))
#echo $(date +%Y-%m-%d) $(date +'%H:%M:%S') >> /backup/segdb/segments/monitor/regression_test_backup.txt
echo "$start_date  $start_time - $end_time = $duration_sec sec" >> /backup/segdb/segments/monitor/regression_test_backup.txt

echo "### INFO ### Finished dqsegdb_regression_tests DB backup:  " `date`
