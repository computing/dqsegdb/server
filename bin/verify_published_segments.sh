#!/bin/bash
# This is a script that will run other scripts and commands and compare some files, to verify that the segments in some DQXML files match the corresponding segments in DQSegDB.
# This script was started by Robert Bruntz on 2019.04.22
# It is expected that the script will be run by a cron job on segments.ligo.org
# Run as: '/root/bin/verify_published_segments.sh'   - that's all; uses the current GPS time to decide what range of times to compare
#     or: '/root/bin/verify_published_segments.sh  [verbose]'   - to specify verbosity level (0 = only output standard files; 1 = lots of output)
#     or: '/root/bin/verify_published_segments.sh  [verbose]  [gps_start]  [gps_end]'   - to specify verbosity level and what the starting and ending GPS times should be; note that currently *no sanity checking is done* for user-supplied GPS times
# Process:
#   * Starting and ending GPS times are chosen, starting a certain number of minutes in the past and continuing for a certain number of minutes
#   * An 'extract_dqxml_segments' script is run for each IFO, for certain flags for each, using the start and end GPS times, producing output files for each IFO/flag combination
#   * A DQSegDB query is run for each IFO/flag combination, using the the start and end GPS times, producing output files for each
#   * The output files from the DQXML file analysis and the DQSegDB query are compared, and a final output file is produced and saved in a location where a remote Nagios process can find it
#   * A remote Nagios process grabs the final output file, parses it, and displays the results on monitor.ligo.org / dashboard
# to do:
#   * set up calls for V, G IFOs (i.e., fill in function that populates file list)
#   * improve FAIL and UNKNOWN messages printed in JSON file by 'compare_dqxml_to_db.awk'
#   * create some sanity checks on user-supplied GPS times (e.g., both are given; both are numbers; both are in sane ranges (1*10^6 - 2*10^6?); grab the smaller for start, larger for end, regardless of order, etc.)
#   * modify the code so that it runs for only a single, user-specified IFO?  (easier to test specific IFOs; can run at different cadences; etc.)



# Setup variables
verbose=0
if [ "$1" == 1 ]; then verbose=1; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### 'verify_published_segments' started:  " $(date); fi

#backtrack_start=$((60*10))   ### this is the number of seconds before present time to go back, for the start of the period of interest
backtrack_start=$((60*15))   ### this is the number of seconds before present time to go back, for the start of the period of interest; changed from 10 min to 15 min, due to lots of errors after changing to 5 min publishing cadence in Oct. 2020
backtrack_duration=$((60*5))   ### this is the number of seconds which will comprise the period of interest
db_buffer=$((60*6))   # the DB query will only return results where the start *and* end time are within the query time, so we have to query outside any possible publishing boundary; 
   # publishing is currently done every minute, but we used to do it every 5 minutes, so we'll make our buffer 6 minutes (before and after the start and end times of interest); if we later coalesce the DB this might be a problem
output_dir=/tmp/monitor_logs
output_dir2=/root/bin/monitor_files
nagios_dir=/var/www/nagios
#nagios_dir=/tmp/
tmp_dir=/tmp/monitor_logs
if [ "$3" == "" ]   # this block is only executed if there are no command-line GPS times, so that we don't report on and test values that won't be used
then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Backtrack start = $backtrack_start"; echo "### INFO ### Backtrack duration = $backtrack_duration"; fi
  if [ $backtrack_start -le $backtrack_duration ]; then
    echo "### ERROR ### The backtrack start time ( $backtrack_start seconds) is less than the backtrack duration ( $backtrack_duration seconds), which would require data from the future."
    echo "          ### Please fix the backtrack_start and/or backtrack_duration variable in verify_published_segments.sh, then re-run the script."
    exit
  fi
# Get starting and ending GPS times
  gps_current_time=$((`date +%s` - 315964782))
  gps_start_time=$((gps_current_time - backtrack_start))
  gps_end_time=$((gps_start_time + backtrack_duration))
fi
if [ "$3" != "" ]   # this gives the option to pass in start and end times from the command line; note that these values are not tested at all
then
  if [ $2 -lt $3 ]; then gps_start_time=$2; gps_end_time=$3; fi
  if [ $2 -gt $3 ]; then gps_start_time=$3; gps_end_time=$2; fi
  if [ $2 -eq $3 ]; then echo "### ERROR ### GPS start and end times are the same ( $2 ).  This is not a valid run.  Exiting."; exit; fi
fi
if [ $verbose -eq 1 ]; then echo "### INFO ### GPS start time = $gps_start_time"; echo "### INFO ### GPS end time = $gps_end_time"; fi


#if [ 0 -eq 1 ]; then
# Run command to extract segments from DQ XML files
# command format: [extract_dqxml_segments script name]  [ifo]  [flag]  [GPS start time]  [GPS end time]  [verbose flag (0=off, 1=on)]
/root/bin/extract_dqxml_segments.awk  H  DMT-ANALYSIS_READY  $gps_start_time  $gps_end_time  1  >  $output_dir/dqxml_h1_694_${gps_start_time}_${gps_end_time}.txt
/root/bin/extract_dqxml_segments.awk  H  GRD-ISC_LOCK_OK     $gps_start_time  $gps_end_time  1  >  $output_dir/dqxml_h1_1462_${gps_start_time}_${gps_end_time}.txt
/root/bin/extract_dqxml_segments.awk  L  DMT-ANALYSIS_READY  $gps_start_time  $gps_end_time  1  >  $output_dir/dqxml_l1_693_${gps_start_time}_${gps_end_time}.txt
/root/bin/extract_dqxml_segments.awk  L  GRD-ISC_LOCK_OK     $gps_start_time  $gps_end_time  1  >  $output_dir/dqxml_l1_1459_${gps_start_time}_${gps_end_time}.txt
/root/bin/extract_dqxml_segments.awk  V  ITF_SCIENCE         $gps_start_time  $gps_end_time  1  >  $output_dir/dqxml_v1_1265_${gps_start_time}_${gps_end_time}.txt
/root/bin/extract_dqxml_segments.awk  G  GEO-UP              $gps_start_time  $gps_end_time  1  >  $output_dir/dqxml_g1_1284_${gps_start_time}_${gps_end_time}.txt
#fi


# Run MariaDB query of DQSegDB
# commands will look something like this:
# mysql -e "use dqsegdb; select * from tbl_segments where dq_flag_version_fk = "693" and segment_start_time >= "1123500000" and segment_stop_time <= "1124000000";" > output_file.txt
# for known segments, replace "tbl_segments" above with "tbl_segment_summary"
# DMT-ANALYSIS_READY = 693 for L1, 694 for H1
# GRD-ISC_LOCK_OK = 1459 for L1, 1462 for H1
# ITF_SCIENCE = 1265 for V1
# GEO-UP = 1284 for G1
db_start_time=$((gps_start_time - db_buffer))
db_end_time=$((gps_end_time + db_buffer))
id=693; ifo=l1
mysql -e "use dqsegdb; select * from tbl_segments where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_active_${gps_start_time}_${gps_end_time}.txt
mysql -e "use dqsegdb; select * from tbl_segment_summary where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_known_${gps_start_time}_${gps_end_time}.txt
id=694; ifo=h1
mysql -e "use dqsegdb; select * from tbl_segments where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_active_${gps_start_time}_${gps_end_time}.txt
mysql -e "use dqsegdb; select * from tbl_segment_summary where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_known_${gps_start_time}_${gps_end_time}.txt
id=1459; ifo=l1
mysql -e "use dqsegdb; select * from tbl_segments where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_active_${gps_start_time}_${gps_end_time}.txt
mysql -e "use dqsegdb; select * from tbl_segment_summary where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_known_${gps_start_time}_${gps_end_time}.txt
id=1462; ifo=h1
mysql -e "use dqsegdb; select * from tbl_segments where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_active_${gps_start_time}_${gps_end_time}.txt
mysql -e "use dqsegdb; select * from tbl_segment_summary where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_known_${gps_start_time}_${gps_end_time}.txt
id=1265; ifo=v1
mysql -e "use dqsegdb; select * from tbl_segments where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_active_${gps_start_time}_${gps_end_time}.txt
mysql -e "use dqsegdb; select * from tbl_segment_summary where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_known_${gps_start_time}_${gps_end_time}.txt
id=1284; ifo=g1
mysql -e "use dqsegdb; select * from tbl_segments where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_active_${gps_start_time}_${gps_end_time}.txt
mysql -e "use dqsegdb; select * from tbl_segment_summary where dq_flag_version_fk = `echo $id` and segment_start_time >= `echo $db_start_time` and segment_stop_time <= `echo $db_end_time`;" \
    > $output_dir/dqsegdb_${ifo}_${id}_known_${gps_start_time}_${gps_end_time}.txt


verbose_temp=$verbose
verbose=1
# Compare DQXML results to MariaDB results and produce output file
# ./compare_dqxml_to_db.awk  L1  693  1459  1239990600  1239992000  1  /tmp/verify_published_l1.txt
ifo=h1; id1=694; id2=1462
#/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_H1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_log.txt
/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_H1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_${gps_start_time}_${gps_end_time}_log.txt
ifo=l1; id1=693; id2=1459
#/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_L1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_log.txt
/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_L1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_${gps_start_time}_${gps_end_time}_log.txt
ifo=v1; id1=1265; id2=1265
#/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_V1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_log.txt
/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_V1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_${gps_start_time}_${gps_end_time}_log.txt
ifo=g1; id1=1284; id2=1284
#/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_G1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_log.txt
/root/bin/compare_dqxml_to_db.awk  $ifo  $id1  $id2  $gps_current_time  $gps_start_time  $gps_end_time  $verbose  $nagios_dir/verify_published_segments_G1.json  &>  /$tmp_dir/verify_published_segments_${ifo}_${gps_start_time}_${gps_end_time}_log.txt
verbose=$verbose_temp

# check for failed tests; if found, save all files from this time range for possible later analysis
fail_count=`grep -l -i fail $tmp_dir/verify_published_segments_*_${gps_start_time}_${gps_end_time}_log.txt  |  wc --lines`
if [ "$fail_count" -gt 0 ]; then cp -pn  $tmp_dir/*${gps_start_time}_${gps_end_time}*  $output_dir2/; fi



if [ $verbose -eq 1 ]; then echo "### INFO ### 'verify_published_segments' finished:  " $(date); fi



exit

# testing:
# * run all 4 in default mode, as cron jobs
# * change file permissions on a DQXML file that falls in the search range, to force a FAIL (and verify that it does)
# * run with a 1-second (start, end) interval
# * run with other (valid) flags
# * run with invalid flags
# * (make sure that no flags found and no segments found both still produce an output file even if it's empty; make sure that that output file still works in the process
#     (e.g., for invalid flags, no segments are found in either DQ XML files or DB; results match, so PASS)

