#!/bin/bash
### This is a hacked-together script to pull dqxml files from remote sites.  Originally it was a stop-gap while dqxml_pull_from_obs was being worked on, but that might never come back now, so this is being further patched.
### Created by Robert Bruntz on ~2019.03.06 and modified thereafter.
### Run with something like: " nohup  /root/bin/manual_rsync_LLO.sh  >>  /root/bin/manual_rsync_LLO_log_$(date +%Y.%m.%d).txt  & "
### 2023.04.18 - replaced ldas-llo.ligo-la.caltech.edu with llodmt.ligo-la.caltech.edu due to change from DQXML files being pushed from DMT to CIT to being pulled by CIT from DMT/GDS
### To-do and possible issues
###   * Small chance that the dir rollover could happen between pulling the latest files and checking for a newer dir, so last files in the old dir aren't ever pulled
###   * 'current_lho' et al. are set to fixed values at the start, so if not set to current (e.g., if a dir rollover has happened), when run new, will always check an old (presumably sync'd) dir
###   * No logging of any kind at this point; maybe redirect output to a file?

echo
echo "### INFO ### Starting manual_rsync.sh: $(date)"
sleep_length=45
run_count=5
current_lho=H-DQ_Segments-13664
#current_llo=L-DQ_Segments-13664
current_llo=`cd /dqxml/L1/; ls -1 | tail -n 1`
current_vgo=V-DQ_Segments-13664
current_geo=G-DQ_Segments-13664
timeout_val=50

while [ 1 -eq 1 ]
do

# a cron job writes a 1 to this file every minute; this while loop idles as long as the file contains a 0 (actually, anything but 1), then lets the rest of the script run when the file contains a 1, but also writes a 0 to the file,
#   so that if the script finishes in under 1 minute, it just idles until the next time cron writes a 1 to the file; also, if an rsync command times out, that command writes a 1, so the script will run again immediately
while [ `cat /root/bin/start_manual_rsync_LLO.txt` -eq 0 ]; do sleep 5; done
echo 0 > /root/bin/start_manual_rsync_LLO.txt

# start by pulling any new files
echo "### INFO ### Starting pull loop (run_count=$run_count):  $(date)"

# these lines of code (mostly) pulled from the end of the script and modified, to make sure all files have been pulled from the old dir before switching to the new dir
# this detects a new dir but doesn't switch the dir to be pulled to to the new one until pulling to the old one is done
new_dir=0   # track if a new dir was found remotely and created locally
if [ $run_count -ge 5 ]
then
  echo "### INFO ### Checking for new remote directories"
  latest_llo_remote=`rsync --list-only --exclude "*.tar.gz" --include "L-DQ_Segments-*" --exclude "*" --timeout=$timeout_val llodmt.ligo-la.caltech.edu::dqxml/ | grep L-DQ_Segments-* | awk '{print $NF}' | sort | tail -n 1`
  latest_llo_local=`cd /dqxml/L1/; ls -1 | tail -n 1`
  if [ $latest_llo_remote != $latest_llo_local ]; then mkdir /dqxml/L1/$latest_llo_remote ; chown dqxml:dqxml /dqxml/L1/$latest_llo_remote;  echo "### INFO ### Made new dir: /dqxml/L1/$latest_llo_remote"; new_dir=1; fi
  run_count=0   # reset the run count, so we don't check this again for (run_count) loops
fi

#echo "### INFO ### Pulling LHO files for $current_lho"
#rsync -av --progress --timeout=$timeout_val  ldas.ligo-wa.caltech.edu::dqxml/$current_lho/  /dqxml/H1/$current_lho/  2>&1 | tee /tmp/rsync_lho_output.txt
#if [ `grep -ci error /tmp/rsync_lho_output.txt` -ne 0 ]; then echo 1 > /root/bin/start_manual_rsync.txt; echo "### WARNING ### rsync to LHO produced an error.  Re-running manual_rsync.sh immediately after this loop."; fi
echo "### INFO ### Pulling LLO files for $current_llo"
#rsync -av --progress --timeout=$timeout_val  ldas-llo.ligo-la.caltech.edu::dqxml/$current_llo/  /dqxml/L1/$current_llo/  2>&1 | tee /tmp/rsync_llo_output.txt
rsync -av --chown=dqxml:dqxml --progress --timeout=$timeout_val  --exclude '*.tar.gz'  --exclude '*scrunch*'  --exclude '\.H*'  --exclude '\.L*'  llodmt.ligo-la.caltech.edu::dqxml/$current_llo/  /dqxml/L1/$current_llo/  2>&1 | tee /tmp/rsync_llo_output.txt
if [ `grep -ci error /tmp/rsync_llo_output.txt` -ne 0 ]; then echo 1 > /root/bin/start_manual_rsync_LLO.txt; echo "### WARNING ### rsync to LLO produced an error.  Re-running manual_rsync.sh immediately after this loop."; fi
#echo "### INFO ### Pulling Virgo files for $current_vgo"
#rsync -av --progress --timeout=$timeout_val  dqsegments.virgo.infn.it::dqxml/$current_vgo/  /dqxml/V1/$current_vgo/  2>&1 | tee /tmp/rsync_vgo_output.txt
#if [ `grep -ci error /tmp/rsync_vgo_output.txt` -ne 0 ]; then echo 1 > /root/bin/start_manual_rsync.txt; echo "### WARNING ### rsync to VGO produced an error.  Re-running manual_rsync.sh immediately after this loop."; fi
#echo "### INFO ### Pulling GEO files for $current_geo"
#rsync -av --progress --timeout=$timeout_val  rsync://130.75.117.72/dqxml/$current_geo/      /dqxml/G1/$current_geo/  2>&1 | tee /tmp/rsync_geo_output.txt
#if [ `grep -ci error /tmp/rsync_geo_output.txt` -ne 0 ]; then echo 1 > /root/bin/start_manual_rsync.txt; echo "### WARNING ### rsync to GEO produced an error.  Re-running manual_rsync.sh immediately after this loop."; fi
echo "### INFO ### Ending pull loop: $(date)"

# new part to handle new dir; having just pulled all files to the old dir, set the current dir to the new dir, then run the rsync loop again immediately
if [ "$new_dir" -eq 1 ]; then
  current_llo=$latest_llo_remote
  echo 1 > /root/bin/start_manual_rsync_LLO.txt
  new_dir=0
  run_count=1   # reset the run count, so we don't check this again for (run_count) loops
  continue   # if there's a new dir, there are probably files in it that we haven't pulled, so go right to doing that, without the 'sleep'
fi

# old section; some of the code from here was moved to earlier in the loop, and this part was sabotaged to never run
# after pulling new files for (run_count) loops, check to see if there's a new run dir (do this after pulling b/c we don't want to miss the last files in a dir - which could still happen, but very seldom)
# process: get the newest remote dir names and newest local dir names (last part of dir name, not full dir name); if different, create new local dir to match remote and set that as the new current dir for rsync (local and remote)
#if [ $run_count -ge 5 ]
if [ $run_count -ge 5000 ]
then
  echo "### INFO ### Checking for new remote directories"
#  latest_lho_remote=`rsync --list-only --exclude "*.tar.gz" --include "H-DQ_Segments-*" --exclude "*" --timeout=$timeout_val ldas.ligo-wa.caltech.edu::dqxml/ | grep H-DQ_Segments-* | awk '{print $NF}' | sort | tail -n 1`
#  latest_llo_remote=`rsync --list-only --exclude "*.tar.gz" --include "L-DQ_Segments-*" --exclude "*" --timeout=$timeout_val ldas-llo.ligo-la.caltech.edu::dqxml/ | grep L-DQ_Segments-* | awk '{print $NF}' | sort | tail -n 1`
  latest_llo_remote=`rsync --list-only --exclude "*.tar.gz" --include "L-DQ_Segments-*" --exclude "*" --timeout=$timeout_val llodmt.ligo-la.caltech.edu::dqxml/ | grep L-DQ_Segments-* | awk '{print $NF}' | sort | tail -n 1`
#  latest_vgo_remote=`rsync --list-only --exclude "*.tar.gz" --include "V-DQ_Segments-*" --exclude "*" --timeout=$timeout_val dqsegments.virgo.infn.it::dqxml/ | grep V-DQ_Segments-* | awk '{print $NF}' | sort | tail -n 1`
#  latest_geo_remote=`rsync --list-only --exclude "*.tar.gz" --include "G-DQ_Segments-*" --exclude "*" --timeout=$timeout_val rsync://130.75.117.72/dqxml/ | grep G-DQ_Segments-* | awk '{print $NF}' | sort | tail -n 1`
#  latest_lho_local=`cd /dqxml/H1/; ls -1 | tail -n 1`
  latest_llo_local=`cd /dqxml/L1/; ls -1 | tail -n 1`
#  latest_vgo_local=`cd /dqxml/V1/; ls -1 | tail -n 1`
#  latest_geo_local=`cd /dqxml/G1/; ls -1 | tail -n 1`
#  if [ $latest_lho_remote != $latest_lho_local ]; then mkdir /dqxml/H1/$latest_lho_remote ; echo "### INFO ### Made new dir: /dqxml/H1/$latest_lho_remote"; current_lho=$latest_lho_remote; echo 1 > /root/bin/start_manual_rsync.txt; fi
  if [ $latest_llo_remote != $latest_llo_local ]; then mkdir /dqxml/L1/$latest_llo_remote ; chown dqxml:dqxml /dqxml/L1/$latest_llo_remote; \
    echo "### INFO ### Made new dir: /dqxml/L1/$latest_llo_remote"; current_llo=$latest_llo_remote; echo 1 > /root/bin/start_manual_rsync_LLO.txt; fi
#  if [ $latest_vgo_remote != $latest_vgo_local ]; then mkdir /dqxml/V1/$latest_vgo_remote ; echo "### INFO ### Made new dir: /dqxml/V1/$latest_vgo_remote"; current_vgo=$latest_vgo_remote; echo 1 > /root/bin/start_manual_rsync.txt; fi
#  if [ $latest_geo_remote != $latest_geo_local ]; then mkdir /dqxml/G1/$latest_geo_remote ; echo "### INFO ### Made new dir: /dqxml/G1/$latest_geo_remote"; current_geo=$latest_geo_remote; echo 1 > /root/bin/start_manual_rsync.txt; fi
  # this next part accounts for if the script was started in an old dir, but the new dir has already been created (so the commands in the if's above aren't activated)
#  if [ $current_lho != $latest_lho_local ]; then current_lho=$latest_lho_local; fi
  if [ $current_llo != $latest_llo_local ]; then current_llo=$latest_llo_local; fi
#  if [ $current_vgo != $latest_vgo_local ]; then current_vgo=$latest_vgo_local; fi
#  if [ $current_geo != $latest_geo_local ]; then current_geo=$latest_geo_local; fi
  run_count=1   # reset the run count, so we don't check this again for (run_count) loops
  continue   # if there's a new dir, there are probably files in it that we haven't pulled, so go right to doing that, without the 'sleep'
fi

#echo "### INFO ### Sleep for $sleep_length seconds..."
#sleep $sleep_length
echo "### INFO ### Waiting for signal to re-run the rsync loop..."
run_count=$(($run_count+1))
done

exit


# rsync --list-only --timeout=32 ldas.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-12358/
# rsync --list-only --timeout=32 ldas-lho.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-12358/
# rsync --list-only --timeout=32 lhodmt.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-12358/
# rsync --list-only --timeout=32 ldas.ligo-la.caltech.edu::dqxml/L-DQ_Segments-12358/
# rsync --list-only --timeout=32 ldas-llo.ligo-la.caltech.edu::dqxml/L-DQ_Segments-12358/
# rsync --list-only --timeout=32 llodmt.ligo-la.caltech.edu::dqxml/L-DQ_Segments-12358/
# rsync --list-only --timeout=32 dqsegments.virgo.infn.it::dqxml/
# rsync --list-only --timeout=32 rsync://130.75.117.72/dqxml/G-DQ_Segments-12361
