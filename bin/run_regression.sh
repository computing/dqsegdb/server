#!/bin/bash
# Started by Robert Bruntz, 2019.11.13
# This is a script to run the DB regression tests, but send the output to a log file, then check whether the regression test suite ran successfully (whether it found issues in the DB or not) and whether there were issues in the DB.
#   In either of those cases, print some run info to std out and echo the log file, so all of that can be emailed to admins by a cron job.  If everything worked and there are no issues in the DB, nothing is sent (but the log file is still available).
# The commands to run the Python script are from the old version, available as run_regression.sh_2019.11.18.bak
# The main benefits of this method are:
#   1. The status of the process can be monitored in real time, by viewing the log file
#   2. The log file is persistent, so it can be easily read anytime, rather than having to search through emails (which might not be available at a future date)
#   3. If the script crashes for any reason (incl. the machine being rebooted, hard drive filling up, etc.), the incomplete log file is available, whereas a cron job only sends an email at the end of the job
#   4. The output of the script is captured, even if run from the command line (vs. only cron jobs have their output emailed)
#   5. We can do some searching, filtering, post-processing of the log file, if desired
#   6. Probably other reasons, too
# This script is run from a cron job, like this:   30 03 * * * /root/bin/run_regression.sh
# The output log file can be analyzed separately, with no side effects, since the output is only printed to the screen, e.g.:
#   # log_file=/root/log/regression_tests_2020.10.27-03.30.01.txt
#   # /root/bin/check_regression_test_log_file.awk  $log_file
# Find lines without 7 fields in them (should only be 2 per file):
#   # awk '{if (NF != 7) print $0}'  $log_file
# Find lines where the expected values don't match (db_val != json_val) (should only be 2 lines, with atypical text in them):
#   # awk 'BEGIN{FS="[; \t\n]+"} {if (NF != 7) {print $0} else {db_val = $5; json_val = $7; if (db_val != json_val) {print $0} } }'  $log_file
# To do (maybe):
#   * Verify that the log dir is writable by the user running this script
#   * [done, in the 'check_regression_test_log_file.awk' script] Add code to check the log file for errors; if found, do something with that info


# set up variables
start_date=$(date +%Y.%m.%d)
start_time=$(date +%H.%M.%S)
exit_code1=-1   # set to -1 so it's obvious if it never gets changed
exit_code2=-1   # set to -1 so it's obvious if it never gets changed
log_dir=/root/regression_tests_logs
log_file=${log_dir}/regression_tests_${start_date}-${start_time}.txt
#echo "### INFO ### DB regression tests started on $(date +%Y-%m-%d) at $(date +%H:%M:%S).  Log file = $log_file"

# run the script, with std out and std err redirected to the log file
# note that "&>>" is used by Bash 4+; if used on another system, older Bash, etc., replace with ">>$log_file 2>>$log_file" - https://stackoverflow.com/questions/876239/how-to-redirect-and-append-both-stdout-and-stderr-to-a-file-with-bash
cd /opt/dqsegdb/regression_test_suite/src
python3 main.py  &>> $log_file
exit_code1=$?
end_date=$(date +%Y.%m.%d)
end_time=$(date +%H.%M.%S)

# check if the regression test suite ran successfully; if so, don't print anything, save date and time to the monitor file, and move on to the next block of code; if not, print output to std out (to be emailed) and exit
### note that failure of the regression test script to run is NOT the same as some of the tests in the script failing; we check for the former here and the latter in the next block of code
#if [ $exit_code1 -eq 0 ]; then echo "### INFO ### Script run completed successfully.  Next, echoing contents of log file. ( $(date) )"; echo $(date +%Y-%m-%d) $(date +'%H:%M:%S') >> /backup/segdb/segments/monitor/run_regression.txt; fi
if [ $exit_code1 -ne 0 ]; then
  echo "### ERROR ### Regression test script failed to run properly!  (exit code = $exit_code1)"   # this will be the first line that shows up in an email
  echo "### INFO ### DB regression tests started on $start_date at $start_time.  Log file = $log_file"
  echo "### INFO ### DB regression tests finished on $end_date at $end_time."
  echo "### INFO ### Running the file analysis on the log file ('/root/bin/check_regression_test_log_file.awk $log_file')"
  /root/bin/check_regression_test_log_file.awk $log_file
  echo "### INFO ### Echoing contents of log file. ( $(date) )"   ###; echo $(date +%Y-%m-%d) $(date +'%H:%M:%S') >> /backup/segdb/segments/monitor/run_regression.txt   # we don't record failed runs of this script in the monitor text file
  echo "         ###"
  # print the log file, so it will be sent in the cron job email (same as it would be if the messages were reported to the cron job in real time)
  cat $log_file
  exit $exit_code1
else
  # successful run of the regression tests (regardless of results of those tests) is noted in this monitor file
  echo $end_date $end_time >> /backup/segdb/segments/monitor/run_regression.txt
fi

# if we get this far, the regression test script ran successfully; check for failures of the tests run by the script and other issues (too few lines in log file, etc. - all done in the awk script)
# if all tests passed, there will be no output from the script and the exit code will be 0; if there are any issues, there will be a message printed to std out (which will be emailed by cron) and exit code will be non-zero
#/root/bin/check_regression_test_log_file.awk  $log_file  1   # this version of the command (arg 2 = "1") will print out total, pass, fail, and skip counts, regardless of whether there are any fails or other issues
/root/bin/check_regression_test_log_file.awk  $log_file
exit_code2=$?
if [ "$exit_code2" -ne 0 ]; then
  echo "### INFO ### DB regression tests started on $start_date at $start_time.  Log file = $log_file"
  echo "### INFO ### DB regression tests finished on $end_date at $end_time."
  echo "### ERROR ### Issues were found in output from the regression tests.  (exit code = $exit_code2)"
  echo "          ### Run this to get analysis results again: ' /root/bin/check_regression_test_log_file.awk  $log_file  1 ' "
  echo "          ### Run this to see any lines without (db_val == json_val) : ' awk 'BEGIN{FS=\"[; \t\n]+\"} {if (NF != 7) {print \$0} else {db_val = \$5; json_val = \$7; if (db_val != json_val) {print \$0} } }' $log_file  ' "
  echo "          ### Run this to re-run the regression test (which will save output to a new log file): ' /root/bin/run_regression.sh ' "
  echo "          ### Running second option now; some lines may be text, but not errors (so check them all):"
  awk 'BEGIN{FS="[; \t\n]+"} {if (NF != 7) {print $0} else {db_val = $5; json_val = $7; if (db_val != json_val) {print $0} } }' $log_file
  echo "### INFO ### Echoing contents of log file. ( $(date) )"
  echo "         ###"
  # print the log file, so it will be sent in the cron job email (same as it would be if the messages were reported to the cron job in real time)
  cat $log_file
fi
exit $exit_code2
