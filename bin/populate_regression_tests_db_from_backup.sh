#!/bin/bash
# Directed (~manual) DB restore does not write to the DB, so some lines are commented out, with a comment preceding those lines.
# Run as:   populate_from_backup_directed.sh  [backup_dir]  [db_name]
#   where:  [backup_dir] = a dir that already has the unpacked DB files (i.e., the .txt and .sql files) in it; .tar.gz or .tgz isn't enough - they have to be unpacked already
#     and:  [db_name] = the name that the DB will have when it's populated
# to do:
#   * maybe add the option to untarball a file, if that's all that is in the backup dir
#   * add a check that the user is ldbd (if that's still important)
#   * add a message about how to check that the DB restore worked

echo "### INFO ### Starting 'populate_from_backup_for_installation_script.sh'"

# Set the following line to the directory with the backed-up files in it (e.g., tbl_test_runs.sql et al.) (this should NOT be a tarball file!)
if [ "$1" != "" ]; then backup_dir=$1; else   ### if there's a command-line arg, that will be the source of the DB files; if not, use the dir specified below
  #backup_dir=/root/empty_database_untar/backup/segdb/segments-dev/tmp/mysql_dump/
  #backup_dir=/var/backup_of_dqsegdb/
  backup_dir=/backup/segdb/segments/install_support/tmp/mysql/
fi
# Set the following line to the name of the DB that you want to create and populate (WARNING: Note that if it exists, it will be deleted first!)
if [ "$2" != "" ]; then db_name=$2; else
  echo "### ERROR ### Run as: ' (script name)  (backup dir)  (DB name) '; DB name must be specified."
  exit 1
  #db_name="dqsegdb"   ### this is the name of the segments DB on segments.ligo.org
  #db_name="segments_backup"   ### this is the name for 'dqsegdb' DB on segments-backup
  #db_name="dqsegdb_regression_tests"   ### this is the name of the regression tests DB on segments-backup
fi
echo "### INFO ### Source for DB files is: " $backup_dir
echo "### INFO ### Database name to be populated is: " $db_name

if [ ! -d $backup_dir ]; then
  echo "### ERROR ### The source for DB files to be restored ( $backup_dir ) does not exist."
  echo "          ### Can't fix this, so exiting."
  exit 1
fi
#if [ -e $backup_dir/*.sql ]; then
if [ `ls $backup_dir/*.sql | wc -l` -gt 0 ]; then
  echo "## INFO ##: Files seem to be available in the backup directory ($backup_dir)."
else
  echo "## ERROR ##: The backup directory ($backup_dir) does not seem to have the expected files, such as files matching *.sql."
  echo "             Please put the files there or change the 'backup_dir' variable in the script."
  exit 1
fi

cd $backup_dir

# To import from --tab option

date1=$(date -u +"%s")
# First clean the database out:
mysql -u root -e "DROP DATABASE IF EXISTS $db_name"
mysql -u root -e "CREATE DATABASE $db_name"
mysql -u root -e "use $db_name"
mysql -u root -e "GRANT SELECT, INSERT, UPDATE ON $db_name.* TO 'dqsegdb_user'@'localhost'"
mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost'"

# Next create the structure:
cat $backup_dir/tbl_backups.sql |mysql -u root $db_name
cat $backup_dir/tbl_test_results.sql |mysql -u root $db_name
cat $backup_dir/tbl_test_runs.sql |mysql -u root $db_name
cat $backup_dir/tbl_value_groups.sql |mysql -u root $db_name
cat $backup_dir/tbl_values.sql |mysql -u root $db_name

# Then import the data
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_backups.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_test_results.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_test_runs.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_value_groups.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_values.txt
# Note that doing it this way doubles the import speed by running the two segment tables in parallel
date2=$(date -u +"%s")
diff=$(($date2-$date1))
# Following line commented out for directed DB restore 
#echo "$(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import." >> /usr1/ldbd/backup_logging/import_time.log
echo "## INFO ## $(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import."

echo "## INFO ##: Database restore complete.  If there were no error messages reported, you could probably check that it worked somehow, but I'm not sure how yet."


