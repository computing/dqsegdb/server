#!/bin/bash
# This script (populate_from_backup_2019.10.31a.sh) is a copy of populate_from_backup_2019.09.23a.sh, modified to restore all of the DB except for tbl_processes
# This script (populate_from_backup_2019.11.12a.sh) is a copy of populate_from_backup_2019.10.31a.sh, which modifies that one to use backup DB /backup/segdb/segments/monthly/17-01-01.tar.gz (b/c it's smaller), rather than the newest backup (which is bigger);
#   it also creates loops for tbl_segments and tbl_segment_summary, to detect if the table wasn't loaded propoerly, and if so, tries again, up to some limit (currently, 3 tries each, total).
# This script (populate_from_backup_2019.11.13b.sh) copies and modifies populate_from_backup_2019.11.12a.sh, echoing some information about file /var/lib/mysql/ibdata1, and doesn't bother with the extra log file (done by 'run_populate_from_backup.sh' now).
# This script (populate_from_backup_2019.11.15a.sh) copies and modifies populate_from_backup_2019.11.13b.sh, removing the steps that wipe out the old DB and create a new copy, and instead wipe out each standard-name table one-by-one; this way,
#   any renamed tables are kept, but we don't have to worry about having a mix of old copies of standard tables and new copies.  Also, added tests to make sure that there are the right number (or at least a sufficient number) of .sql and .txt files
#   from the tarball, and that all the right filenames were untarballed.
# populate_from_backup_2019.11.18a.sh - added echo of the script name (by listing the symlink), some pauses, and later lines to revoke user dqsegdb_user privileges during the DB restore, then reinstate them at the end
# populate_from_backup_2019.11.30a.sh - modified populate_from_backup_2019.11.18a.sh to do normal DB restores by making the parts that delete and re-create the DB toggleable (on) and turning off the parts that revoke and reinstate user privileges;
#   choosing which DB to restore must still be done manually (by commenting/uncommenting "N=" line and "cp [dir]" line)
# populate_from_backup_alpha_2019.11.30a.sh - modified populate_from_backup_2019.11.30a.sh to include some code + ideas from populate_from_backup_alpha_2019.11.18a.sh; wipes out both old_db and new_db, then populates new_db

exit_code=0   # this might be changed during the run, to indicate an essential step failed
verbose=1
log_to_db=1   # save info on DB backup to dqsegdb_regression_tests.tbl_backups
wipe_db=1
revoke_privileges=0
old_db=segments_backup
new_db=segments_backup_beta

# Set backup start date.
backup_date=$(date +%Y-%m-%d)
start_date=$(date +%Y.%m.%d)
start_time=$(date +%H:%M:%S)
start_sec=$(date -u +"%s")

# tmp dir is where a new dir will be created
#tmp_dir=/backup/segdb/segments/tmp/
tmp_dir=/local/ldbd/tmp/
mysql_dir=$tmp_dir/mysql_import_$(uname -n)_$(date +%Y.%m.%d-%H.%M.%S)

echo "### INFO ### Starting populate from backup beta on $(uname -n).  $(date)"
echo "### INFO ### Script being used:   `ls -l /local/ldbd/bin/populate_from_backup_beta.sh`"

# Set backup import start time.
backup_import_start_time=$(date +'%H:%M:%S')
# Add backup import start date/time.
if [ $log_to_db -eq 1 ]
then
  echo "### INFO ### Logging DB restore info to DB table dqsegdb_regression_tests.tbl_backups"
  mysql -u dqsegdb_backup -pdqsegdb_backup_pw -e "UPDATE dqsegdb_regression_tests.tbl_backups SET import_time_start = '${backup_import_start_time}' WHERE backup_date LIKE '${backup_date}%'"
fi

# First, get the appropriate file and copy it over to the temp directory
echo "### INFO ### Copying tarballed DB to tmp dir ($mysql_dir).  $(date)"
mkdir -p $mysql_dir
if [ ! -d $mysql_dir ]; then echo "### ERROR ### Could not create dir for mysql files ($mysql_dir).  Exiting script."; exit; fi
N=$(date +%y-%m-%d).tar.gz
#N=17-01-01.tar.gz
#N=17-02-01.tar.gz
#N=17-06-01.tar.gz
cd $mysql_dir
cp /backup/segdb/segments/primary/$N  .
#cp /backup/segdb/segments/monthly/$N  .
if [ ! -e $mysql_dir/$N ]; then echo "### ERROR ### Did not find tarballed DB ($N) in tmp dir ($mysql_dir).  Exiting script."; exit; fi
echo "### INFO ### Unpackinging tarballed DB ($N).  $(date)"
tar -xvzf $N
sql_count=`ls -1 *sql | wc --lines`
txt_count=`ls -1 *txt | wc --lines`
# note that we expect to find tbl_processes.sql, but tbl_processes.txt is optional (might not be there)
exit_code=0
if [ $sql_count -lt 7 ]; then echo "### ERROR ### The tarball should have unpacked at least 7 *.sql files, but there are only $sql_count.  Script will exit.  $(date)"; exit_code=1; fi
if [ $txt_count -lt 6 ]; then echo "### ERROR ### The tarball should have unpacked at least 6 *.txt files, but there are only $sql_count.  Script will exit.  $(date)"; exit_code=1; fi
if [ ! -e tbl_dq_flags.sql ] || [ ! -e tbl_dq_flag_versions.sql ] || [ ! -e tbl_processes.sql ] || [ ! -e tbl_segments.sql ] || [ ! -e tbl_segment_summary.sql ] || [ ! -e tbl_value_groups.sql ] || [ ! -e tbl_values.sql ]
then
  echo "### ERROR ### One or more *.sql file is missing.  Script will exit.  $(date)"
  echo "          ### Tarball should have all of these: tbl_dq_flags.sql  tbl_dq_flag_versions.sql  tbl_processes.sql  tbl_segments.sql  tbl_segment_summary.sql  tbl_value_groups.sql  tbl_values.sql."
  exit_code=1
fi
if [ ! -e tbl_dq_flags.txt ] || [ ! -e tbl_dq_flag_versions.txt ] || [ ! -e tbl_segments.txt ] || [ ! -e tbl_segment_summary.txt ] || [ ! -e tbl_value_groups.txt ] || [ ! -e tbl_values.txt ]
then
  echo "### ERROR ### One or more *.txt file is missing.  Script will exit.  $(date)"
  echo "          ### Tarball should have all of these: tbl_dq_flags.txt  tbl_dq_flag_versions.txt  tbl_processes.txt  tbl_segments.txt  tbl_segment_summary.txt  tbl_value_groups.txt  tbl_values.txt."
  exit_code=1
fi
if [ $exit_code -ne 0 ]; then echo $PWD; ls -lAh; exit $exit_code; fi

# To import from --tab option

if [ $revoke_privileges -eq 1 ]; then
  echo "### INFO ### Revoking mysql privileges for user 'dqsegdb_user'"
  mysql -u root -e "REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'dqsegdb_user'@'localhost';"
fi

date1=$(date -u +"%s")
if [ $wipe_db -eq 1 ]
then
  # First clean the database out:
  echo "### INFO ### Cleaning out old DB (deleting '$old_db' and/or '$new_db').  $(date)"
  mysql -u root -e "DROP DATABASE IF EXISTS $old_db"
  mysql -u root -e "DROP DATABASE IF EXISTS $new_db"
  echo "### INFO ### Creating new DB ('$new_db').  $(date)"
  mysql -u root -e "CREATE DATABASE $new_db"
  mysql -u root -e "use $new_db"
  mysql -u root -e "GRANT SELECT, INSERT, UPDATE ON $new_db.* TO 'dqsegdb_user'@'localhost'"
  mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost'"
else
  # First, clean the old standard-named tables out
  echo "### INFO ### Cleaning out old tables.  $(date)"   # trying dropping tables in the reverse order from how they're created/imported
  if [ $verbose -eq 1 ]; then echo "### INFO ### Dropping table tbl_segmentt_summary; $(date)"; fi
  mysql -u root -e "USE $old_db; DROP TABLE IF EXISTS tbl_segment_summary;"
  mysql -u root -e "USE $new_db; DROP TABLE IF EXISTS tbl_segment_summary;"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Dropping table tbl_segments; $(date)"; fi
  mysql -u root -e "USE $old_db; DROP TABLE IF EXISTS tbl_segments;"
  mysql -u root -e "USE $new_db; DROP TABLE IF EXISTS tbl_segments;"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Dropping table tbl_processes; $(date)"; fi
  mysql -u root -e "USE $old_db; DROP TABLE IF EXISTS tbl_processes;"
  mysql -u root -e "USE $new_db; DROP TABLE IF EXISTS tbl_processes;"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Dropping table tbl_dq_flag_versions; $(date)"; fi
  mysql -u root -e "USE $old_db; DROP TABLE IF EXISTS tbl_dq_flag_versions;"
  mysql -u root -e "USE $new_db; DROP TABLE IF EXISTS tbl_dq_flag_versions;"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Dropping table tbl_dq_flags; $(date)"; fi
  mysql -u root -e "USE $old_db; DROP TABLE IF EXISTS tbl_dq_flags;"
  mysql -u root -e "USE $new_db; DROP TABLE IF EXISTS tbl_dq_flags;"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Dropping table tbl_values; $(date)"; fi
  mysql -u root -e "USE $old_db; DROP TABLE IF EXISTS tbl_values;"
  mysql -u root -e "USE $new_db; DROP TABLE IF EXISTS tbl_values;"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Dropping table tbl_value_groups; $(date)"; fi
  mysql -u root -e "USE $old_db; DROP TABLE IF EXISTS tbl_value_groups;"
  mysql -u root -e "USE $new_db; DROP TABLE IF EXISTS tbl_value_groups;"
  if [ $verbose -eq 1 ]; then echo "### INFO ### Finished dropping tables; $(date)"; fi
fi

# Next create the structure:
echo "### INFO ### Creating structure of new DB (tables).  $(date)"
cat $mysql_dir/tbl_value_groups.sql     | mysql -u root $new_db
cat $mysql_dir/tbl_values.sql           | mysql -u root $new_db
cat $mysql_dir/tbl_dq_flags.sql         | mysql -u root $new_db
cat $mysql_dir/tbl_dq_flag_versions.sql | mysql -u root $new_db
cat $mysql_dir/tbl_processes.sql        | mysql -u root $new_db
cat $mysql_dir/tbl_segments.sql         | mysql -u root $new_db
cat $mysql_dir/tbl_segment_summary.sql  | mysql -u root $new_db

# Then import the data
echo "### INFO ### Importing data to new DB.  $(date)"
echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
if [ $verbose -eq 1 ]; then echo "### INFO ### Importing data for tbl_value_groups; $(date)"; fi
mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_value_groups.txt
  error_code=$?
  if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; else echo "### INFO ### Table import failed (exit code = $error_code)."; exit 4; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Importing data for tbl_values; $(date)"; fi
mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_values.txt
  error_code=$?
  if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; else echo "### INFO ### Table import failed (exit code = $error_code)."; exit 4; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Importing data for tbl_dq_flags; $(date)"; fi
mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_dq_flags.txt
  error_code=$?
  if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; else echo "### INFO ### Table import failed (exit code = $error_code)."; exit 4; fi
if [ $verbose -eq 1 ]; then echo "### INFO ### Importing data for tbl_dq_flag_versions; $(date)"; fi
mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_dq_flag_versions.txt
  error_code=$?
  if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; else echo "### INFO ### Table import failed (exit code = $error_code)."; exit 4; fi
echo "### INFO ### Skipping import of tbl_processes.txt"
#mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_processes.txt
echo "### INFO ### Roughly half-way through importing DB data.  (About to start tbl_segments*)  $(date)"
echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
echo "### INFO ### Importing tbl_segments; $(date)"
mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_segments.txt
error_code=$?
if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; exit_code=$error_code; i=4; else echo "### INFO ### Table import failed (exit code = $error_code)."; i=1; fi
while [ $i -lt 4 ]
do
  echo "In populate_from_backup_beta.sh, starting re-try # $i of tbl_segments restore: $(date)"
  echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
  mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_segments.txt
  error_code=$?
  echo "In populate_from_backup_beta.sh, finishing loop $i of tbl_segments restore: $(date)"
  echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
  if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; exit_code=$error_code; i=4; else echo "### INFO ### Table import # $i failed (exit code = $error_code)."; i=$((i + 1)); fi
  if [ $i -eq 4 ] && [ "$error_code" -ne 0 ]; then exit_code=2; exit $exit_code; fi   # i=4 means no more tries, either because 1 worked or the original and all 3 re-tries failed; it worked if $error_code == 0
done
echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
echo "### INFO ### Importing tbl_segment_summary; $(date)"
mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_segment_summary.txt
error_code=$?
if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; exit_code=$error_code; i=4; else echo "### INFO ### Table import failed (exit code = $error_code)."; i=1; fi
while [ $i -lt 4 ]
do
  echo "In populate_from_backup_beta.sh, starting loop $i of tbl_segment_summary restore: $(date)"
  echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
  mysqlimport -u root --use-threads=4 --local $new_db $mysql_dir/tbl_segment_summary.txt
  error_code=$?
  echo "In populate_from_backup_beta.sh, finishing loop $i of tbl_segment_summary restore: $(date)"
  echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
  if [ $error_code -eq 0 ]; then echo "### INFO ### Table import successful (exit code = $error_code)."; exit_code=$error_code; i=4; else echo "### INFO ### Table import # $i failed (exit code = $error_code)."; i=$((i + 1)); fi
  if [ $i -eq 4 ] && [ "$error_code" -ne 0 ]; then exit_code=3; exit $exit_code; fi   # i=4 means no more tries, either because 1 worked or the original and all 3 re-tries failed; it worked if $error_code == 0
done
# old comment [don't think it applies anymore]: # Note that doing it this way doubles the import speed by running the two segment tables in parallel
echo "### INFO ### Finished importing data.  $(date)"
echo "### INFO ### ibdata file:"; ls -lh /var/lib/mysql/ibdata1
date2=$(date -u +"%s")
diff=$(($date2-$date1))
time_msg="$(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import (cleaning out old DB, creating new DB, and importing data to new DB)."
#echo "$(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import." >> /local/ldbd/backup_logging/import_time.log
echo $time_msg >> /local/ldbd/backup_logging/import_time.log
echo "### INFO #### $time_msg"

if [ $revoke_privileges -eq 1 ]; then
  echo "### INFO ### Restoring privileges for user dqsegdb_user"
  mysql -uroot -A < /local/ldbd/bin/dqsegdb_user_mysql_privileges.sql
fi

# Lastly, clean up the temp directory:
echo "### INFO ### Deleting temp dir ($mysql_dir).  $(date)"
rm -rf $mysql_dir

# Set backup import end time.
backup_import_stop_time=$(date +'%H:%M:%S')
# Add backup import end date/time.
if [ $log_to_db -eq 1 ]
then
  echo "### INFO ### Logging DB restore info to DB table dqsegdb_regression_tests.tbl_backups"
  mysql -u dqsegdb_backup -pdqsegdb_backup_pw -e "UPDATE dqsegdb_regression_tests.tbl_backups SET import_time_stop = '${backup_import_stop_time}' WHERE backup_date LIKE '${backup_date}%'"
fi

end_time=$(date +%H:%M:%S)
end_sec=$(date -u +"%s")
duration_sec=$((end_sec - start_sec))
#echo $(date +%Y-%m-%d) $(date +'%H:%M:%S') >> /backup/segdb/segments/monitor/populate_from_backup.txt
echo "$start_date  $start_time - $end_time = $duration_sec sec" >> /backup/segdb/segments/monitor/populate_from_backup.txt
echo "### INFO ### Finished populate from backup on $(uname -n).  $(date)"
exit $exit_code
