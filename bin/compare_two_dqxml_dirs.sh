#!/bin/bash
# This is a script that compares the dqxml files in a 'reference' dir (possibly an IFO dir) and a 'test' dir (possibly a /dqxml/X1/ dir).
#   It reports if the dqxml file contents of the two dirs are identical, and if not, which dqxml files exist only on the reference dir.
#   If not identical, it reports how old the oldest missing dqxml file is (based on current GPS time and the GPS time in the filename).
#   Command line args are designed to be progressively ignorable, making assumptions if not given.  For an IFO, "IFO" can be used instead of a dir, and the corresponding remote dir will be used (based on "ifo_value").
# Started by Robert Bruntz on 2019.07.29, based on compare_ifo_dir_to_ifocache_dir.sh (started 2019.07.14).

# Run like this: '  [script_name]  [ifo_value]  [gps_major_number]  [verbose_value]  [reference_dir]  [test_base_dir]  [output_base_dir]'
# Example: '  /root/bin/compare_two_dqxml_dirs.sh  L  12470  1  '
# Example: '  /root/bin/compare_two_dqxml_dirs.sh  L  12470  1  /ifocache/DQ/L1  /dqxml/L1  '
# Example: '  /root/bin/compare_two_dqxml_dirs.sh  L  12470  1  IFO  /dqxml/L1  '
# Example: '  /root/bin/compare_two_dqxml_dirs.sh  L  12470  1  IFO  /ifocache/DQ/L1  '
# Example: '  for i in {12459..12480}; do echo $i; /root/bin/compare_two_dqxml_dirs.sh H $i IFO /dqxml/H1 &> comparison_H_${i}.txt; done &  '
# Outline:
# * From command-line args, create vars for reference and test base dirs, output file, error file
# * Assemble and run rsync command; save std out to output file in output_dir, std err to error file in output_dir, return code to a var
# * If any errors, report that and exit.

# * If no errors, grep the output file for '.xml' (indicates dqxml filename) and sort output; take the first value from the sorted list (i.e., oldest GPS times); extract the GPS time from the filename
# * Report number of dqxml files missing from test dir and the age of the oldest missing file.
# * Not implemented in this file: If oldest file is less than X seconds, test passes; write corresponding JSON file; if oldest file is more than X seconds, test fails; write corresponding JSON file.
# To do:
#   * warning/error if user tries to use IFO as test dir?
#   * test that ref_base_dir, test_base_dir, ref_total_dir, and test_total_dir all exist prior to executing test (or maybe test each only if there's an error?)
#   * 


# set default values
timeout=128
ifo=H
gps_time_start=$((`date +%s` - 315964782))
gps_major_number=`echo $gps_time_start | cut -c1-5`   # default major value (GPS time for dir name) = current (e.g., for GPS time 1248028200, major number = 12480; dir = ${ifo}-DQ_Segments-12480)
verbose=1
ref_base_dir=IFO
test_base_dir=/dqxml/H1
output_dir=/tmp
margin=$((5*60))   ### limit on age, in number of seconds, for a file that is in IFO dir but not ifocache dir
echo "### INFO ### Run start time:  $(date)  - GPS time: ~$gps_time_start"

# get values from command-line args 
if [ "$#" -eq 0 ]   # if *no* command-line args, report how to run, then run with default values
then
  echo "### INFO ### Run like this: '  [script_name]  [ifo_value]  [gps_major_number]  [verbose_value]  [reference_base_dir]  [test_base_dir]  [output_dir]  '"
  echo "         ### Example: '  /root/bin/compare_two_dqxml_dirs.sh  $ifo  $gps_major_number  1  '"
  echo "         ### Example: '  /root/bin/compare_two_dqxml_dirs.sh  $ifo  $gps_major_number  1  $ref_base_dir  $test_base_dir  $output_dir  '"
  echo "### NOTICE ### Script will run with default values, above, in 5 seconds (unless stopped)"
  sleep 5
fi
if [ "$#" -ge 1 ]; then ifo=$1; fi
if [ "$#" -ge 2 ]; then gps_major_number=$2; fi
if [ "$#" -ge 3 ]; then if [ "$3" -eq 0 ]; then verbose=0; fi; fi   # only change verbose from 1 to 0 if 0 is specified on the command line; anything else, it stays at 1
if [ "$#" -ge 4 ]; then ref_base_dir=$4; fi
if [ "$#" -ge 5 ]; then test_base_dir=$5; fi
if [ "$#" -ge 6 ]; then output_dir=$6; fi

if [ $ifo == "H1" ]; then ifo="H"; fi
if [ $ifo == "L1" ]; then ifo="L"; fi
if [ $ifo == "V1" ]; then ifo="V"; fi
if [ $ifo == "G1" ]; then ifo="G"; fi
if [ "$ref_base_dir" == "ifo" ]; then ref_base_dir=IFO; fi
if [ "$test_base_dir" == "ifo" ]; then test_base_dir=IFO; fi
if [ "$#" -lt 5 ]; then test_base_dir=/dqxml/${ifo}1; fi

# set up rsync variables
if [ "$ref_base_dir" == "IFO" ]; then
#  if [ $ifo == "H" ]; then ref_base_dir="ldas.ligo-wa.caltech.edu::dqxml"; fi
  if [ $ifo == "H" ]; then ref_base_dir="ldas-lho.ligo-wa.caltech.edu::dqxml"; fi   # replaced the old URL with this one on 2021.06.22
  if [ $ifo == "L" ]; then ref_base_dir="ldas-llo.ligo-la.caltech.edu::dqxml"; fi   # replaced the old URL with this one on 2021.08.26
  if [ $ifo == "V" ]; then ref_base_dir="dqsegments.virgo.infn.it::dqxml"; fi
  if [ $ifo == "G" ]; then ref_base_dir="rsync://130.75.117.72/dqxml"; fi
fi
if [ "$test_base_dir" == "IFO" ]; then
#  if [ $ifo == "H" ]; then test_base_dir="ldas.ligo-wa.caltech.edu::dqxml"; fi
  if [ $ifo == "H" ]; then test_base_dir="ldas-lho.ligo-wa.caltech.edu::dqxml"; fi   # replaced the old URL with this one on 2021.06.22
  if [ $ifo == "L" ]; then test_base_dir="ldas-llo.ligo-la.caltech.edu::dqxml"; fi   # replaced the old URL with this one on 2021.08.26
  if [ $ifo == "V" ]; then test_base_dir="dqsegments.virgo.infn.it::dqxml"; fi
  if [ $ifo == "G" ]; then test_base_dir="rsync://130.75.117.72/dqxml"; fi
fi

ref_total_dir=$ref_base_dir/${ifo}-DQ_Segments-${gps_major_number}
test_total_dir=$test_base_dir/${ifo}-DQ_Segments-${gps_major_number}
output_file=${output_dir}/rsync_${ifo}_${gps_major_number}_${gps_time_start}_output
error_file=${output_dir}/rsync_${ifo}_${gps_major_number}_${gps_time_start}_error
if [ $verbose -eq 1 ]; then echo "### INFO ### IFO = $ifo; GPS major number = $gps_major_number; ref_total_dir = $ref_total_dir ; test_total_dir = $test_total_dir ; output_dir = $output_dir"; fi

# run rsync command and get output
if [ $verbose -eq 1 ]
then 
  echo "### INFO ### rsync command to be run:"
  echo "             time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=$timeout  --size-only  $ref_total_dir/  $test_total_dir/  >  $output_file  2>  $error_file"
  echo "### INFO ### (running...)"
fi
time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=$timeout  --size-only  $ref_total_dir/  $test_total_dir/  >  $output_file  2>  $error_file
#      >  /tmp/rsync_${ifo}_${gps_major_number}_${gps_time_start}_output  2>  /tmp/rsync_${ifo}_${gps_major_number}_${gps_time_start}_error
return_code=$?
xml_count=`grep "\.xml" $output_file | wc --lines`
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Output and error files:"
  ls -l $output_file
  ls -l $error_file
  if [ $return_code -ne 0 ]
  then
    echo "### NOTICE ### Return code from rsync command is $return_code, which indicates an error occurred."
  else
    echo "### NOTICE ### Return code from rsync command is $return_code, which indicates that the command executed successfully."
    echo "### INFO ### xml_count = $xml_count"
    if [ $xml_count -gt 0 ]
    then
      echo "### INFO ### .xml files:"; grep "\.xml" $output_file
      oldest_xml=`grep "\.xml" $output_file | sort | head -n 1`
echo "### INFO ### Oldest xml file = $oldest_xml"
    fi

  fi
fi

gps_time_end=$((`date +%s` - 315964782))
echo "### INFO ### Run end time:  $(date)  - GPS time: ~$gps_time_end"


exit


# Reference
#see what rsync *should* do:
#rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=32 ldas.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-12359  /dqxml/H1
#rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=32 ldas-lho.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-12359  /dqxml/H1
#rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=32 ldas-llo.ligo-la.caltech.edu::dqxml/L-DQ_Segments-12359  /dqxml/L1
#rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=32 dqsegments.virgo.infn.it::dqxml/V-DQ_Segments-12359  /dqxml/V1
#rsync -avn --progress rsync://130.75.117.72/dqxml/G-DQ_Segments-12359  /dqxml/G1/
#view contents of a remote dir (e.g., to see if there are files that *should* be pulled but aren't):
#rsync --list-only ldas.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-12359/
#rsync --list-only ldas-lho.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-12359/

# --size-only = only compare sizes of files, not timestamps or checksums
# -a = --archive = -rlptgoD = "syncs directories recursively, transfer special and block devices, preserve symbolic links, modification times, group, ownership, and permissions."
# -n = --dry-run = dry run (probably want -v with this)
# -v = verbose

# sample output from this script:
# rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=128  --size-only  ldas-llo.ligo-la.caltech.edu::dqxml/L-DQ_Segments-12479/  /ifocache/DQ/L1/L-DQ_Segments-12479/  >  /tmp/rsync_L_12479_1247979249_output  2>  /tmp/rsync_L_12479_1247979249_error

