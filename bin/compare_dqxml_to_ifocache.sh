#!/bin/bash
ifo=H
major_number=1242?

if [ "$#" -eq 0 ]; then echo "### NOTICE ### No run arguments found."; echo "### INFO ### Usage:  ' (command name)  (ifo)  (major number pattern) '"; echo "         ### Example:  ' /root/bin/compare_dqxml_to_ifocache.sh H 1242? '"; \
                        echo "         ### Running '(command name)  $ifo  $major_number' in 5 seconds..."; sleep 5;  fi
if [ "$#" -ge 1 ]; then ifo=$1; fi
if [ "$#" -ge 2 ]; then major_number=$2; fi
if [ $ifo == "H1" ]; then ifo="H"; fi
if [ $ifo == "L1" ]; then ifo="L"; fi

cd /dqxml/${ifo}1/
for dir_name in `ls -1d ${ifo}-DQ_Segments-${major_number}`; do
  echo $dir_name
  echo "   In /dqxml/${ifo}1/${dir_name}/:       `ls -1 /dqxml/${ifo}1/$dir_name | wc --lines` files"
  echo "   In /ifocache/DQ/${ifo}1/${dir_name}/: `ls -1 /ifocache/DQ/${ifo}1/$dir_name | wc --lines` files"
  diff -r -x .${ifo}-*.xml.?????? $dir_name /ifocache/DQ/${ifo}1/$dir_name
  if [ "$?" -ne 0 ]; then echo "### WARNING ### Directory contents differ."; fi
done

echo "To delete all dirs:  rm -rf /dqxml/${ifo}1/${ifo}-DQ_Segments-${major_number}  "
