#!/bin/bash
# This is a script to check for issues with rsync or publishing, associated with parallel publishing on seg and -dev, to test the P3 versions of dqsegdb and glue.
# Started by Robert Bruntz on 2022.04.29

verbose=0
run_date=$(date)
if [ "$#" -gt 0 ]; then verbose=$1; fi

rsync_output=`ps aux | grep rsync | grep -v -e "grep rsync" -e "grep --color=auto rsync" -e check_rsync_publish_issues.sh`
publish_output=`ps aux | grep publish | grep -v -e "grep publish" -e "grep --color=auto publish" -e check_rsync_publish_issues.sh`
rsync_count=`echo $rsync_output | wc --lines`
publish_count=`echo $publish_output | wc --lines`
if [ "$rsync_output" == "" ]; then rsync_count=0; fi
if [ "$publish_output" == "" ]; then publish_count=0; fi

if [ "$verbose" -eq 1 ]; then 
  echo "rsync count = $rsync_count ; publish count = $publish_count"
  echo "rsync_output:"
  echo $rsync_output
  echo -
  echo "publish_output:"
  echo $publish_output
fi

if [ "$rsync_count" -gt 1 ] || [ "$publish_count" -gt 1 ]; then
  echo "run time = $run_date"
  echo "rsync_count = $rsync_count"
  echo "publish_count = $publish_count"
  echo "rsync_output:"
  echo $rsync_output
  echo -
  echo "publish_output:"
  echo $publish_output
  exit 1
fi

exit 0

# Reference
[root@segments-dev logs]# ps aux | grep rsync
root     23076  0.0  0.0 112812   976 pts/1    S+   07:40   0:00 grep --color=auto rsync
[root@segments-dev logs]# ps aux | grep publish
root     23083  0.0  0.0 112812   976 pts/1    S+   07:41   0:00 grep --color=auto publish
