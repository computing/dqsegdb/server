#/bin/bash
# This is a script to do a brief analysis of DQSegDB python server log files.
# Started by Robert Bruntz on 2020.05.19
# Usage:  (script name)  (log file name or pattern)  (date + time search pattern)
# Example:  /root/bin/analyze_python_log_file.sh  /opt/dqsegdb/python_server/logs/2020-05-18.log  "2020-05-18 11:"  # to search for all entries from 11:00-11:59 AM PDT on 2020.05.18 in the file /opt/dqsegdb/python_server/logs/2020-05-18.log
# Example:  /root/bin/analyze_python_log_file.sh  /opt/dqsegdb/python_server/logs/2020-05-18.log  \"2020-05-18 11:\"  summary  # same thing, but only 1 dense line of output; note: does not warn if counts don't add up right
# Maybe useful:  for i in {00..01}; do date=$(date +%Y-%m-$i); out_msg=`/root/bin/analyze_python_log_file.sh /backup/segdb/segments/maintenance/automatic/opt_dqsegdb_python_server_logs/$date.log "$date" summary`; echo $date: $out_msg; done
# Maybe useful:  for ii in {00..59}; do out_msg=`/root/bin/analyze_python_log_file.sh /opt/dqsegdb/python_server/logs/2020-05-18.log "2020-05-18 11:${ii}" summary`; echo $ii:  $out_msg; done
# Maybe useful:  for ii in {00..59}; do out_msg=`grep -v PATCH /root/bin/python_server_log_files/2020-05-18.log | grep "2020-05-18 11:${ii}" | wc`; echo $ii:  $out_msg; done
# How it works:
#   * search the input log file for the input string; copy all matching lines to a temp file (to reduce the amount of file access for the following steps; this file will almost always be a much smaller file than the original log file)
#   * search the temp file for the number of occurrences of a number of known strings, indicating what each line is reporting on
#   * print the output for the user, summarizing how many lines were analyzed and how many fell into each category
#   * maybe print certain lines out in their entirety - e.g., authentication failures and 'unmatched' lines
# to do:
#   * check that entered log file exists
#   * format output, so that numbers always take up the same number of columns
#   * [done] run a check for categories - INFO, DEBUG, WARNING, ERROR, CRITICAL (etc.?)
#   * maybe modify the script so that the user enters the date and time string (e.g., '/root/bin/analyze_python_log_file.sh  2020-05-18  11:') and the script figures out which log file to use - or have that as a "dash to use default" option,
#       e.g., '/root/bin/analyze_python_log_file.sh  -  2020-05-18  11:', with the option to fill it in to use a specific file (e.g., a file on /backup/)
#   * decide if we want to print out category results (INFO, DEBUG, etc.) in summary output
#   * create a second 'summary' mode, which prints out number of WARNING, ERROR, etc., columns
#   * 

# set up default variables
usage1="Usage:  (script name)	 (log file name	or pattern)  (date + time search pattern)"
usage2="Example:  /root/bin/analyze_python_log_file.sh  /opt/dqsegdb/python_server/logs/2020-05-18.log  \"2020-05-18 11:\"  # to search for all entries from 11:00-11:59 AM PDT on 2020.05.18 in the file /opt/dqsegdb/python_server/logs/2020-05-18.log"
usage3="Example:  /root/bin/analyze_python_log_file.sh  /opt/dqsegdb/python_server/logs/2020-05-18.log  \"2020-05-18 11:\"  summary  # same thing, but only 1 dense line of output; note: does not warn if counts don't add up right"
usage4="Maybe useful:  for i in {00..01}; do date=$(date +%Y-%m-\$i); out_msg=\`/root/bin/analyze_python_log_file.sh /backup/segdb/segments/maintenance/automatic/opt_dqsegdb_python_server_logs/\$date.log \"\$date\" summary\`; echo \$date: \$out_msg; done"
usage5="Maybe useful:  for ii in {00..59}; do out_msg=\`/root/bin/analyze_python_log_file.sh /opt/dqsegdb/python_server/logs/2020-05-18.log \"2020-05-18 11:\$\{ii\}\" summary\`; echo \$ii:  \$out_msg; done"
usage6="Maybe useful:  for ii in {00..59}; do out_msg=\`grep -v PATCH /root/bin/python_server_log_files/2020-05-18.log | grep \"2020-05-18 11:\${ii}\" | wc\`; echo \$ii:  \$out_msg; done"
start_time=$(date +%Y.%m.%d-%H.%M.%S)
tmp_basename=/tmp/analyze_python_log_file
output_file=${tmp_basename}_${start_time}.txt
summary=0   # default is to print the full output, not the 1-line summary

# get values from command line
if [ $# -lt 2 ]; then echo "### ERROR ### Command line should have at least 2 arguments."; echo $usage1; echo $usage2; echo $usage3; echo $usage4; echo $usage5; echo $usage6; exit; fi
log_file=$1
search_string=$2
if [ $# -gt 2 ]; then if [ "$3" == "summary" ]; then summary=1; fi ; fi

# check variables to be used; report on issues or values to be used
# (do some checks here)
if [ $summary -ne 1 ]; then
  echo "### INFO ### Started analysis at $start_time"
  echo "### INFO ### log file = $log_file; search_string = $search_string; output file = $output_file"
fi

# run check
grep "$search_string" $log_file > $output_file
total_line_count=`cat $output_file | wc --lines`
patch_line_count=`grep PATCH $output_file | wc --lines`
non_patch_line_count=`grep -v PATCH $output_file | wc --lines`
attempt_line_count=`grep -v PATCH $output_file | grep "Attempt made" | wc --lines`
completed_line_count=`grep -v PATCH $output_file | grep "Completed successfully" | wc --lines`
obdb_problem_line_count=`grep -v PATCH $output_file | grep "Problem with ODBC connection" | wc --lines`
odbc_problem_plus_patch_line_count=`grep "Problem with ODBC connection" $output_file | grep PATCH | wc --lines`
auth_failure_line_count=`grep "Authentication failure (Unable to load certificate)" $output_file | wc --lines`
unmatched_line_count=`grep -v PATCH $output_file | grep -v "Attempt made" | grep -v "Completed successfully" | grep -v "Completed successfully" | grep -v "Problem with ODBC connection" | grep -v "Authentication failure (Unable to load certificate)" | wc --lines`
matched_line_count=$((patch_line_count + attempt_line_count + completed_line_count + obdb_problem_line_count + auth_failure_line_count))
# these are categories of lines, such as ":DEBUG:" in "2020-05-18 11:00:01,699:DEBUG:GET /dq/H1/DMT-UP/1?s=1273816819&e=1273860019&include=known - Attempt made"
info_line_count=`grep ":INFO:" $output_file | wc --lines`
debug_line_count=`grep ":DEBUG:" $output_file | wc --lines`
warning_line_count=`grep ":WARNING:" $output_file | wc --lines`
error_line_count=`grep ":ERROR:" $output_file | wc --lines`
critical_line_count=`grep ":CRITICAL:" $output_file | wc --lines`
uncategorized_line_count=`grep -v ":INFO:" $output_file | grep -v ":DEBUG:" | grep -v ":WARNING:" | grep -v ":ERROR:" | grep -v ":CRITICAL:" | wc --lines`
#critical_line_count=`grep "::" $output_file | wc --lines`

# summary mode
# outputs something like this: "230540  =  223082 + 7458  =  223082 + 3729 + 2197 + 1520 + 12 + 0   (total = patch + non-patch = patch + attempt + completed + ODBC prob. + auth failure + unmatched);  /tmp/analyze_python_log_file_2020.05.20-02.25.38.txt"
if [ "$summary" -eq 1 ]; then
  echo "$total_line_count  =  $patch_line_count + $non_patch_line_count  =  $patch_line_count + $attempt_line_count + $completed_line_count + $obdb_problem_line_count + $auth_failure_line_count + $unmatched_line_count\
   (total = patch + non-patch = patch + attempt + completed + ODBC prob. + auth failure + unmatched);  $output_file"
  sleep 1   # if the script runs too fast (e.g., looped and analyzing a very small amount of time or analyzing a very small log file), multiple runs could have the exact same run second, so one temp file would be overwritten by the next one
  exit 0
fi

# report results  (only run if not in summary mode)
end_time=$(date +%Y.%m.%d-%H.%M.%S)
echo "### INFO ### Finished analysis at $end_time"
echo "$total_line_count   = total lines analyzed ( cat $output_file | wc --lines )"
echo "$patch_line_count   = lines containing 'PATCH' ( grep PATCH $output_file | wc --lines )"
echo "$non_patch_line_count   = lines NOT containing 'PATCH' ( grep -v PATCH $output_file | wc --lines )"
echo "$attempt_line_count   = lines containing 'Attempt made' ( grep -v PATCH $output_file | grep \"Attempt made\" | wc --lines )"
echo "$completed_line_count   = lines containing 'Completed successfully' ( grep -v PATCH $output_file | grep \"Completed successfully\" | wc --lines )"
echo "$obdb_problem_line_count   = lines containing 'Problem with ODBC connection' ( grep -v PATCH $output_file | grep \"Problem with ODBC connection\" | wc --lines ) ( + $odbc_problem_plus_patch_line_count lines included in PATCH count)"
echo "$auth_failure_line_count   = lines containing 'Authentication failure (Unable to load certificate)' ( grep \"Authentication failure (Unable to load certificate)\" $output_file | wc --lines )"
echo "$unmatched_line_count   = lines not matching patch, attempt, completed, ODBC, or auth failure strings ( grep -v PATCH $output_file | grep -v \"Attempt made\" | grep -v \"Completed successfully\" | grep -v \"Completed successfully\" | grep -v \"Problem with ODBC connection\" | grep -v \"Authentication failure (Unable to load certificate)\" | wc --lines )"
echo "$matched_line_count   = lines matching patch, attempt, completed, ODBC, or auth failure strings ( = $patch_line_count + $attempt_line_count + $completed_line_count + $obdb_problem_line_count + $auth_failure_line_count)"
if [ $((matched_line_count + unmatched_line_count)) -ne $total_line_count ]; then 
  echo "### WARNING ### Total line count ($total_line_count) should equal matched line count ($matched_line_count) + unmatched line count ($unmatched_line_count) (= $((matched_line_count + unmatched_line_count)))"
  echo "            ### You might want to investigate what happened there."
fi
echo "-"
echo "$info_line_count   = lines in 'INFO' category ( grep ":INFO:" $output_file | wc --lines )"
echo "$debug_line_count   = lines in 'DEBUG' category ( grep ":DEBUG:" $output_file | wc --lines )"
echo "$warning_line_count   = lines in 'WARNING' category ( grep ":WARNING:" $output_file | wc --lines )"
echo "$error_line_count   = lines in 'ERROR' category ( grep ":ERROR:" $output_file | wc --lines )"
echo "$critical_line_count   = lines in 'CRITICAL' category ( grep ":CRITICAL:" $output_file | wc --lines )"
echo "$uncategorized_line_count   = lines not in any of the above 4 categories ( grep -v ":INFO:" $output_file | grep -v ":DEBUG:" | grep -v ":ERROR:" | grep -v ":CRITICAL:" | wc --lines )"
echo "(check: total = $total_line_count; INFO + DEBUG + ERROR + CRITICAL + uncategorized = $((info_line_count + debug_line_count + error_line_count + critical_line_count + uncategorized_line_count))"
sleep 1   # if the script runs too fast (e.g., looped and analyzing a very small amount of time or analyzing a very small log file), multiple runs could have the exact same run second, so one temp file would be overwritten by the next one

exit 0

### Reference
#[root@segments ~]# grep -v PATCH /root/bin/python_server_log_files/2020-05-18.log | grep "2020-05-18 11:" | grep -v "Completed successfully" | grep -v "Attempt made" | grep -v "Problem with ODBC connection" 
#2020-05-18 11:00:04,428:ERROR:GET /dq/V1/ITF_SCIENCE - Authentication failure (Unable to load certificate) 

# sample lines (get and add more representative lines from /root/bin/python_server_log_files/2020-05-18.log)
2020-05-18 11:00:01,699:DEBUG:GET /dq/H1/DMT-UP/1?s=1273816819&e=1273860019&include=known - Attempt made
2020-05-18 11:00:01,704:DEBUG:GET /dq/L1/DMT-UP/1?s=1273816819&e=1273860019&include=known - Attempt made
2020-05-18 11:00:01,704:DEBUG:GET /dq/L1/DMT-UP/1?s=1273816819&e=1273860019&include=known - Attempt made
2020-05-18 11:00:06,163:DEBUG:GET /dq/L1/DMT-UP/1?s=1273816819&e=1273860019&include=known - Completed successfully
2020-05-18 11:00:06,280:DEBUG:GET /dq/H1/DMT-UP/1?s=1273816819&e=1273860019&include=known - Completed successfully
2020-05-18 11:00:06,823:DEBUG:PATCH /dq/H1/DMT-UP/1 - Attempt made
2020-05-18 11:00:06,825:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (serving request) 
2020-05-18 11:00:06,825:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (loading JSON) 
2020-05-18 11:00:06,825:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (checking payload) 
2020-05-18 11:00:06,825:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (handling URI) 
2020-05-18 11:00:06,826:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (adding query info to passed JSON) 
2020-05-18 11:00:06,829:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (putting known segments) 
2020-05-18 11:00:06,830:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (putting active segments) 
2020-05-18 11:00:06,830:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (updating segment totals) 
2020-05-18 11:00:06,830:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (inserting process) 
2020-05-18 11:00:06,831:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (committing transaction to database) 
2020-05-18 11:00:06,831:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process (transaction committed) 
2020-05-18 11:00:06,831:DEBUG:PATCH /dq/H1/DMT-UP/1 - Completed successfully
2020-05-18 11:00:06,831:INFO:PATCH /dq/H1/DMT-UP/1 - In inserting segments process
2020-05-18 11:00:04,425:DEBUG:GET /dq/V1/ITF_SCIENCE - Attempt made
2020-05-18 11:00:04,428:ERROR:GET /dq/V1/ITF_SCIENCE - Authentication failure (Unable to load certificate) 
2020-05-18 11:11:12,536:CRITICAL:PATCH /dq/H1/GRD-SUS_OM3_OK/1 - Problem with ODBC connection
