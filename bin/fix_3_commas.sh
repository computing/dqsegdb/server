#!/bin/bash

pull_dir=/dqxml/G1
pub_dir=/dqxml/G1_test
temp_dir=/dqxml/G1_temp
log_file=/dqxml/fix_3_commas_log_file.txt

#for newdir in `ls -1 $pull_dir`
for newdir in `ls -1 $pull_dir | tail -n 30`
do
  if [ ! -d $pub_dir/$newdir ]
  then
    mkdir $pub_dir/$newdir  2>>  $log_file
    echo "### INFO ### Ran 'mkdir $pub_dir/$newdir' - $(date)"  >>  $log_file
  fi
  cd $pull_dir/$newdir
  for newfile in `ls -1 G-DQ_Segments-??????????-60.xml  2>>  $log_file`
  do
    if [ ! -e $pub_dir/$newdir/$newfile ]
    then
      cp  $pull_dir/$newdir/$newfile  $temp_dir  2>>  $log_file
      sed  -i  's/,,,/,,/g'  $temp_dir/$newfile  2>>  $log_file
      grep ",,," $temp_dir/$newfile   ### return value: 0 = found it; 1 = didn't find it  2>>  $log_file
      if [ $? -eq 1 ]; then  mv  $temp_dir/$newfile  $pub_dir/$newdir/  2>>  $log_file ; fi
#      if [ $? -eq 1 ]; then  echo "would move  $temp_dir/$newfile  to  $pub_dir/$newdir/"; fi
    fi
  done
done
exit
