#!/bin/bash
# A quick script to automate testing of rsync to an IFO.
# Started on 2022.08.24 by Robert Bruntz.
str1=" Usage: ' (script)  (IFO - H, L, V, G, K or '-' for default (H))  (GPS/metric day - optional)  (timeout, in sec - optional; default = 120) '"
str2=" Example: ' /root/bin/test_rsync.sh  H ' "
str3=" Example: ' /root/bin/test_rsync.sh  L  31444 ' "
str3=" Example: ' /root/bin/test_rsync.sh  K  31444  30 ' "
str4=" Runs something like ' time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=128  --size-only  ldas-lho.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-13444/  /dqxml/H1/H-DQ_Segments-13444/ ' "
# to do:
#   * GEO command pipes into other commands, so exit code is not accurate
#   * set up to run on both LHO and LLO if no arguments given?
#   * set up check for metric day - number, 5 digits
#   * give commands to list an arbitrary remote dir's contents

#date
echo "$(date)  - GPS time: ~$((`date +%s` - 315964782))"
if [ $# -lt 1 ]; then echo $str1; echo $str2; echo $str3; echo $str4; exit 1; fi

# set default IFO and metric day
ifo=H
mnow=$((`date +%s` - 315964782))
mday=`echo $mnow | cut -c1-5`
timeout=120

# get command-line args, if any, and turn into versions we want to use
if [ $# -gt 0 ] && [ "$1" != "-" ]; then ifo="$1"; fi
if [ $# -gt 1 ] && [ "$2" != "-" ]; then mday="$2"; fi
if [ $# -gt 2 ] && [ "$3" != "-" ]; then timeout="$3"; fi
ifo="${ifo^^}"   # convert to upper case
if [ "$ifo" == "H" ] || [ "$ifo" == "H1" ] || [ "$ifo" == "LHO" ];   then ifo=H; fi
if [ "$ifo" == "L" ] || [ "$ifo" == "L1" ] || [ "$ifo" == "LLO" ];   then ifo=L; fi
if [ "$ifo" == "V" ] || [ "$ifo" == "V1" ] || [ "$ifo" == "VIRGO" ]; then ifo=V; fi
if [ "$ifo" == "G" ] || [ "$ifo" == "G1" ] || [ "$ifo" == "GEO" ];   then ifo=G; fi
if [ "$ifo" == "K" ] || [ "$ifo" == "K1" ] || [ "$ifo" == "KAGRA" ]; then ifo=K; fi

echo "ifo = $ifo; metric day = $mday"
exit_code=-1   # in case it doesn't get set later
rsync_errors="rsync errors: 5 = '@ERROR: chroot failed'; 10 = 'failed to connect to receiver; Connection timed out'; 12 = 'safe_read failed to read 1 bytes: Connection reset by peer'; 23 = 'Too many open files in system'; 30 = 'io timeout after [N] seconds -- exiting'"

function run_rsync {
  # $cmd is a global variable that will be set for the respective IFO before the command is run
  echo "Running this command: ' $cmd '"
  eval $cmd
  exit_code=$?
  echo "exit code = $exit_code"
  if [ "$exit_code" -ne 0 ]; then echo "There was a problem with the rsync.  Common $rsync_errors"; fi
  echo "To run the same command live (i.e., not a dry run), remove the 'n' from '-anv'."
}

# print and run rsync command
case $ifo in
  [H])  echo "Running test rsync for LHO"
    echo "### Primary LHO rsync server:"
    cmd="time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=$timeout  --size-only  lhodmt.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-${mday}/  /dqxml/H1/H-DQ_Segments-${mday}/"
    run_rsync
    echo "Maybe try: ' rsync --list-only lhodmt.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-${mday}/ ' or ' rsync --list-only lhodmt.ligo-wa.caltech.edu::dqxml/ ' "
    echo "### Secondary LHO rsync server:"
    cmd="time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=$timeout  --size-only  ldas-lho.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-${mday}/  /dqxml/H1/H-DQ_Segments-${mday}/"
    run_rsync
    echo "Maybe try: ' rsync --list-only ldas-lho.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-${mday}/ ' or ' rsync --list-only ldas-lho.ligo-wa.caltech.edu::dqxml/ ' "
    ;;
  [L])  echo "Running test rsync for LLO"
    echo "### Primary LLO rsync server:"
    cmd="time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=$timeout  --size-only  llodmt.ligo-la.caltech.edu::dqxml/L-DQ_Segments-${mday}/  /dqxml/L1/L-DQ_Segments-${mday}/"
    run_rsync
    echo "Maybe try: ' rsync --list-only llodmt.ligo-la.caltech.edu::dqxml/L-DQ_Segments-${mday}/ ' or ' rsync --list-only llodmt.ligo-la.caltech.edu::dqxml/ ' "
    echo "### Secondary LLO rsync server:"
    cmd="time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=$timeout  --size-only  ldas-llo.ligo-la.caltech.edu::dqxml/L-DQ_Segments-${mday}/  /dqxml/L1/L-DQ_Segments-${mday}/"
    run_rsync
    echo "Maybe try: ' rsync --list-only ldas-llo.ligo-la.caltech.edu::dqxml/L-DQ_Segments-${mday}/ ' or ' rsync --list-only ldas-llo.ligo-la.caltech.edu::dqxml/ ' "
    ;;
  [V])  echo "Running test rsync for Virgo"
    cmd="time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=${timeout}  dqsegments.virgo.infn.it::dqxml/V-DQ_Segments-${mday}  /dqxml/V1"
    #cmd="time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=${timeout}  dqsegments.virgo.infn.it::dqxml/  /dqxml/V1"
    run_rsync
    echo "Maybe try: ' rsync --list-only dqsegments.virgo.infn.it::dqxml/V-DQ_Segments-${mday}/ ' or ' rsync --list-only dqsegments.virgo.infn.it::dqxml/ ' "
    ;;
  [G])  echo "Running test rsync for GEO"
    cmd="time rsync --list-only --exclude '*.tar.gz' --include 'G-DQ_Segments-*' --exclude '*' --timeout=$timeout  rsync://bute2.aei.uni-hannover.de/dqxml/ | grep G-DQ_Segments-* | awk '{print $NF}' | sort | tail -n 1"
    run_rsync
    #echo "[the following doesn't quite work:] A live rsync command for GEO *might* look something like this: ' time rsync -av --exclude '*.tar.gz' --include 'G-DQ_Segments-*' --exclude '*' --timeout=$timeout  rsync://bute2.aei.uni-hannover.de/dqxml/G-DQ_Segments-${mday}  /dqxml/G1/G-DQ_Segments-${mday}/ ' "
    #echo "Maybe try: ' time rsync -av --exclude '*.tar.gz' --include 'G-DQ_Segments-*' --exclude '*' --timeout=$timeout  rsync://bute2.aei.uni-hannover.de/dqxml/G-DQ_Segments-${mday}  /dqxml/G1/G-DQ_Segments-${mday}/ ' "
    echo "Maybe try: ' time rsync --list-only --timeout=120  rsync://bute2.aei.uni-hannover.de/dqxml/G-DQ_Segments-${mday}/ | tail ' or ' time rsync --list-only --timeout=120  rsync://bute2.aei.uni-hannover.de/dqxml/ | tail ' "
    ;;
  [K])  echo "Running test rsync for KAGRA"
    cmd="time rsync -anvP --port 10873 gwdet.icrr.u-tokyo.ac.jp::kagrasegments  /dqxml/K1_test/"
    run_rsync
    echo "Maybe try: ' time rsync --list-only --port 10873 gwdet.icrr.u-tokyo.ac.jp::kagrasegments  | tail ' or ' time rsync --list-only --port 10873 gwdet.icrr.u-tokyo.ac.jp::kagrasegments/K1-GRD_SCIENCE_MODE/$(date +%Y)/  | tail ' "
    echo "       or: ' time rsync --list-only --port 10873 gwdet.icrr.u-tokyo.ac.jp::kagrasegments/*/2023/*$(TZ=":Japan" date +%Y-%m-%d --date="yesterday")* ' "
    ;;
  *)   echo "IFO unrecognized: $ifo .  Should be something like H, L, V, G, or K, or LHO, LLO, Virgo, GEO, or KAGRA."; exit_code=-3  ;;
esac

date
exit $exit_code   # exit code from the rsync command

# Reference
# time rsync -anv --exclude '*.tar.gz'  --exclude '\.H*'  --exclude '\.L*' --timeout=128  --size-only  ldas-lho.ligo-wa.caltech.edu::dqxml/H-DQ_Segments-13444/  /dqxml/H1/H-DQ_Segments-13444/
LHO:
lhodmt.ligo-wa.caltech.edu = marble.ligo-wa.caltech.edu = 198.129.208.114
ldas-lho.ligo-wa.caltech.edu = 198.129.208.213
LLO:
llodmt.ligo-la.caltech.edu = 208.69.128.37
ldas-llo.ligo-la.caltech.edu = 208.69.128.67
Virgo:
dqsegments.virgo.infn.it = olserver54.virgo.infn.it = 90.147.136.144
GEO:
bute2.aei.uni-hannover.de = 130.75.117.72
KAGRA:
gwdet.icrr.u-tokyo.ac.jp = 157.82.231.190
