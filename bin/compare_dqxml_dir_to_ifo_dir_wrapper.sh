#!/bin/bash
# This is a wrapper for compare_dqxml_dir_to_ifo_dir.sh, so that we can capture output files for bad runs.

# That script is run like this: '  (script name)  (ifo)  [verbose level]  [warning age (sec.)]  [critical age (sec.)]  [log file dir]  [full path to compare_two_dqxml_dirs.sh]  '
# Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir.sh  H  '
# Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir.sh  H  1  300  600  /root/bin/monitor_files_2  '

this_log_file_dir=/root/bin/monitor_files_2 
gps_time=$((`date +%s` - 315964782))
this_verbose=0
if [ $this_verbose -eq 1 ]; then echo "### INFO ### Arguments: $#; 1 = $1; 2 = $2; 3 = $3; 4 = $4; 5 = $5; 6 = $6"; fi

if [ "$#" -eq 0 ]
then
  echo "### ERROR ### At least 1 command-line arg is required."
  echo "Run like this: '  (script name)  (ifo)  [verbose level]  [warning age (sec.)]  [critical age (sec.)]  [log file dir]  [full path to compare_two_dqxml_dirs.sh]  '"
  echo "Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir_wrapper.sh  H  '"
  echo "Example: '  /root/bin/compare_dqxml_dir_to_ifo_dir_wrapper.sh  H  1  300  600  /root/bin/monitor_files_2  '"
  exit
fi
if [ "$#" -ge 1 ]; then ifo=$1; fi
if [ "$#" -ge 2 ]; then if [ "$2" -eq 0 ]; then verbose=0; else verbose=1; fi; fi   # only change verbose from 1 to 0 if 0 is specified on the command line; anything else, it stays at 1
if [ "$#" -ge 3 ]; then warning_age=$3; else warning_age=""; fi
if [ "$#" -ge 4 ]; then critical_age=$4; else critical_age=""; fi
if [ "$#" -ge 5 ]; then log_file_dir=$5; else log_file_dir=""; fi
if [ "$#" -ge 6 ]; then path_to_compare_script=$6; else path_to_compare_script=""; fi

log_file=compare_dqxml_dir_to_ifo_dir_${ifo}_${gps_time}_log.txt
full_log_file=/tmp/$log_file
if [ $this_verbose -eq 1 ]; then echo "### INFO ### Wrapper log file = $full_log_file"; fi

if [ $this_verbose -eq 1 ]; then echo "### INFO ### script call variables: ifo = $ifo ; verbose = $verbose ; warning_age = $warning_age ; critical_age = $critical_age ; log_file_dir = $log_file_dir ; path = $path_to_compare_script"; fi
# run the script
/root/bin/compare_dqxml_dir_to_ifo_dir.sh  $ifo  $verbose  $warning_age  $critical_age  $log_file_dir  $path_to_compare_script  &>  $full_log_file

# decide if test result is OK or not
nagios_file=`grep "Output file" $full_log_file | awk '{print $NF}'`
test_was_ok=`grep OK $nagios_file | wc --lines`
if [ $test_was_ok -ne 1 ]
then
  moved_log_file=/root/bin/monitor_files_2/$log_file
  if [ $this_verbose -eq 1 ]; then echo "### INFO ### Test result was not OK; moved log file = $moved_log_file"; fi
  cp  $full_log_file  $moved_log_file
  echo "-----" >> $moved_log_file

  main_log_file1=`grep "this script" $full_log_file | awk '{print $9}'`
  echo "Contents of $main_log_file1 log file:" >> $moved_log_file
  echo "-----" >> $moved_log_file
  cat $main_log_file1 >> $moved_log_file
  echo "-----" >> $moved_log_file

  main_log_file2=`grep "this script" $full_log_file | awk '{print $11}'`
  echo "Contents of $main_log_file2 log file:" >> $moved_log_file
  echo "-----" >> $moved_log_file
  cat $main_log_file2 >> $moved_log_file
  echo "-----" >> $moved_log_file

  output_file1=`grep "output file" $full_log_file | head -n 1 | awk '{print $7}'`
  echo "Contents of $output_file1 log file:" >> $moved_log_file
  echo "-----" >> $moved_log_file
  cat $output_file1 >> $moved_log_file
  echo "-----" >> $moved_log_file

  error_file1=`grep "error file" $full_log_file | head -n 1 | awk '{print $12}'`
  echo "Contents of $error_file1 log file:" >> $moved_log_file
  echo "-----" >> $moved_log_file
  cat $error_file1 >> $moved_log_file
  echo "-----" >> $moved_log_file

  output_file2=`grep "output file" $full_log_file | tail -n 1 | awk '{print $7}'`
  echo "Contents of $output_file2 log file:" >> $moved_log_file
  echo "-----" >> $moved_log_file
  cat $output_file2 >> $moved_log_file
  echo "-----" >> $moved_log_file

  error_file2=`grep "error file" $full_log_file | tail -n 1 | awk '{print $12}'`
  echo "Contents of $error_file2 log file:" >> $moved_log_file
  echo "-----" >> $moved_log_file
  cat $error_file2 >> $moved_log_file
  echo "-----" >> $moved_log_file
else
  if [ $this_verbose -eq 1 ]; then echo "### INFO ### Test was OK."; fi
fi

exit
